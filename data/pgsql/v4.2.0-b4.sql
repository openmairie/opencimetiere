--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v4.2.0-b4 depuis la version v4.2.0-b3
--
-- @package opencimetiere
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
-- BEGIN - #10031 - Augmentation de la taille de certains champs pour les autorisations et les emplacements
--

ALTER TABLE emplacement ALTER COLUMN monument TYPE character varying(255);
ALTER TABLE emplacement ALTER COLUMN numeroacte TYPE character varying(30);
ALTER TABLE autorisation ALTER COLUMN nom TYPE character varying(50);
ALTER TABLE autorisation ALTER COLUMN marital TYPE character varying(50);
ALTER TABLE autorisation ALTER COLUMN prenom TYPE character varying(50);
ALTER TABLE autorisation ALTER COLUMN parente TYPE text;
ALTER TABLE defunt ALTER COLUMN nom TYPE character varying(50);
ALTER TABLE defunt ALTER COLUMN marital TYPE character varying(50);
ALTER TABLE defunt ALTER COLUMN prenom TYPE character varying(50);
ALTER TABLE defunt ALTER COLUMN lieudeces TYPE character varying(50);
ALTER TABLE defunt ALTER COLUMN lieunaissance TYPE character varying(50);

--
-- END - #10031 - Augmentation de la taille de certains champs pour les autorisations et les emplacements
--

--
-- BEGIN - #10032 Ajout de nouvelles natures d'enveloppe mortuaires et d'un champ facilitant la reprise de données
--

ALTER TABLE defunt ADD COLUMN IF NOT EXISTS id_temp character varying(255);
ALTER TABLE defunt ALTER COLUMN nature TYPE character varying(50);

--
-- END - #10032 Ajout de nouvelles natures d'enveloppe mortuaires et d'un champ facilitant la reprise de données
--
