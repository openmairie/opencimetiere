--
-- BEGIN [#10260] - L'archivage des opérations ne fonctionne pas
--

ALTER TABLE operation_archive ADD COLUMN IF NOT EXISTS edition_operation character varying(40);
ALTER TABLE operation DROP CONSTRAINT IF EXISTS operation_emplacement_transfert_fkey;
ALTER TABLE operation_defunt_archive DROP CONSTRAINT IF EXISTS operation_defunt_archive_pkey;
ALTER TABLE operation_defunt_archive ADD CONSTRAINT operation_defunt_archive_pkey PRIMARY KEY (operation_defunt);
--
-- END [#10260] - L'archivage des opérations ne fonctionne pas
--

--
-- BEGIN [#10272] - Impossible d'ajouter un courrier - Une erreur s'est produite. Contacter votre administrateur
--

ALTER TABLE courrier ALTER COLUMN lettretype TYPE character varying(255);

--
-- END [#10272] - Impossible d'ajouter un courrier - Une erreur s'est produite. Contacter votre administrateur
--
