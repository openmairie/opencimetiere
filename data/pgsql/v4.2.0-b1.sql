--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v4.2.0-b1 depuis la version v4.1.0
--
-- @package opencimetiere
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- BEGIN - [#9928] - Ajout de champs supplémentaires dans les tables opération et entreprise
--

ALTER TABLE entreprise
    ADD COLUMN IF NOT EXISTS courriel character varying(100);

ALTER TABLE operation
    ADD COLUMN IF NOT EXISTS consigne_acces character varying(255),
    ADD COLUMN IF NOT EXISTS prive boolean;

ALTER TABLE operation_archive
    ADD COLUMN IF NOT EXISTS consigne_acces character varying(255),
    ADD COLUMN IF NOT EXISTS prive boolean;

ALTER TABLE cimetiere
    ADD COLUMN IF NOT EXISTS information_generale text;

--
-- END - [#9928] - Ajout de champs supplémentaires dans les tables opération et entreprise
----
-- BEGIN -  [#9941] - Ajout d'une action permettant l'affichage du plan en coupe de l'emplacement
--

ALTER TABLE ONLY sepulture_type
    ADD COLUMN IF NOT EXISTS colonne integer NOT NULL DEFAULT 0,
    ADD COLUMN IF NOT EXISTS ligne integer NOT NULL DEFAULT 0;

UPDATE sepulture_type SET colonne = 0, ligne = 0;

ALTER TABLE ONLY defunt
    ADD COLUMN IF NOT EXISTS x integer,
    ADD COLUMN IF NOT EXISTS y integer;

ALTER TABLE ONLY emplacement
    ADD COLUMN IF NOT EXISTS colonne integer,
    ADD COLUMN IF NOT EXISTS ligne integer;

--
-- END -  [#9941] - Ajout d'une action permettant l'affichage du plan en coupe de l'emplacement
----

--
-- BEGIN
--
CREATE TABLE IF NOT EXISTS lien_parente (
    lien_parente integer NOT NULL,
    libelle character varying(255),
    niveau integer,
    meme_personne boolean,
    lien_inverse character varying(255)
);

CREATE SEQUENCE IF NOT EXISTS lien_parente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_parente_seq OWNED BY lien_parente.lien_parente;

CREATE TABLE IF NOT EXISTS genealogie (
    genealogie integer NOT NULL,
    emplacement integer NOT NULL,
    autorisation_p1 integer,
    defunt_p1 integer,
    personne_1 integer,
    personne_2 integer,
    lien_parente integer NOT NULL,
    autorisation_p2 integer,
    defunt_p2 integer
);

ALTER TABLE ONLY genealogie
    DROP CONSTRAINT IF EXISTS genealogie_emplacement_fkey,
    ADD CONSTRAINT genealogie_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement),
    DROP CONSTRAINT IF EXISTS genealogie_autorisation_p1_fkey,
    ADD CONSTRAINT genealogie_autorisation_p1_fkey FOREIGN KEY (autorisation_p1) REFERENCES autorisation(autorisation),
    DROP CONSTRAINT IF EXISTS genealogie_autorisation_p2_fkey,
    ADD CONSTRAINT genealogie_autorisation_p2_fkey FOREIGN KEY (autorisation_p2) REFERENCES autorisation(autorisation),
    DROP CONSTRAINT IF EXISTS genealogie_defunt_p1_fkey,
    ADD CONSTRAINT genealogie_defunt_p1_fkey FOREIGN KEY (defunt_p1) REFERENCES defunt(defunt),
    DROP CONSTRAINT IF EXISTS genealogie_defunt_p2_fkey,
    ADD CONSTRAINT genealogie_defunt_p2_fkey FOREIGN KEY (defunt_p2) REFERENCES defunt(defunt),
    DROP CONSTRAINT IF EXISTS genealogie_lien_parente_fkey,
    DROP CONSTRAINT IF EXISTS genealogie_pkey;

ALTER TABLE ONLY lien_parente
    DROP CONSTRAINT IF EXISTS lien_parente_pkey,
    ADD CONSTRAINT lien_parente_pkey PRIMARY KEY (lien_parente);

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_pkey PRIMARY KEY (genealogie),
    ADD CONSTRAINT genealogie_lien_parente_fkey FOREIGN KEY (lien_parente) REFERENCES lien_parente(lien_parente);

CREATE SEQUENCE IF NOT EXISTS genealogie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE genealogie_seq OWNED BY genealogie.genealogie;

ALTER TABLE ONLY autorisation
    ADD COLUMN IF NOT EXISTS genealogie text;

ALTER TABLE ONLY defunt
    ADD COLUMN IF NOT EXISTS genealogie text;

ALTER TABLE ONLY defunt
    ALTER COLUMN parente TYPE text;

ALTER TABLE ONLY autorisation
    ALTER COLUMN parente TYPE text;
--
-- END
--
