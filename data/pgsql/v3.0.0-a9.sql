--  Ajout de la date de renouvellement de la concession dans la requête Courrier
UPDATE om_requete SET
requete = 'SELECT
  to_char(courrier.datecourrier, ''DD/MM/YYYY'') as datecourrier,
  courrier.complement,
  titre_de_civilite.code as titre,
  autorisation.nom, 
  autorisation.prenom, 
  autorisation.marital,
  autorisation.adresse1,
  autorisation.adresse2,
  autorisation.cp,
  autorisation.ville,
  autorisation.emplacement,
  autorisation.nature as autorisation_nature,
  emplacement.nature,
  emplacement.superficie,
  emplacement.numero as emplacement_adresse_numero,
  emplacement.complement as emplacement_adresse_complement,
  voie_type.libelle as type_de_voie,
  voielib,
  zone_type.libelle as type_de_zone,
  zonelib,
  emplacement.terme, 
  emplacement.numeroacte,
  to_char(emplacement.dateterme,''DD/MM/YYYY'') as dateterme,
  to_char(emplacement.datevente,''DD/MM/YYYY'') as datevente,
  to_char(emplacement.daterenouvellement,''DD/MM/YYYY'') as daterenouvellement,
  emplacement.famille,
  emplacement.duree,
  emplacement.placeconstat,
  cimetierelib

FROM
  &DB_PREFIXEcourrier
  LEFT JOIN &DB_PREFIXEautorisation on courrier.destinataire = autorisation.autorisation
  LEFT JOIN &DB_PREFIXEtitre_de_civilite on autorisation.titre = titre_de_civilite.titre_de_civilite
  LEFT JOIN &DB_PREFIXEemplacement on courrier.emplacement = emplacement.emplacement
  LEFT JOIN &DB_PREFIXEvoie on emplacement.voie = voie.voie
  LEFT JOIN &DB_PREFIXEvoie_type on voie.voietype = voie_type.voie_type
  LEFT JOIN &DB_PREFIXEzone on voie.zone = zone.zone
  LEFT JOIN &DB_PREFIXEzone_type on zone.zonetype = zone_type.zone_type
  LEFT JOIN &DB_PREFIXEcimetiere on zone.cimetiere = cimetiere.cimetiere

WHERE
  courrier.courrier = &idx
;',
merge_fields = '-- Courrier
[datecourrier]
[complement]

-- Autorisation
[titre]
[nom]
[prenom]
[marital]
[adresse1]
[adresse2]
[cp]
[ville]

[autorisation_nature]

-- Emplacement
[emplacement]
[nature]
[superficie]
[emplacement_adresse_numero]
[emplacement_adresse_complement]
[terme]
[numeroacte]
[dateterme]
[datevente]
[daterenouvellement]
[famille]
[duree]
[placeconstat]

-- Voie
[type_de_voie]
[voielib]

-- Zone
[type_de_zone]
[zonelib]

-- Cimetière
[cimetierelib]'
WHERE libelle = 'Requête COURRIER';