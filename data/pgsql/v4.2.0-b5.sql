--
-- BEGIN -[#10075] - Erreur de base de données lors de l'archivage de concession : la taille du champ est dépassée (parenté)
--

ALTER TABLE emplacement_archive ALTER COLUMN numeroacte TYPE character varying(30);
ALTER TABLE autorisation_archive ALTER COLUMN nom TYPE character varying(50);
ALTER TABLE autorisation_archive ALTER COLUMN marital TYPE character varying(50);
ALTER TABLE autorisation_archive ALTER COLUMN prenom TYPE character varying(50);
ALTER TABLE autorisation_archive ALTER COLUMN parente TYPE text;
ALTER TABLE defunt_archive ALTER COLUMN nom TYPE character varying(50);
ALTER TABLE defunt_archive ALTER COLUMN marital TYPE character varying(50);
ALTER TABLE defunt_archive ALTER COLUMN prenom TYPE character varying(50);
ALTER TABLE defunt_archive ALTER COLUMN lieudeces TYPE character varying(50);
ALTER TABLE defunt_archive ALTER COLUMN lieunaissance TYPE character varying(50);

--
-- END - [#10075] - Erreur de base de données lors de l'archivage de concession : la taille du champ est dépassée (parenté)
--

--
-- BEGIN - [#10074] - Ajout du système de blocage de case sur le plan en coupe 
--

ALTER TABLE emplacement ADD COLUMN IF NOT EXISTS etat_case_plan_en_coupe character varying(255);

--
-- END - [#10074] - Ajout du système de blocage de case sur le plan en coupe 
--

--
-- BEGIN [#10092] - Ajout d'un deuxième champ de consigne d'accès pour les opérations de transfert
--

ALTER TABLE operation ADD COLUMN IF NOT EXISTS consigne_acces_transfert character varying(255);
ALTER TABLE operation_archive ADD COLUMN IF NOT EXISTS consigne_acces_transfert character varying(255);

--
-- END [#10092] - Ajout d'un deuxième champ de consigne d'accès pour les opérations de transfert
--


--
-- BEGIN [#10095] - Enlever les champs inutiles 'colonne' et 'ligne' dans emplacement
--

ALTER TABLE ONLY emplacement DROP COLUMN IF EXISTS colonne;
ALTER TABLE ONLY emplacement DROP COLUMN IF EXISTS ligne;

--
-- END [#10095] - Enlever les champs inutiles 'colonne' et 'ligne' dans emplacement
--