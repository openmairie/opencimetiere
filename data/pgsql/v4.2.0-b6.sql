--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v4.2.0-b6 depuis la version v4.2.0-b5
--
-- @package opencimetiere
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- BEGIN - [#10143] - Ajout d'un champ permettant d'afficher un autre intitulé du type de sépulture à la place du libellé dans les éditions
--
ALTER TABLE sepulture_type ADD COLUMN IF NOT EXISTS libelle_edition character varying(255);

--
-- END - [#10143] - Ajout d'un champ permettant d'afficher un autre intitulé du type de sépulture à la place du libellé dans les éditions
--


--
-- BEGIN - [#10150] - Augmentation de la taille des champs de la table operation_defunt
--

ALTER TABLE operation_defunt ALTER COLUMN defunt_nature TYPE character varying(50);
ALTER TABLE operation_defunt ALTER COLUMN defunt_nom TYPE character varying(50);
ALTER TABLE operation_defunt ALTER COLUMN defunt_marital TYPE character varying(50);
ALTER TABLE operation_defunt ALTER COLUMN defunt_prenom TYPE character varying(50);
ALTER TABLE operation_defunt ALTER COLUMN defunt_lieudeces TYPE character varying(50);
ALTER TABLE operation_defunt ALTER COLUMN defunt_lieunaissance TYPE character varying(50);

--
-- END - [#10150] - Augmentation de la taille des champs de la table operation_defunt
--

--
-- BEGIN - framework 4.11.0-a1
--

-- * Évolution : Fiabiliser/pérenniser le module SIG.
--   - Suppression du champ retour de la table om_sig_map non utilisé.
ALTER TABLE om_sig_map DROP COLUMN IF EXISTS retour;

-- * Évolution : Fiabiliser/pérenniser le module SIG.
--   - Ajout d'un nouveau champ dans om_sig_map pour sélectionner la librairie de cartographie à utiliser
ALTER TABLE om_sig_map
    ADD COLUMN IF NOT EXISTS librairie_cartographie character varying(40);
COMMENT ON COLUMN "om_sig_map"."librairie_cartographie" IS 'Librairie de cartographie utilisée par la carte.';
UPDATE om_sig_map SET librairie_cartographie='openlayers2';
ALTER TABLE om_sig_map ALTER COLUMN librairie_cartographie SET NOT NULL;

-- * Évolution : Fiabiliser/pérenniser le module SIG.
--   - Suppression du champ layer_info de la table om_sig_map non utilisé.
ALTER TABLE om_sig_map DROP COLUMN IF EXISTS layer_info;

--
-- END - framework 4.11.0-a1
--
