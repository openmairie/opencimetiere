--------------------------------------------------------------------------------
-- Script de mise à jour de la version 3.0.0-a9 vers la version 3.0.0
--
--
-- @package opencimetiere
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- -----------------------------------------------------
--  creation de la version SIG - version unique
--  sig version 3.0.0 passage en multi pour les polygones
--  enleve les champs geom en text
--  *** VERSION 3.0.0 ***
-- -----------------------------------------------------

CREATE EXTENSION IF NOT EXISTS postgis;

-- les champs geom en 3.0.0-a5 sont en txt 
-- les champs geom sont Polygon en  3.0.0-a9 ne sont pas  MultiPolygon
-- le champ en 3.0.0-a9 geom de voie est linestring et ne peut pas être pris en compte
-- les requêtes suivantes  prennent en compte les trois cas de figure
-- * passage des champs en multi si les champs geom ont été initialisé en polygon
-- * pas de passage MultiPolygon pour le champ voie en Linestring
-- * passage en postgis si les champs sont en text (version 3.0.0-a5)

-- cimetiere - champ geom txt ou polygon -> multipolygon avec transformation data en multi
ALTER TABLE cimetiere
    RENAME COLUMN geom TO geom_old;
ALTER TABLE cimetiere
    ADD COLUMN geom geometry(MultiPolygon,2154);
UPDATE cimetiere
    SET geom=CASE WHEN a.data_type='text' THEN geom ELSE ST_MULTI(geom_old) END
    FROM (select data_type from information_schema.columns where table_name = 'cimetiere' AND column_name = 'geom') a
    WHERE geom_old IS NOT NULL;
ALTER TABLE cimetiere
    DROP COLUMN geom_old;
-- zone
ALTER TABLE zone
    RENAME COLUMN geom TO geom_old;
ALTER TABLE zone
    ADD COLUMN geom geometry(MultiPolygon,2154);
UPDATE zone
    SET geom=CASE WHEN a.data_type='text' THEN geom ELSE ST_MULTI(geom_old) END
    FROM (select data_type from information_schema.columns where table_name = 'zone' AND column_name = 'geom') a
    WHERE geom_old IS NOT NULL;
ALTER TABLE zone
    DROP COLUMN geom_old;
-- voie linestring conservé en geom_old (version 3.0.0-a9) 
ALTER TABLE voie
    RENAME COLUMN geom TO geom_old;
ALTER TABLE voie
    ADD COLUMN geom geometry(MultiPolygon,2154);
-- emplacement: si geom = text alors 
ALTER TABLE emplacement
    RENAME COLUMN geom TO geom_old;
ALTER TABLE emplacement
    ADD COLUMN geom geometry(Point,2154);
UPDATE emplacement
    SET geom=CASE WHEN a.data_type='text' THEN null ELSE geom_old END
    FROM (select data_type from information_schema.columns where table_name = 'emplacement' AND column_name = 'geom') a
    WHERE geom_old IS NOT NULL;
ALTER TABLE emplacement
    DROP COLUMN geom_old;
-- emplacement : nouvelle géométrie MultiPolygon 
ALTER TABLE emplacement ADD COLUMN pgeom geometry(MultiPolygon,2154);


-- vue pour centrer en creation sur cimetiere, zone, voie
CREATE OR REPLACE VIEW geo_loc_emplacement AS 
SELECT e.emplacement,
	CASE WHEN v.geom IS NOT NULL THEN st_boundary(v.geom)::geometry(MultiLineString,2154) 
	     WHEN z.geom IS NOT NULL THEN st_boundary(z.geom)::geometry(MultiLineString,2154) 
	     ELSE st_boundary(c.geom)::geometry(MultiLineString,2154)
	END AS geom
FROM emplacement e
JOIN voie v ON e.voie = v.voie 
JOIN zone z ON z.zone = v.zone 
JOIN cimetiere c ON c.cimetiere = z.cimetiere
WHERE v.geom IS NOT NULL OR z.geom IS NOT NULL OR c.geom IS NOT NULL;


-- parametrage sig_interne (a voir)
-- update om_parametre  set valeur =  'sig_interne' where om_parametre = 3;
INSERT INTO  om_sig_extent (om_sig_extent, nom, extent)
SELECT 1405, 'Commune: Arles (13004)', '4.42622847525433,43.3301107926678,4.87635225540216,43.7603968152557'
WHERE NOT EXISTS ( 
   SELECT om_sig_extent FROM om_sig_extent WHERE om_sig_extent = 1405
);

-- ** paramétrage tables sig **
-- map 1 cimetiere
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (1, 1, 'cimetiere', 'Cimetière', true, '17', true, false, false, true, 'EPSG:2154', 
'../app/emplacement.php?idx=',
 'select st_astext(emplacement.geom) as geom, (numero||'' ''||voietype||''  ''||voielib) as titre, (nature ||'' ''|| famille) as description,emplacement as idx from &DB_PREFIXEemplacement  inner join &DB_PREFIXEvoie on emplacement.voie = voie.voie inner join &DB_PREFIXEzone on voie.zone = zone.zone  where zone.cimetiere = &idx order by geom,emplacement',
  '../scr/form.php?obj=cimetiere&idx=', true, false, true, 1, '1', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (2, 1, 'zone', 'Zone', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=zone&idx=', 'SELECT 
      st_astext(ST_PointOnSurface(ST_Buffer(z.geom,0.1))) as geom, 
      z.zonelib as titre,  
      c.cimetierelib||'' ''||zt.libelle||'' ''||z.zonelib||'' (''||z.zone||'')'' as description, 
      z.zone as idx
from &DB_PREFIXEzone z 
JOIN &DB_PREFIXEcimetiere c 
      ON c.cimetiere=z.cimetiere 
JOIN &DB_PREFIXEzone_type zt 
      ON zt.zone_type=z.zonetype 
WHERE 
       z.cimetiere IN (SELECT distinct cimetiere FROM  &DB_PREFIXEzone WHERE zone IN (&idx))', '../scr/form.php?obj=zone&idx=', true, false, false, NULL, '3', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (3, 1, 'concession', 'concession', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, NULL, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');

INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (4, 1, 'colombarium', 'colombarium', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, 3, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (5, 1, 'enfeu', 'enfeu', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, 3, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (6, 1, 'ossuaire', 'ossuaire', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, 3, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (7, 1, 'depositoire', 'depositoire', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, 3, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (8, 1, 'terraincommunal', 'terraincommunal', true, '18', true, false, false, true, 'EPSG:2154', '../scr/form.php?obj=concession&idx=', 'SELECT 
      ST_AsText(e.geom) as geom, 
      e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,  
      e'''' as description, 
      e.emplacement as idx     
FROM  &DB_PREFIXEemplacement e
JOIN  &DB_PREFIXEvoie v
      ON v.voie=e.voie
JOIN  &DB_PREFIXEvoie_type vt
      ON vt.voie_type=v.voietype
JOIN  &DB_PREFIXEzone z
      ON z.zone=v.zone
JOIN  &DB_PREFIXEzone_type zt
      ON zt.zone_type=z.zonetype
JOIN  &DB_PREFIXEcimetiere c
      ON c.cimetiere=z.cimetiere
WHERE 
      emplacement IN (&idx)', '../scr/form.php?obj=concession&idx=', true, false, false, 3, 'osm', 1405, true, NULL, NULL, '01010000206A0800002622872A505E2941C9667C3666FC5741');
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, layer_info, projection_externe, url, om_sql, retour, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage) 
VALUES (9, 1, 'voie', 'Voie', true, '18', true, false, false, false, 'EPSG:2154', '../scr/form.php?obj=voie&idx= ', 'SELECT 
      st_astext(ST_PointOnSurface(ST_Buffer(v.geom,0.1))) as geom, 
      vt.libelle||'' ''||v.voielib as titre,  
      c.cimetierelib||'' ''||zt.libelle||'' ''||z.zonelib||'' (''||z.zone||'')''||vt.libelle||'' ''||v.voielib||'' (''||v.voie||'')'' as description, 
      z.zone as idx
from &DB_PREFIXEvoie v
JOIN &DB_PREFIXEvoie_type vt ON vt.voie_type = v.voietype
JOIN &DB_PREFIXEzone z ON z.zone=v.zone 
JOIN &DB_PREFIXEcimetiere c 
      ON c.cimetiere=z.cimetiere 
JOIN &DB_PREFIXEzone_type zt 
      ON zt.zone_type=z.zonetype 
WHERE 
       v.zone IN (SELECT distinct zone FROM  voie WHERE voie IN (&idx))', '../scr/form.php?obj=voie&idx= ', true, false, false, NULL, 'osm', 1405, true, NULL, NULL, NULL);

SELECT pg_catalog.setval('om_sig_map_seq', 9, true);

-- geometrie dans les map

INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) 
VALUES (1, 1, 'Emprise', 1, true, true, 'multipolygon', 'cimetiere', 'geom', 'cimetiere', 'cimetiere');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (2, 2, 'périmètre', 1, true, true, 'multipolygon', 'zone', 'geom', 'zone', 'zone');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (4, 3, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (3, 3, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (5, 3, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (6, 4, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (7, 5, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (8, 6, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (9, 7, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (10, 8, 'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (11, 4, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (12, 5, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (13, 6, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (14, 7, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (15, 8, 'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (16, 4, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (17, 5, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (18, 6, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (19, 7, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (20, 8, 'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement');
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) VALUES (21, 9, 'Voie', 1, true, true, 'multipolygon', 'voie', 'geom', 'voie', 'voie');

SELECT pg_catalog.setval('om_sig_map_comp_seq', 21, true);

\set wms_opencimetiere_url '\'http://localhost/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&map=/var/www/opencimetiere/app/qgis/opencimetiere.qgs\''

INSERT INTO om_sig_flux (om_sig_flux, libelle, om_collectivite, id, attribution, chemin, couches, cache_type, cache_gfi_chemin, cache_gfi_couches) 
    VALUES (2, 'paniers', 1, 'paniers', 'Paniers', :wms_opencimetiere_url, 'paniers', NULL, NULL, NULL);
INSERT INTO om_sig_flux (om_sig_flux, libelle, om_collectivite, id, attribution, chemin, couches, cache_type, cache_gfi_chemin, cache_gfi_couches) 
    VALUES (1, 'metier', 1, 'metier', NULL, :wms_opencimetiere_url, 'habillage,metier', NULL, NULL, NULL);
SELECT pg_catalog.setval('om_sig_flux_seq', 2, true);

INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (1, 1, 1, 'openCimetiere (fond)', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', true, true, 24);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (2, 1, 1, 'metier', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', false, true, 24);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (3, 1, 2, 'openCimetiere (fond)', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', true, true, 24);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (4, 1, 2, 'metier', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', false, true, NULL);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (5, 1, 3, 'Métier', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', false, true, NULL);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (6, 1, 3, 'fond métier', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', true, true, 24);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (7, 1, 9, 'openCimetiere (fond)', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', true, true, 24);
INSERT INTO om_sig_map_flux (om_sig_map_flux, om_sig_flux, om_sig_map, ol_map, ordre, visibility, panier, pa_nom, pa_layer, pa_attribut, pa_encaps, pa_sql, pa_type_geometrie, sql_filter, baselayer, singletile, maxzoomlevel) 
    VALUES (8, 1, 9, 'metier', 1, false, false, NULL, NULL, NULL, NULL, '', NULL, '', false, true, NULL);
SELECT pg_catalog.setval('om_sig_map_flux_seq', 8, true);
