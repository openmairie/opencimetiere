--
-- PostgreSQL database dump
--

-- Dumped from database version 12.16 (Ubuntu 12.16-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.16 (Ubuntu 12.16-1.pgdg20.04+1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Name: opencimetiere; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA opencimetiere;


-- SET default_tablespace = '';

-- SET default_table_access_method = heap;

--
-- Name: autorisation; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE autorisation (
    autorisation integer NOT NULL,
    emplacement integer,
    nature character varying(15) NOT NULL,
    titre integer,
    nom character varying(50),
    marital character varying(50),
    prenom character varying(50),
    datenaissance date,
    adresse1 character varying(40),
    adresse2 character varying(40),
    cp character varying(5),
    ville character varying(50),
    telephone1 character varying(15),
    dcd boolean,
    parente text,
    observation text,
    telephone2 character varying(15),
    courriel character varying(100),
    genealogie text,
    CONSTRAINT check_autorisation_nature CHECK ((((nature)::text = 'concessionnaire'::text) OR ((nature)::text = 'ayantdroit'::text) OR ((nature)::text = 'autre'::text)))
);


--
-- Name: autorisation_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE autorisation_archive (
    autorisation integer NOT NULL,
    emplacement integer,
    nature character varying(15),
    titre integer,
    nom character varying(50),
    marital character varying(50),
    prenom character varying(50),
    datenaissance date,
    adresse1 character varying(40),
    adresse2 character varying(40),
    cp character varying(5),
    ville character varying(50),
    telephone1 character varying(15),
    dcd boolean,
    parente text,
    observation text,
    telephone2 character varying(15),
    courriel character varying(100)
);


--
-- Name: autorisation_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE autorisation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: autorisation_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE autorisation_seq OWNED BY autorisation.autorisation;


--
-- Name: cimetiere; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE cimetiere (
    cimetiere integer NOT NULL,
    cimetierelib character varying(40) NOT NULL,
    adresse1 character varying(40),
    adresse2 character varying(40),
    cp character varying(5),
    ville character varying(40),
    observations text,
    geom public.geometry(MultiPolygon,2154),
    information_generale text
);


--
-- Name: cimetiere_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE cimetiere_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cimetiere_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE cimetiere_seq OWNED BY cimetiere.cimetiere;


--
-- Name: contrat; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE contrat (
    contrat integer NOT NULL,
    emplacement integer NOT NULL,
    datedemande date NOT NULL,
    datevente date NOT NULL,
    origine character varying(50) NOT NULL,
    dateterme date,
    terme character varying(15) NOT NULL,
    duree real NOT NULL,
    montant numeric(11,2),
    monnaie character varying(30),
    valide boolean,
    observation text,
    CONSTRAINT check_contrat_origine CHECK ((((origine)::text = 'achat'::text) OR ((origine)::text = 'renouvellement'::text) OR ((origine)::text = 'transformation'::text))),
    CONSTRAINT check_contrat_terme_duree CHECK (((((terme)::text = 'temporaire'::text) AND (duree > (0)::double precision)) OR (((terme)::text = 'perpetuite'::text) AND (duree = (0)::double precision))))
);


--
-- Name: contrat_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE contrat_archive (
    contrat integer NOT NULL,
    emplacement integer NOT NULL,
    datedemande date NOT NULL,
    datevente date NOT NULL,
    origine character varying(50) NOT NULL,
    dateterme date,
    terme character varying(15) NOT NULL,
    duree real NOT NULL,
    montant numeric(11,2),
    monnaie character varying(30),
    valide boolean,
    observation text
);


--
-- Name: contrat_archive_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE contrat_archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contrat_archive_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE contrat_archive_seq OWNED BY contrat_archive.contrat;


--
-- Name: contrat_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE contrat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contrat_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE contrat_seq OWNED BY contrat.contrat;


--
-- Name: courrier; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE courrier (
    courrier integer NOT NULL,
    destinataire integer,
    datecourrier date,
    lettretype character varying(255),
    complement text,
    emplacement integer,
    contrat integer
);


--
-- Name: courrier_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE courrier_archive (
    courrier integer NOT NULL,
    destinataire integer,
    datecourrier date,
    lettretype character varying(40),
    complement text,
    emplacement integer,
    contrat integer
);


--
-- Name: courrier_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE courrier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courrier_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE courrier_seq OWNED BY courrier.courrier;


--
-- Name: defunt; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE defunt (
    defunt integer NOT NULL,
    nature character varying(50),
    taille real,
    emplacement bigint,
    titre integer,
    nom character varying(50),
    prenom character varying(50),
    marital character varying(50),
    datenaissance date,
    datedeces date,
    lieudeces character varying(50),
    dateinhumation date,
    exhumation character(3),
    dateexhumation date,
    observation text,
    reduction character(3),
    datereduction date,
    historique text,
    verrou character(3),
    parente text,
    lieunaissance character varying(50),
    x integer,
    y integer,
    genealogie text,
    id_temp character varying(255)
);


--
-- Name: defunt_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE defunt_archive (
    defunt integer NOT NULL,
    nature character varying(15),
    taille real,
    emplacement integer,
    titre integer,
    nom character varying(50),
    prenom character varying(50),
    marital character varying(50),
    datenaissance date,
    datedeces date,
    lieudeces character varying(50),
    dateinhumation date,
    exhumation character(3),
    dateexhumation date,
    observation text,
    reduction character(3),
    datereduction date,
    historique text,
    verrou character(3),
    parente character varying(100),
    lieunaissance character varying(50)
);


--
-- Name: defunt_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE defunt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: defunt_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE defunt_seq OWNED BY defunt.defunt;


--
-- Name: dossier; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE dossier (
    dossier integer NOT NULL,
    emplacement integer,
    fichier character varying(40) NOT NULL,
    datedossier date,
    observation text,
    typedossier character varying(20)
);


--
-- Name: dossier_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE dossier_archive (
    dossier integer NOT NULL,
    emplacement integer,
    fichier character varying(40),
    datedossier date,
    observation text,
    typedossier character varying(20)
);


--
-- Name: dossier_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE dossier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE dossier_seq OWNED BY dossier.dossier;


--
-- Name: emplacement; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE emplacement (
    emplacement integer NOT NULL,
    nature character varying(20),
    numero integer,
    complement character varying(6),
    voie integer,
    numerocadastre character varying(15),
    famille character varying(40),
    numeroacte character varying(30),
    datevente date,
    terme character varying(15),
    duree real,
    dateterme date,
    nombreplace real,
    placeoccupe real,
    superficie real,
    placeconstat real,
    dateconstat date,
    observation text,
    plans integer,
    positionx real,
    positiony real,
    photo character varying(20),
    libre character(3),
    sepulturetype integer,
    daterenouvellement date,
    typeconcession character varying(40),
    temp1 character varying(100),
    temp2 character varying(100),
    temp3 character varying(100),
    temp4 character varying(100),
    temp5 character varying(100),
    videsanitaire character varying(3),
    semelle character varying(20),
    etatsemelle character varying(10),
    monument character varying(255),
    etatmonument character varying(10),
    largeur real,
    profondeur real,
    abandon character varying(3),
    date_abandon date,
    dateacte date,
    geom public.geometry(Point,2154),
    pgeom public.geometry(MultiPolygon,2154),
    etat_case_plan_en_coupe character varying(255)
);


--
-- Name: emplacement_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE emplacement_archive (
    emplacement integer NOT NULL,
    nature character varying(20),
    numero bigint,
    complement character varying(6),
    voie integer,
    numerocadastre character varying(15),
    famille character varying(40),
    numeroacte character varying(30),
    datevente date,
    terme character varying(20),
    duree real,
    dateterme date,
    nombreplace real,
    placeoccupe real,
    superficie real,
    placeconstat real,
    dateconstat date,
    observation text,
    plans character varying(50),
    positionx real,
    positiony real,
    photo character varying(20),
    libre character(3),
    sepulturetype integer
);


--
-- Name: emplacement_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE emplacement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: emplacement_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE emplacement_seq OWNED BY emplacement.emplacement;


--
-- Name: entreprise; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE entreprise (
    entreprise integer NOT NULL,
    nomentreprise character varying(60) NOT NULL,
    pf character(3) DEFAULT 'Non'::bpchar NOT NULL,
    adresse1 character varying(40),
    adresse2 character varying(40),
    cp character varying(5),
    ville character varying(40),
    telephone character varying(40),
    courriel character varying(100),
    numero_habilitation character varying(40),
    date_fin_validite_habilitation date
);


--
-- Name: entreprise_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE entreprise_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entreprise_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE entreprise_seq OWNED BY entreprise.entreprise;


--
-- Name: genealogie; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE genealogie (
    genealogie integer NOT NULL,
    emplacement integer NOT NULL,
    autorisation_p1 integer,
    defunt_p1 integer,
    personne_1 integer,
    personne_2 integer,
    lien_parente integer NOT NULL,
    autorisation_p2 integer,
    defunt_p2 integer
);


--
-- Name: genealogie_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE genealogie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: genealogie_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE genealogie_seq OWNED BY genealogie.genealogie;


--
-- Name: voie; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE voie (
    voie integer NOT NULL,
    zone integer NOT NULL,
    voietype integer NOT NULL,
    voielib character varying(40) NOT NULL,
    geom public.geometry(MultiPolygon,2154)
);


--
-- Name: zone; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE zone (
    zone integer NOT NULL,
    cimetiere integer NOT NULL,
    zonetype integer NOT NULL,
    zonelib character varying(40) NOT NULL,
    geom public.geometry(MultiPolygon,2154)
);


--
-- Name: geo_loc_emplacement; Type: VIEW; Schema: opencimetiere; Owner: -
--

CREATE VIEW geo_loc_emplacement AS
 SELECT e.emplacement,
        CASE
            WHEN (v.geom IS NOT NULL) THEN (public.st_boundary(v.geom))::public.geometry(MultiLineString,2154)
            WHEN (z.geom IS NOT NULL) THEN (public.st_boundary(z.geom))::public.geometry(MultiLineString,2154)
            ELSE (public.st_boundary(c.geom))::public.geometry(MultiLineString,2154)
        END AS geom
   FROM (((emplacement e
     JOIN voie v ON ((e.voie = v.voie)))
     JOIN zone z ON ((z.zone = v.zone)))
     JOIN cimetiere c ON ((c.cimetiere = z.cimetiere)))
  WHERE ((v.geom IS NOT NULL) OR (z.geom IS NOT NULL) OR (c.geom IS NOT NULL));


--
-- Name: lien_parente; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE lien_parente (
    lien_parente integer NOT NULL,
    libelle character varying(255),
    niveau integer,
    meme_personne boolean,
    lien_inverse character varying(255)
);


--
-- Name: lien_parente_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE lien_parente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_parente_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE lien_parente_seq OWNED BY lien_parente.lien_parente;


--
-- Name: operation; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE operation (
    operation integer NOT NULL,
    numdossier character varying(10),
    date date,
    heure time without time zone,
    emplacement integer,
    societe_coordonnee text,
    pf_coordonnee text,
    etat character varying(6),
    categorie character varying(20),
    particulier character(3),
    emplacement_transfert integer,
    observation text,
    consigne_acces character varying(255),
    prive boolean,
    edition_operation character varying(40),
    consigne_acces_transfert character varying(255)
);


--
-- Name: operation_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE operation_archive (
    operation integer NOT NULL,
    numdossier character varying(10),
    date date,
    heure time without time zone,
    emplacement integer,
    societe_coordonnee text,
    pf_coordonnee text,
    etat character varying(6),
    categorie character varying(20),
    particulier character(3),
    emplacement_transfert integer,
    observation text,
    consigne_acces character varying(255),
    prive boolean,
    consigne_acces_transfert character varying(255),
    edition_operation character varying(40)
);


--
-- Name: operation_archive_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE operation_archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: operation_archive_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE operation_archive_seq OWNED BY operation_archive.operation;


--
-- Name: operation_defunt; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE operation_defunt (
    operation_defunt integer NOT NULL,
    operation integer,
    defunt integer,
    defunt_titre integer,
    defunt_nom character varying(50),
    defunt_marital character varying(50),
    defunt_prenom character varying(50),
    defunt_datenaissance date,
    defunt_datedeces date,
    defunt_lieudeces character varying(50),
    defunt_nature character varying(50),
    defunt_parente character varying(100),
    defunt_lieunaissance character varying(50)
);


--
-- Name: operation_defunt_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE operation_defunt_archive (
    operation_defunt integer NOT NULL,
    operation integer NOT NULL,
    defunt integer,
    defunt_titre integer,
    defunt_nom character varying(40),
    defunt_marital character varying(40),
    defunt_prenom character varying(40),
    defunt_datenaissance date,
    defunt_datedeces date,
    defunt_lieudeces character varying(40),
    defunt_nature character varying(15)
);


--
-- Name: operation_defunt_archive_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE operation_defunt_archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: operation_defunt_archive_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE operation_defunt_archive_seq OWNED BY operation_defunt_archive.operation;


--
-- Name: operation_defunt_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE operation_defunt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: operation_defunt_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE operation_defunt_seq OWNED BY operation_defunt.operation_defunt;


--
-- Name: operation_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE operation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: operation_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE operation_seq OWNED BY operation.operation;


--
-- Name: plans; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE plans (
    plans integer NOT NULL,
    planslib character varying(40) NOT NULL,
    fichier character varying(100) NOT NULL
);


--
-- Name: plans_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE plans_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plans_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE plans_seq OWNED BY plans.plans;


--
-- Name: sepulture_type; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE sepulture_type (
    sepulture_type integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    description character varying(200),
    om_validite_debut date,
    om_validite_fin date,
    colonne integer DEFAULT 0 NOT NULL,
    ligne integer DEFAULT 0 NOT NULL,
    libelle_edition character varying(255)
);


--
-- Name: sepulture_type_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE sepulture_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sepulture_type_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE sepulture_type_seq OWNED BY sepulture_type.sepulture_type;


--
-- Name: tarif; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE tarif (
    tarif integer NOT NULL,
    annee integer NOT NULL,
    origine character varying(50) NOT NULL,
    terme character varying(15) NOT NULL,
    duree real,
    nature character varying(15),
    sepulture_type integer,
    montant numeric(11,2) NOT NULL,
    monnaie character varying(30) NOT NULL,
    CONSTRAINT check_tarif_nature CHECK ((((nature)::text = 'concession'::text) OR ((nature)::text = 'colombarium'::text) OR ((nature)::text = 'enfeu'::text) OR ((nature)::text = 'terraincommunal'::text))),
    CONSTRAINT check_tarif_origine CHECK ((((origine)::text = 'achat'::text) OR ((origine)::text = 'renouvellement'::text))),
    CONSTRAINT check_tarif_terme_duree CHECK (((((terme)::text = 'temporaire'::text) AND (duree > (0)::double precision)) OR (((terme)::text = 'perpetuite'::text) AND (duree = (0)::double precision))))
);


--
-- Name: tarif_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE tarif_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tarif_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE tarif_seq OWNED BY tarif.tarif;


--
-- Name: titre_de_civilite; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE titre_de_civilite (
    titre_de_civilite integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    description character varying(200),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: titre_de_civilite_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE titre_de_civilite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: titre_de_civilite_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE titre_de_civilite_seq OWNED BY titre_de_civilite.titre_de_civilite;


--
-- Name: travaux; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE travaux (
    travaux integer NOT NULL,
    entreprise integer,
    emplacement integer,
    datedebinter date,
    datefininter date,
    observation text,
    naturedemandeur character varying(20),
    naturetravaux integer
);


--
-- Name: travaux_archive; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE travaux_archive (
    travaux integer NOT NULL,
    entreprise integer,
    emplacement integer,
    datedebinter date,
    datefininter date,
    observation text NOT NULL,
    naturedemandeur character varying(20),
    naturetravaux integer
);


--
-- Name: travaux_nature; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE travaux_nature (
    travaux_nature integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    description character varying(200),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: travaux_nature_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE travaux_nature_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: travaux_nature_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE travaux_nature_seq OWNED BY travaux_nature.travaux_nature;


--
-- Name: travaux_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE travaux_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: travaux_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE travaux_seq OWNED BY travaux.travaux;


--
-- Name: voie_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE voie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE voie_seq OWNED BY voie.voie;


--
-- Name: voie_type; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE voie_type (
    voie_type integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    description character varying(200),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: voie_type_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE voie_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_type_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE voie_type_seq OWNED BY voie_type.voie_type;


--
-- Name: zone_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE zone_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zone_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE zone_seq OWNED BY zone.zone;


--
-- Name: zone_type; Type: TABLE; Schema: opencimetiere; Owner: -
--

CREATE TABLE zone_type (
    zone_type integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    description character varying(200),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: zone_type_seq; Type: SEQUENCE; Schema: opencimetiere; Owner: -
--

CREATE SEQUENCE zone_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zone_type_seq; Type: SEQUENCE OWNED BY; Schema: opencimetiere; Owner: -
--

ALTER SEQUENCE zone_type_seq OWNED BY zone_type.zone_type;


--
-- Name: autorisation_archive autorisation_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY autorisation_archive
    ADD CONSTRAINT autorisation_archive_pkey PRIMARY KEY (autorisation);


--
-- Name: autorisation autorisation_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY autorisation
    ADD CONSTRAINT autorisation_pkey PRIMARY KEY (autorisation);


--
-- Name: cimetiere cimetiere_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY cimetiere
    ADD CONSTRAINT cimetiere_pkey PRIMARY KEY (cimetiere);


--
-- Name: contrat_archive contrat_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY contrat_archive
    ADD CONSTRAINT contrat_archive_pkey PRIMARY KEY (contrat);


--
-- Name: contrat contrat_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY contrat
    ADD CONSTRAINT contrat_pkey PRIMARY KEY (contrat);


--
-- Name: courrier_archive courrier_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY courrier_archive
    ADD CONSTRAINT courrier_archive_pkey PRIMARY KEY (courrier);


--
-- Name: courrier courrier_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_pkey PRIMARY KEY (courrier);


--
-- Name: defunt_archive defunt_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY defunt_archive
    ADD CONSTRAINT defunt_archive_pkey PRIMARY KEY (defunt);


--
-- Name: defunt defunt_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY defunt
    ADD CONSTRAINT defunt_pkey PRIMARY KEY (defunt);


--
-- Name: dossier_archive dossier_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY dossier_archive
    ADD CONSTRAINT dossier_archive_pkey PRIMARY KEY (dossier);


--
-- Name: dossier dossier_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_pkey PRIMARY KEY (dossier);


--
-- Name: emplacement_archive emplacement_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement_archive
    ADD CONSTRAINT emplacement_archive_pkey PRIMARY KEY (emplacement);


--
-- Name: emplacement emplacement_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement
    ADD CONSTRAINT emplacement_pkey PRIMARY KEY (emplacement);


--
-- Name: entreprise entreprise_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY entreprise
    ADD CONSTRAINT entreprise_pkey PRIMARY KEY (entreprise);


--
-- Name: genealogie genealogie_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_pkey PRIMARY KEY (genealogie);


--
-- Name: lien_parente lien_parente_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY lien_parente
    ADD CONSTRAINT lien_parente_pkey PRIMARY KEY (lien_parente);


--
-- Name: operation_archive operation_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_archive
    ADD CONSTRAINT operation_archive_pkey PRIMARY KEY (operation);


--
-- Name: operation_defunt_archive operation_defunt_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_defunt_archive
    ADD CONSTRAINT operation_defunt_archive_pkey PRIMARY KEY (operation_defunt);


--
-- Name: operation_defunt operation_defunt_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_defunt
    ADD CONSTRAINT operation_defunt_pkey PRIMARY KEY (operation_defunt);


--
-- Name: operation operation_numdossier_unique_key; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT operation_numdossier_unique_key UNIQUE (numdossier);


--
-- Name: operation operation_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT operation_pkey PRIMARY KEY (operation);


--
-- Name: plans plans_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY plans
    ADD CONSTRAINT plans_pkey PRIMARY KEY (plans);


--
-- Name: sepulture_type sepulture_type_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY sepulture_type
    ADD CONSTRAINT sepulture_type_pkey PRIMARY KEY (sepulture_type);


--
-- Name: tarif tarif_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY tarif
    ADD CONSTRAINT tarif_pkey PRIMARY KEY (tarif);


--
-- Name: tarif tarif_unique_key; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY tarif
    ADD CONSTRAINT tarif_unique_key UNIQUE (annee, origine, terme, duree, nature, sepulture_type);


--
-- Name: titre_de_civilite titre_de_civilite_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY titre_de_civilite
    ADD CONSTRAINT titre_de_civilite_pkey PRIMARY KEY (titre_de_civilite);


--
-- Name: travaux_archive travaux_archive_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux_archive
    ADD CONSTRAINT travaux_archive_pkey PRIMARY KEY (travaux);


--
-- Name: travaux_nature travaux_nature_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux_nature
    ADD CONSTRAINT travaux_nature_pkey PRIMARY KEY (travaux_nature);


--
-- Name: travaux travaux_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux
    ADD CONSTRAINT travaux_pkey PRIMARY KEY (travaux);


--
-- Name: voie voie_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_pkey PRIMARY KEY (voie);


--
-- Name: voie_type voie_type_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY voie_type
    ADD CONSTRAINT voie_type_pkey PRIMARY KEY (voie_type);


--
-- Name: zone zone_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_pkey PRIMARY KEY (zone);


--
-- Name: zone_type zone_type_pkey; Type: CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY zone_type
    ADD CONSTRAINT zone_type_pkey PRIMARY KEY (zone_type);


--
-- Name: tarif_unique_index_key; Type: INDEX; Schema: opencimetiere; Owner: -
--

CREATE UNIQUE INDEX tarif_unique_index_key ON tarif USING btree (annee, origine, terme, duree, nature, COALESCE(sepulture_type, '-1'::integer));


--
-- Name: autorisation_archive autorisation_archive_titre_de_civilite_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY autorisation_archive
    ADD CONSTRAINT autorisation_archive_titre_de_civilite_fkey FOREIGN KEY (titre) REFERENCES titre_de_civilite(titre_de_civilite);


--
-- Name: autorisation autorisation_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY autorisation
    ADD CONSTRAINT autorisation_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: autorisation autorisation_titre_de_civilite_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY autorisation
    ADD CONSTRAINT autorisation_titre_de_civilite_fkey FOREIGN KEY (titre) REFERENCES titre_de_civilite(titre_de_civilite);


--
-- Name: contrat contrat_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY contrat
    ADD CONSTRAINT contrat_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: courrier courrier_autorisation_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_autorisation_fkey FOREIGN KEY (destinataire) REFERENCES autorisation(autorisation);


--
-- Name: courrier courrier_contrat_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_contrat_fkey FOREIGN KEY (contrat) REFERENCES contrat(contrat);


--
-- Name: courrier courrier_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: defunt_archive defunt_archive_titre_de_civilite_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY defunt_archive
    ADD CONSTRAINT defunt_archive_titre_de_civilite_fkey FOREIGN KEY (titre) REFERENCES titre_de_civilite(titre_de_civilite);


--
-- Name: defunt defunt_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY defunt
    ADD CONSTRAINT defunt_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: defunt defunt_titre_de_civilite_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY defunt
    ADD CONSTRAINT defunt_titre_de_civilite_fkey FOREIGN KEY (titre) REFERENCES titre_de_civilite(titre_de_civilite);


--
-- Name: dossier dossier_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: emplacement_archive emplacement_archive_sepulture_type_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement_archive
    ADD CONSTRAINT emplacement_archive_sepulture_type_fkey FOREIGN KEY (sepulturetype) REFERENCES sepulture_type(sepulture_type);


--
-- Name: emplacement emplacement_plans_key; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement
    ADD CONSTRAINT emplacement_plans_key FOREIGN KEY (plans) REFERENCES plans(plans);


--
-- Name: emplacement emplacement_sepulture_type_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement
    ADD CONSTRAINT emplacement_sepulture_type_fkey FOREIGN KEY (sepulturetype) REFERENCES sepulture_type(sepulture_type);


--
-- Name: emplacement emplacement_voie_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY emplacement
    ADD CONSTRAINT emplacement_voie_fkey FOREIGN KEY (voie) REFERENCES voie(voie);


--
-- Name: genealogie genealogie_autorisation_p1_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_autorisation_p1_fkey FOREIGN KEY (autorisation_p1) REFERENCES autorisation(autorisation);


--
-- Name: genealogie genealogie_autorisation_p2_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_autorisation_p2_fkey FOREIGN KEY (autorisation_p2) REFERENCES autorisation(autorisation);


--
-- Name: genealogie genealogie_defunt_p1_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_defunt_p1_fkey FOREIGN KEY (defunt_p1) REFERENCES defunt(defunt);


--
-- Name: genealogie genealogie_defunt_p2_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_defunt_p2_fkey FOREIGN KEY (defunt_p2) REFERENCES defunt(defunt);


--
-- Name: genealogie genealogie_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: genealogie genealogie_lien_parente_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY genealogie
    ADD CONSTRAINT genealogie_lien_parente_fkey FOREIGN KEY (lien_parente) REFERENCES lien_parente(lien_parente);


--
-- Name: operation_defunt operation_defunt_defunt_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_defunt
    ADD CONSTRAINT operation_defunt_defunt_fkey FOREIGN KEY (defunt) REFERENCES defunt(defunt);


--
-- Name: operation_defunt operation_defunt_operation_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_defunt
    ADD CONSTRAINT operation_defunt_operation_fkey FOREIGN KEY (operation) REFERENCES operation(operation);


--
-- Name: operation_defunt operation_defunt_titre_de_civilite_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation_defunt
    ADD CONSTRAINT operation_defunt_titre_de_civilite_fkey FOREIGN KEY (defunt_titre) REFERENCES titre_de_civilite(titre_de_civilite);


--
-- Name: operation operation_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT operation_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: tarif tarif_sepulture_type_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY tarif
    ADD CONSTRAINT tarif_sepulture_type_fkey FOREIGN KEY (sepulture_type) REFERENCES sepulture_type(sepulture_type);


--
-- Name: travaux_archive travaux_archive_travaux_nature_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux_archive
    ADD CONSTRAINT travaux_archive_travaux_nature_fkey FOREIGN KEY (naturetravaux) REFERENCES travaux_nature(travaux_nature);


--
-- Name: travaux travaux_emplacement_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux
    ADD CONSTRAINT travaux_emplacement_fkey FOREIGN KEY (emplacement) REFERENCES emplacement(emplacement);


--
-- Name: travaux travaux_entreprise_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux
    ADD CONSTRAINT travaux_entreprise_fkey FOREIGN KEY (entreprise) REFERENCES entreprise(entreprise);


--
-- Name: travaux travaux_travaux_nature_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY travaux
    ADD CONSTRAINT travaux_travaux_nature_fkey FOREIGN KEY (naturetravaux) REFERENCES travaux_nature(travaux_nature);


--
-- Name: voie voie_voie_type_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_voie_type_fkey FOREIGN KEY (voietype) REFERENCES voie_type(voie_type);


--
-- Name: voie voie_zone_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_zone_fkey FOREIGN KEY (zone) REFERENCES zone(zone);


--
-- Name: zone zone_cimetiere_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_cimetiere_fkey FOREIGN KEY (cimetiere) REFERENCES cimetiere(cimetiere);


--
-- Name: zone zone_zone_type_fkey; Type: FK CONSTRAINT; Schema: opencimetiere; Owner: -
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_zone_type_fkey FOREIGN KEY (zonetype) REFERENCES zone_type(zone_type);


--
-- PostgreSQL database dump complete
--

