--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v4.2.0-b3 depuis la version v4.2.0-b2
--
-- @package opencimetiere
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
-- BEGIN -  [#10000] - Ajout des champs habilitation pour les entreprises
--

ALTER TABLE entreprise
    ADD COLUMN IF NOT EXISTS numero_habilitation character varying(40),
    ADD COLUMN IF NOT EXISTS date_fin_validite_habilitation date;

--
-- END -  [#10000] - Ajout des champs habilitation pour les entreprises
--

---
--- BEGIN [#10001] Ajout de l'action d'édition sur les opérations traitées
---

ALTER TABLE ONLY operation
    ADD COLUMN IF NOT EXISTS edition_operation character varying(40);

---
--- END [#10001] Ajout de l'action d'édition sur les opérations traitées
---
