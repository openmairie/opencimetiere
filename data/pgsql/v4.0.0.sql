--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v4.0.0 d'openCimetière
--
-- @package opencimetiere
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- BEGIN / [#9017] Mise à jour du framework openMairie vers la version 4.9.0
--

-- Les scripts app/ liés aux opérations sont désormais gérés dans des méthodes
-- de la classe opération
UPDATE operation set categorie = 'reduction' where categorie = 'reduction_enfeu';

--
-- END / [#9017] Mise à jour du framework openMairie vers la version 4.9.0
--
