--
-- BEGIN - [#10284] - Ajout du champ commentaire pour la généalogie 
--

ALTER TABLE ONLY genealogie 
	ADD COLUMN IF NOT EXISTS commentaire text;

--
-- END - [#10284] - Ajout du champ commentaire pour la généalogie 
--