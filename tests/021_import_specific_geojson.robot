*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...

*** Test Cases ***
Import GeoJSON
    [Documentation]  L'import GeoJSON permet la mise à jour des informations
    ...  géographiques de données métier dans l'application. Les actions
    ...  possibles sont l'ajout, la modification ou la suppression de la valeur
    ...  dans un champ géographique donné. Par exemple, on peut mettre à jour
    ...  le champ pgeom d'un emplacement.

    Depuis la page d'accueil  admin  admin

    # On ne remplit aucun champ
    Depuis l'écran principal du module 'Import'
    Element Should Contain  css=#import-list .import-list-specific  geojson
    Click Element  css=a#action-import-specific_geojson-importer
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-geojson-import form div.formControls input
    Error Message Should Contain  Le champ Fichier GEOJson est obligatoire
    Error Message Should Contain  Le champ Table est obligatoire
    Error Message Should Contain  Le champ Clé primaire - Nom de la colonne dans la table est obligatoire
    Error Message Should Contain  Le champ Champ géométrique - Nom de la colonne dans la table est obligatoire
    Error Message Should Contain  Le champ Clé primaire - Nom de la propriété dans le fichier GEOJson est obligatoire

    # La table n'existe pas
    Depuis l'écran principal du module 'Import'
    Element Should Contain  css=#import-list .import-list-specific  geojson
    Click Element  css=a#action-import-specific_geojson-importer
    Add File  fic1  import-geojson-01.geojson
    Input Text  table  nexistepas
    Input Text  primarykey_fieldname  emplacement
    Input Text  geom_fieldname  pgeom
    Input Text  primarykey_propertyname  id_conc
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-geojson-import form div.formControls input
    Error Message Should Be  Le champ *Table* passé en paramètre n'existe pas.

    # La clé primaire n'existe pas
    Depuis l'écran principal du module 'Import'
    Element Should Contain  css=#import-list .import-list-specific  geojson
    Click Element  css=a#action-import-specific_geojson-importer
    Add File  fic1  import-geojson-01.geojson
    Input Text  table  emplacement
    Input Text  primarykey_fieldname  nexistepas
    Input Text  geom_fieldname  pgeom
    Input Text  primarykey_propertyname  id_conc
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-geojson-import form div.formControls input
    Error Message Should Be  Le champ *Clé primaire - Nom de la colonne dans la table* passé en paramètre n'existe pas.

    # La propriété json n'existe pas
    Depuis l'écran principal du module 'Import'
    Element Should Contain  css=#import-list .import-list-specific  geojson
    Click Element  css=a#action-import-specific_geojson-importer
    Add File  fic1  import-geojson-01.geojson
    Input Text  table  emplacement
    Input Text  primarykey_fieldname  emplacement
    Input Text  geom_fieldname  pgeom
    Input Text  primarykey_propertyname  nexistepas
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-geojson-import form div.formControls input
    Valid Message Should Be  Le traitement d'import est terminé. 0 éléments importés avec succès sur 8.

    # Les champs sont obligatoires
    Depuis l'écran principal du module 'Import'
    Element Should Contain  css=#import-list .import-list-specific  geojson
    Click Element  css=a#action-import-specific_geojson-importer
    Add File  fic1  import-geojson-01.geojson
    Input Text  table  emplacement
    Input Text  primarykey_fieldname  emplacement
    Input Text  geom_fieldname  pgeom
    Input Text  primarykey_propertyname  id_conc
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-geojson-import form div.formControls input
    Valid Message Should Be  Le traitement d'import est terminé. 7 éléments importés avec succès sur 8.

