*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Set Suite Variable  ${testid}  500
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{voie02_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=V${testid}-02
    Ajouter la voie  ${voie02_zone01_cim01}
    Set Suite Variable  ${voie02_zone01_cim01}
    #
    #
    &{concession02} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}
    Set Suite Variable  ${concession02_id}
    #
    &{ossuaire01} =  Create Dictionary
    ...  famille=OSSUAIRE${testid}
    ...  numero=18
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${ossuaire01_id} =  Ajouter l'ossuaire  ${ossuaire01}
    Set Suite Variable  ${ossuaire01_id}
    Set Suite Variable  ${ossuaire01}
    #
    &{entreprise01} =  Create Dictionary
    ...  nomentreprise=ENTREPRISE${testid}-01
    ...  pf=Non
    ...  adresse1=12 rue de la République
    ...  adresse2=Parc de la Durance
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  telephone=00999999999
    Ajouter l'entreprise  ${entreprise01}
    Set Suite Variable  ${entreprise01}


Listings d'éléments archivés
    [Documentation]  Ce TestCase permet simplement d'afficher les listings d'éléments archivés
    ...  pour vérifier que l'intégration est correcte et qu'aucune erreur n'est levée.
    #
    Depuis la page d'accueil  admin  admin
    # Emplacements
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  emplacement_archive
    Page Title Should Be  Application > Emplacement Archivé
    First Tab Title Should Be  Emplacement Archivé
    # Défunts
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  defunt_archive
    Page Title Should Be  Application > Défunt Archivé
    First Tab Title Should Be  Défunt Archivé
    # Autorisations
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  autorisation_archive
    Page Title Should Be  Archives > Contact Archivé
    First Tab Title Should Be  Contact Archivé
    # Contrat
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  contrat_archive
    Page Title Should Be  Archives > Contrat Archivé
    First Tab Title Should Be  Contrat Archivé
    # Opérations
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  operation_archive
    Page Title Should Be  Application > Opération Archivée
    First Tab Title Should Be  Opération Archivée


Fin de concession
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01}
    Set Suite Variable  ${concession01_id}
    #
    &{operation01} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=Marcel
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${operation01_id} =  Ajouter l'opération d'inhumation sur concession  ${operation01}
    Valider l'opération d'inhumation sur concession  ${operation01_id}
    #
    &{dossier01} =  Create Dictionary
    ...  fichier=p-concession-1.jpg
    ...  datedossier=04/06/2013
    ...  typedossier=photo
    ...  observation=Photo de l'emplacement
    Ajouter le dossier dans le contexte de la concession  ${dossier01}  ${concession01_id}
    #
    &{concessionnaire01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL${testid}
    ...  marital=
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 rue de la République
    ...  adresse2=
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  dcd=true
    ...  observation=
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire01}  ${concession01_id}
    #
    &{lettretype01} =  Create Dictionary
    ...  id=test${testid}concessionscourrier
    ...  libelle=test${testid}concessionscourrier actif
    ...  titre=test${testid}concessionscourrier
    ...  corps=test${testid}concessionscourrier
    ...  sql=Requête COURRIER
    ...  actif=true
    Ajouter la lettre-type depuis le menu
    ...  ${lettretype01.id}
    ...  ${lettretype01.libelle}
    ...  ${lettretype01.titre}
    ...  ${lettretype01.corps}
    ...  ${lettretype01.sql}
    ...  ${lettretype01.actif}
    #
    &{courrier01} =  Create Dictionary
    ...  destinataire=(concessionnaire) ${concessionnaire01.nom} ${concessionnaire01.prenom}
    ...  lettretype=${lettretype01.id} ${lettretype01.libelle}
    ...  complement=Complément ${testid}
    Ajouter le courrier dans le contexte de la concession  ${courrier01}  ${concession01_id}
    #
    &{travaux01} =  Create Dictionary
    ...  entreprise=${entreprise01.nomentreprise}
    ...  datedebinter=12/01/2015
    ...  datefininter=13/01/2015
    ...  observation=COmmentaires ${testid}
    ...  naturedemandeur=concessionnaire
    ...  naturetravaux=Autorisation de recouvrement
    Ajouter le travaux dans le contexte de la concession  ${travaux01}  ${concession01_id}
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession01_id}
    Click On Link  ${concession01_id}
    #
    Page Title Should Be  Archives > Fin De Concession > ${concession01_id} ${concession01.famille}
    La page ne doit pas contenir d'erreur
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    #
    Page Title Should Be  Archives > Fin De Concession > ${concession01_id} ${concession01.famille}
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Error Message Should Contain  Le champ date d'exhumation est obligatoire
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession01_id}
    Click On Link  ${concession01_id}
    #
    Page Title Should Be  Archives > Fin De Concession > ${concession01_id} ${concession01.famille}
    La page ne doit pas contenir d'erreur
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2017
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    #
    Page Title Should Be  Archives > Fin De Concession > ${concession01_id} ${concession01.famille}
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement est terminé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement ajouté table emplacement_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement supprimé table emplacement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Télécharger le fichier traces
    #
    Click On Back Button
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession


Fin de colombarium
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_colombarium
    Page Title Should Be  Archives > Fin De Colombarium
    First Tab Title Should Be  Fin De Colombarium


Fin de terrain communal
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_terrain_communal
    Page Title Should Be  Archives > Fin De Terrain Communal
    First Tab Title Should Be  Fin De Terrain Communal


Fin d'enfeu
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_enfeu
    Page Title Should Be  Archives > Fin D'enfeu
    First Tab Title Should Be  Fin D'enfeu


Archivage d'une concession ayant des travaux liés
    [Documentation]  Archivage d'une concession ayant des travaux liés.
    #
    Depuis la page d'accueil  admin  admin
    &{concession_travaux} =  Create Dictionary
    ...  famille=DURANDTRAVAUX
    ...  numero=57
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession_travaux_id} =  Ajouter la concession  ${concession_travaux}
    #
    &{entreprise} =  Create Dictionary
    ...  nomentreprise=ENTDURANDTRAVAUX
    Ajouter l'entreprise  ${entreprise}

    # On ajoute des travaux sans aucune données
    Depuis l'onglet 'travaux' de la concession  ${concession_travaux_id}
    Click On Add Button JS
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On ajoute des travaux avec une nature des travaux
    Depuis l'onglet 'travaux' de la concession  ${concession_travaux_id}
    Click On Add Button JS
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label  css=#sousform-travaux #naturetravaux  Autorisation de recouvrement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On ajoute des travaux avec une entreprise
    Depuis l'onglet 'travaux' de la concession  ${concession_travaux_id}
    Click On Add Button JS
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label  css=#sousform-travaux #entreprise  ENTDURANDTRAVAUX
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # Tentative d'archivage
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession_travaux_id}
    Click On Link  ${concession_travaux_id}
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2018
    Select From List By Label  ossuaire  ${ossuaire01_id} - ${ossuaire01.famille}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Page Should Contain  1 enregistrement ajouté table travaux_archive


Archivage d'un concession ayant des opérations
    [Documentation]  Archivage d'une concession ayant une opération
    ...  de transfert de défunt vers une autre concession.
    #
    Depuis la page d'accueil  admin  admin

    &{concession_op_1} =  Create Dictionary
    ...  famille=PIERETTEOP
    ...  numero=13
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession_op_1.id} =  Ajouter la concession  ${concession_op_1}

    &{concession_op_2} =  Create Dictionary
    ...  famille=MICHELOP
    ...  numero=69
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession_op_2.id} =  Ajouter la concession  ${concession_op_2}

    # Ajout de defunt dans cette concession 1
    &{defunt_operation} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=PIERREBIENTOTMICHEL
    ...  marital=
    ...  prenom=
    ...  observation=
    ...  nature=cercueil
    ${defunt_operation.id} =  Ajouter le defunt dans le contexte de la concession  ${defunt_operation}  ${concession_op_1.id}

    # opération de transfert
    &{operation01} =  Create Dictionary
    ...  date=31/05/2018
    ...  heure=09:00:00
    ...  emplacement_autocomplete_search=(${concession_op_1.id})
    ...  emplacement_autocomplete_id=${concession_op_1.id}
    ...  emplacement_transfert_autocomplete_search=(${concession_op_2.id})
    ...  emplacement_transfert_autocomplete_id=${concession_op_2.id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ${operation01.id} =  Ajouter l'opération de transfert  ${operation01}

    # Selection du defunt à transférer
    Depuis le contexte de l'opération de transfert  ${operation01.id}
    ${operation01.numdossier} =  Get Text  css=#numdossier
    Click Element  operation_defunt
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=.add-16
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label  defunt  PIERREBIENTOTMICHEL
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]

    Valider l'opération de transfert  ${operation01.id}

    # opération d'inhumation
    &{operation02} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession_op_1.id})
    ...  emplacement_autocomplete_id=${concession_op_1.id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=MARCEL
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${operation02.id} =  Ajouter l'opération d'inhumation sur concession  ${operation02}
    ${operation02.numdossier} =  Get Text  css=#numdossier
    Valider l'opération d'inhumation sur concession  ${operation02.id}

    # opération de réduction
    &{defunt01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=SUPONT${testid}MERGEFIELDS
    ...  marital=
    ...  prenom=MARCELMERGEFIELDS
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt01}  ${concession_op_1.id}

    &{operation03} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession_op_1.id})
    ...  emplacement_autocomplete_id=${concession_op_1.id}
    ${operation03.id} =  Ajouter l'opération de réduction sur concession  ${operation03}

    Depuis le contexte de l'opération de réduction sur concession  ${operation03.id}
    ${operation03.numdossier} =  Get Text  css=#numdossier
    Click Element  css=#operation_defunt
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Add Button

    Select From List By Label  css=#defunt  ${defunt01.nom} ${defunt01.prenom}
    Click On Submit Button In Subform

    Valider l'opération de réduction sur concession  ${operation03.id}

    # Tentative d'archivage
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession_op_2.id}
    Click On Link  ${concession_op_2.id}
    # Archivage
    Input Text  css=input[name="dateexhumation"]  10/05/2018
    Select From List By Label  ossuaire  ${ossuaire01_id} - ${ossuaire01.famille}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    # Il ne faut pas d'erreur de bdd
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement est terminé.

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    Page Title Should Be  Archives > Fin De Concession
    First Tab Title Should Be  Fin De Concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession_op_1.id}
    Click On Link  ${concession_op_1.id}
    # Archivage
    Input Text  css=input[name="dateexhumation"]  10/05/2018
    Select From List By Label  ossuaire  ${ossuaire01_id} - ${ossuaire01.famille}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    # Il ne faut pas d'erreur de bdd
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement est terminé.
    # On vérifie le retour pour les trois opérations présentes dans l'emplacement archivé
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  archive concession ${concession_op_1.id}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement transfert ${operation01.id} n° de dossier ${operation01.numdossier} dans la table operation_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement ${defunt_operation.nom} ${defunt_operation.marital} ${defunt_operation.prenom} id d'operation : ${operation01.id} dans la table operation_defunt_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement inhumation ${operation02.id} n° de dossier ${operation02.numdossier} dans la table operation_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement ${operation02.defunt_nom} ${operation02.defunt_marital} ${operation02.defunt_prenom} id d'operation : ${operation02.id} dans la table operation_defunt_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement reduction ${operation03.id} n° de dossier ${operation03.numdossier} dans la table operation_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  enregistrement ${defunt01.nom} ${defunt01.marital} ${defunt01.prenom} id d'operation : ${operation03.id} dans la table operation_defunt_archive

    # On vérifie que les opérations apparaîssent dans le menu archive
    Depuis le listing des emplacements archivées
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Use Simple Search  famille  ${concession_op_1.famille}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  link:${concession_op_1.famille}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#operation_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  transfert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  inhumation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  reduction

Archivage d'une concession possédant un courrier sans destinataire
    Depuis la page d'accueil  admin  admin
    &{cimetiere03} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-03
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere03}
    #
    &{zone01_cim03} =  Create Dictionary
    ...  cimetiere=${cimetiere03.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-03
    Ajouter la zone  ${zone01_cim03}
    #
    &{voie01_zone01_cim03} =  Create Dictionary
    ...  zone=${zone01_cim03.zonetype} ${zone01_cim03.zonelib} (${zone01_cim03.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-03
    Ajouter la voie  ${voie01_zone01_cim03}
    #
    &{lettretype02} =  Create Dictionary
    ...  id=02test${testid}concessionscourrier
    ...  libelle=02test${testid}concessionscourrier actif
    ...  titre=02test${testid}concessionscourrier
    ...  corps=02test${testid}concessionscourrier
    ...  sql=Requête COURRIER
    ...  actif=true
    Ajouter la lettre-type depuis le menu
    ...  ${lettretype02.id}
    ...  ${lettretype02.libelle}
    ...  ${lettretype02.titre}
    ...  ${lettretype02.corps}
    ...  ${lettretype02.sql}
    ...  ${lettretype02.actif}
    #
    &{concession_sans_dest} =  Create Dictionary
    ...  famille=TEST${testid}NOM
    ...  numero=12
    ...  cimetierelib=${cimetiere03.cimetierelib}
    ...  zonelib=${zone01_cim03.zonetype} ${zone01_cim03.zonelib}
    ...  voielib=${voie01_zone01_cim03.voietype} ${voie01_zone01_cim03.voielib}
    ${concession_sans_dest_id} =  Ajouter la concession  ${concession_sans_dest}
    &{courrier01} =  Create Dictionary
    ...  lettretype=${lettretype02.id} ${lettretype02.libelle}
    ...  complement=Complément ${testid}
    Ajouter le courrier dans le contexte de la concession  ${courrier01}  ${concession_sans_dest_id}
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession_sans_dest_id}
    Click On Link  ${concession_sans_dest_id}
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2017
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    #
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement est terminé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement ajouté table emplacement_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement supprimé table emplacement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement ajouté table courrier_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement supprimé table courrier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Télécharger le fichier traces


Archivage d'un terrain communal avec création d'un emplacement libre
    [Documentation]  Vérifie que le terme et la durée sont présents sur l'emplacement libre créé
    &{terrain_communal_01} =  Create Dictionary
    ...  famille=TEST500FAMILLETC
    ...  numero=145
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  datevente=12/02/2015
    ${terrain_communal_01_id} =  Ajouter le terrain communal  ${terrain_communal_01}

    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_terrain_communal
    # On recherche l'enregistrement
    Use Simple Search  id  ${terrain_communal_01_id}
    Click Element  link:${terrain_communal_01_id}
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2017
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Submit Button

    Depuis le listing des terrains communaux
    Click Element  link:emplacement libre

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#terme  temporaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#duree  5

Archivage d'une concession ayant des contrats lié
    [Documentation]  Archivage d'une concession ayant des travaux liés.
    #
    Depuis la page d'accueil  admin  admin
    &{concession_contrat} =  Create Dictionary
    ...  famille=DURANDCONTRAT
    ...  numero=57
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession_contrat_id} =  Ajouter la concession  ${concession_contrat}
    #
    &{contrat01} =  Create Dictionary
    ...  emplacement=${concession_contrat_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=15
    ...  datevente=19/03/2020
    ...  valide=true
    ${contrat01_id} =  Ajouter un contrat depuis l'emplacement  ${contrat01}
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession_contrat_id}
    Click On Link  ${concession_contrat_id}
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2017
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    #
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le traitement est terminé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement ajouté table emplacement_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement supprimé table emplacement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement ajouté table contrat_archive
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  1 enregistrement supprimé table contrat
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Télécharger le fichier traces
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  contrat_archive
    Use Simple Search  contrat  ${contrat01_id}
    Click Element  link:${contrat01.emplacement}
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=#contrat  ${contrat01_id}
    Element Should Contain  css=#emplacement  ${contrat01.emplacement}
    Element Should Contain  css=#datedemande  ${contrat01.datedemande}
    Element Should Contain  css=#datevente  ${contrat01.datevente}
    Element Should Contain  css=#origine  achat


Gestion des champs de défunt et contact lors d'un archivage
    [Documentation]  Vérifie que les données sont bien présent lors d'un archivage
    Depuis la page d'accueil  admin  admin
    &{concession02} =  Create Dictionary
    ...  famille=DURAND${testid}MANAGEFIELDSARCHIVE
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}
    #
    &{concessionnaire02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL${testid}MANAGEFIELDSARCHIVE
    ...  marital=
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  telephone1=06.68.50.49.67
    ...  telephone2=06.87.46.49.56
    ...  courriel=henri.lamalice@mail.com
    ...  parente=LOLO
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire02}  ${concession02_id}
    #
    Element Should Contain  css=#nom  ${concessionnaire02.nom}
    Element Should Contain  css=#prenom  ${concessionnaire02.prenom}
    Element Should Contain  css=#datenaissance  ${concessionnaire02.datenaissance}
    Element Should Contain  css=#adresse1  ${concessionnaire02.adresse1}
    Element Should Contain  css=#cp  ${concessionnaire02.cp}
    Element Should Contain  css=#ville  ${concessionnaire02.ville}
    Element Should Contain  css=#telephone1  ${concessionnaire02.telephone1}
    Element Should Contain  css=#telephone2  ${concessionnaire02.telephone2}
    Element Should Contain  css=#courriel  ${concessionnaire02.courriel}
    Element Should Contain  css=#parente  ${concessionnaire02.parente}
    #
    Go To Dashboard
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  archive  fin_concession
    # On recherche l'enregistrement
    Use Simple Search  id  ${concession02_id}
    Click On Link  ${concession02_id}
    #
    Input Text  css=input[name="dateexhumation"]  10/05/2017
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    #
    Go To Submenu In Menu  archive  autorisation_archive
    #
    Click Element  link:${concessionnaire02.nom}
    #
    Element Should Contain  css=#nom  ${concessionnaire02.nom}
    Element Should Contain  css=#prenom  ${concessionnaire02.prenom}
    Element Should Contain  css=#datenaissance  ${concessionnaire02.datenaissance}
    Element Should Contain  css=#adresse1  ${concessionnaire02.adresse1}
    Element Should Contain  css=#cp  ${concessionnaire02.cp}
    Element Should Contain  css=#ville  ${concessionnaire02.ville}
    Element Should Contain  css=#telephone1  ${concessionnaire02.telephone1}
    Element Should Contain  css=#telephone2  ${concessionnaire02.telephone2}
    Element Should Contain  css=#courriel  ${concessionnaire02.courriel}
    Element Should Contain  css=#parente  ${concessionnaire02.parente}

