*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Ajout d'un contrat depuis le menu Recherche Contrat
    Depuis la page d'accueil  admin  admin
    #
    &{concession01} =  Create Dictionary
    ...  famille=DUPONT120
    ...  numero=14
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 1
    ...  voielib=ALLEE A
    ${concession01_id} =  Ajouter la concession  ${concession01}

    Depuis la page d'accueil  admin  admin
    #
    Depuis le formulaire d'ajout d'un contrat
    Page Title Should Be   > Contrats
    #
    Click On Submit Button
    Error Message Should Contain  Le champ date de demande est obligatoire
    Error Message Should Contain  Le champ date de vente est obligatoire
    Error Message Should Contain  Le champ durée est obligatoire
    Error Message Should Contain  Le champ emplacement est obligatoire
    Error Message Should Contain  Le champ origine est obligatoire
    Error Message Should Contain  Le champ terme est obligatoire
    Error Message Should Contain  SAISIE NON ENREGISTRÉE

    &{contrat01} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    Saisir un contrat  ${contrat01}
    Click On Submit Button

    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${contrat01_id} =  Get Value  css=div.contrat-form #contrat
    #
    &{contrat01_valide} =  Create Dictionary
    ...  valide=true
    Modifier un contrat  ${contrat01_id}  ${contrat01_valide}


Vérification du bon fonctionnement du lien vers l'emplacement
    Depuis la page d'accueil  admin  admin
    #
    &{concession02} =  Create Dictionary
    ...  famille=DUPONT12002
    ...  numero=14
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 2
    ...  voielib=ALLEE B
    ${concession02_id} =  Ajouter la concession  ${concession02}

    Depuis la page d'accueil  admin  admin

    &{contrat02} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession02_id})
    ...  emplacement_autocomplete_id=${concession02_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    ${contrat02_id} =  Ajouter un contrat  ${contrat02}
    Log  ${contrat02_id}
    Depuis le contexte du contrat  ${contrat02_id}

    Element Should Contain  css=#link_emplacement  concession (${concession02_id}) - Famille : ${concession02.famille}
    Click Element  css=#link_emplacement
    Page Title Should Be  Emplacements > Concession > ${concession02_id} ${concession02.famille}
    La page ne doit pas contenir d'erreur

    &{contrat02_valide} =  Create Dictionary
    ...  valide=true
    Modifier un contrat  ${contrat02_id}  ${contrat02_valide}


Vérification du bon fonctionnement du widget des contrats
    Depuis la page d'accueil  admin  admin
    #
    &{concession03} =  Create Dictionary
    ...  famille=DUPONT12003
    ...  numero=14
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 3
    ...  voielib=ALLEE C
    ${concession03_id} =  Ajouter la concession  ${concession03}
    #
    Depuis la page d'accueil  admin  admin
    #
    &{contrat03} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    ${contrat03_id} =  Ajouter un contrat  ${contrat03}
    # On vérifie que l'action de consultation amène bien sur le contrat
    Depuis la page d'accueil  admin  admin
    Click Element  css=#action-tab-contrat-left-consulter-${contrat03_id}
    Page Title Should Be  > Contrats > Id ${contrat03_id} - Achat Du ${DATE_FORMAT_DD/MM/YYYY}
    La page ne doit pas contenir d'erreur
    # On vérifie que le lien vers tout les contrats à valider amène bien vers le listing des contrats avec valider à "Non"
    Depuis la page d'accueil  admin  admin
    Click Element  link:Voir tous les contrats a valider
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Value Should Be  css=#advanced-form select#valide  false
    # On vérifie que le widget prend bien en compte les contrats non validés
    Depuis la page d'accueil  admin  admin
    &{contrat03_valide} =  Create Dictionary
    ...  valide=true
    Modifier un contrat  ${contrat03_id}  ${contrat03_valide}
    Depuis la page d'accueil  admin  admin
    Element Should Contain  css= div.widget_contrat_a_valider .widget-content  Aucun enregistrement.


Vérification du bon fonctionnement de l'action valider
    Depuis la page d'accueil  admin  admin
    #
    &{concession04} =  Create Dictionary
    ...  famille=DUPONT12003
    ...  numero=14
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 3
    ...  voielib=ALLEE C
    ${concession04_id} =  Ajouter la concession  ${concession04}
    #
    Depuis la page d'accueil  admin  admin
    #
    &{contrat04} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession04_id})
    ...  emplacement_autocomplete_id=${concession04_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    ${contrat04_id} =  Ajouter un contrat  ${contrat04}

    Depuis le contexte du contrat  ${contrat04_id}
    Portlet Action Should Be In Form  contrat  valider
    Click On Form Portlet Action  contrat  valider  modale
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Message Should Contain  Le contrat a bien été validé.
    #
    Portlet Action Should Not Be In Form  contrat  valider


Ajout d'un courrier dans le contexte d'un contrat
    [Documentation]  Ce test a pour but de vérifier que losqu'on ajout un courrier
    ...  depuis un contrat, celui-ci soit bien lié à l'emplacement associé
    Depuis la page d'accueil  admin  admin

    # Pour être sûr que le filtre sur les destinataires est correct, on ajoute
    # une concession avec un concessionnaire que l'on ne va pas utiliser pour
    # être sûr que le contrat de test ne porte pas sur la première
    # concession et son concessionnaire
    &{concession05} =  Create Dictionary
    ...  famille=DUPONT12005
    ...  numero=14
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 3
    ...  voielib=ALLEE C
    ${concession05_id} =  Ajouter la concession  ${concession05}
    &{concessionnaire05} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=DUPONT12005-UNIQUE
    ...  prenom=HENRI
    ${concessionnaire05.id} =  Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire05}  ${concession05_id}

    # On ajoute ici une concession, un concessionnaire et un contrat pour tester
    # l'ajout du courrier
    &{concession06} =  Create Dictionary
    ...  famille=DUPONT12006
    ...  numero=15
    ...  cimetierelib=CIMETIÈRE DE MOULÈS
    ...  zonelib=CARRE 3
    ...  voielib=ALLEE C
    ${concession06_id} =  Ajouter la concession  ${concession06}
    &{concessionnaire06} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=DUPONT12006-UNIQUE
    ...  prenom=JACQUES
    ${concessionnaire06.id} =  Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire06}  ${concession06_id}
    &{contrat06} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession06_id})
    ...  emplacement_autocomplete_id=${concession06_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    ${contrat06_id} =  Ajouter un contrat  ${contrat06}

    # XXX Il faudrait vérifier ici la non présence du concessionnaire de l'autre concession
    #     dans le select des destinataires d'un nouveau courrier

    # On teste l'ajout du courrier
    &{courrier_edition} =  Create Dictionary
    ...  datecourrier=${DATE_FORMAT_DD/MM/YYYY}
    ...  lettretype=titre_de_recettes Titre de recettes provisoire
    ...  destinataire=(concessionnaire) ${concessionnaire06.nom} ${concessionnaire06.prenom}
    ${courrier_id} =  Ajouter le courrier dans le contexte du contrat  ${courrier_edition}  ${contrat06_id}

    # On vérifie que le courrier est bien présent dans l'onglet courrier dans le contexte de l'emplacement
    Depuis l'onglet 'courrier' de la concession  ${concession06_id}
    Element Should Contain  css=a.lienTable  ${courrier_id}


