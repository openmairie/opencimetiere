*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Vérification de l'action de prévisualisation sur les lettres types
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le contexte de la lettre-type  titre_de_recettes
    #
    Click On Form Portlet Action  om_lettretype  previsualiser  new_window
    #
    Open PDF  ${OM_PDF_TITLE}
    #
    Close PDF

Vérification de l'action de prévisualisation sur l'état operation
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le contexte de l'état  operation_10_inhumation
    #
    Click On Form Portlet Action  om_etat  previsualiser  new_window
    #
    Open PDF  ${OM_PDF_TITLE}
    #
    Close PDF

Vérification de l'action de prévisualisation sur l'état concession
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # XXX On ne paut pas utiliser le mot clé 'depuis le contexte de l'état' car
    #     il utilise click link sur la châine 'concession' qui est une entrée
    #     de menu. L'identifiant 1 ici n'est viable que parce que c'est le
    #     premier état initialisé mais ce n'est pas non plus une solution
    #     pérenne.
    Depuis le listing des états
    Click Element  css=#action-tab-om_etat-left-consulter-1
    Click On Form Portlet Action  om_etat  previsualiser  new_window
    #
    Open PDF  ${OM_PDF_TITLE}
    #
    Close PDF

Vérification de l'action de prévisualisation sur l'état défunt
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le contexte de l'état  defunt
    #
    Click On Form Portlet Action  om_etat  previsualiser  new_window
    #
    Open PDF  ${OM_PDF_TITLE}
    #
    Close PDF

Vérification de l'action de prévisualisation sur l'état voie
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le contexte de l'état  null  Récapitulatif VOIE
    #
    Click On Form Portlet Action  om_etat  previsualiser  new_window
    #
    Open PDF  ${OM_PDF_TITLE}
    #
    Close PDF
