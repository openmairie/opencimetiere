-------------------------------------------------------------------------------
-- 
--
-- @package opencimetiere
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
INSERT INTO emplacement (emplacement, nature, numero, complement, voie, numerocadastre, famille, numeroacte, datevente, terme, duree, dateterme, nombreplace, placeoccupe, superficie, placeconstat, dateconstat, observation, plans, positionx, positiony, photo, libre, sepulturetype, daterenouvellement, typeconcession, temp1, temp2, temp3, temp4, temp5, videsanitaire, semelle, etatsemelle, monument, etatmonument, largeur, profondeur, abandon, date_abandon, dateacte, geom, pgeom)
VALUES
(nextval('emplacement_seq'),
	'concession', 11, NULL, 18, '0', 'DUCHEMIN', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	397, 370,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A0800001002F97A9FA62941D29B28B412FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 13, NULL, 18, '0', 'DUPONTEL', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	414, 366,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A0800003013763CA3A62941707CAEE312FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 1, NULL, 18, '0', 'DURAND', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	280, 409,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A0800009F519E0590A62941E52B36F911FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 3, NULL, 18, '0', 'DUPONT', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	307, 404,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A08000080190CFB92A6294173235C1012FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 5, NULL, 18, '0', 'MARTIN', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	331, 396,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A08000067CF8B0097A629416153134A12FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 7, NULL, 18, '0', 'GARCIA', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	354, 388,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A0800000D57E51E9AA62941BEE4E36712FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 9, NULL, 18, '0', 'LOPEZ', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	374, 377,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A080000B6A1257A9CA6294104FF118C12FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 15, NULL, 18, '0', 'GAUTHIER', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	438, 362,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A08000096FD5F58A6A62941E2DB4B0F13FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'concession', 17, NULL, 18, '0', 'FAURE', '0', NULL, 'temporaire', 0, NULL, 0, 0, 0, 0, NULL, '',
	(SELECT plans FROM plans WHERE planslib='PLAN CIMETIÈRE DE MOULÈS'),
	464, 353,
	NULL, 'Non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Non', NULL, NULL,
	'01010000206A0800002D7C608FA9A629417BFC6F3E13FA5741',
	NULL)
,
(nextval('emplacement_seq'),
	'terraincommunal', 3, 'bis', 39, '0', 'GERARD', '0', NULL, 'temporaire', 5, NULL, 0, 0, 2, 0, NULL, '',
	NULL,
	10, 10,
	'', 'Non', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	'01010000206A0800006F4478097F942941A9E0F4D26DF95741', 
	NULL)
,
(nextval('emplacement_seq'),
	'ossuaire', 5, 'ter', 16, '0', 'OSSUAIRE MOULÈS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
	NULL, 
	NULL, NULL, 
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL,
	NULL)
;

