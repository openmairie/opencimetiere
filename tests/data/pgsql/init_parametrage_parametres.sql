--
-- PostgreSQL database dump
--

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;

-- SET search_path = opencimetiere, pg_catalog;

--
-- Data for Name: om_parametre; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

-- parametrage plan par defaut
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (3, 'prefixe_edition_substitution_vars', '', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (4, 'maire', 'M. DURAND', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (5, 'nom', 'M. DURAND', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (6, 'delaville', 'de la ville de Libreville', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (7, 'alavillede', 'à la ville de Libreville', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (8, 'adm_mairie', 'Mairie de Libreville', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (9, 'adm_cimetiere', 'Conservation des cimetières', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (10, 'adm_adresse', '10, Rue de la République', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (11, 'adm_fax', '04 05 06 07 08', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (12, 'adm_tel', '04 05 06 07 07', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (13, 'adm_ville', 'LIBREVILLE', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (14, 'adm_cp', '13000', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (15, 'adm_complement', 'Bât. A', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (16, 'taille_cercueil', '1', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (17, 'taille_urne', '0.1', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (18, 'taille_reduction', '0.5', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (19, 'temps_reduction', '5', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (20, 'duree_terraincommunal', '5', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (21, 'superficie_terraincommunal', '2', 1);


--
-- Name: om_parametre_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('om_parametre_seq', 22, false);


--
-- Data for Name: sepulture_type; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, '-', 'Fosse maçonnée haute', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, '-', 'Fosse maçonnée basse', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (3, '-', 'Cavurne', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (4, '-', 'Pierre tombale', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (5, '-', 'Caveau T2 haut', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (6, '-', 'Caveau T2 bas', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (7, '-', 'Caveau T1 haut', NULL, NULL, NULL);
INSERT INTO sepulture_type (sepulture_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (8, '-', 'Caveau T1 bas', NULL, NULL, NULL);


--
-- Name: sepulture_type_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('sepulture_type_seq', 9, false);


--
-- Data for Name: titre_de_civilite; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

INSERT INTO titre_de_civilite (titre_de_civilite, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, 'M.', 'Monsieur', NULL, NULL, NULL);
INSERT INTO titre_de_civilite (titre_de_civilite, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, 'Mlle', 'Mademoiselle', NULL, NULL, NULL);
INSERT INTO titre_de_civilite (titre_de_civilite, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (3, 'Mme', 'Madame', NULL, NULL, NULL);


--
-- Name: titre_de_civilite_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('titre_de_civilite_seq', 4, false);


--
-- Data for Name: travaux_nature; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, '-', 'Autorisation de travaux', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, '-', 'Permis de construire', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (3, '-', 'Autorisation de recouvrement', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (4, '-', 'Construction caveau T2 haut', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (5, '-', 'Construction caveau T2 bas', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (6, '-', 'Construction caveau T1 haut', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (7, '-', 'Construction caveau T1 bas', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (8, '-', 'Construction fosse maçonnée haute', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (9, '-', 'Construction fosse maçonnée  basse', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (10, '-', 'Construction d''un cavurne', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (11, '-', 'Construction pierre tombale', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (12, '-', 'Enlèvement pierre tombale', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (13, '-', 'Remise en place pierre tombale', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (14, '-', 'Enlèvement porte', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (15, '-', 'Remise en place porte', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (16, '-', 'Démolition-Reconstruction à l''identique', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (17, '-', 'Démolition-Reconstruction Transformation', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (18, '-', 'Creusement', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (19, '-', 'Surélévation', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (20, '-', 'Nettoyage-Consolidation', NULL, NULL, NULL);
INSERT INTO travaux_nature (travaux_nature, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (21, '-', 'Pompage eau', NULL, NULL, NULL);


--
-- Name: travaux_nature_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('travaux_nature_seq', 22, false);


--
-- Data for Name: voie_type; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, '-', 'ALLEE', NULL, NULL, NULL);
INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, '-', 'PLACE', NULL, NULL, NULL);
INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (3, '-', 'ILOT', NULL, NULL, NULL);
INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (4, '-', 'PASSAGE', NULL, NULL, NULL);
INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (5, '-', 'RANGEE', NULL, NULL, NULL);
INSERT INTO voie_type (voie_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (6, '-', 'DIVISION', NULL, NULL, NULL);


--
-- Name: voie_type_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('voie_type_seq', 7, false);


--
-- Data for Name: zone_type; Type: TABLE DATA; Schema: opencimetiere; Owner: -
--

INSERT INTO zone_type (zone_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, '-', 'COLLINE', NULL, NULL, NULL);
INSERT INTO zone_type (zone_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, '-', 'CARRE', NULL, NULL, NULL);
INSERT INTO zone_type (zone_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (3, '-', 'ENCLOS', NULL, NULL, NULL);
INSERT INTO zone_type (zone_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (4, '-', 'EXTENSION', NULL, NULL, NULL);
INSERT INTO zone_type (zone_type, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (5, '-', 'SECTION', NULL, NULL, NULL);


--
-- Name: zone_type_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: -
--

SELECT pg_catalog.setval('zone_type_seq', 6, false);


--
-- PostgreSQL database dump complete
--

