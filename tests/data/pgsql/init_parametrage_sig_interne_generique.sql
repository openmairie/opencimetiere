-------------------------------------------------------------------------------
-- 
--
-- @package opencimetiere
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
--
--
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite)
VALUES
(nextval('om_parametre_seq'), 'option_localisation', 'sig_interne', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'));
--
--
--
INSERT INTO om_sig_extent (om_sig_extent, nom, extent, valide) VALUES
(nextval('om_sig_extent_seq'), 'LIBREVILLE', '4.23012696738327,43.3301107926678,5.24752957309877,43.9240967872379', TRUE);
--
--
--
INSERT INTO "om_sig_flux" ("om_sig_flux", "libelle", "om_collectivite", "id", "attribution", "chemin", "couches", "cache_type", "cache_gfi_chemin", "cache_gfi_couches")
VALUES
(nextval('om_sig_flux_seq'),
    'Cadastre - Section',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_section',
    NULL,
    'https://inspire.cadastre.gouv.fr/scpc/13004.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857& ',
    'AMORCES_CAD',
    NULL,   NULL,   NULL
),
(nextval('om_sig_flux_seq'),
    'Cadastre - Parcellaire',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_parcellaire',
    NULL,
    'https://inspire.cadastre.gouv.fr/scpc/13004.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'CP.CadastralParcel',
    NULL,   NULL,   NULL
),
(nextval('om_sig_flux_seq'),
    'Cadastre - Bâti',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_bati',
    NULL,
    'https://inspire.cadastre.gouv.fr/scpc/13004.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'BU.Building',
    NULL,   NULL,   NULL
),
(nextval('om_sig_flux_seq'),
    'Cadastre - Autres',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_autres',
    NULL,
    'https://inspire.cadastre.gouv.fr/scpc/13004.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'LIEUDIT,SUBFISCAL,CLOTURE,DETAIL_TOPO,HYDRO,VOIE_COMMUNICATION,BORNE_REPERE',
    NULL,   NULL,   NULL
),
(nextval('om_sig_flux_seq'),
    'Cadastre - Toutes les couches',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cadastre_toutes',
    NULL,
    'https://inspire.cadastre.gouv.fr/scpc/13004.wms?service=WMS&request=GetMap&VERSION=1.3&CRS=EPSG:3857&',
    'AMORCES_CAD,LIEUDIT,CP.CadastralParcel,SUBFISCAL,CLOTURE,DETAIL_TOPO,HYDRO,VOIE_COMMUNICATION,BU.Building,BORNE_REPERE',
    NULL,   NULL,   NULL
),
(nextval('om_sig_flux_seq'),
    'Ortho',
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'ortho',
    NULL,
    'http://extranet.arles.fr',
    'ortho_demo',
    'SMT', NULL, NULL
)
;
--
--
--
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, projection_externe, url, om_sql, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage, librairie_cartographie)
VALUES
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_flux_generiques',
    'Carte pour flux génériques',
    false,
    '0', true, false, false, 'EPSG:2154',
    '...',
    '...',
    false,
    false,
    false,
    null,
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
)
;
--
--
--
INSERT INTO "om_sig_map_flux" ("om_sig_map_flux", "om_sig_flux", "om_sig_map", "ol_map", "ordre", "visibility", "panier", "pa_nom", "pa_layer", "pa_attribut", "pa_encaps", "pa_sql", "pa_type_geometrie", "sql_filter", "baselayer", "singletile", "maxzoomlevel") 
VALUES
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_section'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Section',
    15,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL
),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_parcellaire'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Parcellaire',
    20,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL
),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_bati'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Bâti',
    25,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL
),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_autres'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Autres',
    30,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL
),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='cadastre_toutes'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Cadastre - Toutes',
    50,
    'f',    'f',    NULL,   NULL,   NULL,   NULL,   '', NULL,   '', 'f',    'f',    NULL
),
(nextval('om_sig_map_flux_seq'),
    (SELECT om_sig_flux FROM om_sig_flux WHERE id='ortho'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'Ortho',
    2, 
    false, false, NULL, NULL, NULL, NULL, '', NULL, '', true, false, 22
)
;
--
--
--
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, projection_externe, url, om_sql, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage, librairie_cartographie)
VALUES
-- CIMETIERE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_cimetiere',
    'cimetiere',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=emplacement&action=12&idx=',
    'SELECT 
            st_astext(emplacement.geom) as geom,
            (numero||'' ''||voietype||''  ''||voielib) as titre,
            (nature ||'' ''|| famille) as description,
            emplacement as idx 
        FROM &DB_PREFIXEemplacement 
            INNER JOIN &DB_PREFIXEvoie 
                ON emplacement.voie = voie.voie 
            INNER JOIN &DB_PREFIXEzone
                ON voie.zone = zone.zone
        WHERE
            zone.cimetiere = &idx
        ORDER BY
            geom,emplacement',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'leaflet'
),
-- CIMETIERE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'cimetiere',
    'cimetiere',
    true,
    '12', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=emplacement&action=11&idx=',
    'SELECT 
            st_astext(emplacement.geom) as geom,
            (numero||'' ''||voietype||''  ''||voielib) as titre,
            (nature ||'' ''|| famille) as description,
            emplacement as idx 
        FROM &DB_PREFIXEemplacement 
            INNER JOIN &DB_PREFIXEvoie 
                ON emplacement.voie = voie.voie 
            INNER JOIN &DB_PREFIXEzone
                ON voie.zone = zone.zone
        WHERE
            zone.cimetiere = &idx
        ORDER BY
            geom,emplacement',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- ZONE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'zone',
    'zone',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=zone&action=3&idx=',
    'SELECT 
            st_astext(ST_PointOnSurface(ST_Buffer(z.geom,0.1))) as geom, 
            z.zonelib as titre,  
            c.cimetierelib||'' ''||zt.libelle||'' ''||z.zonelib||'' (''||z.zone||'')'' as description, 
            z.zone as idx
        FROM &DB_PREFIXEzone z
            JOIN &DB_PREFIXEcimetiere c 
                ON c.cimetiere=z.cimetiere 
            JOIN &DB_PREFIXEzone_type zt 
                ON zt.zone_type=z.zonetype 
        WHERE
            z.cimetiere IN (SELECT distinct cimetiere FROM &DB_PREFIXEzone WHERE zone IN (&idx))',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- VOIE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'voie',
    'voie',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=voie&action=3&idx=',
    'SELECT 
        st_astext(ST_PointOnSurface(ST_Buffer(v.geom,0.1))) as geom, 
        vt.libelle||'' ''||v.voielib as titre,  
        c.cimetierelib||'' ''||zt.libelle||'' ''||z.zonelib||'' (''||z.zone||'')''||vt.libelle||'' ''||v.voielib||'' (''||v.voie||'')'' as description, 
        z.zone as idx
        FROM &DB_PREFIXEvoie v
            JOIN &DB_PREFIXEvoie_type vt 
                ON vt.voie_type = v.voietype
            JOIN &DB_PREFIXEzone z
                ON z.zone=v.zone 
            JOIN &DB_PREFIXEcimetiere c 
                ON c.cimetiere=z.cimetiere 
            JOIN &DB_PREFIXEzone_type zt 
                ON zt.zone_type=z.zonetype 
        WHERE 
            v.zone IN (SELECT distinct zone FROM &DB_PREFIXEvoie WHERE voie IN (&idx))',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- EMPLACEMENT
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_emplacement',
    'Carte de l''emplacement',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=concession&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'leaflet'
),
-- EMPLACEMENT - CONCESSION
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'concession',
    'Carte emplacement ''concession''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=concession&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
);
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, projection_externe, url, om_sql, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage, librairie_cartographie)
VALUES
-- EMPLACEMENT - COLOMBARIUM
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'colombarium',
    'Carte emplacement ''colombarium''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=colombarium&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- EMPLACEMENT - ENFEU
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'enfeu',
    'Carte emplacement ''enfeu''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=enfeu&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- EMPLACEMENT - OSSUAIRE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'ossuaire',
    'Carte emplacement ''ossuaire''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=ossuaire&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- EMPLACEMENT - DEPOSITOIRE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'depositoire',
    'Carte emplacement ''depositoire''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=depositoire&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
),
-- EMPLACEMENT - TERRAINCOMMUNAL
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'terraincommunal',
    'Carte emplacement ''terraincommunal''',
    true,
    '18', true, false, false, 'EPSG:2154',
    '../app/index.php?module=form&obj=terraincommunal&action=12&idx=',
    'SELECT
        ST_AsText(e.geom) as geom,
        e.nature||'' ''||CASE WHEN e.libre =''Oui'' THEN ''Libre'' ELSE COALESCE(famille,'''') END||''<BR>''||e.numero||COALESCE('' ''||complement,'''')||'' ''||vt.libelle||'' ''||v.voielib||'' ''||zt.libelle||'' ''||z.zonelib||''<BR>''||c.cimetierelib as titre,
        e'''' as description,
        e.emplacement as idx
        FROM  &DB_PREFIXEemplacement e
            JOIN  &DB_PREFIXEvoie v
                ON v.voie=e.voie
            JOIN  &DB_PREFIXEvoie_type vt
                ON vt.voie_type=v.voietype
            JOIN  &DB_PREFIXEzone z
                ON z.zone=v.zone
            JOIN  &DB_PREFIXEzone_type zt
                ON zt.zone_type=z.zonetype
            JOIN  &DB_PREFIXEcimetiere c
                ON c.cimetiere=z.cimetiere
        WHERE
            emplacement IN (&idx)',
    true,
    false,
    false,
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'openlayers2'
)
;
--
--
--
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class) 
VALUES 
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='cimetiere'),
    'Emprise', 1, true, true, 'multipolygon', 'cimetiere', 'geom', 'cimetiere', 'cimetiere'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='zone'),
    'Périmètre', 1, true, true, 'multipolygon', 'zone', 'geom', 'zone', 'zone'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='voie'),
    'Voie', 1, true, true, 'multipolygon', 'voie', 'geom', 'voie', 'voie'
)
,
--
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='colombarium'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='enfeu'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='ossuaire'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='depositoire'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='terraincommunal'),
    'Point', 1, true, true, 'point', 'emplacement', 'geom', 'emplacement', 'emplacement'
)
,
--
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='colombarium'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='enfeu'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='ossuaire'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='depositoire'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='terraincommunal'),
    'Emplacement', 2, true, true, 'multipolygon', 'emplacement', 'pgeom', 'emplacement', 'emplacement'
)
;

--
--
--
INSERT INTO om_sig_map_comp (om_sig_map_comp, om_sig_map, libelle, ordre, actif, comp_maj, type_geometrie, comp_table_update, comp_champ, comp_champ_idx, obj_class)
VALUES 
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='concession'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='colombarium'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='enfeu'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='ossuaire'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='depositoire'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
,
(nextval('om_sig_map_comp_seq'),
    (SELECT om_sig_map FROM om_sig_map WHERE id='terraincommunal'),
    'Secteur', 3, true, false, 'multilinestring', 'geo_loc_emplacement', 'geom', 'emplacement', 'geo_loc_emplacement'
)
;


--
--
--
INSERT INTO om_sig_map (om_sig_map, om_collectivite, id, libelle, actif, zoom, fond_osm, fond_bing, fond_sat, projection_externe, url, om_sql, util_idx, util_reqmo, util_recherche, source_flux, fond_default, om_sig_extent, restrict_extent, sld_marqueur, sld_data, point_centrage, librairie_cartographie)
VALUES
-- CARTE GLOBALE
(nextval('om_sig_map_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'),
    'carte_globale',
    'Carte globale',
    true,
    '12', true, false, false, 'EPSG:2154',
    '...',
    'SELECT 
        st_astext(ST_PointOnSurface(ST_Buffer(cimetiere.geom,0.1))) as geom,
        '''' as titre,
        '''' as description,
        cimetiere as idx 
        FROM &DB_PREFIXEcimetiere',
    true,
    false,
    true,
    (SELECT om_sig_map FROM om_sig_map WHERE id='carte_flux_generiques'),
    'osm',
    (SELECT om_sig_extent FROM om_sig_extent WHERE nom='LIBREVILLE'),
    true,
    null,
    null,
    null,
    'leaflet'
);
INSERT INTO om_widget (om_widget, libelle, lien, texte, type, script, arguments) VALUES
(nextval('om_widget_seq'), 'Emplacement des cimetières', '../app/index.php?module=map&mode=tab_sig&obj=carte_globale', '<iframe style=''width:100%;border: 0 none; height: 350px;'' src=''../app/index.php?module=map&mode=tab_sig&obj=carte_globale&popup=1''></iframe>', 'web', '', '');
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES
(nextval('om_dashboard_seq'),
    (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
    'C2',
    2,
    (SELECT om_widget FROM om_widget WHERE libelle='Emplacement des cimetières')
);
