--
--
--
--
--

--
-- OM_LOGO
--

INSERT INTO om_logo (om_logo, id, libelle, description, fichier, resolution, actif, om_collectivite) VALUES 
(nextval('om_logo_seq'), 'logopdf.png', 'logopdf.png', NULL, 'b449b5fae2367bf41ccee5cf974de989', 150, true, 1);

--
-- OM_REQUETE
--

INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(nextval('om_requete_seq'), 'req_def', 'Requête DEFUNT',  NULL, NULL, NULL, 'objet', 'defunt', 'get_all_merge_fields'),
(nextval('om_requete_seq'), 'req_ope', 'Requête OPÉRATION',  NULL, NULL, NULL, 'objet', 'operation', 'get_all_merge_fields'),
(nextval('om_requete_seq'), 'req_emp', 'Requête EMPLACEMENT', NULL, NULL, NULL, 'objet', 'emplacement', 'get_all_merge_fields'),
(nextval('om_requete_seq'), 'req_cou', 'Requête COURRIER', NULL, NULL, NULL, 'objet', 'courrier', 'get_all_merge_fields'),
(nextval('om_requete_seq'), 'req_voi', 'Requête VOIE', NULL, NULL, NULL, 'objet', 'voie', 'get_all_merge_fields');

--
-- OM_ETAT
--
-- XXX attention concession est le premier état, son id 1 est utilisé dans les tests en dur.
INSERT INTO om_etat
(om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop,
    titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure,
    corps_om_htmletatex,
    om_sql,
    se_font, se_couleurtexte, margeleft, margetop, margeright, margebottom,
    header_om_htmletat, header_offset,
    footer_om_htmletat, footer_offset) 
VALUES

(nextval('om_etat_seq'), 1, 'concession', 'Récapitulatif CONCESSION', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif CONCESSION</span><br /><span style="font-weight: bold; font-size: 14pt;">Famille [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: helvetica;''>Famille : [famille]<br />Cimetière : [cimetierelib]<br />Emplacement : [numero] [complement] [voietype] [voielib] - ([zonelib])<br /><br />Caveau de [nombreplace] place(s)<br /><br />Place(s) constatée(s) : [placeconstat] le [dateconstat]<br />Place(s) occupée(s) calculée(s) : [placeoccupe]</span></span></p>
<p> </p>
<p><span id=''defunt'' class=''mce_sousetat''>Liste DEFUNTS / EMPLACEMENT</span></p>
<p><span id=''concessionnaire'' class=''mce_sousetat''>Liste CONCESSIONNAIRES / EMPLACEMENT</span></p>
<p><span id=''ayantdroit'' class=''mce_sousetat''>Liste AYANTS DROIT / EMPLACEMENT</span></p>
<p><span id=''travaux'' class=''mce_sousetat''>Liste TRAVAUX / EMPLACEMENT</span></p>
<p><span id=''courrier'' class=''mce_sousetat''>Liste COURRIERS / EMPLACEMENT</span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'colombarium', 'Récapitulatif COLOMBARIUM', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif COLOMBARIUM</span><br /><span style="font-weight: bold; font-size: 14pt;">Famille [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: helvetica;''>Famille : [famille]<br />Cimetière : [cimetierelib]<br />Emplacement : [numero] [complement] [voietype] [voielib] - ([zonelib])<br /><br />Colombarium de [nombreplace] place(s)<br /><br />Place(s) constatée(s) : [placeconstat] le [dateconstat]<br />Place(s) occupée(s) calculée(s) : [placeoccupe]</span></span></p>
<p> </p>
<p><span id=''defunt'' class=''mce_sousetat''>Liste DEFUNTS / EMPLACEMENT</span></p>
<p><span id=''concessionnaire'' class=''mce_sousetat''>Liste CONCESSIONNAIRES / EMPLACEMENT</span></p>
<p><span id=''ayantdroit'' class=''mce_sousetat''>Liste AYANTS DROIT / EMPLACEMENT</span></p>
<p><span id=''travaux'' class=''mce_sousetat''>Liste TRAVAUX / EMPLACEMENT</span></p>
<p><span id=''courrier'' class=''mce_sousetat''>Liste COURRIERS / EMPLACEMENT</span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-244', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'enfeu', 'Récapitulatif ENFEU', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif ENFEU</span><br /><span style="font-weight: bold; font-size: 14pt;">Famille [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: helvetica;''>Famille : [famille]<br />Cimetière : [cimetierelib]<br />Emplacement : [numero] [complement] [voietype] [voielib] - ([zonelib])<br /><br />Caveau de [nombreplace] place(s)<br /><br />Place(s) constatée(s) : [placeconstat] le [dateconstat]<br />Place(s) occupée(s) calculée(s) : [placeoccupe]</span></span></p>
<p style=''text-align: justify;''> </p>
<p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: helvetica;''> </span></span></p>
<p><span id=''defunt'' class=''mce_sousetat''>Liste DEFUNTS / EMPLACEMENT</span></p>
<p><span id=''concessionnaire'' class=''mce_sousetat''>Liste CONCESSIONNAIRES / EMPLACEMENT</span></p>
<p><span id=''ayantdroit'' class=''mce_sousetat''>Liste AYANTS DROIT / EMPLACEMENT</span></p>
<p><span id=''travaux'' class=''mce_sousetat''>Liste TRAVAUX / EMPLACEMENT</span></p>
<p><span id=''courrier'' class=''mce_sousetat''>Liste COURRIERS / EMPLACEMENT</span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'ossuaire', 'Récapitulatif OSSUAIRE', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif OSSUAIRE</span><br /><span style="font-weight: bold; font-size: 14pt;">Ossuaire [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: helvetica;">[famille]&nbsp;-&nbsp;[numero]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;-&nbsp;([zonelib])<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[cimetierelib]<br/>Caveau&nbsp;de&nbsp;[nombreplace]&nbsp;place(s)<br/><br/>Place(s)&nbsp;constatée(s)&nbsp;:&nbsp;[placeconstat]&nbsp;le&nbsp;[dateconstat]<br/>Place(s)&nbsp;occupée(s)&nbsp;calculée(s)&nbsp;:&nbsp;[placeoccupe]</span></span></p><br /><span class="mce_sousetat" id="defunt">defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'depositoire', 'Récapitulatif DEPOSITOIRE', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif DEPOSITOIRE</span><br /><span style="font-weight: bold; font-size: 14pt;">Dépositoire [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: helvetica;">[famille]&nbsp;-&nbsp;[numero]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;-&nbsp;([zonelib])<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[cimetierelib]<br/>Depositoire&nbsp;de&nbsp;[nombreplace]&nbsp;place(s)<br/><br/>Place(s)&nbsp;occupée(s)&nbsp;calculée(s)&nbsp;:&nbsp;[placeoccupe]</span></span></p><br /><span class="mce_sousetat" id="defunt">defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-255', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'terraincommunal', 'Récapitulatif TERRAIN COMMUNAL', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif TERRAIN COMMUNAL</span><br /><span style="font-weight: bold; font-size: 14pt;">Famille [emplacement.famille]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: helvetica;''>Famille : [famille]<br />Cimetière : [cimetierelib]<br />Emplacement : [numero] [complement] [voietype] [voielib] - ([zonelib])<br /><br />Terrain communal de [nombreplace] place(s)<br /><br />Place(s) occupée(s) calculée(s) : [placeoccupe]</span></span></p>
<p> </p>
<p><span id=''defunt'' class=''mce_sousetat''>Liste DEFUNTS / EMPLACEMENT</span></p>
<p><span id=''concessionnaire'' class=''mce_sousetat''>Liste CONCESSIONNAIRES / EMPLACEMENT</span></p>
<p><span id=''travaux'' class=''mce_sousetat''>Liste TRAVAUX / EMPLACEMENT</span></p>
<p><span id=''courrier'' class=''mce_sousetat''>Liste COURRIERS / EMPLACEMENT</span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-244', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'terraincommunallibre', 'Récapitulatif TERRAIN COMMUNAL libre', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 15px;"><span style="font-family: helvetica;"><span style="font-weight: bold;">T&nbsp;E&nbsp;R&nbsp;R&nbsp;A&nbsp;I&nbsp;N&nbsp;&nbsp;&nbsp;&nbsp;C&nbsp;O&nbsp;M&nbsp;M&nbsp;U&nbsp;N&nbsp;A&nbsp;L<br/>[famille]<br/>État&nbsp;au&nbsp;&aujourdhui</span></span></span></p>', 101, 19, 100, 12, '1',
    '<hr /><p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: helvetica;">Cimetière&nbsp;:&nbsp;[cimetierelib]<br/>Adresse&nbsp;:&nbsp;[numero]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;-&nbsp;([zonelib])<br/><br/>Terrain&nbsp;communal&nbsp;de&nbsp;[nombreplace]&nbsp;place(s)<br/><br/>superficie&nbsp;=&nbsp;[superficie]&nbsp;&nbsp;&nbsp;m2<br/>Durée&nbsp;:&nbsp;5&nbsp;ans</span></span></p><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-255', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'concessionlibre', 'Récapitulatif CONCESSION libre', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 15px;"><span style="font-family: helvetica;"><span style="font-weight: bold;">C&nbsp;O&nbsp;N&nbsp;C&nbsp;E&nbsp;S&nbsp;S&nbsp;I&nbsp;O&nbsp;N&nbsp;<br/>[famille]<br/>État&nbsp;au&nbsp;&aujourdhui</span></span></span></p>', 101, 19, 100, 12, '1',
    '<hr /><p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: helvetica;">Cimetière&nbsp;:&nbsp;[cimetierelib]<br/>Adresse&nbsp;:&nbsp;[numero]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;-&nbsp;([zonelib])<br/><br/>Caveau&nbsp;de&nbsp;[nombreplace]&nbsp;place(s)</span></span></p><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-255', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'colombariumlibre', 'Récapitulatif COLOMBARIUM libre', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 15px;"><span style="font-family: helvetica;"><span style="font-weight: bold;">C&nbsp;O&nbsp;L&nbsp;O&nbsp;M&nbsp;B&nbsp;A&nbsp;R&nbsp;I&nbsp;U&nbsp;M<br/>[famille]<br/>État&nbsp;au&nbsp;&aujourdhui</span></span></span></p>', 101, 19, 100, 12, '1',
    '<hr /><p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: helvetica;">Cimetière&nbsp;:&nbsp;[cimetierelib]<br/>Adresse&nbsp;:&nbsp;[numero]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;-&nbsp;([zonelib])<br/><br/>Colombarium&nbsp;de&nbsp;[nombreplace]&nbsp;place(s)</span></span></p><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_emp'),
    'helvetica', '0-36-255', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'defunt', 'Récapitulatif DEFUNT', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif DEFUNT</span><br /><span style="font-weight: bold; font-size: 14pt;">Défunt [defunt.defunt]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>Defunt : [dtitre] [dnom] [dprenom]<br />Ne(e) le : [datenaissance]<br />Decede(e) le : [datedeces] a [lieudeces]<br /><br />N. de concession : [numero]<br />Cimetiere : [cimetierelib]<br />Emplacement : [numero] [complement] [voietype] [voielib] [zonelib]</span></span></p>
<p> </p>
<p><span id=''operation_defunt_historique_actif'' class=''mce_sousetat''>operation_defunt_historique_actif</span></p>
<p><span id=''operation_defunt_historique_trt'' class=''mce_sousetat''>operation_defunt_historique_trt</span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_def'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'voie', 'Récapitulatif VOIE', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Récapitulatif VOIE</span><br /><span style="font-weight: bold; font-size: 14pt;">Voie [voie.voie]</span><br /><span style="font-weight: bold; font-size: 14pt;">[type_de_voie] [voielib]</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr /><p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: helvetica;">code&nbsp;[voie]&nbsp;&nbsp;:&nbsp;[voietype]&nbsp;&nbsp;[voielib]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[zonetype]&nbsp;&nbsp;[zonelib]&nbsp;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[cimetierelib]</span></span></p><br /><span class="mce_sousetat" id="voiedefunt">voiedefunt</span><br />
    <hr /><p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: helvetica;">code&nbsp;[voie]&nbsp;&nbsp;:&nbsp;[voietype]&nbsp;&nbsp;[voielib]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[zonetype]&nbsp;&nbsp;[zonelib]&nbsp;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[cimetierelib]</span></span></p><br /><span class="mce_sousetat" id="voieconcession">voieconcession</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_voi'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_10_reduction', 'Réduction', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">AUTORISATION&nbsp;DE&nbsp;REDUCTION</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">&adm_cimetiere<br/>&adm_adresse<br/>&adm_complement<br/>&adm_cp&nbsp;&adm_ville<br/><br/>Tel&nbsp;:&nbsp;&adm_tel<br/>Fax&nbsp;:&nbsp;&adm_fax<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;de&nbsp;l''operation&nbsp;:&nbsp;[dateoperation]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Heure&nbsp;:&nbsp;[heureoperation]<br/><br/><br/>&nbsp;Nous,&nbsp;Maire&nbsp;&delaville<br/>&nbsp;Autorisons&nbsp;au&nbsp;cimetiere&nbsp;[cimetiere]<br/>&nbsp;Vu&nbsp;la&nbsp;demande&nbsp;presente&nbsp;par&nbsp;:&nbsp;[societe]<br/><br/>&nbsp;en&nbsp;vue&nbsp;d''exhumer&nbsp;le&nbsp;ou&nbsp;les&nbsp;corps&nbsp;inhumes<br/>&nbsp;dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;[numero]<br/><br/><br/>&nbsp;en&nbsp;date&nbsp;du&nbsp;[dateoperation]<br/>&nbsp;à&nbsp;[heureoperation]<br/><br/>&nbsp;Localisation&nbsp;:&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Observations&nbsp;:<br/>[observation]</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_10_exhumation', 'Exhumation', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">AUTORISATION&nbsp;D''EXHUMATION</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">&adm_cimetiere<br/>&adm_adresse<br/>&adm_complement<br/>&adm_cp&nbsp;&adm_ville<br/><br/>Tel&nbsp;:&nbsp;&adm_tel<br/>Fax&nbsp;:&nbsp;&adm_fax<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;de&nbsp;l''operation&nbsp;:&nbsp;[dateoperation]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Heure&nbsp;:&nbsp;[heureoperation]<br/><br/><br/>&nbsp;Nous,&nbsp;Maire&nbsp;&delaville<br/>&nbsp;Autorisons&nbsp;l''inhumation&nbsp;au&nbsp;cimetière&nbsp;[cimetiere]<br/>&nbsp;par&nbsp;[societe]<br/><br/>&nbsp;dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;[numero]<br/><br/>&nbsp;en&nbsp;date&nbsp;du&nbsp;[dateoperation]<br/>&nbsp;à&nbsp;[heureoperation]<br/><br/>&nbsp;Localisation&nbsp;:&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Observations&nbsp;:<br/>[observation]</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_10_inhumation', 'Opération d''inhumation', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">AUTORISATION&nbsp;D''INHUMATION</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">&adm_cimetiere<br/>&adm_adresse<br/>&adm_complement<br/>&adm_cp&nbsp;&adm_ville<br/><br/>Tel&nbsp;:&nbsp;&adm_tel<br/>Fax&nbsp;:&nbsp;&adm_fax<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;de&nbsp;l''opération&nbsp;:&nbsp;[dateoperation]<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Heure&nbsp;:&nbsp;[heureoperation]<br/><br/>&nbsp;Nous,&nbsp;Maire&nbsp;&delaville<br/><br/>&nbsp;Autorisons&nbsp;l''inhumation&nbsp;au&nbsp;cimetière&nbsp;[cimetiere]<br/><br/>&nbsp;par&nbsp;[societe]<br/><br/>&nbsp;dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;[numero]<br/><br/>&nbsp;en&nbsp;date&nbsp;du&nbsp;[dateoperation]<br/><br/>&nbsp;à&nbsp;[heureoperation]<br/><br/>&nbsp;Localisation&nbsp;:&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Observations&nbsp;:<br/><br/>[observation]</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_20_convocation_police_inhumation', 'convocation police', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">CONVOCATION&nbsp;DE&nbsp;POLICE</span></span></span></p>', 41, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">Monsieur&nbsp;le&nbsp;commissaire,<br/><br/>En&nbsp;vue&nbsp;de&nbsp;l''exhumation&nbsp;du/des&nbsp;défunt(s)<br/>pour&nbsp;être&nbsp;inhumé(s)&nbsp;même&nbsp;cimetière&nbsp;même&nbsp;concession.<br/><br/>Aura&nbsp;lieu&nbsp;au&nbsp;cimetière&nbsp;le&nbsp;[dateoperation]&nbsp;à&nbsp;[heureoperation]<br/>au&nbsp;cimetière&nbsp;de&nbsp;[cimetiere]<br/><br/>Dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;:&nbsp;[numero]<br/>Localisation&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Par&nbsp;:&nbsp;[societe]<br/><br/>Je&nbsp;vous&nbsp;saurais&nbsp;gré&nbsp;d''assurer&nbsp;par&nbsp;votre&nbsp;présence&nbsp;le&nbsp;respect&nbsp;des&nbsp;articles&nbsp;du&nbsp;Code&nbsp;Funéraire.<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L''Officier&nbsp;de&nbsp;l''état-civil&nbsp;par&nbsp;délégation</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_20_convocation_police_reduction', 'Convocation police', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">CONVOCATION&nbsp;DE&nbsp;POLICE</span></span></span></p>', 41, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">Monsieur&nbsp;le&nbsp;commissaire,<br/><br/>En&nbsp;vue&nbsp;de&nbsp;l''exhumation&nbsp;du/des&nbsp;défunt(s)<br/>pour&nbsp;être&nbsp;inhumé(s)&nbsp;même&nbsp;cimetière&nbsp;même&nbsp;concession.<br/><br/>Aura&nbsp;lieu&nbsp;au&nbsp;cimetière&nbsp;le&nbsp;[dateoperation]&nbsp;à&nbsp;[heureoperation]<br/>au&nbsp;cimetière&nbsp;de&nbsp;[cimetiere]<br/><br/>Dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;:&nbsp;[numero]<br/>Localisation&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Par&nbsp;:&nbsp;[societe]<br/><br/>Je&nbsp;vous&nbsp;saurais&nbsp;gré&nbsp;d''assurer&nbsp;par&nbsp;votre&nbsp;présence&nbsp;le&nbsp;respect&nbsp;des&nbsp;articles&nbsp;du&nbsp;Code&nbsp;Funéraire.<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L''Officier&nbsp;de&nbsp;l''état-civil&nbsp;par&nbsp;délégation</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_40_compte_rendu_exhumation', 'Compte rendu', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>COMPTE RENDU D’OPÉRATION<br />N° : [numdossier]</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>  <br />                                                                          Date de l’opération : [dateoperation]<br />                                                                          Heure : [heureoperation]<br /><br /> Cimetière : [cimetiere]<br /><br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Emplacement : [numero] [complement] [voietype] [voielib] [zonelib]<br /><br /> En vue de l''exhumation du ou des défunts<br /> <br />  OUVERTURE FERMETURE POUR RÉDUCTIONS<br /><br /> Par : [societe]</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_20_convocation_police_exhumation', 'Convocation police', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">CONVOCATION&nbsp;DE&nbsp;POLICE</span></span></span></p>', 41, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">Monsieur&nbsp;le&nbsp;commissaire,<br/><br/>En&nbsp;vue&nbsp;de&nbsp;l''exhumation&nbsp;du/des&nbsp;défunt(s)<br/>pour&nbsp;être&nbsp;inhumé(s)&nbsp;même&nbsp;cimetière&nbsp;même&nbsp;concession.<br/><br/>Aura&nbsp;lieu&nbsp;au&nbsp;cimetière&nbsp;le&nbsp;[dateoperation]&nbsp;à&nbsp;[heureoperation]<br/>au&nbsp;cimetière&nbsp;de&nbsp;[cimetiere]<br/><br/>Dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;:&nbsp;[numero]<br/>Localisation&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Par&nbsp;:&nbsp;[societe]<br/><br/>Je&nbsp;vous&nbsp;saurais&nbsp;gré&nbsp;d''assurer&nbsp;par&nbsp;votre&nbsp;présence&nbsp;le&nbsp;respect&nbsp;des&nbsp;articles&nbsp;du&nbsp;Code&nbsp;Funéraire.<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L''Officier&nbsp;de&nbsp;l''état-civil&nbsp;par&nbsp;délégation</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_10_transfert', 'Transfert', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">AUTORISATION&nbsp;DE&nbsp;TRANSFERT</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">&adm_cimetiere<br/>&adm_adresse<br/>&adm_complement<br/>&adm_cp&nbsp;&adm_ville<br/><br/>Tel&nbsp;:&nbsp;&adm_tel<br/>Fax&nbsp;:&nbsp;&adm_fax<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;de&nbsp;l''operation&nbsp;:&nbsp;[dateoperation]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Heure&nbsp;:&nbsp;[heureoperation]<br/><br/><br/>&nbsp;Nous,&nbsp;Maire&nbsp;&delaville<br/>&nbsp;Autorisons&nbsp;au&nbsp;cimetière&nbsp;[cimetiere]<br/>&nbsp;Vu&nbsp;la&nbsp;demande&nbsp;présentée&nbsp;par&nbsp;:&nbsp;[societe]<br/><br/>&nbsp;en&nbsp;vue&nbsp;d''exhumer&nbsp;le&nbsp;ou&nbsp;les&nbsp;corps&nbsp;inhumés<br/>&nbsp;dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;[numero]<br/><br/>&nbsp;en&nbsp;date&nbsp;du&nbsp;[dateoperation]<br/>&nbsp;à&nbsp;[heureoperation]<br/><br/>&nbsp;Localisation&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/><br/>Observations&nbsp;:<br/>[observation]</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_20_convocation_police_transfert', 'Convocation police', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 20px;"><span style="font-family: arial;"><span style="font-weight: bold;">CONVOCATION&nbsp;DE&nbsp;POLICE</span></span></span></p>', 41, 38, 0, 10, '0',
    '<p style="text-align: justify;"><span style="font-size: 12px;"><span style="font-family: times;">Monsieur&nbsp;le&nbsp;commissaire,<br/><br/>En&nbsp;vue&nbsp;de&nbsp;l''exhumation&nbsp;du/des&nbsp;défunt(s)<br/>pour&nbsp;être&nbsp;inhumé(s)&nbsp;même&nbsp;cimetière&nbsp;même&nbsp;concession.<br/><br/>Aura&nbsp;lieu&nbsp;au&nbsp;cimetière&nbsp;le&nbsp;[dateoperation]&nbsp;à&nbsp;[heureoperation]<br/>au&nbsp;cimetière&nbsp;de&nbsp;[cimetiere]<br/><br/>Dans&nbsp;la&nbsp;concession&nbsp;N°&nbsp;:&nbsp;[numero]<br/>Localisation&nbsp;[num_adresse]&nbsp;[complement]&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonelib]<br/><br/>Par&nbsp;:&nbsp;[societe]<br/><br/>Je&nbsp;vous&nbsp;saurais&nbsp;gré&nbsp;d''assurer&nbsp;par&nbsp;votre&nbsp;présence&nbsp;le&nbsp;respect&nbsp;des&nbsp;articles&nbsp;du&nbsp;Code&nbsp;Funéraire.<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L''Officier&nbsp;de&nbsp;l''état-civil&nbsp;par&nbsp;délégation</span></span></p><br /><span class="mce_sousetat" id="operation_defunt">operation_defunt</span><br />',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_30_travaux_transfert', 'Travaux', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>AUTORISATION DE TRAVAUX</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>&amp;adm_cimetiere<br />&amp;adm_adresse<br />&amp;adm_complement<br />&amp;adm_cp &amp;adm_ville<br /><br />Tel : &amp;adm_tel<br />Fax : &amp;adm_fax<br />                                                                Délai d''exécution d''un mois à dater de ce jour : [dateoperation]<br />                                                                Heure : [heureoperation]<br /><br /> Il est permis à : [societe]<br /><br /> D’exécuter les travaux : OUVERTURE FERMETURE POUR TRANSFERT<br /><br /> Conformément à la déclaration d''autorisation concernant la concession désignée ci-dessous.<br /><br /> Cimetière : [cimetiere]<br /> Numéro concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /> Surface : [superficie] M2<br /><br /> La présente autorisation est à remettre au gardien du cimetière.</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_30_travaux_exhumation', 'Travaux', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>AUTORISATION DE TRAVAUX</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>&amp;adm_cimetiere<br />&amp;adm_adresse<br />&amp;adm_complement<br />&amp;adm_cp &amp;adm_ville<br /><br />Tel : &amp;adm_tel<br />Fax : &amp;adm_fax<br />                                                                Délai d''exécution d''un mois à dater de ce jour : [dateoperation]<br />                                                                Heure : [heureoperation]<br /><br /> Il est permis à : [societe]<br /><br /> D''exécuter les travaux : OUVERTURE FERMETURE POUR RÉDUCTIONS<br /><br /> Conformément à la déclaration d''autorisation concernant la concession designée ci-dessous.<br /><br /> Cimetière : [cimetiere]<br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /> Surface : [superficie] M2<br /><br /> La présente autorisation est à remettre au gardien du cimetière.</span></span></p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_40_compte_rendu_transfert', 'Compte rendu', true, 'P', 'A4', 'logopdf.png', 10, 10,

    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>COMPTE RENDU D’OPÉRATION<br />N° : [numdossier]</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>  <br />                                                                          Date de l''opération : [dateoperation]<br />                                                                          Heure : [heureoperation]<br /><br /> Cimetière : [cimetiere]<br /><br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /><br /> En vue de l''inhumation du ou des défunts<br /> <br />  OUVERTURE FERMETURE DE CAVEAU POUR INHUMATION<br /><br /> Par : [societe]</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_40_compte_rendu_inhumation', 'Compte rendu', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>COMPTE RENDU D''OPÉRATION<br />N° : [numdossier]</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>  <br />                                                                          Date de l''opération : [dateoperation]<br />                                                                          Heure : [heureoperation]<br /><br /> Cimetière : [cimetiere]<br /><br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /><br /> En vue de l''inhumation du ou des défunts<br /> <br />  OUVERTURE FERMETURE DE CAVEAU POUR INHUMATION<br /><br /> Par : [societe]</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_30_travaux_reduction', 'Travaux', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>AUTORISATION DE TRAVAUX</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>&amp;adm_cimetiere<br />&amp;adm_adresse<br />&amp;adm_complement<br />&amp;adm_cp &amp;adm_ville<br /><br />Tel : &amp;adm_tel<br />Fax : &amp;adm_fax<br />                                                                Délai d''exécution d''un mois à dater de ce jour : [dateoperation]<br />                                                                Heure : [heureoperation]<br /><br /> Il est permis à : [societe]<br /><br /> D''exécuter les travaux : OUVERTURE FERMETURE POUR RÉDUCTIONS<br /><br /> Conformément à la déclaration d''autorisation concernant la concession designée ci-dessous.<br /><br /> Cimetière : [cimetiere]<br /> Numéro de concession : [numero]<br /> Vendue le : [datevente]<br /> Localisation : [numero] [complement] [voietype] [voielib] [zonelib]<br /> Surface : [surface] M2<br /><br /> La présente autorisation est à remettre au gardien du cimetière.</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_30_travaux_inhumation', 'Travaux', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>AUTORISATION DE TRAVAUX</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>&amp;adm_cimetiere<br />&amp;adm_adresse<br />&amp;adm_complement<br />&amp;adm_cp &amp;adm_ville<br /><br />Tel : &amp;adm_tel<br />Fax : &amp;adm_fax<br />                                                                Délai d''exécution d''un mois a dater de ce jour : [dateoperation]<br />                                                                Heure : [heureoperation]<br /><br /> Il est permis à : [societe]<br /><br /> D’exécuter les travaux : OUVERTURE FERMETURE DE CAVEAU POUR INHUMATION<br /> En vue de l''inhumation du ou des corps<br /><br /> Conformément a la déclaration d''autorisation concernant la concession désignée ci-dessous.<br /><br /> Cimetière : [cimetiere]<br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /> Surface : [superficie] M2<br /><br /> La présente autorisation est a remettre au gardien du cimetière.</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_etat_seq'), 1, 'operation_40_compte_rendu_reduction', 'Compte rendu', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>COMPTE RENDU D’OPÉRATION<br />N° : [numdossier]</span></span></span></p>', 36, 38, 0, 10, '0',
    '<p style=''text-align: justify;''><span style=''font-size: 12px;''><span style=''font-family: times;''>  <br />                                                                          Date de l''opération : [dateoperation]<br />                                                                          Heure : [heureoperation]<br /><br /> Cimetière : [cimetiere]<br /><br /> Numéro de concession : [numero]<br /> Du : [datevente]<br /> Localisation : [num_adresse] [complement] [voietype] [voielib] [zonelib]<br /><br /> En vue de l''inhumation du ou des défunts<br /> <br />  OUVERTURE FERMETURE DE CAVEAU POUR INHUMATION<br /><br /> Par : [societe]</span></span></p>
<p> </p>
<p><span id=''operation_concessionnaire'' class=''mce_sousetat''>operation_concessionnaire</span></p>
<p> </p>
<p><span id=''operation_defunt'' class=''mce_sousetat''>operation_defunt</span></p>
<p> </p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_ope'),
    'helvetica', '0-0-0', 10, 10, 10, 10,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);

--
--
--

INSERT INTO om_lettretype 
(om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop,
    titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure,
    corps_om_htmletatex,
    om_sql,
    margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte,
    header_om_htmletat, header_offset,
    footer_om_htmletat, footer_offset)
VALUES 

(nextval('om_lettretype_seq'), 1, 'autorisationcreusement', 'import du 17/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: center;"><span style="font-size: 16px;"><span style="font-family: times;"><span style="font-weight: bold;">DEMANDE&nbsp;D''AUTORISATION&nbsp;DE&nbsp;CREUSEMENT</span></span></span></p>', 105, 20, 85, 10, '0', 
    '<p style="text-align: justify;"><span style="font-size: 11px;"><span style="font-family: times;">&adm_mairie<br/>&adm_cimetiere<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Je&nbsp;soussigné(e)&nbsp;[titre]&nbsp;[nom]&nbsp;[prenom],&nbsp;demeurant&nbsp;à&nbsp;:<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[adresse1]&nbsp;[adresse2]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[cp]&nbsp;[ville]<br/><br/>agissant&nbsp;en&nbsp;qualité&nbsp;de&nbsp;[autorisation_nature]&nbsp;de&nbsp;la&nbsp;concession&nbsp;au&nbsp;cimetière&nbsp;[cimetierelib]&nbsp;:<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[emplacement_adresse_numero]&nbsp;[emplacement_adresse_complement],&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonetype]&nbsp;[zonelib]<br/><br/>demande&nbsp;l''autorisation&nbsp;d''ouverture&nbsp;de&nbsp;la&nbsp;dite&nbsp;concession&nbsp;afin&nbsp;de&nbsp;procéder&nbsp;à&nbsp;l''inhumation&nbsp;de&nbsp;[complement].<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Par&nbsp;la&nbsp;présente&nbsp;demande&nbsp;dont&nbsp;j''assure&nbsp;la&nbsp;pleine&nbsp;et&nbsp;entière&nbsp;responsabilité,&nbsp;je&nbsp;m''engage&nbsp;à&nbsp;garantir&nbsp;&alavillede&nbsp;contre&nbsp;toute&nbsp;réclamation&nbsp;qui&nbsp;pourrait&nbsp;survenir&nbsp;à&nbsp;l''occasion&nbsp;de&nbsp;l''inhumation&nbsp;qui&nbsp;en&nbsp;fait&nbsp;l''objet.<br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fait&nbsp;à&nbsp;&ville&nbsp;le&nbsp;[datecourrier]<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[nom]&nbsp;[marital]&nbsp;[prenom]</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, NULL, NULL,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'renouvellement', 'import du 18/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: left;"><span style="font-size: 12px;"><span style="font-family: arial;"><span style="font-style: italic;">A&nbsp;&ville&nbsp;le&nbsp;[datecourrier]<br/><br/>[titre]&nbsp;[nom]&nbsp;[prenom]<br/>[adresse1]<br/>[adresse2]&nbsp;&nbsp;ddd<br/>[cp]&nbsp;[ville]</span></span></span></p>', 100, 22, 0, 5, '0',
    '<p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: times;">Objet:&nbsp;Renouvellement&nbsp;de&nbsp;concession<br/>References&nbsp;:&nbsp;concession&nbsp;numéro&nbsp;:&nbsp;numero&nbsp;[emplacement]<br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[titre]&nbsp;[nom]&nbsp;[prenom]&nbsp;[marital],<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Votre&nbsp;concession&nbsp;[terme]&nbsp;d''une&nbsp;durée&nbsp;de&nbsp;[duree]&nbsp;ans&nbsp;&nbsp;ci&nbsp;dessous&nbsp;arrive&nbsp;à&nbsp;échéance&nbsp;le&nbsp;[dateterme].<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Famille&nbsp;:&nbsp;[famille]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[emplacement_adresse_numero]&nbsp;[emplacement_adresse_complement],&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonetype]&nbsp;[zonelib]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cimetière&nbsp;:&nbsp;[cimetierelib]<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Je&nbsp;vous&nbsp;prie&nbsp;donc&nbsp;de&nbsp;bien&nbsp;vouloir&nbsp;prendre&nbsp;contact&nbsp;avec&nbsp;le&nbsp;service&nbsp;des&nbsp;cimetières&nbsp;dans&nbsp;les&nbsp;meilleurs&nbsp;délais&nbsp;pour&nbsp;permettre&nbsp;le&nbsp;renouvellement&nbsp;ou&nbsp;l&nbsp;arret&nbsp;definitif.<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vous&nbsp;souhaitant&nbsp;bonne&nbsp;reception,<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Je&nbsp;vous&nbsp;prie&nbsp;d''agréer,&nbsp;[titre]&nbsp;[nom]&nbsp;[prenom],&nbsp;l&nbsp;expression&nbsp;de&nbsp;mes&nbsp;sentiments&nbsp;dévoués<br/><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Le&nbsp;MAIRE&nbsp;d''&ville&nbsp;<br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nom</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, NULL, NULL,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'caveauverification', 'import du 17/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style="text-align: left;"><span style="font-size: 10px;"><span style="font-family: arial;"><span style="font-weight: bold;">à&nbsp;&ville&nbsp;le&nbsp;[datecourrier]<br/><br/>&nom<br/>Maire&nbsp;de&nbsp;la&nbsp;ville&nbsp;d''&ville<br/><br/>à&nbsp;<br/><br/>[titre]&nbsp;[nom]&nbsp;[prenom]&nbsp;[marital]<br/>[adresse1]<br/>[adresse2]<br/>[cp]&nbsp;[ville]</span></span></span></p>', 113, 13, 0, 5, '0',
    '<p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: times;">&adm_mairie<br/>&adm_cimetiere<br/><br/>Objet&nbsp;:&nbsp;Vérification&nbsp;de&nbsp;Caveau<br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cher&nbsp;[titre]&nbsp;[nom]&nbsp;[prenom],<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vous&nbsp;voudrez&nbsp;bien&nbsp;noté&nbsp;que&nbsp;le&nbsp;service&nbsp;de&nbsp;la&nbsp;conservation&nbsp;des&nbsp;Cimetières&nbsp;a&nbsp;fait&nbsp;procéder&nbsp;comme&nbsp;convenu&nbsp;et&nbsp;suite&nbsp;à&nbsp;votre&nbsp;autorisation,&nbsp;à&nbsp;l''ouverture&nbsp;de&nbsp;votre&nbsp;caveau&nbsp;familial&nbsp;situé&nbsp;au&nbsp;cimetière&nbsp;[cimetierelib]:<br/><br/>[emplacement_adresse_numero]&nbsp;[emplacement_adresse_complement],&nbsp;[voietype]&nbsp;[voielib]&nbsp;[zonetype]&nbsp;[zonelib]<br/><br/>Il&nbsp;en&nbsp;ressort&nbsp;les&nbsp;éléments&nbsp;suivants:<br/><br/>-&nbsp;places&nbsp;constatées&nbsp;:&nbsp;[placeconstat]&nbsp;<br/><br/>-&nbsp;[complement]<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Le&nbsp;service&nbsp;de&nbsp;la&nbsp;conservation&nbsp;des&nbsp;cimetières&nbsp;reste&nbsp;à&nbsp;votre&nbsp;disposition&nbsp;pour&nbsp;tout&nbsp;renseignement&nbsp;supplementaire&nbsp;au&nbsp;numero&nbsp;et&nbsp;à&nbsp;l&nbsp;adresse&nbsp;indiqué&nbsp;ci&nbsp;dessous:<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&adm_mairie<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&adm_cimetiere<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&adm_adresse<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&adm_complement<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&adm_cp&nbsp;&adm_ville<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tél&nbsp;:&nbsp;&adm_tel<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Veuilles&nbsp;agréer,&nbsp;&nbsp;[titre]&nbsp;[nom]&nbsp;[prenom],&nbsp;l''assurance&nbsp;de&nbsp;nos&nbsp;sentiments&nbsp;les&nbsp;meilleurs<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Le&nbsp;MAIRE&nbsp;d''&ville&nbsp;<br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nom</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, NULL, NULL,
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'acteperpetuite', 'import du 17/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: left;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>ACTE DE CONCESSION<br /><br />PERPETUITE</span></span></span></p>', 93, 12, 0, 5, '0', 
    '<p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: times;''>&amp;adm_mairie<br />&amp;adm_cimetiere<br /><br />Nous, &amp;nom, Maire &amp;delaville ;<br />Vu l''ordonnance du 6 décembre 1943, dans ses dispositions relatives aux concessions des terrains pour fondations de sépultures privées dans les cimetières ;<br />Vu les articles L2223 -13 à 16 du code général des Collectivités territoriales ;<br />Vu la délibération n°2005-317 adoptée le 2 Novembre 2005 et portant réglement des Cimetières &amp;delaville ;<br />Vu la délibération n°02.014 du 31 Janvier 2002 fixant les différents tarifs ;<br />Vu la demande en concession de terrain dans le cimetiere de [cimetierelib]<br /><br />À la date de ce jour,<br /><br /><span style=''font-weight: bold;''>ARRETONS</span><br /><br />Article premier : il est concédé à perpetuité à<br /><br />         [titre] [nom] [prenom] [marital],<br /><br />acceptant une portion de terrain de [superficie] metres carré dans le cimetière de [cimetierelib] pour y fonder la sepulture <br /><br />         Famille : [famille]<br />         [emplacement_adresse_numero] [emplacement_adresse_complement], [voietype] [voielib] [zonetype] [zonelib]<br /><br />Article 2 : Cette concession est faite moyennant la somme de 419 € qui sera versée par le concessionnaire dans la caisse municipale.<br /><br />Article 3 : le concessionnaire sera tenu de se conformer en outre aux dispositions du règlement local précité et de tous autres concernant la police des cimetières.<br /><br /><br />Fait à &amp;ville, en Hôtel de Ville le [datecourrier]<br /><br />  <br /><br />                                                                                                                 Le MAIRE d''&amp;ville <br /><br /><br /><br />                                                                                                                  &amp;nom</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, 'helvetica', '0-0-0',
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'actecolombarium', 'import du 17/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: left;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>ACTE DE VENTE D''UN CAVEAU<br />COLOMBARIUM et AUTRES</span></span></span></p>', 93, 12, 0, 10, '0', 
    '<p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: times;''>&amp;adm_mairie<br />&amp;adm_cimetiere<br /><br />Nous, &amp;nom, Maire &amp;delaville ;<br />Vu l''ordonnance du 6 décembre 1943, dans ses dispositions relatives aux concessions des terrains pour fondations de sépultures privées dans les cimetières ;<br />Vu les articles L2223 -13 à 16 du code général des Collectivités territoriales ;<br />Vu la délibération n°2005-317 adoptée le 2 Novembre 2005 et portant réglement des Cimetières &amp;delaville ;<br />Vu la délibération n°02.014 du 31 Janvier 2002 fixant les différents tarifs ;<br />Vu l''acte de concession n° [numeroacte] du [datevente] attribuant une parcelle de terrain de [superficie] metres carré dans le cimetière de [cimetierelib] :<br /><br />         [titre] [nom] [prenom] [marital]<br />         [emplacement_adresse_numero] [emplacement_adresse_complement], [voietype] [voielib] [zonetype] [zonelib]<br /><br />À la date de ce jour,<br /><br />ARRETONS<br /><br />Article 1 : La vente de caveau construit sur ladite concession est faite au prix coutant moyennant la somme de 241,02 € qui sera versée par le concessionnaire dans la caisse municipale.<br /><br />Article 2 : En raison du caractère particulier du cimetière paysager  [cimetierelib], seul un marbrier agréé est autorisé, après examen de son projet au service de la conservation des Cimetières, à effectuer des travaux de recouvrement dudit caveau<br /><br />Article 3 : Pour la même raison, aucun ornement funéraire ne devra être déposé dans l''allée, ni d''une façon générale en dehors des limites de la concession bâtie.<br /><br />Fait à &amp;ville, en Hôtel de Ville le [datecourrier]<br /><br /><br />              Le concessionnaire                                                                   Le MAIRE d''&amp;ville <br /><br /><br /><br />                                                                                                                  &amp;nom</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, 'helvetica', '0-0-0',
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'actetemporaire', 'import du 17/10/2011', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<p style=''text-align: left;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>ACTE DE CONCESSION<br /><br />CONCESSION NOUVELLE</span></span></span></p>', 93, 12, 0, 5, '0', 
    '<p style=''text-align: justify;''><span style=''font-size: 10px;''><span style=''font-family: times;''>&amp;adm_mairie<br />&amp;adm_cimetiere<br /><br />Nous, &amp;nom, Maire &amp;delaville ;<br />Vu l''ordonnance du 6 décembre 1943, dans ses dispositions relatives aux concessions des terrains pour fondations de sépultures privées dans les cimetières ;<br />Vu les articles L2223 -13 à 16 du code général des Collectivités territoriales;<br />Vu la délibération n°2005-317 adoptée le 2 Novembre 2005 et portant réglement des Cimetières &amp;delaville ;<br />Vu la délibération n°02.014 du 31 Janvier 2002 fixant les différents tarifs ;<br />Vu la demande en concession de terrain dans le cimetiere de [cimetierelib]<br /><br />À la date de ce jour,<br /><br />ARRETONS<br /><br />Article premier : il est concédé pour une duree de [duree] ans<br /><br />         [titre] [nom] [prenom] [marital],<br /><br />acceptant une portion de terrain de [superficie] metres carré dans le cimetière de [cimetierelib] pour y fonder la sepulture <br /><br />         Famille : [famille]<br />         [emplacement_adresse_numero] [emplacement_adresse_complement], [voietype] [voielib] [zonetype] [zonelib]<br /><br />Article 2 : Cette concession est faite moyennant la somme de 229 €qui sera versée par le concessionnaire dans la caisse municipale.<br /><br />Article 3 : le concessionnaire sera tenu de se conformer en outre aux dispositions du règlement local précité et de tous autres concernant la police des cimetières.<br /><br /><br />Fait à &amp;ville, en Hôtel de Ville le [datecourrier]<br /><br />Cette concession prendra fin le [dateterme]  <br /><br /><br /><br />              Le concessionnaire                                                                   Le MAIRE d''&amp;ville <br /><br /><br /><br />                                                                                                                  &amp;nom</span></span></p>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, 'helvetica', '0-0-0',
    '', 12,
    '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12),

(nextval('om_lettretype_seq'), 1, 'titre_de_recettes', 'Titre de recettes provisoire', true, 'P', 'A4', 'logopdf.png', 10, 10,
    '<table style="width: 100%;"><tbody><tr><td style="width: 25%;"><br /><br /><br /><br /><br /><br /><br /><span style="font-size: 8pt;">&amp;adm_mairie</span><br /><span style="font-size: 8pt;">&amp;adm_cimetiere</span></td><td style="width: 75%; text-align: center;"><br /><br />
<span style="font-size: 14pt; font-weight: bold;">Titre de recettes provisoire</span><br /><span style="font-weight: bold; font-size: 14pt;">Concession funéraire</span></td></tr></tbody></table>', 10, 15, 190, 12, '0',
    '<hr/>
    <p style=''text-align: center;''><span style=''font-size: 20px;''><span style=''font-family: arial;''><span style=''font-weight: bold;''>Cimetière [cimetierelib]</span></span></span></p>
    <p>N° d''ordre : C/[contrat.annee]/[contrat.contrat]</p>
<p>N° de concession: [emplacement.emplacement] ([contrat.origine_code])</p>
<p> </p>
<p>Le receveur Municipal de la commune est informé que :</p>
<p> </p>
<p><span style=''font-weight: bold;''>[autorisation.nom] [autorisation.prenom]</span></p>
<p>domicilié/e [autorisation.adresse1] [autorisation.cp] [autorisation.ville]</p>
<p> </p>
<p>a demandé une concession de [contrat.duree_lettre] ans de [emplacement.superficie] mètres carrés.</p>
<p>(RENOUVELLEMENT AU TARIF EN VIGUEUR AU MOMENT DE L''ECHEANCE)</p>
<p> </p>
<p>Il/Elle devra verser au dit receveur la somme de [contrat.montant_lettre] pour cette concession suivant le détail ci-dessous :</p>
<p> </p>
<p>Terrain : [contrat.montant] [contrat.monnaie_sigle]</p>
<p>    Dont :</p>
<p>    - 2/3 pour la commune : [contrat.montant_part_communale] [contrat.monnaie_sigle]</p>
<p>    - 1/3 pour le C.C.A.S. : [contrat.montant_autre_part] [contrat.monnaie_sigle]</p>
<p><span style=''font-weight: bold;''>Total à encaisser : [contrat.montant] [contrat.monnaie_sigle]</span></p>
<p> </p>
<p> </p>
<table border=''0'' style=''width: 100%;''>
<tbody>
<tr>
<td> </td>
<td style=''width: 30%;''>
<p>&amp;ville, le [courrier.datecourrier_format_jj_mois_aaaa]</p>
<p>Pour le Maire</p>
</td>
</tr>
</tbody>
</table>',
    (SELECT om_requete.om_requete FROM om_requete WHERE code='req_cou'),
    10, 10, 10, 10, 'helvetica', '0-0-0',
    '<p style="text-align: right;"><span style="font-size: 8pt;">openCimetière - Édition du &amp;aujourdhui</span></p>', 10,
    '<p style="text-align:center;font-size:8pt;"><em>Page &amp;numpage/&amp;nbpages</em></p>', 12);

--
-- TOC entry 3505 (class 0 OID 548017)
-- Dependencies: 216
-- Data for Name: om_sousetat; Type: TABLE DATA; Schema: opencimetiere; Owner: administrateur
--

INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (2, 1, 'defunt', 'Liste DEFUNTS / EMPLACEMENT', true, 'Défunt(s)', 8, 'helvetica', 'B', 10, '1', 'C', '1', '44-119-244', '0-0-100', 5, 5, '0', '0', '0|90|90|90|90|90|90', 27, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-189', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 10, '80|25|25|20|15|15|15', 'TL|TL|LTB|LTB|LTB|LTB|LTBR', 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'L|L|L|C|R|L|L', '1', 8, 10, '250-242-234', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '1', 8, 10, '250-232-214', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '1', 8, 10, '249-218-188', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '999|999|999|999|2|999|999', '0|0|0|0|1|0|0', '0|0|0|0|0|0|0', '0|0|0|0|0|0|0', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(marital,'''') || '' '' || COALESCE(prenom,'''')) as nom,
  to_char(datenaissance,''DD/MM/YYYY'') as naissance,
  to_char(dateinhumation,''DD/MM/YYYY'') as dateinhumation,
  nature,
  taille,
  reduction,
  exhumation

FROM
  &DB_PREFIXEdefunt

WHERE
  emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (3, 1, 'defunt_archive', 'Liste DEFUNTS / EMPLACEMENT archive', true, 'Liste des Defunt(s) enregistrée le &aujourdhui', 8, 'helvetica', 'B', 12, '1', 'C', '1', '44-119-244', '0-0-100', 5, 15, '0', '1', '0|0|0|0|0|0|0', 20, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-189', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '80|25|25|20|15|15|15', 'TL|TL|LTB|LTB|LTB|LTB|LTBR', 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'L|L|L|C|R|L|L', '1', 10, 15, '250-242-234', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '1', 10, 5, '250-232-214', 'BTL|BT|BTLR', 'L|L|C', '1', 10, 15, '249-218-188', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '999|999|999|999|2|999|999', '0|0|0|0|1|0|0', '0|0|0|0|0|0|0', '0|0|0|0|0|0|0', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(marital,'''') || '' '' || COALESCE(prenom,'''')) as nom,
  to_char(datenaissance,''DD/MM/YYYY'') as naissance,
  to_char(dateinhumation,''DD/MM/YYYY'') as dateinhumation,
  nature,
  taille,
  reduction,
  exhumation

FROM
  &DB_PREFIXEdefunt_archive

WHERE
  emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (4, 1, 'ayantdroit', 'Liste AYANTS DROIT / EMPLACEMENT', true, ' Ayant(s) droit', 8, 'helvetica', 'B', 10, '0', 'L', '0', '243-246-246', '0-0-0', 5, 5, '1', '1', '0|0|0', 7, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 8, 10, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 8, 10, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 8, 10, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|1', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(prenom,'''')) as ayantdroit,
  (COALESCE(adresse1,'''') || '' '' || COALESCE(cp,'''') || '' '' || COALESCE(ville,'''')) as adresse,
  to_char(datenaissance,''DD/MM/YYYY'') as datenaissance  

FROM
  &DB_PREFIXEautorisation 

WHERE
  emplacement=&idx
  AND nature=''ayantdroit''
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (5, 1, 'ayantdroit_archive', 'Liste AYANTS DROIT / EMPLACEMENT archive', true, ' Ayant droit', 8, 'helvetica', 'B', 12, '0', 'L', '0', '243-246-246', '0-0-0', 10, 15, '1', '1', '0|0|0', 20, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 10, 15, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 10, 5, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|1', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(prenom,'''')) as ayantdroit,
  (COALESCE(adresse1,'''') || '' '' || COALESCE(cp,'''') || '' '' || COALESCE(ville,'''')) as adresse,
  to_char(datenaissance,''DD/MM/YYYY'') as datenaissance

FROM
  &DB_PREFIXEautorisation_archive

WHERE
  emplacement=&idx
  AND nature=''ayantdroit''
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (6, 1, 'concessionnaire', 'Liste CONCESSIONNAIRES / EMPLACEMENT', true, 'Concessionnaire(s)', 12, 'helvetica', 'B', 10, '0', 'L', '0', '243-246-246', '0-0-0', 5, 5, '1', '1', '0|0|0', 7, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 8, '8-32-154', '243-246-246', '255-255-255', '1', 10, '75|75|45', 'TL|TL|TLR', 'L|L|LR', 'L|L|C', '0', 8, 10, '221-239-248', 'TL|T|TLR', 'L|L|C', '1', 8, 10, '219-233-240', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '234-246-252', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(marital,'''') || '' '' || COALESCE(prenom,'''')) as nom,
  (COALESCE(adresse1,'''') || '' '' || COALESCE(adresse2,'''') || '' '' || COALESCE(cp,'''') || '' '' || COALESCE(ville,'''')) as adresse,
  marital

FROM
  &DB_PREFIXEautorisation

WHERE
  emplacement=&idx
  AND nature=''concessionnaire''
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (7, 1, 'concessionnaire_archive', 'Liste CONCESSIONNAIRES / EMPLACEMENT archive', true, 'Concessionnaire(s) &aujourdhui', 12, 'helvetica', 'B', 12, '0', 'L', '0', '243-246-246', '0-0-0', 50, 100, '1', '1', '0|0|0', 20, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 10, '8-32-154', '243-246-246', '255-255-255', '1', 15, '75|75|45', 'TL|TL|TLR', 'L|L|LR', 'L|L|C', '0', 10, 15, '221-239-248', 'TL|T|TLR', 'L|L|C', '1', 10, 5, '219-233-240', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '234-246-252', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (COALESCE(nom,'''') || '' '' || COALESCE(marital,'''') || '' '' || COALESCE(prenom,'''')) as nom,
  (COALESCE(adresse1,'''') || '' '' || COALESCE(adresse2,'''') || '' '' || COALESCE(cp,'''') || '' '' || COALESCE(ville,'''')) as adresse,
  marital

FROM
  &DB_PREFIXEautorisation_archive

WHERE
  emplacement=&idx
  AND nature=''concessionnaire''
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (8, 1, 'courrier', 'Liste COURRIERS / EMPLACEMENT', true, ' Courrier(s) expédié(s)', 8, 'helvetica', 'B', 10, '0', 'L', '0', '243-246-246', '0-0-0', 5, 5, '1', '1', '0|0|0', 8, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 8, 10, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 8, 10, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 8, 10, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (''numéro ''||COALESCE(courrier,''0'')||'' ''||COALESCE(nom,'''')||'' ''||COALESCE(prenom,'''')) as destinataire,
  lettretype,
  to_char(datecourrier,''DD/MM/YYYY'') as date

FROM
  &DB_PREFIXEcourrier
  LEFT JOIN &DB_PREFIXEautorisation on courrier.destinataire=autorisation.autorisation

WHERE
  courrier.emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (9, 1, 'courrier_archive', 'Liste COURRIERS / EMPLACEMENT archive', true, ' Courriers  expédiés', 8, 'helvetica', 'B', 12, '0', 'L', '0', '243-246-246', '0-0-0', 10, 15, '1', '1', '0|0|0', 20, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 10, 15, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 10, 5, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (''numéro ''||COALESCE(courrier,''0'')||'' ''||COALESCE(nom,'''')||'' ''||COALESCE(prenom,'''')) as destinataire,
  lettretype,
  to_char(datecourrier,''DD/MM/YYYY'') as date

FROM
  &DB_PREFIXEcourrier_archive
  LEFT JOIN &DB_PREFIXEautorisation_archive on courrier_archive.destinataire=autorisation_archive.autorisation

WHERE
  courrier_archive.emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (10, 1, 'travaux', 'Liste TRAVAUX / EMPLACEMENT', true, ' Travau(x)', 8, 'helvetica', 'B', 10, '0', 'L', '0', '243-246-246', '0-0-0', 5, 5, '1', '1', '0|0|0', 8, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 8, 10, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 8, 10, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 8, 10, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (''numero ''||COALESCE(travaux,''0'')||'' ''||COALESCE(entreprise.nomentreprise,'''')) as travaux,
  naturetravaux,
  to_char(datedebinter,''DD/MM/YYYY'') as dates

FROM
  &DB_PREFIXEtravaux
  LEFT JOIN &DB_PREFIXEentreprise on travaux.entreprise=entreprise.entreprise

WHERE
  travaux.emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (11, 1, 'travaux_archive', 'Liste TRAVAUX / EMPLACEMENT archive', true, ' Travaux au &aujourdhui', 8, 'helvetica', 'B', 12, '0', 'L', '0', '243-246-246', '0-0-0', 10, 15, '1', '1', '0|0|0', 20, 'TLB|LTB|LTBR', 'C|C|C', '145-184-189', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 10, 15, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 10, 5, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|0', '
SELECT
  (''numero ''||COALESCE(travaux,''0'')||'' ''||COALESCE(entreprise.nomentreprise,'''')) as travaux,
  naturetravaux,
  to_char(datedebinter,''DD/MM/YYYY'') as dates

FROM
  &DB_PREFIXEtravaux_archive
  LEFT JOIN &DB_PREFIXEentreprise on travaux_archive.entreprise=entreprise.entreprise

WHERE
  travaux_archive.emplacement=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (12, 1, 'voiedefunt', 'Liste DEFUNTS / VOIE', true, 'Liste des Defunt(s) enregistrée le &aujourdhui', 8, 'helvetica', 'B', 12, '1', 'C', '0', '243-246-215', '0-0-0', 5, 15, '1', '1', '0|0|0|0|0|0|0', 20, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '248-204-160', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '80|25|25|20|15|15|15', 'TL|TL|LTB|LTB|LTB|LTB|LTBR', 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'L|L|L|C|R|L|L', '1', 10, 15, '250-242-234', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '1', 10, 5, '250-232-214', 'BTL|BT|BTLR', 'L|L|C', '1', 10, 15, '249-218-188', 'TBL|TB|TB|TB|TBLR|TBL|TBR', 'L|L|C|C|R|C|C', '999|999|999|999|2|999|999', '0|0|0|0|1|0|0', '0|0|0|0|0|0|0', '0|0|0|0|0|0|0', '
SELECT
  (COALESCE(nom,'''')||'' ''||COALESCE(marital,'''')||'' ''||COALESCE(prenom,'''')||'' concession ''||COALESCE(famille,'''')||''(''||COALESCE(emplacement.emplacement,''0'')||'')'') as nom,
  to_char(datenaissance,''DD/MM/YYYY'') as naissance,
  to_char(dateinhumation,''DD/MM/YYYY'') as dateinhumation,
  defunt.nature,
  taille,
  reduction,
  exhumation

FROM
  &DB_PREFIXEdefunt
  LEFT JOIN &DB_PREFIXEemplacement on defunt.emplacement=emplacement.emplacement

WHERE
  emplacement.voie=&idx
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (13, 1, 'voieconcession', 'Liste EMPLACEMENTS / VOIE', true, 'Concessions', 8, 'helvetica', 'B', 12, '0', 'L', '0', '243-246-246', '0-0-0', 10, 15, '1', '1', '0|0|0', 20, 'TLB|LTB|LTBR', 'C|C|C', '195-224-69', '0-0-0', 195, '1', 10, '0-0-0', '243-246-246', '255-255-255', '1', 10, '90|70|35', 'LTBR|LTBRL|LTBR', 'LTBR|LTBR|LTBR', 'L|L|C', '1', 10, 15, '196-213-215', 'TBL|TBL|TBLR', 'L|L|C', '1', 10, 5, '212-219-220', 'BTL|BTL|BTLR', 'L|L|C', '1', 10, 5, '227-231-231', 'TBL|TBL|TBLR', 'L|L|C', '999|999|999', '0|0|0', '0|0|0', '0|0|1', '
SELECT
  famille,
  (COALESCE(numero,''0'')||'' ''||COALESCE(complement,'''')) as position,
  to_char(datevente,''DD/MM/YYYY'') as datevente

FROM
  &DB_PREFIXEemplacement

WHERE
  voie=&idx

ORDER BY
  numero,
  complement
;
');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (17, 1, 'operation_defunt', 'operation_defunt', true, 'Defunts concernes :', 8, 'helvetica', 'B', 10, '0', 'L', '0', '44-119-247', '0-0-0', 10, 0, '1', '1', '0|0|0|0|0|0|0', 10, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-184', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 8, '90|35|35|35', 'TL|TL|LTB|LTBR', 'TL|LTB|LTB|LTBR', 'L|C|C|C', '1', 8, 10, '255-255-255', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '250-232-216', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '249-218-186', 'TBL|TB|TB|TB', 'L|L|C|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT 

 (COALESCE(defunt_titre,''0'') || '' '' || COALESCE(defunt_nom,'''') || '' '' || COALESCE(defunt_prenom,'''') || '' ('' || COALESCE(defunt_marital,'''') || '')'' ) as nom,

 to_char(defunt_datenaissance,''DD/MM/YYYY'') as datenaissance,

 to_char(defunt_datedeces,''DD/MM/YYYY'') as datedeces,

 defunt_lieudeces as lieudeces


FROM 

 &DB_PREFIXEoperation_defunt

WHERE 

 operation = ''&idx'';');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (14, 1, 'operation_concessionnaire', 'operation_concessionnaire', true, 'Concessionnaires ou ayant-droits concernes :', 8, 'helvetica', 'B', 10, '0', 'L', '0', '44-119-247', '0-0-0', 10, 0, '1', '1', '0|0|0|0|0|0|0', 10, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-184', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 8, '90|35|35|35', 'TL|TL|LTB|LTBR', 'TL|LTB|LTB|LTBR', 'L|C|C|C', '1', 8, 10, '255-255-255', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '250-232-216', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '249-218-186', 'TBL|TB|TB|TB', 'L|L|C|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT 

 (COALESCE(a.titre,''0'') || '' '' || COALESCE(a.nom,'''') || '' '' || COALESCE(a.prenom,'''') ) as nom,
 a.marital as marital,
 to_char(a.datenaissance,''DD/MM/YYYY'') as datenaissance,
 a.nature
 
FROM 

 &DB_PREFIXEautorisation a, 
 &DB_PREFIXEoperation o

WHERE 
 a.dcd is FALSE AND
 o.emplacement = a.emplacement AND
 o.operation = ''&idx'';');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (16, 1, 'operation_defunt_historique_trt', 'operation_defunt_historique_trt', true, 'Operations (trt)  :', 8, 'helvetica', 'B', 10, '0', 'L', '0', '44-119-247', '0-0-0', 10, 0, '1', '1', '0|0|0|0|0|0|0', 10, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-184', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 8, '35|35|35|90', 'TL|TL|LTB|LTBR', 'TL|LTB|LTB|LTBR', 'C|C|C|L', '1', 8, 10, '255-255-255', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '250-232-216', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '249-218-186', 'TBL|TB|TB|TB', 'L|L|C|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT 
numdossier as Dossier,
to_char(date,''DD/MM/YYYY'') as Date_Operation,
heure as Heure_Operation,
operation.categorie as Type_Operation
FROM  &DB_PREFIXEoperation_defunt inner join &DB_PREFIXEoperation on operation.operation=operation_defunt.operation
WHERE etat = ''trt'' AND defunt = &idx');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (15, 1, 'operation_defunt_historique_actif', 'operation_defunt_historique_actif', true, 'Operations (actif)  :', 8, 'helvetica', 'B', 10, '0', 'L', '0', '44-119-247', '0-0-0', 10, 0, '1', '1', '0|0|0|0|0|0|0', 10, 'TL|LTB|LTB|LTB|LTB|LTB|LTBR', 'C|C|C|C|C|C|C', '145-184-184', '0-0-0', 195, '1', 8, '0-0-0', '243-246-246', '255-255-255', '1', 8, '35|35|35|90', 'TL|TL|LTB|LTBR', 'TL|LTB|LTB|LTBR', 'C|C|C|L', '1', 8, 10, '255-255-255', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '250-232-216', 'TBL|TB|TB|TB', 'L|L|C|C', '1', 8, 10, '249-218-186', 'TBL|TB|TB|TB', 'L|L|C|C', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT 
numdossier as Dossier,
to_char(date,''DD/MM/YYYY'') as Date_Operation,
heure as Heure_Operation,
operation.categorie as Type_Operation
FROM &DB_PREFIXEoperation_defunt inner join &DB_PREFIXEoperation on operation.operation=operation_defunt.operation
where etat = ''actif'' AND defunt = &idx');


--
-- TOC entry 3513 (class 0 OID 0)
-- Dependencies: 217
-- Name: om_sousetat_seq; Type: SEQUENCE SET; Schema: opencimetiere; Owner: administrateur
--

SELECT pg_catalog.setval('om_sousetat_seq', 18, false);

--
--
--
