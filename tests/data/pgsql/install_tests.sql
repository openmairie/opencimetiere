-------------------------------------------------------------------------------
-- Script d'installation des jeux de données pour les tests
--
-- ATTENTION ce script est prévu pour être appliqué après le install.sql
--
-- @package opencimetiere
-- @version SVN : $Id$
-------------------------------------------------------------------------------
--
START TRANSACTION;
--
\set schema 'opencimetiere'
\set ON_ERROR_STOP on
--
SET search_path = :schema, public, pg_catalog;
--
\i tests/data/pgsql/init_parametrage_widget_dashboard.sql
\i tests/data/pgsql/init_parametrage_parametres.sql
\i tests/data/pgsql/init_parametrage_editions.sql
--
\i tests/data/pgsql/init_parametrage_sig_interne_generique.sql
-- \i tests/data/pgsql/init_parametrage_sig_interne_qgis_exemple.sql
\i tests/data/pgsql/init_data_parametrage_localisation.sql
--
\i tests/data/pgsql/init_data_emplacements.sql
--
COMMIT;
