-------------------------------------------------------------------------------
-- 
--
-- @package opencimetiere
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
INSERT INTO om_widget (om_widget, libelle, type, lien, texte, script, arguments)
VALUES 
(nextval('om_widget_seq'), 'Localisation', 'file', '', '', 'localisation', '')
,
(nextval('om_widget_seq'), 'Recherche globale', 'file', '', '', 'search', '')
,
(nextval('om_widget_seq'), 'Supervision', 'file', '', '', 'supervision', '')
,
(nextval('om_widget_seq'), 'Concessions à terme', 'file', '', '', 'concession_a_terme', '')
,
(nextval('om_widget_seq'), 'Contrat à valider', 'file', '', '', 'contrat_a_valider', '')
,
(nextval('om_widget_seq'), 'Opérations en cours', 'file', '', '', 'operation_en_cours', '')
;

--
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
VALUES
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C1', 1,
	(SELECT om_widget FROM om_widget WHERE script='search')
)
,
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C2', 1,
	(SELECT om_widget FROM om_widget WHERE script='localisation')
)
,
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C3', 1,
	(SELECT om_widget FROM om_widget WHERE script='supervision')
)
,
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C1', 2,
	(SELECT om_widget FROM om_widget WHERE script='concession_a_terme')
)
,
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C3', 1,
	(SELECT om_widget FROM om_widget WHERE script='contrat_a_valider')
)
,
(nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'),
	'C1', 3,
	(SELECT om_widget FROM om_widget WHERE script='operation_en_cours')
)
;