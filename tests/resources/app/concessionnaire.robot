*** Settings ***
Documentation  Actions spécifiques aux concessionnaires.

*** Keywords ***
Ajouter le concessionnaire dans le contexte de la concession
    [Documentation]  ...
    [Arguments]  ${values}  ${concession}
    [Tags]
    Depuis l'onglet 'contacts' de la concession  ${concession}
    Click On Add Button JS
    Select From List By Value  nature  concessionnaire
    Saisir les valeurs dans le formulaire du concessionnaire  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    ${concessionnaire_id} =  Get Text  css=#form-content div.autorisation-form div.form-content span#autorisation
    [Return]  ${concessionnaire_id}

Depuis le contexte du concessionnaire de la concession
    [Arguments]  ${concession}  ${concessionnaire}
    [Tags]
    Depuis l'onglet 'contacts' de la concession  ${concession}
    # On clique sur le résultat
    Click On Link  ${concessionnaire}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Saisir les valeurs dans le formulaire du concessionnaire
    [Documentation]  Remplit le formulaire
    ...  &{concessionnaire} =  Create Dictionary
    ...  ...  titre=Monsieur
    ...  ...  nom=MICHEL
    ...  ...  marital=
    ...  ...  prenom=Henri
    ...  ...  datenaissance=10/01/1980
    ...  ...  adresse1=12 rue de la République
    ...  ...  adresse2=
    ...  ...  cp=99678
    ...  ...  ville=LIBREVILLE
    ...  ...  dcd=true
    ...  ...  observation=
    [Arguments]  ${values}
    [Tags]
    # Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    # Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dcd" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "parente" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" sur "css=textarea#observation"

