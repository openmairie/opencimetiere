*** Settings ***
Documentation  Actions spécifiques aux courrier.

*** Keywords ***
Ajouter le travaux dans le contexte de la concession
    [Documentation]  ...
    [Arguments]  ${values}  ${concession}
    [Tags]  travaux
    Depuis l'onglet 'travaux' de la concession  ${concession}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du travaux  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire du travaux
    [Documentation]  Remplit le formulaire
    ...  &{travaux} =  Create Dictionary
    ...  ...  entreprise=PF ENTREPRISE
    ...  ...  datedebinter=10/01/2015
    ...  ...  datefininter=12/01/2015
    ...  ...  observation=complément d'information
    ...  ...  naturedemandeur=concessionnaire
    ...  ...  naturetravaux=Autorisation de recouvrement
    [Arguments]  ${values}
    [Tags]  travaux
    Si "entreprise" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    # Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "datedebinter" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "datefininter" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" sur "css=textarea#observation"
    Si "naturedemandeur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "naturetravaux" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
