*** Settings ***
Documentation  Actions spécifiques aux voies.

*** Keywords ***
Depuis le listing des voies
    [Documentation]  Permet d'accéder au lsiting des voies.
    [Tags]
    Depuis le listing  voie


Depuis le contexte de la voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie}
    [Tags]
    Depuis le listing des voies
    # On recherche l'enregistrement
    Use Simple Search  voie  ${voie}
    # On clique sur le résultat
    Click On Link  ${voie}


Depuis le formulaire d'ajout d'une voie
    [Documentation]  Crée l'enregistrement
    [Tags]
    Depuis le listing des voies
    # On clique sur le bouton ajouter
    Click On Add Button


Depuis le formulaire de modification de la voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie}
    [Tags]
    Depuis le contexte de la voie  ${voie}
    Click On Form Portlet Action  voie  modifier


Ajouter la voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une voie
    Saisir les valeurs dans le formulaire de la voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Modifier la voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${voie}  ${values}
    [Tags]
    Depuis le formulaire de modification de la voie  ${voie}
    Saisir les valeurs dans le formulaire de la voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer la voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${voie}
    [Tags]
    Depuis le contexte de la voie  ${voie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  voie  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  La suppression a ete correctement effectuée.


Saisir les valeurs dans le formulaire de la voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]
    Si "zone" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "voietype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "voielib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire


