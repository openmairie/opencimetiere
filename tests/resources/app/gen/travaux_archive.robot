*** Settings ***
Documentation    CRUD de la table travaux_archive
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte travaux archivés
    [Documentation]  Accède au formulaire
    [Arguments]  ${travaux}

    # On accède au tableau
    Go To Tab  travaux_archive
    # On recherche l'enregistrement
    Use Simple Search  travaux  ${travaux}
    # On clique sur le résultat
    Click On Link  ${travaux}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter travaux archivés
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  travaux_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir travaux archivés  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${travaux} =  Get Text  css=div.form-content span#travaux
    # On le retourne
    [Return]  ${travaux}

Modifier travaux archivés
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${travaux}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte travaux archivés  ${travaux}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  travaux_archive  modifier
    # On saisit des valeurs
    Saisir travaux archivés  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer travaux archivés
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${travaux}

    # On accède à l'enregistrement
    Depuis le contexte travaux archivés  ${travaux_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  travaux  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir travaux archivés
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "entreprise" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datedebinter" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "datefininter" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "naturedemandeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "naturetravaux" existe dans "${values}" on execute "Select From List By Label" dans le formulaire