*** Settings ***
Documentation    CRUD de la table cimetiere
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte cimetière
    [Documentation]  Accède au formulaire
    [Arguments]  ${cimetiere}

    # On accède au tableau
    Go To Tab  cimetiere
    # On recherche l'enregistrement
    Use Simple Search  cimetière  ${cimetiere}
    # On clique sur le résultat
    Click On Link  ${cimetiere}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter cimetière
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  cimetiere
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir cimetière  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${cimetiere} =  Get Text  css=div.form-content span#cimetiere
    # On le retourne
    [Return]  ${cimetiere}

Modifier cimetière
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${cimetiere}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte cimetière  ${cimetiere}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  cimetiere  modifier
    # On saisit des valeurs
    Saisir cimetière  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer cimetière
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${cimetiere}

    # On accède à l'enregistrement
    Depuis le contexte cimetière  ${cimetiere}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  cimetiere  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir cimetière
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "cimetierelib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observations" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire