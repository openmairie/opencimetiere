*** Settings ***
Documentation    CRUD de la table emplacement
...    @author  generated
...    @package openCimetière
...    @version 22/01/2020 19:01

*** Keywords ***

Depuis le contexte emplacement
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}

    # On accède au tableau
    Go To Tab  emplacement
    # On recherche l'enregistrement
    Use Simple Search  emplacement  ${emplacement}
    # On clique sur le résultat
    Click On Link  ${emplacement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter emplacement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  emplacement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${emplacement} =  Get Text  css=div.form-content span#emplacement
    # On le retourne
    [Return]  ${emplacement}

Modifier emplacement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte emplacement  ${emplacement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  emplacement  modifier
    # On saisit des valeurs
    Saisir emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer emplacement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${emplacement}

    # On accède à l'enregistrement
    Depuis le contexte emplacement  ${emplacement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  emplacement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir emplacement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "numerocadastre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "famille" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numeroacte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datevente" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateterme" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "nombreplace" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeoccupe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "superficie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeconstat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateconstat" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "plans" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "positionx" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "positiony" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "photo" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sepulturetype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "daterenouvellement" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "typeconcession" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp5" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "videsanitaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "semelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etatsemelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "monument" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etatmonument" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "largeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "profondeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "abandon" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_abandon" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "dateacte" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pgeom" existe dans "${values}" on execute "Input Text" dans le formulaire