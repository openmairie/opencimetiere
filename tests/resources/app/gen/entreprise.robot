*** Settings ***
Documentation    CRUD de la table entreprise
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte entreprise
    [Documentation]  Accède au formulaire
    [Arguments]  ${entreprise}

    # On accède au tableau
    Go To Tab  entreprise
    # On recherche l'enregistrement
    Use Simple Search  entreprise  ${entreprise}
    # On clique sur le résultat
    Click On Link  ${entreprise}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter entreprise
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  entreprise
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir entreprise  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${entreprise} =  Get Text  css=div.form-content span#entreprise
    # On le retourne
    [Return]  ${entreprise}

Modifier entreprise
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${entreprise}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte entreprise  ${entreprise}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  entreprise  modifier
    # On saisit des valeurs
    Saisir entreprise  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer entreprise
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${entreprise}

    # On accède à l'enregistrement
    Depuis le contexte entreprise  ${entreprise}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  entreprise  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir entreprise
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nomentreprise" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire