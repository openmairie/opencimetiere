*** Settings ***
Documentation    CRUD de la table courrier_archive
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte courrier archivé
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier}

    # On accède au tableau
    Go To Tab  courrier_archive
    # On recherche l'enregistrement
    Use Simple Search  courrier  ${courrier}
    # On clique sur le résultat
    Click On Link  ${courrier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter courrier archivé
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir courrier archivé  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier} =  Get Text  css=div.form-content span#courrier
    # On le retourne
    [Return]  ${courrier}

Modifier courrier archivé
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte courrier archivé  ${courrier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier_archive  modifier
    # On saisit des valeurs
    Saisir courrier archivé  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer courrier archivé
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier}

    # On accède à l'enregistrement
    Depuis le contexte courrier archivé  ${courrier_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir courrier archivé
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "destinataire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datecourrier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lettretype" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire