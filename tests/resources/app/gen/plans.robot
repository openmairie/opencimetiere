*** Settings ***
Documentation    CRUD de la table plans
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte plans
    [Documentation]  Accède au formulaire
    [Arguments]  ${plans}

    # On accède au tableau
    Go To Tab  plans
    # On recherche l'enregistrement
    Use Simple Search  plans  ${plans}
    # On clique sur le résultat
    Click On Link  ${plans}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter plans
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  plans
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plans  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plans} =  Get Text  css=div.form-content span#plans
    # On le retourne
    [Return]  ${plans}

Modifier plans
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plans}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plans  ${plans}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plans  modifier
    # On saisit des valeurs
    Saisir plans  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plans
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plans}

    # On accède à l'enregistrement
    Depuis le contexte plans  ${plans}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plans  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plans
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "planslib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Input Text" dans le formulaire