*** Settings ***
Documentation    CRUD de la table travaux_nature
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte nature des travaux
    [Documentation]  Accède au formulaire
    [Arguments]  ${travaux_nature}

    # On accède au tableau
    Go To Tab  travaux_nature
    # On recherche l'enregistrement
    Use Simple Search  nature des travaux  ${travaux_nature}
    # On clique sur le résultat
    Click On Link  ${travaux_nature}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter nature des travaux
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  travaux_nature
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir nature des travaux  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${travaux_nature} =  Get Text  css=div.form-content span#travaux_nature
    # On le retourne
    [Return]  ${travaux_nature}

Modifier nature des travaux
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${travaux_nature}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte nature des travaux  ${travaux_nature}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  travaux_nature  modifier
    # On saisit des valeurs
    Saisir nature des travaux  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer nature des travaux
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${travaux_nature}

    # On accède à l'enregistrement
    Depuis le contexte nature des travaux  ${travaux_nature}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  travaux_nature  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir nature des travaux
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire