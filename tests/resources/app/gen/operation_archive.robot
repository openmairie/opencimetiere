*** Settings ***
Documentation    CRUD de la table operation_archive
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte opération archivée
    [Documentation]  Accède au formulaire
    [Arguments]  ${operation}

    # On accède au tableau
    Go To Tab  operation_archive
    # On recherche l'enregistrement
    Use Simple Search  opération  ${operation}
    # On clique sur le résultat
    Click On Link  ${operation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter opération archivée
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  operation_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir opération archivée  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${operation} =  Get Text  css=div.form-content span#operation
    # On le retourne
    [Return]  ${operation}

Modifier opération archivée
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${operation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte opération archivée  ${operation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  operation_archive  modifier
    # On saisit des valeurs
    Saisir opération archivée  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer opération archivée
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${operation}

    # On accède à l'enregistrement
    Depuis le contexte opération archivée  ${operation_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  operation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir opération archivée
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "numdossier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "societe_coordonnee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pf_coordonnee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "categorie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particulier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emplacement_transfert" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire