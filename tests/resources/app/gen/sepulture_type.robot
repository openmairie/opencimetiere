*** Settings ***
Documentation    CRUD de la table sepulture_type
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte type de sépulture
    [Documentation]  Accède au formulaire
    [Arguments]  ${sepulture_type}

    # On accède au tableau
    Go To Tab  sepulture_type
    # On recherche l'enregistrement
    Use Simple Search  type de sépulture  ${sepulture_type}
    # On clique sur le résultat
    Click On Link  ${sepulture_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de sépulture
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  sepulture_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de sépulture  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${sepulture_type} =  Get Text  css=div.form-content span#sepulture_type
    # On le retourne
    [Return]  ${sepulture_type}

Modifier type de sépulture
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${sepulture_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de sépulture  ${sepulture_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  sepulture_type  modifier
    # On saisit des valeurs
    Saisir type de sépulture  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de sépulture
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${sepulture_type}

    # On accède à l'enregistrement
    Depuis le contexte type de sépulture  ${sepulture_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  sepulture_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de sépulture
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire