*** Settings ***
Documentation    CRUD de la table tarif
...    @author  generated
...    @package openCimetière
...    @version 22/01/2020 19:01

*** Keywords ***

Depuis le contexte tarif
    [Documentation]  Accède au formulaire
    [Arguments]  ${tarif}

    # On accède au tableau
    Go To Tab  tarif
    # On recherche l'enregistrement
    Use Simple Search  tarif  ${tarif}
    # On clique sur le résultat
    Click On Link  ${tarif}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter tarif
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  tarif
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir tarif  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${tarif} =  Get Text  css=div.form-content span#tarif
    # On le retourne
    [Return]  ${tarif}

Modifier tarif
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${tarif}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte tarif  ${tarif}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  tarif  modifier
    # On saisit des valeurs
    Saisir tarif  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer tarif
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${tarif}

    # On accède à l'enregistrement
    Depuis le contexte tarif  ${tarif}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  tarif  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir tarif
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "annee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "origine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sepulture_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "montant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "monnaie" existe dans "${values}" on execute "Input Text" dans le formulaire