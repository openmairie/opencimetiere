*** Settings ***
Documentation    CRUD de la table voie_type
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte type de voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie_type}

    # On accède au tableau
    Go To Tab  voie_type
    # On recherche l'enregistrement
    Use Simple Search  type de voie  ${voie_type}
    # On clique sur le résultat
    Click On Link  ${voie_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  voie_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${voie_type} =  Get Text  css=div.form-content span#voie_type
    # On le retourne
    [Return]  ${voie_type}

Modifier type de voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${voie_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de voie  ${voie_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  voie_type  modifier
    # On saisit des valeurs
    Saisir type de voie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${voie_type}

    # On accède à l'enregistrement
    Depuis le contexte type de voie  ${voie_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  voie_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire