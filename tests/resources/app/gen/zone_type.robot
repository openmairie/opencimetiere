*** Settings ***
Documentation    CRUD de la table zone_type
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte type de zone
    [Documentation]  Accède au formulaire
    [Arguments]  ${zone_type}

    # On accède au tableau
    Go To Tab  zone_type
    # On recherche l'enregistrement
    Use Simple Search  type de zone  ${zone_type}
    # On clique sur le résultat
    Click On Link  ${zone_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de zone
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  zone_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de zone  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${zone_type} =  Get Text  css=div.form-content span#zone_type
    # On le retourne
    [Return]  ${zone_type}

Modifier type de zone
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${zone_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de zone  ${zone_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  zone_type  modifier
    # On saisit des valeurs
    Saisir type de zone  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de zone
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${zone_type}

    # On accède à l'enregistrement
    Depuis le contexte type de zone  ${zone_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  zone_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de zone
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire