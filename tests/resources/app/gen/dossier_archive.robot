*** Settings ***
Documentation    CRUD de la table dossier_archive
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte dossier archivé
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier}

    # On accède au tableau
    Go To Tab  dossier_archive
    # On recherche l'enregistrement
    Use Simple Search  dossier  ${dossier}
    # On clique sur le résultat
    Click On Link  ${dossier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter dossier archivé
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir dossier archivé  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier} =  Get Text  css=div.form-content span#dossier
    # On le retourne
    [Return]  ${dossier}

Modifier dossier archivé
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte dossier archivé  ${dossier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_archive  modifier
    # On saisit des valeurs
    Saisir dossier archivé  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer dossier archivé
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier}

    # On accède à l'enregistrement
    Depuis le contexte dossier archivé  ${dossier_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir dossier archivé
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datedossier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "typedossier" existe dans "${values}" on execute "Input Text" dans le formulaire