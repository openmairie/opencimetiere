*** Settings ***
Documentation    Ressources de mots-clefs générés
...    @author  generated
...    @package openCimetière
...    @version 04/10/2022 17:10

Resource          autorisation.robot
Resource          autorisation_archive.robot
Resource          cimetiere.robot
Resource          contrat.robot
Resource          contrat_archive.robot
Resource          courrier.robot
Resource          courrier_archive.robot
Resource          defunt.robot
Resource          defunt_archive.robot
Resource          dossier.robot
Resource          dossier_archive.robot
Resource          emplacement.robot
Resource          emplacement_archive.robot
Resource          entreprise.robot
Resource          genealogie.robot
Resource          lien_parente.robot
Resource          operation.robot
Resource          operation_archive.robot
Resource          operation_defunt.robot
Resource          operation_defunt_archive.robot
Resource          plans.robot
Resource          sepulture_type.robot
Resource          tarif.robot
Resource          titre_de_civilite.robot
Resource          travaux.robot
Resource          travaux_archive.robot
Resource          travaux_nature.robot
Resource          voie.robot
Resource          voie_type.robot
Resource          zone.robot
Resource          zone_type.robot