*** Settings ***
Documentation    CRUD de la table defunt
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte défunt
    [Documentation]  Accède au formulaire
    [Arguments]  ${defunt}

    # On accède au tableau
    Go To Tab  defunt
    # On recherche l'enregistrement
    Use Simple Search  défunt  ${defunt}
    # On clique sur le résultat
    Click On Link  ${defunt}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter défunt
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  defunt
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir défunt  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${defunt} =  Get Text  css=div.form-content span#defunt
    # On le retourne
    [Return]  ${defunt}

Modifier défunt
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${defunt}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte défunt  ${defunt}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  defunt  modifier
    # On saisit des valeurs
    Saisir défunt  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer défunt
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${defunt}

    # On accède à l'enregistrement
    Depuis le contexte défunt  ${defunt}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  defunt  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir défunt
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "taille" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "datedeces" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lieudeces" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateinhumation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "exhumation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateexhumation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reduction" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datereduction" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "historique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "verrou" existe dans "${values}" on execute "Input Text" dans le formulaire