*** Settings ***
Documentation    CRUD de la table contrat
...    @author  generated
...    @package openCimetière
...    @version 22/01/2020 19:01

*** Keywords ***

Depuis le contexte contrat
    [Documentation]  Accède au formulaire
    [Arguments]  ${contrat}

    # On accède au tableau
    Go To Tab  contrat
    # On recherche l'enregistrement
    Use Simple Search  contrat  ${contrat}
    # On clique sur le résultat
    Click On Link  ${contrat}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter contrat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  contrat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir contrat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${contrat} =  Get Text  css=div.form-content span#contrat
    # On le retourne
    [Return]  ${contrat}

Modifier contrat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contrat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte contrat  ${contrat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contrat  modifier
    # On saisit des valeurs
    Saisir contrat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer contrat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contrat}

    # On accède à l'enregistrement
    Depuis le contexte contrat  ${contrat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contrat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir contrat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "datedemande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "datevente" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "origine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateterme" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "montant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "monnaie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "valide" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire