*** Settings ***
Documentation    CRUD de la table lien_parente
...    @author  generated
...    @package openCimetière
...    @version 05/10/2022 15:10

*** Keywords ***

Depuis le contexte lien de parenté
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_parente}

    # On accède au tableau
    Go To Tab  lien_parente
    # On recherche l'enregistrement
    Use Simple Search  lien de parenté  ${lien_parente}
    # On clique sur le résultat
    Click On Link  ${lien_parente}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien de parenté
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_parente
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien de parenté  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_parente} =  Get Text  css=div.form-content span#lien_parente
    # On le retourne
    [Return]  ${lien_parente}

Modifier lien de parenté
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_parente}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien de parenté  ${lien_parente}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_parente  modifier
    # On saisit des valeurs
    Saisir lien de parenté  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien de parenté
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_parente}

    # On accède à l'enregistrement
    Depuis le contexte lien de parenté  ${lien_parente}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_parente  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien de parenté
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "niveau" existe dans "${values}" on execute "Input Text" dans le formulaire