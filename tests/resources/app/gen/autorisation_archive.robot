*** Settings ***
Documentation    CRUD de la table autorisation_archive
...    @author  generated
...    @package openCimetière
...    @version 22/01/2020 19:01

*** Keywords ***

Depuis le contexte autorisation archivée
    [Documentation]  Accède au formulaire
    [Arguments]  ${autorisation}

    # On accède au tableau
    Go To Tab  autorisation_archive
    # On recherche l'enregistrement
    Use Simple Search  autorisation  ${autorisation}
    # On clique sur le résultat
    Click On Link  ${autorisation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter autorisation archivée
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  autorisation_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir autorisation archivée  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${autorisation} =  Get Text  css=div.form-content span#autorisation
    # On le retourne
    [Return]  ${autorisation}

Modifier autorisation archivée
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${autorisation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte autorisation archivée  ${autorisation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  autorisation_archive  modifier
    # On saisit des valeurs
    Saisir autorisation archivée  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer autorisation archivée
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${autorisation}

    # On accède à l'enregistrement
    Depuis le contexte autorisation archivée  ${autorisation_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  autorisation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir autorisation archivée
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dcd" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "parente" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire