*** Settings ***
Documentation    CRUD de la table zone
...    @author  generated
...    @package openCimetiere
...    @version 30/01/2018 22:01

*** Keywords ***

Depuis le contexte zone
    [Documentation]  Accède au formulaire
    [Arguments]  ${zone}

    # On accède au tableau
    Go To Tab  zone
    # On recherche l'enregistrement
    Use Simple Search  zone  ${zone}
    # On clique sur le résultat
    Click On Link  ${zone}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter zone
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  zone
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir zone  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${zone} =  Get Text  css=div.form-content span#zone
    # On le retourne
    [Return]  ${zone}

Modifier zone
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${zone}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte zone  ${zone}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  zone  modifier
    # On saisit des valeurs
    Saisir zone  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer zone
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${zone}

    # On accède à l'enregistrement
    Depuis le contexte zone  ${zone}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  zone  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir zone
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "cimetiere" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "zonetype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "zonelib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire