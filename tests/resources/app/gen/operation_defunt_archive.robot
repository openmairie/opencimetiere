*** Settings ***
Documentation    CRUD de la table operation_defunt_archive
...    @author  generated
...    @package openCimetière
...    @version 12/10/2023 13:10

*** Keywords ***

Depuis le contexte opération sur défunt archivée
    [Documentation]  Accède au formulaire
    [Arguments]  ${operation_defunt}

    # On accède au tableau
    Go To Tab  operation_defunt_archive
    # On recherche l'enregistrement
    Use Simple Search  opération défunt  ${operation_defunt}
    # On clique sur le résultat
    Click On Link  ${operation_defunt}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter opération sur défunt archivée
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  operation_defunt_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir opération sur défunt archivée  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${operation_defunt} =  Get Text  css=div.form-content span#operation_defunt
    # On le retourne
    [Return]  ${operation_defunt}

Modifier opération sur défunt archivée
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${operation_defunt}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte opération sur défunt archivée  ${operation_defunt}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  operation_defunt_archive  modifier
    # On saisit des valeurs
    Saisir opération sur défunt archivée  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer opération sur défunt archivée
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${operation_defunt}

    # On accède à l'enregistrement
    Depuis le contexte opération sur défunt archivée  ${operation_defunt_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  operation_defunt  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir opération sur défunt archivée
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "operation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "defunt_datedeces" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "defunt_lieudeces" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_nature" existe dans "${values}" on execute "Input Text" dans le formulaire