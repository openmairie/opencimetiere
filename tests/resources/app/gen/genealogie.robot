*** Settings ***
Documentation    CRUD de la table genealogie
...    @author  generated
...    @package openCimetière
...    @version 13/10/2022 16:10

*** Keywords ***

Depuis le contexte généalogie
    [Documentation]  Accède au formulaire
    [Arguments]  ${genealogie}

    # On accède au tableau
    Go To Tab  genealogie
    # On recherche l'enregistrement
    Use Simple Search  généalogie  ${genealogie}
    # On clique sur le résultat
    Click On Link  ${genealogie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter généalogie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  genealogie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir généalogie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${genealogie} =  Get Text  css=div.form-content span#genealogie
    # On le retourne
    [Return]  ${genealogie}

Modifier généalogie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${genealogie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte généalogie  ${genealogie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  genealogie  modifier
    # On saisit des valeurs
    Saisir généalogie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer généalogie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${genealogie}

    # On accède à l'enregistrement
    Depuis le contexte généalogie  ${genealogie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  genealogie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir généalogie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "autorisation_p1" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "defunt_p1" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "personne_1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "personne_2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lien_parente" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "autorisation_p2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "defunt_p2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire