*** Settings ***
Documentation    CRUD de la table emplacement_archive
...    @author  generated
...    @package openCimetière
...    @version 20/10/2018 00:10

*** Keywords ***

Depuis le contexte emplacement archivé
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}

    # On accède au tableau
    Go To Tab  emplacement_archive
    # On recherche l'enregistrement
    Use Simple Search  emplacement  ${emplacement}
    # On clique sur le résultat
    Click On Link  ${emplacement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter emplacement archivé
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  emplacement_archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir emplacement archivé  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${emplacement} =  Get Text  css=div.form-content span#emplacement
    # On le retourne
    [Return]  ${emplacement}

Modifier emplacement archivé
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte emplacement archivé  ${emplacement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  emplacement_archive  modifier
    # On saisit des valeurs
    Saisir emplacement archivé  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer emplacement archivé
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${emplacement}

    # On accède à l'enregistrement
    Depuis le contexte emplacement archivé  ${emplacement_archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  emplacement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir emplacement archivé
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numerocadastre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "famille" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numeroacte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datevente" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateterme" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "nombreplace" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeoccupe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "superficie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeconstat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateconstat" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "plans" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "positionx" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "positiony" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "photo" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sepulturetype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire