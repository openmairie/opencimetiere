*** Settings ***
Documentation  Actions spécifiques aux zones.

*** Keywords ***
Depuis le listing des zones
    [Documentation]  Permet d'accéder au lsiting des zones.
    [Tags]
    Depuis le listing  zone


Depuis le contexte de la zone
    [Documentation]  Accède au formulaire
    [Arguments]  ${zone}
    [Tags]
    Depuis le listing des zones
    # On recherche l'enregistrement
    Use Simple Search  zone  ${zone}
    # On clique sur le résultat
    Click On Link  ${zone}


Depuis le formulaire d'ajout d'une zone
    [Documentation]  Crée l'enregistrement
    [Tags]
    Depuis le listing des zones
    # On clique sur le bouton ajouter
    Click On Add Button


Depuis le formulaire de modification de la zone
    [Documentation]  Accède au formulaire
    [Arguments]  ${zone}
    [Tags]
    Depuis le contexte de la zone  ${zone}
    Click On Form Portlet Action  zone  modifier


Ajouter la zone
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une zone
    Saisir les valeurs dans le formulaire de la zone  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Modifier la zone
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${zone}  ${values}
    [Tags]
    Depuis le formulaire de modification de la zone  ${zone}
    Saisir les valeurs dans le formulaire de la zone  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer la zone
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${zone}
    [Tags]
    Depuis le contexte de la zone  ${zone}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  zone  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  La suppression a ete correctement effectuée.


Saisir les valeurs dans le formulaire de la zone
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]
    Si "cimetiere" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "zonetype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "zonelib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire


