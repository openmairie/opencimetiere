*** Settings ***
Documentation    CRUD de la table genealogie
...    @author  generated
...    @package openCimetière
...    @version 13/10/2022 16:10

*** Keywords ***

Depuis le contexte de la généalogie
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}  ${genealogie}

    # On accède au tableau
    Depuis l'onglet 'généalogie' de la concession  ${emplacement}
    # On clique sur le résultat
    Click On Link  ${genealogie}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter la généalogie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${emplacement}  ${values}

    # On accède au tableau
    Depuis l'onglet 'généalogie' de la concession  ${emplacement}
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir la généalogie  ${values}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On récupère l'ID du nouvel enregistrement
    ${genealogie} =  Get Text  css=div.form-content span#genealogie
    # On le retourne
    [Return]  ${genealogie}

Modifier la généalogie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${genealogie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte de la généalogie  ${emplacement}  ${genealogie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  genealogie  modifier
    # On saisit des valeurs
    Saisir la généalogie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer la généalogie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${genealogie}

    # On accède à l'enregistrement
    Depuis le contexte de la généalogie  ${genealogie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  genealogie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir la généalogie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "personne_1" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "personne_2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "lien_parente" existe dans "${values}" on execute "Select From List By Label" dans le formulaire