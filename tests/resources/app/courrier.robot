*** Settings ***
Documentation  Actions spécifiques aux courrier.

*** Keywords ***
Ajouter le courrier dans le contexte de la concession
    [Documentation]  ...
    [Arguments]  ${values}  ${concession}
    [Tags]  courrier
    Depuis l'onglet 'courrier' de la concession  ${concession}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du courrier  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire du courrier
    [Documentation]  Remplit le formulaire
    ...  &{courrier} =  Create Dictionary
    ...  ...  destinataire=
    ...  ...  datecourrier=MICHEL
    ...  ...  lettretype=
    ...  ...  complement=Henri
    [Arguments]  ${values}
    [Tags]  courrier
    Si "destinataire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "datecourrier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lettretype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input HTML" dans le formulaire
    # Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

