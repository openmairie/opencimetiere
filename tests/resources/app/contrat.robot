*** Settings ***
Documentation  Actions spécifiques aux contrats.

*** Keywords ***

Depuis le contexte du contrat
    [Documentation]  Accède au formulaire
    [Arguments]  ${contrat}

    # On accède au tableau
    Depuis le listing  contrat
    # On recherche l'enregistrement
    Input Text  css=#id  ${contrat}
    Click On Search Button
    # On clique sur le résultat
    Click On Link  ${contrat}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Depuis le formulaire d'ajout d'un contrat
    # On accède au tableau
    Depuis le listing  contrat
    # On clique sur le bouton ajouter
    Click On Add Button

Depuis l'onglet 'courrier' du contrat
    [Documentation]
    [Arguments]  ${contrat}
    [Tags]
    Depuis le contexte du contrat  ${contrat}
    On clique sur l'onglet  courrier  Courrier

Ajouter le courrier dans le contexte du contrat
    [Documentation]  ...
    [Arguments]  ${values}  ${contrat}
    [Tags]  courrier
    Depuis l'onglet 'courrier' du contrat  ${contrat}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du courrier  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${courrier} =  Get Value  name:courrier
    # On le retourne
    [Return]  ${courrier}

Ajouter un contrat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'un contrat
    # On saisit des valeurs
    Saisir un contrat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${contrat} =  Get Value  css=div.contrat-form #contrat
    # On le retourne
    [Return]  ${contrat}

Ajouter un contrat depuis l'emplacement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis l'onglet 'contrat' de la concession  ${values.emplacement}
    Click On Add Button JS
    # On saisit des valeurs
    Saisir un contrat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${contrat} =  Get Value  css=div.contrat-form #contrat
    # On le retourne
    [Return]  ${contrat}

Modifier un contrat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contrat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte du contrat  ${contrat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contrat  modifier
    # On saisit des valeurs
    Saisir un contrat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer un contrat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contrat}

    # On accède à l'enregistrement
    Depuis le contexte du contrat  ${contrat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contrat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir un contrat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "emplacement_autocomplete_id" existe dans "${values}" on sélectionne l'identifiant sur l'autocomplete "emplacement" dans le formulaire en cherchant "emplacement_autocomplete_search"
    Si "datedemande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    ${is_in_emplacement} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  emplacement_autocomplete_search
    ${is_updating_date} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  datevente
    ${is_updating_duree} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  duree
    Run Keyword If  ${is_in_emplacement}== True and ${is_updating_date} == True  Input Datepicker  datevente  ${values.datevente}
    Run Keyword If  ${is_in_emplacement}== False and ${is_updating_date} == True  Input Text  css=#sousform-contrat #datevente  ${values.datevente}
    Si "origine" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dateterme" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Run Keyword If  ${is_in_emplacement}== True and ${is_updating_duree} == True  Input Text  css=#duree  ${values.duree}
    Run Keyword If  ${is_in_emplacement}== False and ${is_updating_duree} == True  Input Text  css=#sousform-contrat #duree  ${values.duree}
    Si "montant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "monnaie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "valide" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire