*** Settings ***
Documentation  Actions spécifiques aux plans.

*** Keywords ***
Activer l'option de localisation 'plan'
    [Documentation]  ...
    [Tags]
    Ajouter le paramètre depuis le menu  option_localisation  plan  null


Désactiver l'option de localisation 'plan'
    [Documentation]  ...
    [Tags]
    Supprimer le paramètre  option_localisation  plan


Depuis le listing des plans
    [Documentation]  Permet d'accéder au lsiting des plans.
    [Tags]
    Depuis le listing  plans


Depuis le formulaire d'ajout d'un plan
    [Documentation]  Crée l'enregistrement
    [Tags]
    Depuis le listing des plans
    # On clique sur le bouton ajouter
    Click On Add Button


Depuis le formulaire de modification du plan
    [Documentation]  Accède au formulaire
    [Arguments]  ${plans}
    [Tags]
    Depuis le contexte de la plan  ${plans}
    Click On Form Portlet Action  plans  modifier


Depuis le contexte du plan
    [Documentation]  Accède au formulaire
    [Arguments]  ${plans}
    [Tags]
    Depuis le listing des plans
    # On recherche l'enregistrement
    Use Simple Search  nom du plan  ${plans}
    # On clique sur le résultat
    Click On Link  ${plans}


Ajouter le plan
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'un plan
    Saisir les valeurs dans le formulaire du plan  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Modifier le plan
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plans}  ${values}
    [Tags]
    Depuis le formulaire de modification du plan  ${plans}
    Saisir les valeurs dans le formulaire du plan  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer le plan
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plans}
    [Tags]
    Depuis le contexte du plan  ${plans}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plans  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  La suppression a ete correctement effectuée.


Saisir les valeurs dans le formulaire du plan
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]

    Si "planslib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Add File" sur "fichier"


Positionner sur plan avec la fonction localisation
    [Documentation]  Ce Keyword doit être appelé depuis le formulaire de modification
    ...  d'un exmplacement avec l'option de localisation 'plan' activée
    [Arguments]  ${positionx}  ${positiony}
    [Tags]
    # On récupère les anciennes coordonnées du point
    ${old_positionx} =  Get Value  css=#positionx
    ${old_positiony} =  Get Value  css=#positiony
    # On clique sur la fonction localisation
    Click Element  css=a.localisation
    # On récupère la popup ouverte
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Window  name=localisation
    # On vérifie le titre de la page et qu'il n'y a pas d'erreurs
    Page Title Should Be  Localisation
    La page ne doit pas contenir d'erreur
    # On déplace l'élément pour simuler un déplacement utilisateur
    Execute Javascript  plop = $('#plan-draggable').offset();$('#plan-draggable').offset({ top: plop.top-${old_positiony}+${positiony}+1, left: plop.left-${old_positionx}+${positionx}+1});
    # On double clique sur le point pour sauvegarder le déplacement/fermer la fenêtre
    Double Click Element  css=#plan-draggable
    # On récupère la fenêtre principale
    Select Window
    # On vérifie que les nouvelles coordonnées ont bien été positionnées dans le formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Value Should Be  css=#positionx   ${positionx}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Value Should Be  css=#positiony   ${positiony}

