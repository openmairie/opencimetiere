*** Settings ***
Documentation  Actions spécifiques aux pdf.

*** Keywords ***

Vérifier Que Le PDF Contient Des Strings
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${from_system}=False

    Run Keyword If  ${from_system}==False  Open PDF  ${window}
    :FOR  ${string}  IN  @{strings_to_find}
    \    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    \    ...  Page Should Contain  ${string}

    La page ne doit pas contenir d'erreur
    Run Keyword If  ${from_system}==False  Close PDF
