*** Settings ***
Documentation  Actions spécifiques aux emplacements.

*** Keywords ***
Depuis le listing des emplacements archivées
    [Documentation]  Permet d'accéder au listing des concessions archivées.
    [Tags]
    Depuis le listing  emplacement_archive

Depuis l'écran de traitement 'Calcul de la place occupée'
    [Tags]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  operation  concessionplace
    Page Title Should Be  Opérations > Calcul De La Place
    First Tab Title Should Be  Emplacement
    La page ne doit pas contenir d'erreur


Saisir les valeurs dans le formulaire de l'emplacement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]
    #
    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  voielib
    Run Keyword If  ${exist} == True  Sélectionne la voie avec le combobba  ${values.cimetierelib}  ${values.zonelib}  ${values.voielib}
    #
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numerocadastre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "famille" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numeroacte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datevente" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "terme" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateterme" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "nombreplace" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeoccupe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "superficie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "placeconstat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dateconstat" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "plans" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "positionx" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "positiony" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "photo" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sepulturetype" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "daterenouvellement" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "renouvellementconcession" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "typeconcession" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "temp1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp4" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "temp5" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "videsanitaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "semelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etatsemelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "monument" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etatmonument" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "largeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "profondeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "abandon" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_abandon" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "dateacte" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "voie_autocomplete" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "voie" dans le formulaire en cherchant "voie_autocomplete_search"


Sélectionne la voie avec le combobba
    [Documentation]
    [Arguments]  ${cimetierelib}  ${zonelib}  ${voielib}
    [Tags]
    Click Element  css=#autocomplete-voie-link-selection
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Contain  css=#ui-dialog-title-overlay-selection-voie-by-cimetiere-zone  Sélection de voie par cimetière/zone
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Select From List By Label  css=#overlay-selection-voie-by-cimetiere-zone #combo-select-cimetiere  ${cimetierelib}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Select From List By Label  css=#overlay-selection-voie-by-cimetiere-zone #combo-select-zone  ${zonelib}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Select From List By Label  css=#overlay-selection-voie-by-cimetiere-zone #combo-select-voie  ${voielib}
    Click Element  css=a.linkjsclosewindow
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Form Value Should Be  css=#autocomplete-voie-search  ${voielib} [${zonelib}][${cimetierelib}]


