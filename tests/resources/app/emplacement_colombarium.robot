*** Settings ***
Documentation  Actions spécifiques aux colombariums.

*** Keywords ***
Depuis le listing des colombariums
    [Documentation]  Permet d'accéder au listing des colombariums.
    [Tags]
    Depuis le listing  colombarium


Depuis le contexte du colombarium
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    Depuis le listing des colombariums
    Use Simple Search  id  ${emplacement}
    Click Element  css=#action-tab-colombarium-left-consulter-${emplacement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#emplacement  ${emplacement}


Depuis le formulaire d'ajout d'un colombarium
    [Documentation]
    [Tags]
    Depuis le listing des colombariums
    Click On Add Button


Depuis le formulaire de modification du colombarium
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    Depuis le contexte du colombarium  ${emplacement}
    Click On Form Portlet Action  colombarium  modifier


Ajouter le colombarium
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'un colombarium
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${emplacement_id} =  Get Text  css=div.emplacement-form #emplacement
    Log  ${emplacement_id}
    [Return]  ${emplacement_id}


Modifier le colombarium
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${values}
    [Tags]
    Depuis le formulaire de modification du colombarium  ${emplacement}
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Supprimer le colombarium
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  emplacement  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  La suppression a ete correctement effectuée.


Depuis l'onglet 'dossier' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  dossier  Dossier


Depuis l'onglet 'concessionnaire' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  concessionnaire  Concessionnaire


Depuis l'onglet 'défunt' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  defunt  Défunt


Depuis l'onglet 'ayant-droit' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  ayandroit  Ayant-droit


Depuis l'onglet 'courrier' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  courrier  Courrier


Depuis l'onglet 'opérations' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  operation_trt  Opérations


Depuis l'onglet 'travaux' du colombarium
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte du colombarium  ${emplacement}
    On clique sur l'onglet  travaux  Travaux