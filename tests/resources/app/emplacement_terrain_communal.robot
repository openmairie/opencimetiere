*** Settings ***
Documentation  Actions spécifiques aux terrains communaux.

*** Keywords ***
Depuis le listing des terrains communaux
    [Documentation]  Permet d'accéder au listing des colombariums.
    [Tags]
    Depuis le listing  terraincommunal

Depuis le contexte du terrain communal
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    Depuis le listing des terrains communaux
    Use Simple Search  id  ${emplacement}
    Click Element  css=#action-tab-terraincommunal-left-consulter-${emplacement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#emplacement  ${emplacement}

Depuis le formulaire d'ajout d'un terrain communal
    [Documentation]
    [Tags]
    Depuis le listing des terrains communaux
    Click On Add Button

Ajouter le terrain communal
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'un terrain communal
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#datevente
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#dateterme
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${emplacement_id} =  Get Text  css=div.emplacement-form #emplacement
    Log  ${emplacement_id}
    [Return]  ${emplacement_id}
