*** Settings ***
Documentation  Actions spécifiques aux entreprises.

*** Keywords ***
Depuis le listing des entreprises
    [Documentation]  Permet d'accéder au lsiting des entreprises.
    [Tags]
    Depuis le listing  entreprise


Depuis le contexte de l'entreprise
    [Documentation]  Accède au formulaire
    [Arguments]  ${entreprise}
    [Tags]
    Depuis le listing des entreprises
    # On recherche l'enregistrement
    Use Simple Search  entreprise  ${entreprise}
    # On clique sur le résultat
    Click On Link  ${entreprise}


Ajouter l'entreprise
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le listing des entreprises
    # On clique sur le bouton ajouter
    Click On Add Button
    Saisir les valeurs dans le formulaire de l'entreprise  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Modifier l'entreprise
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${entreprise}  ${values}
    [Tags]
    Depuis le contexte de l'entreprise  ${entreprise}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  entreprise  modifier
    Saisir les valeurs dans le formulaire de l'entreprise  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer l'entreprise
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${entreprise}
    [Tags]
    Depuis le contexte de l'entreprise  ${entreprise}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  entreprise  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  La suppression a ete correctement effectuée.


Saisir les valeurs dans le formulaire de l'entreprise
    [Documentation]  Remplit le formulaire
    ...  &{entreprise} =  Create Dictionary
    ...  ...  nomentreprise=Monsieur
    ...  ...  pf=Non
    ...  ...  adresse1=12 rue de la République
    ...  ...  adresse2=Parc de la Durance
    ...  ...  cp=99678
    ...  ...  ville=LIBREVILLE
    ...  ...  telephone=0099999999
    [Arguments]  ${values}
    [Tags]
    Si "nomentreprise" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pf" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire



