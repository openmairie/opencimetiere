*** Settings ***
Documentation  Actions spécifiques aux opérations.

*** Keywords ***
Depuis le listing des opérations d'inhumation sur concession
    [Documentation]  Permet d'accéder au lsiting opérations d'inhumation sur concession.
    [Tags]
    Depuis le listing  inhumation_concession


Depuis le listing des opérations d'inhumation sur terrain communal
    [Documentation]  Permet d'accéder au lsiting opérations d'inhumation sur terrain communal.
    [Tags]
    Depuis le listing  inhumation_terraincommunal


Depuis le listing des opérations d'inhumation sur enfeu
    [Documentation]  Permet d'accéder au lsiting opérations d'inhumation sur enfeu.
    [Tags]
    Depuis le listing  inhumation_enfeu


Depuis le listing des opérations d'inhumation sur colombarium
    [Documentation]  Permet d'accéder au lsiting opérations d'inhumation sur colombarium.
    [Tags]
    Depuis le listing  inhumation_colombarium


Depuis le listing des opérations de réduction sur concession
    [Documentation]  Permet d'accéder au listing opérations de réduction sur concession.
    [Tags]
    Depuis le listing  reduction_concession


Depuis le listing des opérations de réduction sur enfeu
    [Documentation]  Permet d'accéder au listing opérations de réduction sur enfeu.
    [Tags]
    Depuis le listing  reduction_enfeu


Depuis le listing des opérations de transfert
    [Documentation]  Permet d'accéder au lsiting opérations de transfert.
    [Tags]
    Depuis le listing  transfert


Depuis le contexte de l'opération d'inhumation sur concession
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur concession
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération d'inhumation sur terrain communal
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur terrain communal
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération d'inhumation sur enfeu
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur enfeu
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération d'inhumation sur colombarium
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur colombarium
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération de réduction sur concession
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de réduction sur concession
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération de réduction sur enfeu
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de réduction sur enfeu
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Depuis le contexte de l'opération de transfert
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de transfert
    Use Simple Search  id  ${operation}
    Click On Link  ${operation}


Valider l'opération d'inhumation sur concession
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur concession
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-inhumation_concession-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération d'inhumation sur terrain communal
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur terrain communal
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-inhumation_terraincommunal-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération d'inhumation sur enfeu
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur enfeu
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-inhumation_enfeu-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération d'inhumation sur colombarium
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur colombarium
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-inhumation_colombarium-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération de réduction sur concession
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de réduction sur concession
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-reduction_concession-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération de réduction sur enfeu
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de réduction sur enfeu
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-reduction_enfeu-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Valider l'opération de transfert
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de transfert
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-transfert-left-validation-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=#form-content #numdossier
    ${operation_numdossier} =  Get Text  css=#form-content #numdossier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  L'opération ${operation_numdossier} est validée.
    La page ne doit pas contenir d'erreur


Ouvrir l'édition PDF de l'opération d'inhumation sur concession
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations d'inhumation sur concession
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-inhumation_concession-left-edition-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Cet écran permet d'imprimer les éditions disponibles pour votre opération.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Open PDF  ${OM_PDF_TITLE}


Ouvrir l'édition PDF de l'opération de transfert
    [Documentation]  ...
    [Arguments]  ${operation}
    [Tags]
    Depuis le listing des opérations de transfert
    Use Simple Search  id  ${operation}
    Click Element  css=#action-tab-transfert-left-edition-operation-${operation}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Cet écran permet d'imprimer les éditions disponibles pour votre opération.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Open PDF  ${OM_PDF_TITLE}


Depuis le formulaire d'ajout d'une opération d'inhumation sur concession
    [Documentation]
    [Tags]
    Depuis le listing des opérations d'inhumation sur concession
    Click On Add Button


Depuis le formulaire d'ajout d'une opération d'inhumation sur terrain communal
    [Documentation]
    [Tags]
    Depuis le listing des opérations d'inhumation sur terrain communal
    Click On Add Button


Depuis le formulaire d'ajout d'une opération d'inhumation sur enfeu
    [Documentation]
    [Tags]
    Depuis le listing des opérations d'inhumation sur enfeu
    Click On Add Button


Depuis le formulaire d'ajout d'une opération d'inhumation sur colombarium
    [Documentation]
    [Tags]
    Depuis le listing des opérations d'inhumation sur colombarium
    Click On Add Button


Depuis le formulaire d'ajout d'une opération de transfert
    [Documentation]
    [Tags]
    Depuis le listing des opérations de transfert
    Click On Add Button


Depuis le formulaire d'ajout d'une opération de réduction sur concession
    [Documentation]
    [Tags]
    Depuis le listing des opérations de réduction sur concession
    Click On Add Button


Depuis le formulaire d'ajout d'une opération de réduction sur enfeu
    [Documentation]
    [Tags]
    Depuis le listing des opérations de réduction sur enfeu
    Click On Add Button


Ajouter l'opération d'inhumation sur concession
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération d'inhumation sur concession
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Ajouter l'opération d'inhumation sur enfeu
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération d'inhumation sur enfeu
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Ajouter l'opération d'inhumation sur terrain communal
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération d'inhumation sur terrain communal
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Ajouter l'opération d'inhumation sur colombarium
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération d'inhumation sur colombarium
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Ajouter l'opération de réduction sur concession
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération de réduction sur concession
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Ajouter l'opération de réduction sur enfeu
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération de réduction sur enfeu
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}

Ajouter l'opération de transfert
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une opération de transfert
    Saisir les valeurs dans le formulaire de l'opération  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${operation_id} =  Get Text  css=#form-content #operation
    [Return]  ${operation_id}


Depuis l'écran de traitement 'Réinitialisation de la séquence numéro de dossier'
    [Tags]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Go To Submenu In Menu  operation  trt_reset_seq_numdossier
    La page ne doit pas contenir d'erreur


Saisir les valeurs dans le formulaire de l'opération
    [Documentation]  Remplit le formulaire
    ...  &{operation} =  Create Dictionary
    ...  ...  date=31/05/2017
    ...  ...  heure=12:00:00
    ...  ...  emplacement=1
    ...  ...  societe_coordonnee=...
    ...  ...  pf_coordonnee=...
    ...  ...  observation=...
    ...  ...  defunt_titre=Monsieur
    ...  ...  defunt_nom=SUPONT
    ...  ...  defunt_prenom=Marcel
    ...  ...  defunt_marital=...
    ...  ...  defunt_datenaissance=01/01/1970
    ...  ...  defunt_datedeces=25/05/2017
    ...  ...  defunt_lieudeces=LIBREVILLE
    ...  ...  defunt_nature=cercueil
    [Arguments]  ${values}
    [Tags]
    #Si "numdossier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "emplacement" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "societe_coordonnee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pf_coordonnee" existe dans "${values}" on execute "Input Text" dans le formulaire
    #Si "etat" existe dans "${values}" on execute "Input Text" dans le formulaire
    #Si "categorie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "particulier" existe dans "${values}" on execute "Input Text" dans le formulaire
    #Si "emplacement_transfert" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" sur "css=textarea#observation"
    #
    Si "defunt_titre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "defunt_nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "defunt_lieunaissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_datedeces" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "defunt_lieudeces" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defunt_nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "defunt_parente" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "emplacement_autocomplete_id" existe dans "${values}" on sélectionne l'identifiant sur l'autocomplete "emplacement" dans le formulaire en cherchant "emplacement_autocomplete_search"
    Si "emplacement_transfert_autocomplete_id" existe dans "${values}" on sélectionne l'identifiant sur l'autocomplete "emplacement_transfert" dans le formulaire en cherchant "emplacement_transfert_autocomplete_search"


Depuis l'onglet 'opération défunt' de l'opération de transfert
    [Documentation]
    [Arguments]  ${operation}
    [Tags]
    Depuis le contexte de l'opération de transfert  ${operation}
    On clique sur l'onglet  operation_defunt  Opération Défunt

Ajouter un défunt sur l'opération de transfert
    [Documentation]
    [Arguments]  ${operation}  ${label_defunt}
    [Tags]
    Depuis l'onglet 'opération défunt' de l'opération de transfert  ${operation}
    Click On Add Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label  css=#defunt  ${label_defunt}
    Click On Submit Button
