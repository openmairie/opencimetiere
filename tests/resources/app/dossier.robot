*** Settings ***
Documentation  Actions spécifiques aux dossiers.

*** Keywords ***
Ajouter le dossier dans le contexte de la concession
    [Documentation]  ...
    [Arguments]  ${values}  ${concession}
    [Tags]
    Depuis l'onglet 'dossier' de la concession  ${concession}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du dossier  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire du dossier
    [Documentation]  Remplit le formulaire
    ...  &{dossier} =  Create Dictionary
    ...  ...  fichier=fichier.jpg
    ...  ...  datedossier=10/01/2015
    ...  ...  typedossier=photo
    ...  ...  observation=Photo de l'emplacement
    [Arguments]  ${values}
    [Tags]
    Si "emplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Add File" sur "fichier"
    Si "datedossier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "typedossier" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" sur "css=textarea#observation"


