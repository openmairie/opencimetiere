*** Settings ***
Documentation  Actions spécifiques aux types de sépulture.

*** Keywords ***
Depuis le listing des *types de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Accède au listing des *types de sépulture*.
    Depuis le listing  sepulture_type


Depuis le formulaire d'ajout d'un *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un *type de sépulture*.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=sepulture_type&action=0&retour=form


Depuis le contexte du *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Accède à la fiche consultation du *type de sépulture* passé en paramètre.
    [Arguments]  ${sepulture_type_id}
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=sepulture_type&action=3&idx=${sepulture_type_id}&retour=tab


Ajouter le *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'un *type de sépulture*
    Saisir les valeurs dans le formulaire du *type de sépulture*  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${sepulture_type_id} =  Get Text  css=div.form-content span#sepulture_type
    [Return]  ${sepulture_type_id}


Modifier le *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${sepulture_type_id}  ${values}
    Depuis le contexte du *type de sépulture*  ${sepulture_type_id}
    Click On Form Portlet Action  sepulture_type  modifier
    Saisir les valeurs dans le formulaire du *type de sépulture*  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer le *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${sepulture_type_id}
    Depuis le contexte du *type de sépulture*  ${sepulture_type_id}
    Click On Form Portlet Action  sepulture_type  supprimer
    Click On Submit Button
    Valid Message Should Be  La suppression a été correctement effectuée.


Saisir les valeurs dans le formulaire du *type de sépulture*
    [Tags]  type_de_sepulture
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "colonne" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ligne" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_edition" existe dans "${values}" on execute "Input Text" dans le formulaire

