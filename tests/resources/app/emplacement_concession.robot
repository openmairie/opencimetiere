*** Settings ***
Documentation  Actions spécifiques aux concessions.

*** Keywords ***
Depuis le listing des concessions
    [Documentation]  Permet d'accéder au lsiting des concessions.
    [Tags]
    Depuis le listing  concession


Depuis le contexte de la concession
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    Depuis le listing des concessions
    Use Simple Search  id  ${emplacement}
    Click Element  css=#action-tab-concession-left-consulter-${emplacement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#emplacement  ${emplacement}


Depuis le formulaire d'ajout d'une concession
    [Documentation]
    [Tags]
    Depuis le listing des concessions
    Click On Add Button


Depuis le formulaire de modification de la concession
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    Depuis le contexte de la concession  ${emplacement}
    Click On Form Portlet Action  concession  modifier


Ajouter la concession
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une concession
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${emplacement_id} =  Get Text  css=div.emplacement-form #emplacement
    Log  ${emplacement_id}
    [Return]  ${emplacement_id}


Modifier la concession
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${values}
    [Tags]
    Depuis le formulaire de modification de la concession  ${emplacement}
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Supprimer la concession
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  emplacement  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  La suppression a ete correctement effectuée.


Depuis l'onglet 'dossier' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  dossier  Dossier


Depuis l'onglet 'contacts' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  autorisation  Contacts


Depuis l'onglet 'défunt' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  defunt  Défunt


Depuis l'onglet 'ayant-droit' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  ayandroit  Ayant-droit


Depuis l'onglet 'courrier' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  courrier  Courrier

Depuis l'onglet 'contrat' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  contrat  Contrats

Depuis l'onglet 'opérations' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  operation_trt  Opérations


Depuis l'onglet 'travaux' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  travaux  Travaux


Depuis l'onglet 'généalogie' de la concession
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de la concession  ${emplacement}
    On clique sur l'onglet  genealogie  Généalogie