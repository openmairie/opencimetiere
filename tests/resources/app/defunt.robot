*** Settings ***
Documentation  Actions spécifiques aux defunts.

*** Keywords ***
Ajouter le defunt dans le contexte de la concession
    [Documentation]  ...
    [Arguments]  ${values}  ${concession}
    [Tags]
    Depuis l'onglet 'défunt' de la concession  ${concession}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du defunt  ${values}
    Click On Submit Button In Subform

Ajouter le defunt dans le contexte du colombarium
    [Documentation]  ...
    [Arguments]  ${values}  ${colombarium}
    [Tags]
    Depuis l'onglet 'défunt' du colombarium  ${colombarium}
    Click On Add Button JS
    Saisir les valeurs dans le formulaire du defunt  ${values}
    Click On Submit Button In Subform

Depuis le contexte du défunt dans la concession
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}  ${defunt}

    # On accède au tableau
    Depuis l'onglet 'défunt' de la concession  ${emplacement}
    # On clique sur le résultat
    Click On Link  ${defunt}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Saisir les valeurs dans le formulaire du defunt
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]
    Si "titre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "marital" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datenaissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lieunaissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "datedeces" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lieudeces" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "parente" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dateinhumation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "reduction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "datereduction" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "exhumation" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dateexhumation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" sur "css=textarea#observation"
