*** Settings ***
Documentation  Actions spécifiques aux ossuaires.

*** Keywords ***
Depuis le listing des ossuaires
    [Documentation]  Permet d'accéder au lsiting des ossuaires.
    [Tags]
    Depuis le listing  ossuaire


Depuis le contexte de l'ossuaire
    [Documentation]  Accède au formulaire
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le listing des ossuaires
    Use Simple Search  id  ${emplacement}
    Click Element  css=#action-tab-ossuaire-left-consulter-${emplacement}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#emplacement  ${emplacement}


Depuis le formulaire d'ajout d'un ossuaire
    [Documentation]
    [Tags]
    Depuis le listing des ossuaires
    Click On Add Button


Ajouter l'ossuaire
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'un ossuaire
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${emplacement_id} =  Get Text  css=div.emplacement-form #emplacement
    [Return]  ${emplacement_id}


Modifier l'ossuaire
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${emplacement}  ${values}
    [Tags]
    Depuis le contexte de l'ossuaire  ${emplacement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  ossuaire  modifier
    Saisir les valeurs dans le formulaire de l'emplacement  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Supprimer l'ossuaire
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de l'ossuaire  ${emplacement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  ossuaire  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  La suppression a ete correctement effectuée.


Depuis l'onglet 'défunt' de l'ossuaire
    [Documentation]
    [Arguments]  ${emplacement}
    [Tags]
    Depuis le contexte de l'ossuaire  ${emplacement}
    On clique sur l'onglet  defunt  Défunt

