*** Settings ***
Documentation  Actions spécifiques aux cimetières.

*** Keywords ***
Depuis le listing des cimetières
    [Documentation]  Permet d'accéder au lsiting des cimetières.
    [Tags]
    Depuis le listing  cimetiere


Depuis le contexte du cimetière
    [Documentation]  Accède au formulaire
    [Arguments]  ${cimetiere}
    [Tags]
    Depuis le listing des cimetières
    # On recherche l'enregistrement
    Use Simple Search  cimetière  ${cimetiere}
    # On clique sur le résultat
    Click On Link  ${cimetiere}


Depuis le formulaire d'ajout d'un cimetière
    [Documentation]  Crée l'enregistrement
    [Tags]
    Depuis le listing des cimetières
    # On clique sur le bouton ajouter
    Click On Add Button


Depuis le formulaire de modification du cimetière
    [Documentation]  Accède au formulaire
    [Arguments]  ${cimetiere}
    [Tags]
    Depuis le contexte du cimetière  ${cimetiere}
    Click On Form Portlet Action  cimetiere  modifier


Ajouter le cimetière
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'un cimetière
    Saisir les valeurs dans le formulaire du cimetière  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Modifier le cimetière
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${cimetiere}  ${values}
    [Tags]
    Depuis le formulaire de modification du cimetière  ${cimetiere}
    Saisir les valeurs dans le formulaire du cimetière  ${values}
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Supprimer le cimetière
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${cimetiere}
    [Tags]
    Depuis le contexte du cimetière  ${cimetiere}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  cimetiere  supprimer
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Be  La suppression a ete correctement effectuée.


Saisir les valeurs dans le formulaire du cimetière
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    [Tags]
    Si "cimetierelib" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observations" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire


