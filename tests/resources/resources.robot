*** Settings ***
Documentation  Ressources (librairies, keywords et variables)

# Keywords framework
Library  openmairie.robotframework.Library

# Mots-clefs métier
Resource  app${/}cimetiere.robot
Resource  app${/}common.robot
Resource  app${/}concessionnaire.robot
Resource  app${/}contrat.robot
Resource  app${/}courrier.robot
Resource  app${/}dossier.robot
Resource  app${/}emplacement.robot
Resource  app${/}emplacement_concession.robot
Resource  app${/}emplacement_colombarium.robot
Resource  app${/}emplacement_ossuaire.robot
Resource  app${/}emplacement_terrain_communal.robot
Resource  app${/}entreprise.robot
Resource  app${/}genealogie.robot
Resource  app${/}lien_parente.robot
Resource  app${/}operation.robot
Resource  app${/}pdf.robot
Resource  app${/}plans.robot
Resource  app${/}sepulture_type.robot
Resource  app${/}tarif.robot
Resource  app${/}travaux.robot
Resource  app${/}voie.robot
Resource  app${/}zone.robot
Resource  app${/}defunt.robot

*** Variables ***
${SERVER}          localhost
${PROJECT_NAME}    opencimetiere
${BROWSER}         firefox
${DELAY}           0
${ADMIN_USER}      admin
${ADMIN_PASSWORD}  admin
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${TITLE}           :: openMairie :: openCimetière

*** Keywords ***
For Suite Setup
    Reload Library  openmairie.robotframework.Library
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order    resources
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur

Si "${key}" existe dans "${collection}" on sélectionne la valeur sur l'autocomplete "${autocomplete}" dans le formulaire en cherchant "${search}"
    [Tags]

    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${collection}  ${key}
    Return From Keyword If  ${exist} != True  0
    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${collection}  ${search}
    Return From Keyword If  ${exist} != True  0
    Get Value From Dictionary  ${collection}  ${search}
    Input Text  css=#autocomplete-${autocomplete}-search    ${value}
    Get Value From Dictionary  ${collection}  ${key}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${value}

Si "${key}" existe dans "${collection}" on sélectionne l'identifiant sur l'autocomplete "${autocomplete}" dans le formulaire en cherchant "${search}"
    [Tags]

    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${collection}  ${key}
    Return From Keyword If  ${exist} != True  0
    ${exist} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${collection}  ${search}
    Return From Keyword If  ${exist} != True  0
    Get Value From Dictionary  ${collection}  ${search}
    Input Text  css=#autocomplete-${autocomplete}-search    ${value}
    Get Value From Dictionary  ${collection}  ${key}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=a.autocomplete-${autocomplete}-result-${value}
    Form Field Attribute Should Be  autocomplete-${autocomplete}-id  value  ${value}
