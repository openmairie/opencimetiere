*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...

*** Test Cases ***
Constitution du jeu de données
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Set Suite Variable  ${testid}  130
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{zone02_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=ENCLOS
    ...  zonelib=Z${testid}-02
    Ajouter la zone  ${zone02_cim01}
    Set Suite Variable  ${zone02_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{voie02_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=V${testid}-02
    Ajouter la voie  ${voie02_zone01_cim01}
    Set Suite Variable  ${voie02_zone01_cim01}
    #
    &{voie03_zone02_cim01} =  Create Dictionary
    ...  zone=${zone02_cim01.zonetype} ${zone02_cim01.zonelib} (${zone02_cim01.cimetiere})
    ...  voietype=ILOT
    ...  voielib=v${testid}-03
    Ajouter la voie  ${voie03_zone02_cim01}
    Set Suite Variable  ${voie03_zone02_cim01}
    #
    &{voie04_zone02_cim01} =  Create Dictionary
    ...  zone=${zone02_cim01.zonetype} ${zone02_cim01.zonelib} (${zone02_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=v${testid}-04
    Ajouter la voie  ${voie04_zone02_cim01}
    Set Suite Variable  ${voie04_zone02_cim01}
    #
    &{cimetiere02} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-02
    ...  adresse1=RUE DE ROME
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere02}
    Set Suite Variable  ${cimetiere02}
    #
    &{type_de_sepulture_01} =  Create Dictionary
    ...  code=TDS-${testid}-01
    ...  libelle=Type de sépulture ${testid}-01
    ${type_de_sepulture_01.id} =  Ajouter le *type de sépulture*  ${type_de_sepulture_01}
    Set Suite Variable  ${type_de_sepulture_01}
    #
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  sepulturetype=${type_de_sepulture_01.libelle}
    ${concession01.id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01}

Vérification du bon fonctionnement de la généalogie
    [Documentation]  Permet de vérifier que la fonctionnalité de la généalogie
    ...  fonctionne correctement.
    ...  Ajout de lien de parentés
    ...  Ajout d'un défunt et d'un concessionaire
    ...  Ajout d'un défunt qui est la même personne que le concessionnaire
    ...  Ajout d'un lien de parenté entre les deux personnes
    ...  Ajout du lien de parenté même personne et vérification de la correspondance
    ...  des liens de parentés entre défunt et concessionnaire

    Depuis la page d'accueil  admin  admin
    &{lien_parente01} =  Create Dictionary
    ...  libelle=Père
    ...  lien_inverse=Fils
    ${lien_parente01.id} =  Ajouter le lien de parenté  ${lien_parente01}

    &{lien_parente02} =  Create Dictionary
    ...  libelle=Concessionnaire
    ...  meme_personne=true
    ...  lien_inverse=Défunt
    ${lien_parente02.id} =  Ajouter le lien de parenté  ${lien_parente02}

    # Ajout du concessionnaire
    &{concessionnaire} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL
    ...  marital=
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 rue de la République
    ...  adresse2=
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  dcd=true
    ...  observation=
    ${concessionnaire.id} =  Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire}  ${concession01.id}

    # Ajout d'un défunt
    &{defunt01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=SUPONT${testid}
    ...  prenom=MARCEL
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt01}  ${concession01.id}

    ${defunt01.id} =  Get Value  css=div.defunt-form #defunt

    # Ajout de défunt qui sera lié au concessionnaire
    &{defunt02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL
    ...  marital=
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 rue de la République
    ...  adresse2=
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  dcd=true
    ...  observation=
    Ajouter le defunt dans le contexte de la concession  ${defunt02}  ${concession01.id}

    ${defunt02.id} =  Get Value  css=div.defunt-form #defunt

    ${genealogie01} =  Create Dictionary
    ...  personne_1=${concessionnaire.id} ${concessionnaire.nom} ${concessionnaire.prenom} contact
    ...  lien_parente=Père -> Fils
    ...  personne_2=${defunt01.id} ${defunt01.nom} ${defunt01.prenom} defunt
    Ajouter la généalogie  ${concession01.id}  ${genealogie01}

    ${genealogie02} =  Create Dictionary
    ...  personne_1=${concessionnaire.id} ${concessionnaire.nom} ${concessionnaire.prenom} contact
    ...  lien_parente=Concessionnaire -> Défunt
    ...  personne_2=${defunt02.id} ${defunt02.nom} ${defunt02.prenom} defunt
    Ajouter la généalogie  ${concession01.id}  ${genealogie02}

    Depuis le contexte du défunt dans la concession  ${concession01.id}  ${defunt01.nom}
    Element Should Contain  css=div.defunt-form #genealogie  Fils de MICHEL HENRI

    Depuis le contexte du concessionnaire de la concession  ${concession01.id}  ${concessionnaire.nom}
    Element Should Contain  css=div.autorisation-form #genealogie  Père de SUPONT130 MARCEL
    Element Should Contain  css=div.autorisation-form #genealogie  Même personne que le Défunt MICHEL HENRI

    Depuis le contexte du défunt dans la concession  ${concession01.id}  ${defunt02.nom}
    Element Should Contain  css=div.defunt-form #genealogie  Père de SUPONT130 MARCEL
    Element Should Contain  css=div.defunt-form #genealogie  Même personne que le Concessionnaire MICHEL HENRI

