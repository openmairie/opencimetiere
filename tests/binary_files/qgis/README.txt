Exemple de fichier qgis
=======================

Pour fonctionner, le serveur QGIS doit �tre actif.


PARAMETRAGE DU FICHIER modele QGIS 
	. cr�er le fichier opencimetiere.qgs par copie du fichier modele.qgs
	. remplacer dans opencimetiere.qgs les chaines suivantes
		.. [DBNAME] : nom de la base postgresl (ex. opencimetiere)
		.. [HOST] : serveur postgresql (ex. localhost)
		.. [PORT] : port du serveur postgresql (ex. 5432)
		.. [USER] : utilisateur postgresql (ex. postgres)
		.. [PASSWORD] : mot de passe de l'utilisateur postgresql (ex. postgres)
		.. [SCHEMA] : sch�ma abritant l'application (ex. opencimetiere)

modele.qgs : que les tables d'opencimetiere
model_avec_geo.qgs : tables opencimetiere + table geo_*



Dans l exemple opencimetiere.qgs, la connexion initialis�e.
C'est la m�me que dyn/database.inc.php :
	base = opencimetiere
	host = localhost
	port = 5432
	user = postgres
	password = postgres
	schema = opencimetiere
