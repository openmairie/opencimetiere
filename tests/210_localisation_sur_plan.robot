*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    Supprimer le paramètre  option_localisation
    Activer l'option de localisation 'plan'
    #
    Set Suite Variable  ${testid}  210
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{plan01} =  Create Dictionary
    ...  planslib=PLAN CIMETIERE${testid}-01
    ...  fichier=colline.png
    Ajouter le plan  ${plan01}
    Set Suite Variable  ${plan01}
    #
    &{plan02} =  Create Dictionary
    ...  planslib=PLAN CIMETIERE${testid}-02
    ...  fichier=saintpierre.png
    Ajouter le plan  ${plan02}
    Set Suite Variable  ${plan02}
    #
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=17
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan02.planslib}
    ...  positionx=50
    ...  positiony=40
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01_id}

    &{concession02} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=18
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}
    Set Suite Variable  ${concession02_id}


Localisation d'une concession sur plan
    [Documentation]  Ce test permet de vérifier la localisation d'un
    ...  emplacement sur plan.
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le contexte de la concession  ${concession02_id}
    Click On Form Portlet Action  concession  modifier

    # On ne sélectionne pas de plan
    Click Element  css=a.localisation
    Alert Should Be Present  Vous devez d'abord selectionner un plan pour pouvoir localiser l'emplacement.

    # On sélectionne le plan souhaité
    Select From List By Label  plans  ${plan02.planslib}
    Positionner sur plan avec la fonction localisation  95  29
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Déconstitution du jeu de données
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    Désactiver l'option de localisation 'plan'

