*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Vérification du bon fonctionnement de l'import des tarifs
    [Documentation]  On utilise un fichier csv présent dans binary_files
    Depuis la page d'accueil  admin  admin
    Depuis l'import  tarif
    Add File  fic1  import-tarif-test.csv
    Select From List By Label  css=#separateur  , (virgule)
    Click On Submit Button In Import CSV

    # On vérifie que les tarifs sont bien importés
    Depuis le listing des tarifs
    Page Should Not Contain  Aucun enregistrement.
