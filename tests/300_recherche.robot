*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Recherche globale
    [Documentation]  Permet de vérifier la rechercher global, notamment
    ...  l'utilisation d'apostrophe "'" dans celle-ci.
    #
    Depuis la page d'accueil  admin  admin
    # Le champ de recherche est positionné en autofocus à chaque chargement du
    # tableau de bord
    Element Should Be Focused  css=#dashboard .widget_search input[name="recherche"]
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  cp=99607
    ...  ville=LIBREVILLE
    Ajouter le cimetière  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z-01
    Ajouter la zone  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #Création d'une concession avec un apostrophes
    &{concession01} =  Create Dictionary
    ...  famille=D'UPONT
    ...  numero=99
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession01_id} =  Ajouter la concession  ${concession01}

    #
    Go To Submenu In Menu  recherche  recherche
    Page Title Should Be  Recherche > Recherche Globale
    # Le champ de recherche est positionné en autofocus à chaque chargement du
    # tableau de bord
    Element Should Be Focused  css=#rechercheFormModule input[name="recherche"]
    #
    Select Checkbox  name=largeSearch
    Input Text  name=recherche  *
    Click Element  css=button
    La page ne doit pas contenir d'erreur

    # Bug de non régression pour échapper les recherche avec apostrophes
    Input Text  name=recherche  D'UPONT
    Click Element  css=button
    La page ne doit pas contenir d'erreur
    Page Should Not Contain  Erreur SQL
    Element Should Contain  css=.tab-data  D'UPONT
    Form Value Should Be  name=recherche  D'UPONT


Concession à terme
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  recherche  concession_terme
    Page Title Should Be  Recherche > Concession À Terme


Concession libre
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  recherche  concession_libre
    Page Title Should Be  Recherche > Concession Libre


Colombarium libre
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  recherche  colombarium_libre
    Page Title Should Be  Recherche > Colombarium Libre


Terrain Communal libre
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  recherche  terraincommunal_libre
    Page Title Should Be  Recherche > Terrain Communal Libre


Verification de redirection lors d'une recherche
    [Documentation]  On vérifie que lorsque on accède à un emplacement ou un défunt lié a cet emplacement,
    ...  la fiche de l'emplacement est dans le bon contexte (avec la bonne nature).

    #Isolation du contexte
    Depuis la page d'accueil  admin  admin
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE-RECHERCHE
    ...  adresse1=RUE ZERO
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z-RECHERCHE
    Ajouter la zone  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V-RECHERCHE
    Ajouter la voie  ${voie01_zone01_cim01}
    # Création des emplacement de type concession et colombarium
    &{concession_recherche} =  Create Dictionary
    ...  famille=TEST300CONCESSIONRECHERCHE
    ...  numero=123
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${id_concession_recherche} =  Ajouter la concession  ${concession_recherche}

    &{colombarium_recherche} =  Create Dictionary
    ...  famille=TEST300COLOMBARIUMRECHERCHE
    ...  numero=123
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${id_colombarium_recherche} =  Ajouter le colombarium  ${colombarium_recherche}
    # Création des défunts sur les emplacements
    &{defunt_concession_recherche} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=TEST300RECHERCHECONCESSION
    Ajouter le defunt dans le contexte de la concession  ${defunt_concession_recherche}  ${id_concession_recherche}

    &{defunt_colombarium_recherche} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=TEST300RECHERCHECOLOMBARIUM
    Ajouter le defunt dans le contexte du colombarium  ${defunt_colombarium_recherche}  ${id_colombarium_recherche}
    # Vérification du bon contexte lors de la séléction du colombarium depuis la recherche globale
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  recherche  recherche
    Input Text  name=recherche  TEST300
    Select Checkbox  name=largeSearch
    Click Element  css=.boutonFormulaire
    Click Element  link:TEST300COLOMBARIUMRECHERCHE
    Page Title Should Be  Emplacements > Colombarium > ${id_colombarium_recherche} ${colombarium_recherche.famille}
    # Vérification du bon contexte lors de la sélection de la concession depuis la recherche globale
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  recherche  recherche
    Input Text  name=recherche  TEST300
    Select Checkbox  name=largeSearch
    Click Element  css=.boutonFormulaire
    Click Element  link:TEST300CONCESSIONRECHERCHE
    Page Title Should Be  Emplacements > Concession > ${id_concession_recherche} ${concession_recherche.famille}
    # Vérification du bon contexte lors de la sélection du défunt lié au colombarium depuis la recherche globale
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  recherche  recherche
    Input Text  name=recherche  TEST300
    Select Checkbox  name=largeSearch
    Click Element  css=.boutonFormulaire
    Click Element  link:TEST300RECHERCHECOLOMBARIUM
    Page Title Should Be  Emplacements > Colombarium > ${id_colombarium_recherche} ${colombarium_recherche.famille}
    # Vérification du bon contexte lors de la séléction du défunt lié à la concession depuis la recherche globale
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  recherche  recherche
    Input Text  name=recherche  TEST300
    Select Checkbox  name=largeSearch
    Click Element  css=.boutonFormulaire
    Click Element  link:TEST300RECHERCHECONCESSION
    Page Title Should Be  Emplacements > Concession > ${id_concession_recherche} ${concession_recherche.famille}

Vérification la recherche par numéro de concession avec complément
    [Documentation]  Ce test à pour but de vérifier qu'il est possible de faire une recherche sur le numéro de concession
    ...  avec le complément

    &{cimetiere02} =  Create Dictionary
    ...  cimetierelib=CIMETIERE-RECHERCHE-COMPLEMENT
    ...  adresse1=RUE UNIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere02}
    #
    &{zone01_cim02} =  Create Dictionary
    ...  cimetiere=${cimetiere02.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z-RECHERCHECOMPLEMENT
    Ajouter la zone  ${zone01_cim02}
    #
    &{voie01_zone01_cim02} =  Create Dictionary
    ...  zone=${zone01_cim02.zonetype} ${zone01_cim02.zonelib} (${zone01_cim02.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V-RECHERCHECOMPLEMENT
    Ajouter la voie  ${voie01_zone01_cim02}


    &{concession02} =  Create Dictionary
    ...  famille=DUPONTRECHERCHECOMPLEMENT
    ...  numero=99
    ...  complement=bis
    ...  cimetierelib=${cimetiere02.cimetierelib}
    ...  zonelib=${zone01_cim02.zonetype} ${zone01_cim02.zonelib}
    ...  voielib=${voie01_zone01_cim02.voietype} ${voie01_zone01_cim02.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  recherche  recherche
    Input Text  name=recherche  99 bis
    Select From List By Label  name=searchType  Numéro de concession
    Click Element  css=.boutonFormulaire

    Page Should Contain  Numéro de concession (1)