*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Set Suite Variable  ${testid}  110
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{zone02_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=ENCLOS
    ...  zonelib=Z${testid}-02
    Ajouter la zone  ${zone02_cim01}
    Set Suite Variable  ${zone02_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{voie02_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=V${testid}-02
    Ajouter la voie  ${voie02_zone01_cim01}
    Set Suite Variable  ${voie02_zone01_cim01}
    #
    &{voie03_zone02_cim01} =  Create Dictionary
    ...  zone=${zone02_cim01.zonetype} ${zone02_cim01.zonelib} (${zone02_cim01.cimetiere})
    ...  voietype=ILOT
    ...  voielib=v${testid}-03
    Ajouter la voie  ${voie03_zone02_cim01}
    Set Suite Variable  ${voie03_zone02_cim01}
    #
    &{voie04_zone02_cim01} =  Create Dictionary
    ...  zone=${zone02_cim01.zonetype} ${zone02_cim01.zonelib} (${zone02_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=v${testid}-04
    Ajouter la voie  ${voie04_zone02_cim01}
    Set Suite Variable  ${voie04_zone02_cim01}
    #
    &{cimetiere02} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-02
    ...  adresse1=RUE DE ROME
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere02}
    Set Suite Variable  ${cimetiere02}
    #
    &{type_de_sepulture_01} =  Create Dictionary
    ...  code=TDS-${testid}-01
    ...  libelle=Type de sépulture ${testid}-01
    ${type_de_sepulture_01.id} =  Ajouter le *type de sépulture*  ${type_de_sepulture_01}
    Set Suite Variable  ${type_de_sepulture_01}
    #
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  sepulturetype=${type_de_sepulture_01.libelle}
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01_id}
    Set Suite Variable  ${concession01}


Recherche du type de sépulture
    [Documentation]  Ce TestCase permet de vérifier le bon fonctionnement de la
    ...  recherche simple sur le champ *type de sépulture* dans le listing
    ...  des concessions.
    Depuis la page d'accueil  admin  admin
    Depuis le listing des concessions
    Use Simple Search  type de sépulture  plop
    Page Should Contain  Aucun enregistrement.
    Use Simple Search  type de sépulture  ${type_de_sepulture_01.libelle}
    Page Should Contain  ${concession01.famille}


Ajout d'une concession
    [Documentation]  Ce test permet simplement de vérifier l'ajout d'une
    ...  concession avec ses informations obligatoires.
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le formulaire d'ajout d'une concession
    Page Title Should Be  Emplacements > Concession
    Submenu In Menu Should Be Selected  emplacement  concession
    #
    Click On Submit Button
    Error Message Should Contain  Le champ famille est obligatoire
    Error Message Should Contain  Le champ voie est obligatoire
    Error Message Should Contain  Le champ numéro est obligatoire
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    #
    &{concession02} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    Saisir les valeurs dans le formulaire de l'emplacement  ${concession02}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.





Ajout d'un dossier sur une concession
    [Documentation]  Ce test permet de vérifier l'ajout d'un fichier sur un
    ...  emplacement.
    #
    Depuis la page d'accueil  admin  admin
    #
    &{dossier} =  Create Dictionary
    ...  fichier=p-concession-1.jpg
    ...  datedossier=04/06/2013
    ...  typedossier=photo
    ...  observation=Photo de l'emplacement
    Ajouter le dossier dans le contexte de la concession  ${dossier}  ${concession01_id}


Ajout d'un concessionnaire sur une concession
    [Documentation]  Ce test permet de vérifier l'ajout d'un concessionnaire
    ...  emplacement.
    #
    Depuis la page d'accueil  admin  admin
    #
    &{concessionnaire} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL
    ...  marital=
    ...  prenom=Henri
    ...  datenaissance=10/01/1980
    ...  adresse1=12 rue de la République
    ...  adresse2=
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  dcd=true
    ...  observation=
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire}  ${concession01_id}


Ajout d'un courrier sur une concession
    [Documentation]  Seules les lettres types actives doivent être proposées.
    #
    Depuis la page d'accueil  admin  admin
    #
    Ajouter la lettre-type depuis le menu  test110concessionscourrier  test110concessionscourrier actif  test110concessionscourrier  test110concessionscourrier  Requête COURRIER  true
    #
    Depuis l'onglet 'courrier' de la concession  ${concession01_id}
    Click On Add Button JS
    ${listeRecuperee} =    Get List Items  css=#lettretype
    List Should Contain Value    ${listeRecuperee}  test110concessionscourrier test110concessionscourrier actif
    #
    Modifier la lettre-type  test110concessionscourrier  test110concessionscourrier non actif  null  null  null  false
    #
    Depuis l'onglet 'courrier' de la concession  ${concession01_id}
    Click On Add Button JS
    ${listeRecuperee} =    Get List Items  css=#lettretype
    List Should Not Contain Value    ${listeRecuperee}  test110concessionscourrier test110concessionscourrier non actif

    # On vérifie que les lettres types avec plus de 40 caractères sont bien ajoutés
    Ajouter la lettre-type depuis le menu  test dun long libelle avec plein de caractere pour tester lajout  test dun long libelle avec plein de caractere pour tester lajout actif  test dun long libelle avec plein de caractere pour tester lajout  test dun long libelle avec plein de caractere pour tester lajout  Requête COURRIER  true

    Depuis l'onglet 'courrier' de la concession  ${concession01_id}
    Click On Add Button JS
    Select From List By Label  css=#lettretype  test dun long libelle avec plein de caractere pour test dun long libelle avec plein de caractere pour tester lajout actif

    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

Ajout d'un contrat sur une concession
    [Documentation]  Ce test permet de vérifier l'ajout d'un contrat
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis l'onglet 'contrat' de la concession  ${concession01_id}
    Click On Add Button JS
    &{contrat03} =  Create Dictionary
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=03/03/2020
    Saisir un contrat  ${contrat03}
    Click On Submit Button

    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${contrat01_id} =  Get Value  css=div.contrat-form #contrat
    #
    &{contrat01_valide} =  Create Dictionary
    ...  valide=true
    Modifier un contrat  ${contrat01_id}  ${contrat01_valide}

Vérification du rafraîchissement de l'onglet concession lors de l'ajout d'un contrat
    [Documentation]  Ce test permet de vérifier l'ajout d'un contrat
    #
    Depuis la page d'accueil  admin  admin
    #
     &{concession03} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession03_id} =  Ajouter la concession  ${concession03}

    &{contrat04} =  Create Dictionary
    ...  emplacement=${concession03_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=15
    ...  datevente=19/03/2020
    ...  valide=true
    ${contrat04_id} =  Ajouter un contrat depuis l'emplacement  ${contrat04}

    Click Element  css=#main
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#datevente  ${contrat04.datevente}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#terme  ${contrat04.terme}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#duree  15
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#dateterme  19/03/2035
    #

    &{contrat05} =  Create Dictionary
    ...  emplacement=${concession03_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Renouvellement
    ...  terme=temporaire
    ...  duree=20
    ...  datevente=25/03/2020
    ...  valide=true
    ${contrat05_id} =  Ajouter un contrat depuis l'emplacement  ${contrat05}

    Click Element  css=#main
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#datevente  ${contrat04.datevente}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#daterenouvellement  ${contrat05.datevente}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#terme  ${contrat05.terme}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#duree  20
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#dateterme  25/03/2040

Vue sommaire d'un emplacement
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=concession&action=12&idx=${concession01_id}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#form-container  ${concession01.famille}
    La page ne doit pas contenir d'erreur


Vérification du bon fonctionnement de l'overlay de calcul automatique du montant
    [Documentation]  Ce test permet de vérifier le bon fonctionnement de l'overlay
    #
    Depuis la page d'accueil  admin  admin
    #
    &{concession04} =  Create Dictionary
    ...  famille=DUPONT${testid}04
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession04_id} =  Ajouter la concession  ${concession04}

    Depuis l'onglet 'contrat' de la concession  ${concession04_id}
    Click On Add Button JS
    &{contrat06} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=2
    Saisir un contrat  ${contrat06}
    #
    Click Element  css=#contrat-link-montant
    Error Message Should Contain  Recherche de tarif impossible sans date de vente.
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close
    #
    &{contrat07} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=3
    ...  datevente=25/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat07}

    Click Element  css=#contrat-link-montant
    Error Message Should Contain  Aucun tarif ne correspond.
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close

    &{contrat08} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=30
    ...  datevente=25/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat08}

    Click Element  css=#contrat-link-montant
    Element Should Contain  css=.ui-widget #content  125.35 euro(s)
    Click Element  css=.ui-widget .linkjsclosewindow
    Click On Submit Button

    Depuis l'onglet 'contrat' de la concession  ${concession04_id}
    Click On Add Button JS

    &{contrat09} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Renouvellement
    ...  terme=temporaire
    ...  duree=30
    ...  datevente=30/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat09}

    Click Element  css=#contrat-link-montant
    Element Should Contain  css=.ui-widget #content  115.76 euro(s)
    Click Element  css=.ui-widget .linkjsclosewindow
    Click On Submit Button
    La page ne doit pas contenir d'erreur
    ${contrat09_id} =  Get Value  css=div.contrat-form #contrat

    Supprimer un contrat  ${contrat09_id}

    Depuis l'onglet 'contrat' de la concession  ${concession04_id}
    Click On Add Button JS

    &{contrat10} =  Create Dictionary
    ...  datedemande=25/03/2035
    ...  origine=Transformation
    ...  terme=temporaire
    ...  duree=15
    ...  datevente=30/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat10}

    Click Element  css=#contrat-link-montant
    Element Should Contain  css=.ui-widget #content  Détail de la transformation
    Element Should Contain  css=.ui-widget #info_contrat  Contrat transformé : achat du 25/03/2020
    Element Should Contain  css=.ui-widget #ancienne_date  25/03/2020 (année 2020 / mois 3)
    Element Should Contain  css=.ui-widget #new_date  25/03/2035 (année 2035 / mois 3)
    Element Should Contain  css=.ui-widget #duree_initiale  360 mois (30 ans)
    Element Should Contain  css=.ui-widget #mois_ecoule  180 mois
    Element Should Contain  css=.ui-widget #new_duree  180 mois (15 ans)
    Element Should Contain  css=.ui-widget #tarif_initial  125.35 euro(s)
    Element Should Contain  css=.ui-widget #new_tarif  85.36 euro(s)
    Element Should Contain  css=.ui-widget #already_paid  62.68 euro(s)
    Element Should Contain  css=.ui-widget #new_montant  22.68 euro(s)

    Click Element  css=.ui-widget .linkjsclosewindow
    Click On Submit Button


Vérification du bon fonctionnement des champs de fusion d'emplacement
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des lettres-types
    # On désactive la lettre type
    Click Element  link:titre_de_recettes
    ${lettretype_id} =  Get Text  css=#id
    Modifier la lettre-type  ${lettretype_id}  null  null  null  null  false  null  null
    # On copie la lettre type
    Click On Form Portlet Action  om_lettretype  copier  modale
    Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Click On Back Button
    # On accède à la copie de la letttre type et on saisit les champs de fusion à tester
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  modifier
    ${champs_fusions_emplacement} =  Set Variable  [emplacement.emplacement] [emplacement.nature] [emplacement.numero] [emplacement.complement] [emplacement.voie] [emplacement.famille] [emplacement.typeconcession] [emplacement.terme] [emplacement.duree] [emplacement.datevente] [emplacement.dateterme] [emplacement.duree_lettre] [emplacement.sepulturetype]
    Saisir la lettre-type  null  null  null  ${champs_fusions_emplacement}  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    &{concession05} =  Create Dictionary
    ...  famille=DUPONT${testid}04
    ...  numero=14
    ...  complement=A
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  nature_emplacement=concession
    ...  typeconcession=Familiale
    ...  terme=temporaire
    ...  duree=20
    ...  datevente=20/03/2020
    ...  emplacement_date_terme=20/03/2040
    ...  duree_lettre=vingt
    ...  sepulturetype=${type_de_sepulture_01.libelle}
    ${concession05_id} =  Ajouter la concession  ${concession05}
    Set Suite Variable  ${concession05_id}

    &{concessionnaire_1} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 rue de la République
    ...  cp=99678
    ...  ville=LIBREVILLE
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire_1}  ${concession05_id}

    &{concessionnaire_2} =  Create Dictionary
    ...  titre=Madame
    ...  nom=MICHEL
    ...  prenom=JO
    ...  datenaissance=11/01/1980
    ...  adresse1=12 rue de la République
    ...  cp=99678
    ...  ville=LIBREVILLE
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire_2}  ${concession05_id}

    &{concessionnaire_3} =  Create Dictionary
    ...  nom=SANSTITRE
    ...  prenom=TO
    ...  datenaissance=11/01/1980
    ...  adresse1=12 rue de la République
    ...  cp=99678
    ...  ville=LIBREVILLE
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire_3}  ${concession05_id}

    # On ajoute un courrier
    &{courrier_edition} =  Create Dictionary
    ...  datecourrier=04/07/2019
    ...  lettretype=titre_de_recettes copie du ${DATE_FORMAT_DD/MM/YYYY}
    Ajouter le courrier dans le contexte de la concession  ${courrier_edition}  ${concession05_id}
    # On vérifie que les champs de fusion fonctionnent
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    ${contenu_pdf} =  Create List  ${concession05_id} ${concession05.nature_emplacement} ${concession05.numero} ${concession05.complement} ${concession05.voielib} [${concession05.zonelib}][${concession05.cimetierelib}] ${concession05.famille}  ${concession05.typeconcession} ${concession05.terme} ${concession05.duree} ${concession05.datevente} ${concession05.emplacement_date_terme} ${concession05.duree_lettre} ${concession05.sepulturetype}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # On modifie le type de sépulture pour remplir le champ libelle_edition
    &{sepulturetype_args} =  Create Dictionary
    ...  libelle_edition=Caveau
    Modifier le *type de sépulture*  ${type_de_sepulture_01.id}  ${sepulturetype_args}
    Depuis l'onglet 'courrier' de la concession  ${concession05_id}
    Click Element  link:${courrier_edition.datecourrier}
    # On vérifie que les champs de fusion fonctionnent
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    # On remplace le libelle du type de sepulture de l'emplacement par le contenu du champ libelle_edition
    ${contenu_pdf} =  Create List  ${concession05_id} ${concession05.nature_emplacement} ${concession05.numero} ${concession05.complement} ${concession05.voielib} [${concession05.zonelib}][${concession05.cimetierelib}] ${concession05.famille}  ${concession05.typeconcession} ${concession05.terme} ${concession05.duree} ${concession05.datevente} ${concession05.emplacement_date_terme} ${concession05.duree_lettre} ${sepulturetype_args.libelle_edition}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    Depuis le listing des lettres-types
    # On accède à la copie de la letttre type et on saisit les champs de fusion à tester
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  modifier
    ${champs_fusions_emplacement} =  Set Variable  [emplacement.concessionnaires_avec_civilites]
    Saisir la lettre-type  null  null  null  ${champs_fusions_emplacement}  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    Depuis l'onglet 'courrier' de la concession  ${concession05_id}
    Click Element  link:${courrier_edition.datecourrier}
    # On vérifie que les champs de fusion fonctionnent
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    ${contenu_pdf} =  Create List  ${concessionnaire_1.prenom} ${concessionnaire_1.nom}, ${concessionnaire_2.titre} ${concessionnaire_2.prenom} ${concessionnaire_2.nom}, ${concessionnaire_3.prenom} ${concessionnaire_3.nom}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Vérification du bon fonctionnement du champ de fusion [emplacement.defunts]
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des lettres-types
    # On modifie la copie de titre de recettes
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  modifier
    # On saisit les champs de fusion de defunt
    ${champs_fusions_defunt} =  Set Variable  [emplacement.defunts]
    Saisir la lettre-type  null  null  null  ${champs_fusions_defunt}  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    #
    &{defunt01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=SUPONT${testid}MERGEFIELDS
    ...  prenom=MARCELMERGEFIELDS
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt01}  ${concession05_id}
    #
    &{defunt02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=LOLA${testid}MERGEFIELDS
    ...  prenom=LOLAMERGEFIELDS
    ...  datenaissance=01/04/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt02}  ${concession05_id}
    # On vérifie que les champs de fusion fonctionnent
    Depuis l'onglet 'courrier' de la concession  ${concession05_id}
    Click Element  link:titre_de_recettes
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    ${contenu_pdf} =  Create List  ${defunt02.prenom}  ${defunt02.nom}  décédé(e) le 25 mai 2020  à ${defunt02.lieudeces}  ${defunt01.prenom}  ${defunt01.nom}  décédé(e) le 25 mai 2017  à ${defunt01.lieudeces}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Vérification du bon fonctionnement des champs de fusion d'autorisation
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    Depuis le listing des lettres-types
    # On modifie la copie de titre de recettes
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  modifier
    # On saisit les champs de fusion d'autorisation
    ${champs_fusions_autorisation} =  Set Variable  [autorisation.nature] [autorisation.titre] [autorisation.nom] [autorisation.prenom] [autorisation.datenaissance] [autorisation.adresse1] [autorisation.cp] [autorisation.ville]
    Saisir la lettre-type  null  null  null  ${champs_fusions_autorisation}  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    #
    &{concessionnaire01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MICHEL
    ...  prenom=HENRI
    ...  datenaissance=10/01/1980
    ...  adresse1=12 RUE DE LA RÉPUBLIQUE
    ...  cp=99678
    ...  ville=LIBREVILLE
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire01}  ${concession05_id}
    # On vérifie que les champs de fusion fonctionnent
    Depuis l'onglet 'courrier' de la concession  ${concession05_id}
    Click Element  link:titre_de_recettes
    Click On SubForm Portlet Action  courrier  modifier
    &{courrier_edition} =  Create Dictionary
    ...  destinataire=(concessionnaire) MICHEL HENRI
    Saisir les valeurs dans le formulaire du courrier  ${courrier_edition}
    Click On Submit Button In Subform
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    ${contenu_pdf} =  Create List  concessionnaire  ${concessionnaire01.titre}  ${concessionnaire01.nom}  ${concessionnaire01.prenom}  ${concessionnaire01.datenaissance}  ${concessionnaire01.adresse1}  ${concessionnaire01.cp}  ${concessionnaire01.ville}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Vérification du bon fonctionnement des champs de fusion de courrier
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    Depuis le listing des lettres-types
    # On modifie la copie de titre de recettes
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  modifier
    # On saisit les champs de fusion de courrier
    ${champs_fusions_courrier} =  Set Variable  [courrier.datecourrier] [courrier.lettretype] [courrier.contrat] [courrier.datecourrier_format_jj_mois_aaaa]
    Saisir la lettre-type  null  null  null  ${champs_fusions_courrier}  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On vérifie que les champs de fusion fonctionnent
    Depuis l'onglet 'courrier' de la concession  ${concession05_id}
    Click Element  link:titre_de_recettes
    Click On SubForm Portlet Action  courrier  pdf-edition  new_window
    ${contenu_pdf} =  Create List  04/07/2019  titre_de_recettes  04 juillet 2019
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    # On supprime la copie du titre_de_recettes et on réactive le courrier titre_de_recettes pour revenir à un état cohérent
    Depuis le listing des lettres-types
    #
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_lettretype  supprimer
    Click On Submit Button
    Click Element  link:titre_de_recettes
    Click On Form Portlet Action  om_lettretype  modifier
    Saisir la lettre-type  null  null  null  null  null  true  null  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Verification du bon fonctionnement des champs de fusion de défunt
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # On désactive l'état défunt
    Depuis le listing des états
    Use Simple Search  id  defunt
    Click Element  link:defunt
    Click On Form Portlet Action  om_etat  modifier
    Saisir l'état  null  null  null  null  null  false  null
    Click On Submit Button
    # On copie l'état
    Click On Form Portlet Action  om_etat  copier  modale
    Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Click On Back Button
    # On accède à la copie de l'état et on saisit les champs de fusion à tester
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_etat  modifier
    ${champs_fusions_defunt} =  Set Variable  [defunt.titre] [defunt.nom] [defunt.prenom] [defunt.datenaissance] [defunt.lieunaissance] [defunt.datedeces] [defunt.lieudeces] [defunt.parente] [defunt.nature]
    Saisir l'état  null  null  null  ${champs_fusions_defunt}  null  true  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ajoute un défunt
    &{defunt03} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=LOLA${testid}MERGEFIELDS
    ...  prenom=LOLAMERGEFIELDS
    ...  datenaissance=01/04/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt03}  ${concession05_id}

    Depuis l'onglet 'défunt' de la concession  ${concession05_id}
    Click Element  css=#action-soustab-defunt-left-etat-defunt-3
    ${contenu_pdf} =  Create List  ${defunt03.titre}  ${defunt03.nom}  ${defunt03.prenom}  ${defunt03.datenaissance}  ${defunt03.lieunaissance}  ${defunt03.datedeces}  ${defunt03.lieudeces}  ${defunt03.parente}  ${defunt03.nature}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # On supprime la copie du titre_de_recettes et on réactive le courrier titre_de_recettes pour revenir à un état cohérent
    Depuis le listing des états
    Use Simple Search  id  defunt
    #
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_etat  supprimer
    Click On Submit Button
    Click Element  link:defunt
    Click On Form Portlet Action  om_etat  modifier
    Saisir l'état  null  null  null  null  null  true  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Vérification du bon fonctionnement du plan en coupe (paramétrage et déplacement des défunts)
    [Documentation]  Permet de vérifier que l'accès au plan en coupe est fonctionnel
    ...  Ajout d'un type de sépulture au format 2 colonnes * 2 lignes.
    ...  Ajout d'un emplacement avec le type de sépulture contenant le nouveau format
    ...  Ajout de défunts dans l'emplacement.
    ...  Vérification de l'accès au plan en coupe.
    ...  Déplacement d'un défunt.
    ...  Vérification de l'ajout et de la suppression du vide sanitaire sur l'emplacement.
    ...  Vérification du message d'erreur lors de la suppression de colonnes et de lignes
    ...  où un défunt est positionné.

    Depuis la page d'accueil  admin  admin

    # Ajout de l'emplacement
     &{concession_pec} =  Create Dictionary
    ...  famille=DUPONT${testid}pec
    ...  numero=15
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concessionpec.id} =  Ajouter la concession  ${concession_pec}

    Set Suite Variable  ${concession_pec}

    # On vérifie que l'action du plan en coupe n'est pas présente sur la fiche de l'emplacement
    Depuis le contexte de la concession  ${concessionpec.id}
    Portlet Action Should Not Be In Form  concession  plan-en-coupe

    # Ajout du type de sépulture comportant 2 colonnes et 2 lignes pour le plan en coupe
    &{type_de_sepulture_pec} =  Create Dictionary
    ...  code=TDS-${testid}-pec
    ...  libelle=Type de sépulture 2*2
    ...  colonne=2
    ...  ligne=2
    ${type_de_sepulture_pec.id} =  Ajouter le *type de sépulture*  ${type_de_sepulture_pec}

    &{type_de_sepulture_pec2} =  Create Dictionary
    ...  code=TDS-${testid}-pec2
    ...  libelle=Type de sépulture 2*2 Mieux
    ...  colonne=2
    ...  ligne=2
    ${type_de_sepulture_pec2.id} =  Ajouter le *type de sépulture*  ${type_de_sepulture_pec2}

    Set Suite Variable  ${type_de_sepulture_pec}

    # On modifie l'emplacement pour ajouter le type de sépulture contenant les dimensions
    &{concession_pec_modif} =  Create Dictionary
    ...  sepulturetype=${type_de_sepulture_pec.libelle}
    Modifier la concession  ${concessionpec.id}  ${concession_pec_modif}

    # On vérifie que l'action du plan en coupe est présente sur la fiche de l'emplacement
    Depuis le contexte de la concession  ${concessionpec.id}
    Portlet Action Should Be In Form  concession  plan-en-coupe

    # On accède au plan en coupe et on vérifie qu'il n'y a pas d'erreur
    Click On Form Portlet Action  concession  plan-en-coupe
    La page ne doit pas contenir d'erreur

    # On ajoute deux défunts dans l'emplacement
    &{defunt_pec_01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=LOLA${testid}PEC
    ...  prenom=LOLPEC
    ...  datenaissance=01/04/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt_pec_01}  ${concessionpec.id}
    ${id_defunt_pec_01} =  Get Value  css=div.defunt-form #defunt

    &{defunt_pec_02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=LOLO${testid}PEC
    ...  prenom=LOLOPEC
    ...  datenaissance=01/08/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/12/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt_pec_02}  ${concessionpec.id}
    ${id_defunt_pec_02} =  Get Value  css=div.defunt-form #defunt

    # On accède au plan en coupe et on vérifie que les défunts sont positionné dans la colonne de gauche
    # en dehors de la grille
    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    # Sleep  200000000
    # Les cartes des défunts sont identifiées avec id="widget_<id_defunt>"
    # La colonne listant les défunts non positionnés a pour identifiant id="column"
    Element Should Contain  css=#column  ${id_defunt_pec_01} - ${defunt_pec_01.nom} ${defunt_pec_01.prenom}
    Element Should Contain  css=#column  ${id_defunt_pec_02} - ${defunt_pec_02.nom} ${defunt_pec_02.prenom}

    # Déplacement du deuxième défunt sur la case 1;2 dans le plan en coupe en utilisant l'action 11 de défunt
    Go To  ${PROJECT_URL}app/index.php?module=form&obj=defunt&action=11&idx=${id_defunt_pec_02}&x=1&y=2&validation=1

    # Vérification du positionnement des défunts
    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    Element Should Contain  css=#column  ${id_defunt_pec_01} - ${defunt_pec_01.nom} ${defunt_pec_01.prenom}
    Element Should Contain  css=.column_1_2  ${id_defunt_pec_02} - ${defunt_pec_02.nom} ${defunt_pec_02.prenom}

    # On modifie l'emplacement pour ajouter le vide sanitaire
    &{concession_pec_modif} =  Create Dictionary
    ...  videsanitaire=Oui
    Modifier la concession  ${concessionpec.id}  ${concession_pec_modif}

    # Le défunt positionné doit être déplacé d'une ligne
    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    Element Should Contain  css=#column  ${id_defunt_pec_01} - ${defunt_pec_01.nom} ${defunt_pec_01.prenom}
    Element Should Contain  css=.column_1_3  ${id_defunt_pec_02} - ${defunt_pec_02.nom} ${defunt_pec_02.prenom}

    # Déplacement du deuxième défunt dans le vide sanitaire case 1;1
    Go To  ${PROJECT_URL}app/index.php?module=form&obj=defunt&action=11&idx=${id_defunt_pec_02}&x=1&y=1&validation=1

    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  modifier
    Select From List By Label  css=#videsanitaire  Non
    Click On Submit Button
    Error Message Should Contain  Le vide sanitaire ne peut pas être supprimé car des défunts sont placés à l'intérieur.

    # Déplacement du deuxième défunt en dehors du vide sanitaire case 2;2
    Go To  ${PROJECT_URL}app/index.php?module=form&obj=defunt&action=11&idx=${id_defunt_pec_02}&x=2&y=2&validation=1

    # On modifie l'emplacement pour enlever le vide sanitaire
    &{concession_pec_modif} =  Create Dictionary
    ...  videsanitaire=Non
    Modifier la concession  ${concessionpec.id}  ${concession_pec_modif}

    # On modifie l'emplacement pour modifier le type de sépulture contenant les mêmes dimensions que le type précédent
    # La modification doit s'effectuer correctement
    &{concession_pec_modif} =  Create Dictionary
    ...  sepulturetype=${type_de_sepulture_pec2.libelle}
    Modifier la concession  ${concessionpec.id}  ${concession_pec_modif}

    &{concession_pec_modif} =  Create Dictionary
    ...  sepulturetype=${type_de_sepulture_pec.libelle}
    Modifier la concession  ${concessionpec.id}  ${concession_pec_modif}

    # Vérification du message d'erreur lors de la modification des colonnes et lignes du type de sépulture
    Depuis le contexte du *type de sépulture*  ${type_de_sepulture_pec.id}

    # Suppression d'une colonne -> KO
    Click On Form Portlet Action  sepulture_type  modifier
    Input Text  css=#colonne  1
    Click On Submit Button
    Error Message Should Contain  Le type de sépulture ne peut pas être modifié car des défunts sont placés dans les emplacements.

    # Ajout de deux colonnes -> OK
    Input Text  css=#colonne  3
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Suppression d'une ligne -> KO
    Click On Form Portlet Action  sepulture_type  modifier
    Input Text  css=#ligne  0
    Click On Submit Button
    Error Message Should Contain  Le type de sépulture ne peut pas être modifié car des défunts sont placés dans les emplacements.

    # Ajout de deux lignes -> OK
    Input Text  css=#ligne  3
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Suppression de la permission permettant de déplacer les défunts
    Depuis le listing  om_droit
    Use Simple Search  libellé  positionner
    Click On Link  defunt_positionner
    Click On Form Portlet Action  om_droit  supprimer
    Click On Submit Button

    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    ${class} =  Get Element Attribute  css=.column_1_1  class
    Should Contain  ${class}  column_locked


Vérification du bon fonctionnement du plan en coupe (verrouillage de cases et gestion des permissions)
    [Documentation]  Test case dépendant du précédent (on utilise le même type de sépulture)
    ...  Vérification du verrouillage des cases.
    ...  Vérification de la bonnes gestion des permissions

    Depuis la page d'accueil  admin  admin

    Depuis le contexte de la concession  ${concessionpec.id}

    Click On Form Portlet Action  concession  plan-en-coupe
    Element Should Be visible  css=#but_case_1_1
    ${class} =  Get Element Attribute  css=.column_1_1  class
    Should Contain  ${class}  cellUnlocked

    # Verrouillage de la case
    Click Button  css=#but_case_1_1

    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    Element Should Be visible  css=#but_case_1_1
    ${class} =  Get Element Attribute  css=.column_1_1  class
    Should Contain  ${class}  cellLocked

    Element Should Contain  css=.column_1_1 span  Emplacement verrouillé

    # Suppression de la permission permettant le verrouillage des cases
    Depuis le listing  om_droit
    Use Simple Search  libellé  verrouiller
    Click On Link  emplacement_verrouiller
    Click On Form Portlet Action  om_droit  supprimer
    Click On Submit Button

    Depuis le contexte de la concession  ${concessionpec.id}
    Click On Form Portlet Action  concession  plan-en-coupe
    Element Should Not Be visible  css=#but_case_1_1
