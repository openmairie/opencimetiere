*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Le 'Framework' de l'application permet de générer
...  automatiquement certains scripts en fonction du modèle de données. Lors
...  du développement la règle est la suivante : toute modification du
...  modèle de données doit entrainer une regénération complète de tous les
...  scripts. Pour vérifier à chaque modification du code que la règle a bien
...  été respectée, ce 'Test Suite' permet de lancer une génération complète.
...  Si un fichier est généré alors le test doit échoué.


*** Test Cases ***
Génération complète

    Depuis la page d'accueil    admin    admin
    Générer tout

