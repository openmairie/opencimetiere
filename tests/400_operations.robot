*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Set Suite Variable  ${testid}  400
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{voie02_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=DIVISION
    ...  voielib=V${testid}-02
    Ajouter la voie  ${voie02_zone01_cim01}
    Set Suite Variable  ${voie02_zone01_cim01}
    #
    &{concession03} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=21
    ...  voie_autocomplete_search=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib} ${zone01_cim01.zonetype} ${zone01_cim01.zonelib} ${cimetiere01.cimetierelib}
    ...  voie_autocomplete=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib} [${zone01_cim01.zonetype} ${zone01_cim01.zonelib}][${cimetiere01.cimetierelib}]
    ${concession03_id} =  Ajouter la concession  ${concession03}
    Set Suite Variable  ${concession03}
    Set Suite Variable  ${concession03_id}
    #
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01}
    Set Suite Variable  ${concession01_id}
    #
    &{concession02} =  Create Dictionary
    ...  famille=DUPONT${testid}
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}
    Set Suite Variable  ${concession02}
    Set Suite Variable  ${concession02_id}
    #
    &{entreprise01} =  Create Dictionary
    ...  nomentreprise=ENTREPRISE${testid}-01
    ...  pf=Non
    ...  adresse1=12 rue de la République
    ...  adresse2=Parc de la Durance
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  telephone=00999999999
    Ajouter l'entreprise  ${entreprise01}
    Set Suite Variable  ${entreprise01}
    #
    &{entreprise02} =  Create Dictionary
    ...  nomentreprise=ENTREPRISE${testid}-02
    ...  pf=Oui
    ...  adresse1=14 rue de la République
    ...  adresse2=Parc de la Durance
    ...  cp=99678
    ...  ville=LIBREVILLE
    ...  telephone=00888888888
    Ajouter l'entreprise  ${entreprise02}
    Set Suite Variable  ${entreprise02}


Ajout d'une opération d'inhumation concession
    [Documentation]  Ce test permet de créer une opération et de vérifier la
    ...  génération de l'édition liée à cette opération.
    #
    Depuis la page d'accueil  admin  admin
    #
    &{operation01} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=Marcel
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${operation01_id} =  Ajouter l'opération d'inhumation sur concession  ${operation01}
    #
    Ouvrir l'édition PDF de l'opération d'inhumation sur concession  ${operation01_id}
    PDF Page Number Should Contain  1  AUTORISATION D'INHUMATION
    PDF Page Number Should Contain  2  CONVOCATION DE POLICE
    Close PDF
    #
    Valider l'opération d'inhumation sur concession  ${operation01_id}
    #
    Depuis l'onglet 'opérations' de la concession  ${concession01_id}
    Element Should Contain  css=#sousform-operation_trt  inhumation
    Element Should Contain  css=#sousform-operation_trt  ${operation01.defunt_nom}
    #
    Depuis l'onglet 'défunt' de la concession  ${concession01_id}
    Element Should Contain  css=#sousform-defunt  ${operation01.defunt_nom}

    # À utiliser dans le test case vérifiant le fonctionnement du transfert
    Set Suite Variable  ${operation01.defunt_nom}
    Set Suite Variable  ${operation01.defunt_prenom}


Ajout d'une opération de transfert
    [Documentation]  Ce test permet de créer une opération et de vérifier la
    ...  génération de l'édition liée à cette opération.
    #
    Depuis la page d'accueil  admin  admin
    #
    &{operation02} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  emplacement_transfert_autocomplete_search=(${concession02_id})
    ...  emplacement_transfert_autocomplete_id=${concession02_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ${operation02_id} =  Ajouter l'opération de transfert  ${operation02}
    #
    Ouvrir l'édition PDF de l'opération de transfert  ${operation02_id}
    PDF Page Number Should Contain  1  AUTORISATION DE TRANSFERT
    PDF Page Number Should Contain  2  CONVOCATION DE POLICE
    Close PDF
    # On ajoute l'opération défunt
    ${nom_prenom_defunt_to_transfert} =  Convert To Upper Case  ${operation01.defunt_nom} ${operation01.defunt_prenom}
    Ajouter un défunt sur l'opération de transfert  ${operation02_id}  ${nom_prenom_defunt_to_transfert}
    #
    Valider l'opération de transfert  ${operation02_id}
    #
    Depuis l'onglet 'opérations' de la concession  ${concession01_id}
    Element Should Contain  css=#sousform-operation_trt  transfert
    Depuis l'onglet 'défunt' de la concession  ${concession02_id}
    Element Should Contain  css=#sousform-defunt  ${operation01.defunt_nom}



Calcul de la place
    [Documentation]  ...
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Dashboard
    Depuis l'écran de traitement 'Calcul de la place occupée'
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    Alert Should Be Present  Etes-vous sur de vouloir confirmer cette action ?
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Submenu In Menu Should Be Selected  operation  concessionplace
    Page Title Should Be  Opérations > Calcul De La Place
    First Tab Title Should Be  Emplacement
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  calcul de la place occupée
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Télécharger le fichier traces


Gestion des champs de défunt lors d'une inhumation
    [Documentation]  Ce test permet de vérifier que tous les champs sont bien
    ...  pris en compte lors d'une validation d'opération
    #
    Depuis la page d'accueil  admin  admin
    #
    # On ajoute une concession
    &{concession03} =  Create Dictionary
    ...  famille=DUPONT${testid}MANAGEFIELDS
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession03_id} =  Ajouter la concession  ${concession03}

    Depuis l'onglet 'défunt' de la concession  ${concession03_id}
    Click On Add Button

    &{defunt01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=SUPONT${testid}MANAGEFIELDS
    ...  prenom=MARCELMANAGEFIELDS
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Saisir les valeurs dans le formulaire du defunt  ${defunt01}

    Click On Submit Button

    # On vérifie que tous les champs sont bien remplis
    Element Should Contain  css=#titre  ${defunt01.titre}
    Element Should Contain  css=#nom  ${defunt01.nom}
    Element Should Contain  css=#prenom  ${defunt01.prenom}
    Element Should Contain  css=#datenaissance  ${defunt01.datenaissance}
    Element Should Contain  css=#lieunaissance  ${defunt01.lieunaissance}
    Element Should Contain  css=#datedeces  ${defunt01.datedeces}
    Element Should Contain  css=#lieudeces  ${defunt01.lieudeces}
    Element Should Contain  css=#parente  ${defunt01.parente}

    Depuis le formulaire d'ajout d'une opération d'inhumation sur concession
    &{operation03} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=MARCEL
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_lieunaissance=ARLES
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_parente=LOLO
    ...  defunt_nature=cercueil
    Saisir les valeurs dans le formulaire de l'opération  ${operation03}

    Click On Submit Button

    # On vérifie que tous les champs sont bien remplis
    Element Should Contain  css=#defunt_titre  ${operation03.defunt_titre}
    Form Value Should Be  css=#defunt_nom  ${operation03.defunt_nom}
    Form Value Should Be  css=#defunt_prenom  ${operation03.defunt_prenom}
    Form Value Should Be  css=#defunt_datenaissance  ${operation03.defunt_datenaissance}
    Form Value Should Be  css=#defunt_lieunaissance  ${operation03.defunt_lieunaissance}
    Form Value Should Be  css=#defunt_datedeces  ${operation03.defunt_datedeces}
    Form Value Should Be  css=#defunt_lieudeces  ${operation03.defunt_lieudeces}
    Form Value Should Be  css=#defunt_parente  ${operation03.defunt_parente}

    ${operation03_id} =  Get Text  css=#form-content #operation

    Click On Form Portlet Action  inhumation_concession  modifier

    &{operation03_edit} =  Create Dictionary
    ...  defunt_lieunaissance=HERVILLE
    ...  defunt_parente=MOMO

    Saisir les valeurs dans le formulaire de l'opération  ${operation03_edit}
    Click On Submit Button

    # On vérifie que tous les champs sont bien remplis
    Element Should Contain  css=#defunt_titre  ${operation03.defunt_titre}
    Form Value Should Be  css=#defunt_nom  ${operation03.defunt_nom}
    Form Value Should Be  css=#defunt_prenom  ${operation03.defunt_prenom}
    Form Value Should Be  css=#defunt_datenaissance  ${operation03.defunt_datenaissance}
    Form Value Should Be  css=#defunt_lieunaissance  ${operation03_edit.defunt_lieunaissance}
    Form Value Should Be  css=#defunt_datedeces  ${operation03.defunt_datedeces}
    Form Value Should Be  css=#defunt_lieudeces  ${operation03.defunt_lieudeces}
    Form Value Should Be  css=#defunt_parente  ${operation03_edit.defunt_parente}
    #
    Valider l'opération d'inhumation sur concession  ${operation03_id}
    #
    Depuis l'onglet 'opérations' de la concession  ${concession03_id}
    Element Should Contain  css=#sousform-operation_trt  inhumation
    Element Should Contain  css=#sousform-operation_trt  ${operation03.defunt_nom}
    #
    Depuis l'onglet 'défunt' de la concession  ${concession03_id}
    Element Should Contain  css=#sousform-defunt  ${operation03.defunt_nom}
    Click Element  link:${operation03.defunt_nom}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#lieunaissance  ${operation03_edit.defunt_lieunaissance}
    Element Should Contain  css=#parente  ${operation03_edit.defunt_parente}


Champ 'numdossier' automatique en fonction de l'année
    [Documentation]  Le champ 'numdossier' de l'opération est composé de
    ...  l'année et d'un numéro de registre annuel stockée dans une séquence
    ...  par année. Exemple : 2010-123 représente la 123ème opération de
    ...  l'année 2010.
    ...
    ...  Ce *Test Case* vérifie que les séquences annuelles de ce champ
    ...  fonctionnent correctement. Le scénario est le suivant :
    ...  - on ajoute une opération avec une date positionnée dans un an et on vérifie que le champ 'numdossier' est composé correctement [ANNEE N+1]-1.
    ...  - on ajoute une opération avec une date positionnée dans deux ans et on vérifie que le champ 'numdossier' est composé correctement [ANNEE N+2]-1.
    ...  - on ajoute une opération avec une date positionnée dans un an et on vérifie que le champ 'numdossier' est composé correctement [ANNEE N+1]-2.
    ...
    ...  Attention : si des opérations sont créées pour l'année N+1 ou N+2 dans
    ...  les tests précédents, ce 'Test Case' va échouer.

    Depuis la page d'accueil  admin  admin
    # Année N+1
    ${date_jour_add_db_one_year} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  367 days  result_format=%Y-%m-%d
    ${date_jour_add_form_one_year} =  Convert Date  ${date_jour_add_db_one_year}  result_format=%d/%m/%Y
    ${date_time_one_year} =  Convert Date  ${date_jour_add_db_one_year}  result_format=datetime
    # Année N+2
    ${date_jour_add_db_two_year} =  Add Time To Date  ${DATE_FORMAT_YYYY-MM-DD}  734 days  result_format=%Y-%m-%d
    ${date_jour_add_form_two_year} =  Convert Date  ${date_jour_add_db_two_year}  result_format=%d/%m/%Y
    ${date_time_two_year} =  Convert Date  ${date_jour_add_db_two_year}  result_format=datetime
    # On attend [ANNEE N+1]-1
    &{op_l_01} =  Create Dictionary
    ...  date=${date_jour_add_form_one_year}
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  emplacement_transfert_autocomplete_search=(${concession02_id})
    ...  emplacement_transfert_autocomplete_id=${concession02_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ${op_l_01.id} =  Ajouter l'opération de transfert  ${op_l_01}
    Depuis le contexte de l'opération de transfert  ${op_l_01.id}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Contain  css=#numdossier  ${date_time_one_year.year}-1
    # On attend [ANNEE N+2]-1
    &{op_l_02} =  Create Dictionary
    ...  date=${date_jour_add_form_two_year}
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=Marcel
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${op_l_02.id} =  Ajouter l'opération d'inhumation sur concession  ${op_l_02}
    Depuis le contexte de l'opération d'inhumation sur concession  ${op_l_02.id}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Contain  css=#numdossier  ${date_time_two_year.year}-1
    # On attend [ANNEE N+1]-2
    &{op_l_03} =  Create Dictionary
    ...  date=${date_jour_add_form_one_year}
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}
    ...  defunt_prenom=Marcel
    ...  defunt_marital=
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${op_l_03.id} =  Ajouter l'opération d'inhumation sur concession  ${op_l_03}
    Depuis le contexte de l'opération d'inhumation sur concession  ${op_l_03.id}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}
    ...  Element Should Contain  css=#numdossier  ${date_time_one_year.year}-2


Verification du bon fonctionnement des champs de fusion d'opération
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # On désactive l'état défunt
    Depuis le listing des états
    Use Simple Search  id  operation_10_inhumation
    Click Element  link:operation_10_inhumation
    Click On Form Portlet Action  om_etat  modifier
    Saisir l'état  null  null  null  null  null  false  null
    Click On Submit Button
    # On copie l'état
    Click On Form Portlet Action  om_etat  copier  modale
    Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Click On Back Button
    # On accède à la copie de l'état et on saisit les champs de fusion à tester
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_etat  modifier
    ${champs_fusions_operation} =  Set Variable  [operation.date] [operation.heure] [operation.defunt_titre] [operation.defunt_nom] [operation.defunt_marital] [operation.defunt_prenom] [operation.defunt_datenaissance] [operation.defunt_lieunaissance] [operation.defunt_datedeces] [operation.defunt_lieudeces] [operation.defunt_parente] [operation.defunt_nature]
    Saisir l'état  null  null  null  ${champs_fusions_operation}  null  true  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ajoute un défunt
    &{operation07} =  Create Dictionary
    ...  date=${DATE_FORMAT_DD/MM/YYYY}
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}MERGEFIELDS
    ...  defunt_prenom=MARCELMERGEFIELDS
    ...  defunt_marital=LOLO
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_lieunaissance=ARLES
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_parente=LILI
    ...  defunt_nature=cercueil
    ${operation07_id} =  Ajouter l'opération d'inhumation sur concession  ${operation07}

    Depuis le listing des opérations d'inhumation sur concession
    Use Simple Search  id  ${operation07_id}
    Click Element  css=#action-tab-inhumation_concession-left-edition-operation-${operation07_id}
    Unselect Checkbox  //input[contains(@value, 'operation_20_convocation_police_inhumation')]
    Unselect Checkbox  //input[contains(@value, 'operation_30_travaux_inhumation')]
    Unselect Checkbox  //input[contains(@value, 'operation_40_compte_rendu_inhumation')]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[type="submit"]
    ${contenu_pdf} =  Create List  ${operation07.date}  ${operation07.heure}  ${operation07.defunt_titre}  ${operation07.defunt_nom}  ${operation07.defunt_marital}  ${operation07.defunt_prenom}  ${operation07.defunt_datenaissance}  ${operation07.defunt_lieunaissance}  ${operation07.defunt_datedeces}  ${operation07.defunt_lieudeces}  ${operation07.defunt_parente}  ${operation07.defunt_nature}
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # On supprime la copie du titre_de_recettes et on réactive le courrier titre_de_recettes pour revenir à un état cohérent
    Depuis le listing des états
    Use Simple Search  id  operation_10_inhumation
    #
    Click Element  link:copie du ${DATE_FORMAT_DD/MM/YYYY}
    Click On Form Portlet Action  om_etat  supprimer
    Click On Submit Button
    Click Element  link:operation_10_inhumation
    Click On Form Portlet Action  om_etat  modifier
    Saisir l'état  null  null  null  null  null  true  null
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Ajout d'une opération de réduction sur une concession
    [Documentation]  Ce test permet de créer une opération de réduction et de vérifier
    ...  qu'on ne peut choisir que des défunts qui sont présent dans l'emplacement sélectionné
    &{concession04} =  Create Dictionary
    ...  famille=OPERATIONREDUCTION${testid}
    ...  numero=20
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession04_id} =  Ajouter la concession  ${concession04}

    &{defunt02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=SUPONT${testid}REDUCTION
    ...  prenom=MANUELLI
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt02}  ${concession04_id}

    &{defunt03} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=MALOU${testid}REDUCTION
    ...  prenom=MARCELO
    ...  datenaissance=01/01/1970
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2017
    ...  lieudeces=LIBREVILLE
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt03}  ${concession04_id}

    &{operation08} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession04_id})
    ...  emplacement_autocomplete_id=${concession04_id}
    ${operation08_id} =  Ajouter l'opération de réduction sur concession  ${operation08}

    Depuis le contexte de l'opération de réduction sur concession  ${operation08_id}
    Click Element  css=#operation_defunt
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Add Button

    # On vérifie que les défunts de l'emplacement sont présents
    ${defunt_present} =  Create List  MALOU${testid}REDUCTION MARCELO  SUPONT${testid}REDUCTION MANUELLI
    Select List Should Contain List  css=#defunt  ${defunt_present}

    # On vérifie qu'il n'existe pas d'autre défunt
    ${defunt_non_present} =  Create List  SUPONT${testid}MERGEFIELDS MARCELMERGEFIELDS  SUPONT${testid} Marcel
    Select List Should Not Contain List  css=#defunt  ${defunt_non_present}

    Valider l'opération de réduction sur concession  ${operation08_id}

Vérification du bon fonctionnement du widget des opérations en cours
    [Documentation]  Permet de vérifier que le widget des opérations en cours
    ...  affiche correctement les opérations sur le tableau de bord et dans le listing
    ...  dédié.

    Depuis la page d'accueil  admin  admin

    &{operation09} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ${operation09.id} =  Ajouter l'opération de réduction sur concession  ${operation09}
    ${operation09.num_dossier} =  Get Text  css=#numdossier

    &{operation10} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ...  defunt_titre=Monsieur
    ...  defunt_nom=SUPONT${testid}WIDGET
    ...  defunt_prenom=MARCELWIDGET
    ...  defunt_marital=LOULOU
    ...  defunt_datenaissance=01/01/1970
    ...  defunt_lieunaissance=ARLES
    ...  defunt_datedeces=25/05/2017
    ...  defunt_lieudeces=LIBREVILLE
    ...  defunt_nature=cercueil
    ${operation10.id} =  Ajouter l'opération d'inhumation sur concession  ${operation10}
    ${operation10.num_dossier} =  Get Text  css=#numdossier

    Depuis la page d'accueil  admin  admin
    Click Element  css=#action-tab-operation-left-consulter-${operation10.id}
    Page Title Should Be  Opérations > Inhumation En Concession > Id ${operation10.id} - N° De Dossier ${operation10.num_dossier}
    La page ne doit pas contenir d'erreur

    # On vérifie que le lien vers tout les contrats à valider amène bien vers le listing des contrats avec valider à "Non"
    Depuis la page d'accueil  admin  admin
    Click Element  link:Voir toutes les opérations en cours

    Click Element  css=#action-tab-operation-left-consulter-${operation09.id}
    Page Title Should Be  Opérations > Réduction En Concession > Id ${operation09.id} - N° De Dossier ${operation09.num_dossier}
    La page ne doit pas contenir d'erreur

    Click On Portlet Action  reduction_concession  modifier
    Click On Submit Button

    Click On Back Button

    Page Title Should Be  Recherche > Opération En Cours