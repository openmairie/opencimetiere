*** Settings ***
Documentation     TestSuite "Documentation" : cette suite permet d'extraire
...    automatiquement les captures à destination de la documentation.
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown
# A chaque début de Test Case on positionne la taille de la fenêtre
# pour obtenir des captures homogènes
Test Setup    Set Window Size  ${1280}  ${1024}


*** Keywords ***
Highlight heading
    [Arguments]  ${locator}
    # Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}


StrPadLeft0
    [Arguments]  ${element}
    ${element_catenate} =    Catenate    SEPARATOR=    0    ${element}
    ${element} =    Set Variable If
    ...  ${element} < 10  ${element_catenate}
    ...  ${element} >= 10  ${element}
    [Return]  ${element}


*** Test Cases ***
Prérequis
    [Documentation]  L'objet de ce 'Test Case' est de respecter les prérequis
    ...    nécessaires aux captures d'écran.
    [Tags]  doc
    # Création des répertoires destinés à recevoir les captures d'écran
    # selon le respect de l'architecture de la documentation
    Create Directory  results/screenshots
    Create Directory  results/screenshots/ergonomie
    Create Directory  results/screenshots/administration
    Create Directory  results/screenshots/operation
    Create Directory  results/screenshots/emplacement
    Create Directory  results/screenshots/archivage


Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...    données cohérent pour les scénarios fonctionnels qui suivent.
    [Tags]  doc

    # On veut que la version du logiciel affichée sur les captures soit la majeure
    Copy File  ${EXECDIR}${/}binary_files${/}doc_version.inc.php  ${EXECDIR}${/}..${/}dyn${/}version.inc.php
    #
    Depuis la page d'accueil  admin  admin
    #
    Supprimer le paramètre  option_localisation
    Activer l'option de localisation 'plan'
    #
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE DES NEUF COLLINES
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    #
    &{cimetiere02} =  Create Dictionary
    ...  cimetierelib=CIMETIERE SAINT-PIERRE
    ...  adresse1=RUE DE ROME
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere02}
    Set Suite Variable  ${cimetiere02}
    #
    &{plan01} =  Create Dictionary
    ...  planslib=PLAN CIMETIERE DES NEUF COLLINES
    ...  fichier=colline.png
    Ajouter le plan  ${plan01}
    Set Suite Variable  ${plan01}
    #
    &{plan02} =  Create Dictionary
    ...  planslib=PLAN CIMETIERE SAINT-PIERRE
    ...  fichier=saintpierre.png
    Ajouter le plan  ${plan02}
    Set Suite Variable  ${plan02}
    #
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=A
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    #
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    #
    &{voie02_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=02
    Ajouter la voie  ${voie02_zone01_cim01}
    Set Suite Variable  ${voie02_zone01_cim01}
    #
    &{ossuaire01} =  Create Dictionary
    ...  famille=OSSUAIRE
    ...  numero=125
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${ossuaire01_id} =  Ajouter l'ossuaire  ${ossuaire01}
    Set Suite Variable  ${ossuaire01}
    Set Suite Variable  ${ossuaire01_id}
    # Ajout du type de sépulture comportant 2 colonnes et 2 lignes pour le plan en coupe
    &{type_de_sepulture_pec} =  Create Dictionary
    ...  code=TDS-pec
    ...  libelle=Type de sépulture 2*2
    ...  colonne=2
    ...  ligne=2
    ${type_de_sepulture_pec.id} =  Ajouter le *type de sépulture*  ${type_de_sepulture_pec}
    #
    &{concession01} =  Create Dictionary
    ...  famille=DURAND
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=45
    ...  positiony=125
    ...  sepulturetype=${type_de_sepulture_pec.libelle}
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01_id}

    &{lien_parente01} =  Create Dictionary
    ...  libelle=Père
    ...  lien_inverse=Fils
    ${lien_parente01.id} =  Ajouter le lien de parenté  ${lien_parente01}

    #
    &{concessionnaire01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=DURAND
    ...  marital=
    ...  prenom=PHILIPPE
    ...  datenaissance=14/01/1964
    ...  adresse1=33 Boulevard de Normandie
    ...  adresse2=
    ...  cp=94120
    ...  ville=FONTENAY-SOUS-BOIS
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire01}  ${concession01_id}
    #
    &{operation01} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession01_id})
    ...  emplacement_autocomplete_id=${concession01_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=DURAND
    ...  defunt_prenom=PHILIPPO
    ...  defunt_marital=
    ...  defunt_datenaissance=14/01/1995
    ...  defunt_datedeces=12/04/2012
    ...  defunt_lieudeces=FONTENAY-SOUS-BOIS
    ...  defunt_nature=cercueil
    ${operation01_id} =  Ajouter l'opération d'inhumation sur concession  ${operation01}
    Valider l'opération d'inhumation sur concession  ${operation01_id}

    # On ajoute deux défunts dans l'emplacement
    &{defunt_pec_01} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=DURAND
    ...  prenom=GUY
    ...  datenaissance=01/04/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/05/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt_pec_01}  ${concession01_id}
    ${id_defunt_pec_01} =  Get Value  css=div.defunt-form #defunt

    &{defunt_pec_02} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=DURAND
    ...  prenom=BERNARD
    ...  datenaissance=01/08/1981
    ...  lieunaissance=ARLES
    ...  datedeces=25/12/2020
    ...  lieudeces=LIBREVILLE
    ...  parente=LOLO
    ...  nature=cercueil
    Ajouter le defunt dans le contexte de la concession  ${defunt_pec_02}  ${concession01_id}
    ${id_defunt_pec_02} =  Get Value  css=div.defunt-form #defunt

    #On déplace les défunt dans le plan en coupe
    Go To  ${PROJECT_URL}app/index.php?module=form&obj=defunt&action=11&idx=${id_defunt_pec_01}&x=1&y=1&validation=1
    Go To  ${PROJECT_URL}app/index.php?module=form&obj=defunt&action=11&idx=${id_defunt_pec_02}&x=1&y=2&validation=1

    ${genealogie01} =  Create Dictionary
    ...  personne_1=${id_defunt_pec_01} ${defunt_pec_01.nom} ${defunt_pec_01.prenom} defunt
    ...  lien_parente=Père -> Fils
    ...  personne_2=${id_defunt_pec_02} ${defunt_pec_02.nom} ${defunt_pec_02.prenom} defunt
    Ajouter la généalogie  ${concession01_id}  ${genealogie01}

    #
    &{dossier01} =  Create Dictionary
    ...  fichier=p-concession-1.jpg
    ...  datedossier=04/06/2013
    ...  typedossier=photo
    ...  observation=Photo de l'emplacement
    Ajouter le dossier dans le contexte de la concession  ${dossier01}  ${concession01_id}
    #
    &{concession02} =  Create Dictionary
    ...  famille=MARTIN
    ...  numero=15
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=35
    ...  positiony=125
    ${concession02_id} =  Ajouter la concession  ${concession02}
    Set Suite Variable  ${concession02_id}
    #
    &{concessionnaire02} =  Create Dictionary
    ...  titre=Madame
    ...  nom=BOISVERT
    ...  marital=MARTIN
    ...  prenom=MARJOLAINE
    ...  datenaissance=09/01/1951
    ...  adresse1=22 avenue de l'Amandier
    ...  adresse2=
    ...  cp=41000
    ...  ville=BLOIS
    ...  telephone1=02.63.03.82.04
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire02}  ${concession02_id}
    #
    &{operation02} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession02_id})
    ...  emplacement_autocomplete_id=${concession02_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=MARTIN
    ...  defunt_prenom=PAUL
    ...  defunt_marital=
    ...  defunt_datenaissance=18/05/1923
    ...  defunt_datedeces=12/03/2013
    ...  defunt_lieudeces=BLOIS
    ...  defunt_nature=cercueil
    ${operation02_id} =  Ajouter l'opération d'inhumation sur concession  ${operation02}
    Valider l'opération d'inhumation sur concession  ${operation02_id}
    #
    &{concession03} =  Create Dictionary
    ...  famille=BONNET
    ...  numero=6
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=85
    ...  positiony=215
    ${concession03_id} =  Ajouter la concession  ${concession03}
    Set Suite Variable  ${concession03_id}
    #
    &{concessionnaire03} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=BONNET
    ...  marital=
    ...  prenom=JULES
    ...  datenaissance=20/01/1918
    ...  adresse1=51 rue Goya
    ...  adresse2=
    ...  cp=94170
    ...  ville=LE PERREUX-SUR-MARNE
    ...  telephone1=01.43.77.97.47
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire03}  ${concession03_id}
    #
    &{operation03} =  Create Dictionary
    ...  date=31/05/2017
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=BONNET
    ...  defunt_prenom=JEAN
    ...  defunt_marital=
    ...  defunt_datenaissance=18/01/1938
    ...  defunt_datedeces=25/04/2012
    ...  defunt_lieudeces=LE PERREUX-SUR-MARNE
    ...  defunt_nature=cercueil
    ${operation03_id} =  Ajouter l'opération d'inhumation sur concession  ${operation03}
    Valider l'opération d'inhumation sur concession  ${operation03_id}
    #
    &{concession04} =  Create Dictionary
    ...  famille=NEUFVILLE
    ...  numero=14
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=350
    ...  positiony=120
    ${concession04_id} =  Ajouter la concession  ${concession04}
    Set Suite Variable  ${concession04_id}
    #
    &{concessionnaire04} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=NEUFVILLE
    ...  marital=
    ...  prenom=LATIMER
    ...  datenaissance=11/10/1923
    ...  adresse1=44 rue Ernest Renan
    ...  adresse2=
    ...  cp=94600
    ...  ville=CHOISY-LE-ROI
    ...  telephone1=01.47.77.72.86
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire04}  ${concession04_id}

    #
    &{concession05} =  Create Dictionary
    ...  famille=PATEL
    ...  numero=28
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=135
    ...  positiony=415
    ${concession05_id} =  Ajouter la concession  ${concession05}
    Set Suite Variable  ${concession05_id}
    #
    &{concessionnaire05} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=PATEL
    ...  marital=
    ...  prenom=RAINA
    ...  datenaissance=14/01/1964
    ...  adresse1=31 rue de Lille
    ...  adresse2=
    ...  cp=94120
    ...  ville=FONTENAY-SOUS-BOIS
    Ajouter le concessionnaire dans le contexte de la concession  ${concessionnaire05}  ${concession05_id}

    &{operation08} =  Create Dictionary
    ...  date=01/06/2015
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession05_id})
    ...  emplacement_autocomplete_id=${concession05_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Monsieur
    ...  defunt_nom=PATEL
    ...  defunt_prenom=JACQUES
    ...  defunt_marital=
    ...  defunt_datenaissance=18/04/1936
    ...  defunt_datedeces=25/05/2015
    ...  defunt_lieudeces=LE PERREUX-SUR-MARNE
    ...  defunt_nature=cercueil
    ${operation08_id} =  Ajouter l'opération d'inhumation sur concession  ${operation08}
    Valider l'opération d'inhumation sur concession  ${operation08_id}
    #
    &{concession06} =  Create Dictionary
    ...  famille=VARIEUR
    ...  numero=37
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=85
    ...  positiony=235
    ${concession06_id} =  Ajouter la concession  ${concession06}
    Set Suite Variable  ${concession06_id}
    #
    &{operation09} =  Create Dictionary
    ...  date=01/06/2015
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession06_id})
    ...  emplacement_autocomplete_id=${concession06_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt_titre=Madame
    ...  defunt_nom=VARIEUR
    ...  defunt_prenom=JEANNE
    ...  defunt_marital=
    ...  defunt_datenaissance=11/04/1941
    ...  defunt_datedeces=21/08/2016
    ...  defunt_lieudeces=LE PERREUX-SUR-MARNE
    ...  defunt_nature=cercueil
    ${operation09_id} =  Ajouter l'opération d'inhumation sur concession  ${operation09}
    Valider l'opération d'inhumation sur concession  ${operation09_id}
    #
    &{concession07} =  Create Dictionary
    ...  famille=THIBODEAU
    ...  numero=151
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=350
    ...  positiony=100
    ${concession07_id} =  Ajouter la concession  ${concession07}
    Set Suite Variable  ${concession07_id}
    #
    &{concession08} =  Create Dictionary
    ...  famille=GRONDIN
    ...  numero=19
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=435
    ...  positiony=425
    ${concession08_id} =  Ajouter la concession  ${concession08}
    Set Suite Variable  ${concession08_id}
    #
    &{concession09} =  Create Dictionary
    ...  famille=LEMIEUX
    ...  numero=31
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=452
    ...  positiony=180
    ${concession09_id} =  Ajouter la concession  ${concession09}
    Set Suite Variable  ${concession09_id}
    #
    &{concession10} =  Create Dictionary
    ...  famille=MARGAND
    ...  numero=32
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=335
    ...  positiony=534
    ...  datevente=10/01/1992
    ...  duree=20
    ${concession10_id} =  Ajouter la concession  ${concession10}
    Set Suite Variable  ${concession10_id}
    #
    &{concession11} =  Create Dictionary
    ...  famille=LAMOUR
    ...  numero=27
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie02_zone01_cim01.voietype} ${voie02_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=60
    ...  positiony=180
    ...  datevente=10/03/1993
    ...  duree=25
    ${concession11_id} =  Ajouter la concession  ${concession11}
    Set Suite Variable  ${concession11_id}
    #
    &{concession12} =  Create Dictionary
    ...  famille=LOUIS
    ...  numero=3
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ...  plans=${plan01.planslib}
    ...  positionx=435
    ...  positiony=425
    ...  datevente=10/02/1999
    ...  duree=15
    ${concession12_id} =  Ajouter la concession  ${concession12}
    Set Suite Variable  ${concession12_id}

    &{operation04} =  Create Dictionary
    ...  date=15/10/2018
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession03_id})
    ...  emplacement_autocomplete_id=${concession03_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt=${operation03.defunt_nom} ${operation03.defunt_prenom}
    ${operation04_id} =  Ajouter l'opération de réduction sur concession  ${operation04}
    Set Suite Variable  ${operation04}
    Set Suite Variable  ${operation04_id}

    &{operation05} =  Create Dictionary
    ...  date=16/10/2018
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession04_id})
    ...  emplacement_autocomplete_id=${concession04_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ${operation05_id} =  Ajouter l'opération de réduction sur concession  ${operation05}
    Set Suite Variable  ${operation05}
    Set Suite Variable  ${operation05_id}

    &{operation06} =  Create Dictionary
    ...  date=17/10/2018
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession05_id})
    ...  emplacement_autocomplete_id=${concession05_id}
    ...  emplacement_transfert_autocomplete_search=(${concession06_id})
    ...  emplacement_transfert_autocomplete_id=${concession06_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt=${operation08.defunt_nom} ${operation08.defunt_prenom}
    ${operation06_id} =  Ajouter l'opération de transfert  ${operation06}
    Set Suite Variable  ${operation06}
    Set Suite Variable  ${operation06_id}

    &{operation07} =  Create Dictionary
    ...  date=18/10/2018
    ...  heure=12:00:00
    ...  emplacement_autocomplete_search=(${concession06_id})
    ...  emplacement_autocomplete_id=${concession06_id}
    ...  emplacement_transfert_autocomplete_search=(${concession05_id})
    ...  emplacement_transfert_autocomplete_id=${concession05_id}
    ...  societe_coordonnee=...
    ...  pf_coordonnee=...
    ...  observation=...
    ...  defunt=${operation09.defunt_nom} ${operation09.defunt_prenom}
    ${operation07_id} =  Ajouter l'opération de transfert  ${operation07}
    Set Suite Variable  ${operation07}
    Set Suite Variable  ${operation07_id}

    &{contrat01} =  Create Dictionary
    ...  emplacement_autocomplete_search=(${concession05_id})
    ...  emplacement_autocomplete_id=${concession05_id}
    ...  datedemande=${DATE_FORMAT_DD/MM/YYYY}
    ...  origine=Achat
    ...  terme=perpétuité
    ...  datevente=${DATE_FORMAT_DD/MM/YYYY}
    ...  montant=135.25
    ${contrat01_id} =  Ajouter un contrat  ${contrat01}
    Set Suite Variable  ${contrat01_id}

    # On importe les tarifs utilisés dans les tests
    Depuis la page d'accueil  admin  admin
    Depuis l'import  tarif
    Add File  fic1  import-tarif-test.csv
    Select From List By Label  css=#separateur  , (virgule)
    Click On Submit Button In Import CSV

    # On vérifie que les tarifs sont bien importés
    Depuis le listing des tarifs
    Page Should Not Contain  Aucun enregistrement.

Administration & Paramétrage
    [Documentation]  Section 'Administration & Paramétrage'.
    [Tags]  doc
    Depuis la page d'accueil  admin  admin
    #
    Highlight heading  css=li.shortlinks-settings
    Capture and crop page screenshot  screenshots/administration/a_administration_parametrage-action.png
    ...  header
    #
    Depuis le menu 'Administration & Paramétrage'
    Capture and crop page screenshot  screenshots/administration/a_administration_parametrage-menu.png
    ...    css=#content
    #
    Depuis le listing des paramètres
    Capture and crop page screenshot  screenshots/administration/a_administration-parametres-listing.png
    ...  content
    #
    Depuis le listing des plans
    Capture and crop page screenshot  screenshots/administration/a_administration-plans-listing.png
    ...  content
    Depuis le formulaire d'ajout d'un plan
    Capture and crop page screenshot  screenshots/administration/a_administration-plans-formulaire-ajout.png
    ...  content
    Depuis le listing des plans
    Click Element  css=#action-tab-plans-left-localiser-plan-1
    Capture and crop page screenshot  screenshots/administration/a_administration-plans-visualisation-globale-exemple-1.png
    ...  content
    Depuis le listing des plans
    Click Element  css=#action-tab-plans-left-localiser-plan-2
    Capture and crop page screenshot  screenshots/administration/a_administration-plans-visualisation-globale-exemple-2.png
    ...  content
    #
    Depuis le listing des cimetières
    Capture and crop page screenshot  screenshots/administration/a_administration-cimetiere-listing.png
    ...  content
    Depuis le formulaire d'ajout d'un cimetière
    Capture and crop page screenshot  screenshots/administration/a_administration-cimetiere-formulaire-ajout.png
    ...  content
    #
    Depuis le listing des zones
    Capture and crop page screenshot  screenshots/administration/a_administration-zone-listing.png
    ...  content
    Depuis le formulaire d'ajout d'une zone
    Capture and crop page screenshot  screenshots/administration/a_administration-zone-formulaire-ajout.png
    ...  content
    #
    Depuis le listing des voies
    Capture and crop page screenshot  screenshots/administration/a_administration-voie-listing.png
    ...  content
    Depuis le formulaire d'ajout d'une voie
    Capture and crop page screenshot  screenshots/administration/a_administration-voie-formulaire-ajout.png
    ...  content
    #
    Depuis le listing des concessions
    Capture and crop page screenshot  screenshots/administration/a_administration-icone-geolocaliser.png
    ...  css=span.sig-16
    #
    Depuis le listing des voies
    Capture and crop page screenshot  screenshots/administration/a_administration-icone-pdf-etat-voie.png
    ...  css=span.pdf-16
    #
    Modifier le paramètre  option_localisation  sig_interne
    Depuis le listing des cimetières
    Use Simple Search  id  1
    Click Element  css=#action-tab-cimetiere-left-localisation-1
    Sleep  2
    Capture and crop page screenshot  screenshots/administration/a_administration-cimetiere-geolocaliser.png
    ...  content
    Depuis le listing des zones
    Use Simple Search  id  1
    Click Element  css=#action-tab-zone-left-localisation-1
    Sleep  2
    Capture and crop page screenshot  screenshots/administration/a_administration-zone-geolocaliser.png
    ...  content
    Depuis le listing des voies
    Use Simple Search  id  17
    Click Element  css=#action-tab-voie-left-localisation-17
    Sleep  2
    Capture and crop page screenshot  screenshots/administration/a_administration-voie-geolocaliser.png
    ...  content
    Modifier le paramètre  option_localisation  plan

    # Paramétrage état et lettre type
    Depuis la page d'accueil  admin  admin
    Depuis le listing des lettres-types
    Click On Add Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  xpath://legend[contains(text(), "Paramètres généraux de l'édition")]
    Sleep  5
    Capture and crop page screenshot  screenshots/administration/a_parametrage_etat_lettretype_edition.png
    ...  fieldset-form-om_lettretype-edition
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  xpath://legend[contains(text(), "Paramètres du titre de l'édition")]
    Sleep  5
    Capture and crop page screenshot  screenshots/administration/a_parametrage_etat_lettretype_titre.png
    ...  fieldset-form-om_lettretype-titre
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  xpath://legend[contains(text(), "Paramètres des sous-états")]
    Sleep  5
    Capture and crop page screenshot  screenshots/administration/a_parametrage_etat_lettretype_corps.png
    ...  fieldset-form-om_lettretype-corps
    Select From List By Label  css=#om_sql  Requête VOIE
    Sleep  5
    Capture and crop page screenshot  screenshots/administration/a_parametrage_etat_lettretype_sql.png
    ...  fieldset-form-om_lettretype-champs-de-fusion


Emplacement
    [Documentation]  Section 'Emplacement'.
    [Tags]  doc

    Depuis la page d'accueil  admin  admin

    # Capture - Rubrique Recherche du menu
    Go To Submenu In Menu  recherche  recherche
    Capture and crop page screenshot  screenshots/emplacement/a_recherche-menu-rubrik-recherche.png
    ...  menu
    # Capture - Exemple de résultat
    Input Text  name=recherche  DURAND
    Click Element  css=button
    Capture and crop page screenshot  screenshots/emplacement/a_recherche-resultats-de-recherche-globale.png
    ...  content
    # Capture - Rubrique Emplacement du menu
    Depuis le listing des concessions
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-menu-rubrik-emplacement.png
    ...  menu
    # Capture - Listing des concessions
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-listing.png
    ...  content

    Go To Submenu In Menu  emplacement  contrat
    # Capture - Listing des contrats Recherche Contrat
    Capture and crop page screenshot  screenshots/emplacement/a_recherche-recherche_contrat-listing.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/emplacement/a_recherche-recherche_contrat-formulaire-ajout.png
    ...  content

    Depuis le contexte du contrat  ${contrat01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_recherche-recherche_contrat-formulaire-consult.png
    ...  content

    # Capture - Formulaire modifier d'une concession
    Depuis le formulaire de modification de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-formulaire-modification.png
    ...  content
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-formulaire-modification-bloc-localisation-plan.png
    ...  css=.emplacement-bloc-localisation
    Depuis le contexte de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-fiche-visualisation-bloc-localisation-plan.png
    ...  css=.emplacement-bloc-localisation
    Modifier le paramètre  option_localisation  sig_interne
    Depuis le contexte de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-fiche-visualisation-bloc-localisation-sig_interne.png
    ...  css=.emplacement-bloc-localisation
    Modifier le paramètre  option_localisation  plan
    #
    Depuis le contexte de la concession  ${concession01_id}
    # Plan en coupe
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-portlet-action-plan-en-coupe.png
    ...  portlet-actions
    Click On Form Portlet Action  concession  plan-en-coupe
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-plan-en-coupe.png
    ...  content
    # On modifie l'emplacement pour enlever le vide sanitaire
    &{concession_pec_modif} =  Create Dictionary
    ...  videsanitaire=Oui
    Modifier la concession  ${concession01_id}  ${concession_pec_modif}
    Depuis le contexte de la concession  ${concession01_id}
    Click On Form Portlet Action  concession  plan-en-coupe
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-plan-en-coupe-vide-sanitaire.png
    ...  content
    Click Link  Voir plus
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-plan-en-coupe-defunt-overlay.png
    ...  css=.ui-dialog
    #
    Depuis l'onglet 'dossier' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-dossier-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-dossier-formulaire-ajout.png
    ...  content
    Depuis l'onglet 'dossier' de la concession  ${concession01_id}
    Click On Link  photo
    CLick Element  css=#fichier span.consult-16 a
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}
    ...  Element Should Be Visible  css=.file-infos-block
    Capture viewport screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-dossier-visualisation-fichier.png
    #
    Depuis l'onglet 'contacts' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contact-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contact-formulaire-ajout.png
    ...  content
    #
    Depuis l'onglet 'contrat' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-formulaire-ajout.png
    ...  content

    &{contrat02} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=3
    ...  datevente=25/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat02}

    Click Element  css=#contrat-link-montant
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-overlay-no-tarif.png
    ...  css=.ui-dialog
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close

    &{contrat03} =  Create Dictionary
    ...  datedemande=25/03/2020
    ...  origine=Achat
    ...  terme=temporaire
    ...  duree=30
    ...  datevente=25/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat03}

    Click Element  css=#contrat-link-montant
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-overlay-tarif.png
    ...  css=.ui-dialog

    Click Element  css=.ui-widget .linkjsclosewindow
    Click On Submit Button

    Depuis l'onglet 'contrat' de la concession  ${concession01_id}
    Click On Add Button JS

    &{contrat04} =  Create Dictionary
    ...  datedemande=25/03/2035
    ...  origine=Transformation
    ...  terme=temporaire
    ...  duree=15
    ...  datevente=30/03/2020
    ...  valide=true
    Saisir un contrat  ${contrat04}

    Click Element  css=#contrat-link-montant
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-overlay-tarif-transformation.png
    ...  css=.ui-dialog

    Click Element  css=.ui-widget .linkjsclosewindow
    Click On Submit Button

    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-contrat-action-valider.png
    ...  content

    #
    Depuis l'onglet 'défunt' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-defunt-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-defunt-formulaire-ajout.png
    ...  content
    #
    Depuis l'onglet 'courrier' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-courrier-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-courrier-formulaire-ajout.png
    ...  content
    #
    Depuis l'onglet 'travaux' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-travaux-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-travaux-formulaire-ajout.png
    ...  content

    Depuis l'onglet 'généalogie' de la concession  ${concession01_id}
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-genealogie-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/emplacement/a_emplacement-emplacement_concession-onglet-genealogie-formulaire-ajout.png
    ...  content

Archivage
    [Documentation]  Section 'Archivage'.
    [Tags]  doc

    Depuis la page d'accueil  admin  admin

    # Rubrique Archives
    Depuis le listing des emplacements archivées
    Capture and crop page screenshot  screenshots/emplacement/a_archives-menu-rubrik-archives.png
    ...  menu
    #
    Go To Submenu In Menu  archive  fin_concession
    Capture and crop page screenshot  screenshots/archivage/a_archivage-fin_concession-listing.png
    ...  content
    Use Simple Search  id  ${concession12_id}
    Click Link  ${concession12_id}
    Capture and crop page screenshot  screenshots/archivage/a_archivage-fin_concession-formulaire-validation.png
    ...  content


Opération
    [Documentation]  Section 'Opération'.
    [Tags]  doc

    Depuis la page d'accueil  admin  admin
    # Capture - Listing des opérations d'inhumation
    Depuis le listing des opérations d'inhumation sur concession
    Capture and crop page screenshot  screenshots/operation/a_operation-inhumation-listing.png
    ...  content
    # Capture - Formulaire d'ajout d'une opération d'inhumation
    Depuis le formulaire d'ajout d'une opération d'inhumation sur concession
    Capture and crop page screenshot  screenshots/operation/a_operation-inhumation-formulaire-ajout.png
    ...  content
    #
    Depuis le listing des opérations de réduction sur concession
    Capture and crop page screenshot  screenshots/operation/a_operation-reduction-listing.png
    ...  content
    #
    Depuis le formulaire d'ajout d'une opération de réduction sur concession
    Capture and crop page screenshot  screenshots/operation/a_operation-reduction-formulaire-ajout.png
    ...  content
    #
    Depuis le contexte de l'opération de réduction sur concession  ${operation04_id}
    On clique sur l'onglet  operation_defunt  Opération Défunt
    Capture and crop page screenshot  screenshots/operation/a_operation-reduction-onglet-defunt-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/operation/a_operation-reduction-onglet-defunt-formulaire-ajout.png
    ...  content
    Select From List By Label  css=#defunt  ${operation04.defunt}
    Click On Submit Button In Subform
    Depuis l'onglet 'défunt' de la concession  ${concession03_id}
    Capture and crop page screenshot  screenshots/operation/a_operation-reduction-concession-onglet-defunt-listing-verrou.png
    ...  content
    #
    Depuis le listing des opérations de transfert
    Capture and crop page screenshot  screenshots/operation/a_operation-transfert-listing.png
    ...  content
    #
    Depuis le formulaire d'ajout d'une opération de transfert
    Capture and crop page screenshot  screenshots/operation/a_operation-transfert-formulaire-ajout.png
    ...  content
    #
    Depuis le contexte de l'opération de transfert  ${operation06_id}
    On clique sur l'onglet  operation_defunt  Opération Défunt
    Capture and crop page screenshot  screenshots/operation/a_operation-transfert-onglet-defunt-listing.png
    ...  content
    Click On Add Button JS
    Capture and crop page screenshot  screenshots/operation/a_operation-transfert-onglet-defunt-formulaire-ajout.png
    ...  content
    Select From List By Label  css=#defunt  ${operation06.defunt}
    Click On Submit Button In Subform
    Depuis l'onglet 'défunt' de la concession  ${concession05_id}
    Capture and crop page screenshot  screenshots/operation/a_operation-transfert-concession-onglet-defunt-listing-verrou.png
    ...  content
    #


Ergonomie
    [Documentation]  Section 'Ergonomie'.
    [Tags]  doc
    # Les méthodes Suite Setup et Suite Teardown gèrent l'ouverture et la
    # fermeture du navigateur. Dans le cas de ce TestSuite on a besoin de
    # travailler sur un navigateur fraichement ouvert pour être sûr que la
    # variable de session est neuve.
    Fermer le navigateur
    Ouvrir le navigateur
    Depuis la page de login
    Capture viewport screenshot  screenshots/ergonomie/a_connexion_formulaire.png
    #
    Input Username    admin
    Input Password    plop
    Click Button    login.action.connect
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}    Error Message Should Be    Votre identifiant ou votre mot de passe est incorrect.
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_erreur.png
    ...  css=div.message
    #
    Input Username    admin
    Input Password    admin
    Click Button    login.action.connect
    Wait Until Element Is Visible    css=#actions a.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_ok.png
    ...  css=div.message
    #
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_globales.png
    ...  footer
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_personnelles.png
    ...  actions
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_logo.png
    ...  logo
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_raccourcis.png
    ...  shortlinks
    #
    Highlight heading  css=li.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_action.png
    ...  header
    #
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_menu.png
    ...  menu
    #
    Go To Dashboard
    Se déconnecter
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_message_ok.png
    ...  css=div.message
    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    Remove element  dashboard
    Update element style  css=#content  height  300px
    Add pointy note  css=#logo  Logo  position=right
    Highlight heading  css=#menu
    Add note  css=#menu  Menu  position=right
    Add pointy note  css=#shortlinks  Raccourcis  position=bottom
    Add pointy note  css=#actions  Actions personnelles  position=left
    Highlight heading  css=#footer
    Add note  css=#footer  Actions globales  position=top
    Capture viewport screenshot  screenshots/ergonomie/a_ergonomie_generale_detail.png
    #
    Depuis le listing des voies
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-icone-ajouter.png
    ...  css=span.add-16
    Depuis le listing des cimetières
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-icone-visualiser.png
    ...  css=span.consult-16
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-icone-pdf-listing.png
    ...  css=span.print-25
    #
    Depuis le listing des collectivités
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-exemple-listing.png
    ...  content
    Click Link  LIBREVILLE
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-exemple-fiche-visualisation.png
    ...  content
    Click On Form Portlet Action  om_collectivite  modifier
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-exemple-formulaire-modification.png
    ...  content
    On clique sur l'onglet  om_utilisateur  Utilisateur
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie-exemple-onglet-exemple-listing.png
    ...  content
    #
    Set Window Size  ${1280}  ${1024}
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/a_tableau-de-bord-exemple.png
    ...  content
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_recherche_globale.png
    ...  css=div.widget_search
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_localisation_plan.png
    ...  css=div.widget_localisation
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_supervision.png
    ...  css=div.widget_supervision
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_concession_a_terme.png
    ...  css=div.widget_concession_a_terme
    Capture and crop page screenshot  screenshots/ergonomie/a_widget_contrat_a_valider.png
    ...  css=div.widget_contrat_a_valider


Déconstitution du jeu de données
    [Documentation]
    [Tags]  doc
    #
    Remove File  ${EXECDIR}${/}..${/}dyn${/}version.inc.php

