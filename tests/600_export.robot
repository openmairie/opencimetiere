*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  ...


*** Test Cases ***
Édition
    [Documentation]  Ce test permet de vérifier qu'aucune édition pdf du menu
    ...  "Export" -> "Édition" ne produit d'erreur de base de données.
    #
    Depuis la page d'accueil  admin  admin
    # On accède au listing des éditions
    Go To  ${PROJECT_URL}${OM_ROUTE_MODULE_EDITION}
    Page Title Should Be  Export > Éditions
    Submenu In Menu Should Be Selected  edition  edition
    La page ne doit pas contenir d'erreur
    #
    Click Link  xpath=//a[@href="../app/index.php?module=edition&obj=cimetiere"]
    #
    Open PDF  ${OM_PDF_TITLE}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  CIMETIERES
    Close PDF
    #
    Click Link  xpath=//a[@href="../app/index.php?module=edition&obj=voie"]
    #
    Open PDF  ${OM_PDF_TITLE}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Liste VOIES / ZONES / CIMETIERES
    Close PDF


Requêtes Mémorisées
    [Documentation]  Ce test permet de vérifier qu'aucune requête mémorisée reqmo du menu
    ...  "Export" -> "Requêtes mémorisées" ne produit d'erreur de base de données.
    #
    Depuis la page d'accueil  admin  admin
    # On accède au listing des reqmo
    Depuis l'écran principal du module 'Reqmo'
    Page Title Should Be  Export > Requêtes Mémorisées
    Submenu In Menu Should Be Selected  edition  reqmo
    La page ne doit pas contenir d'erreur
    # reqmo standards
    @{reqmo_standards}  Create List
    ...  cimetiere
    #...  defunt_cimetiere_dates > reqmo spécifique
    ...  emplacement_plans
    ...  emplacement_voie
    ...  emplacement
    ...  entreprise
    ...  voie
    ...  voie_zone
    #
    :FOR  ${ELEMENT}  IN  @{reqmo_standards}
        \  Log  ${ELEMENT}
        \  Depuis l'écran principal du module 'Reqmo'
        \  Click Element  css=#action-reqmo-${ELEMENT}-exporter
        \  Click On Submit Button In Reqmo
    # reqmo spécifiques
    # - defunt_cimetiere_dates
    #   (la non saisie de dates provoque une erreur de base de données)
    Depuis l'écran principal du module 'Reqmo'
    Click Element  css=#action-reqmo-defunt_cimetiere_dates-exporter
    Input Text  xpath=//input[@id='inhumation_debut_(AAAA-MM-JJ)']  2017-01-01
    Input Text  xpath=//input[@id='inhumation_fin_(AAAA-MM-JJ)']  2018-01-01
    Click On Submit Button In Reqmo


Requêtes tri defunt
    [Documentation]  Ce test permet de vérifier la reqmo tri_defunt
    # Création de jeu de données
    #
    Depuis la page d'accueil  admin  admin
    #
    Set Suite Variable  ${testid}  53
    # On crée le cimetiere01
    &{cimetiere01} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-01
    ...  adresse1=RUE DE LA REPUBLIQUE
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere01}
    Set Suite Variable  ${cimetiere01}
    # On crée une zone pour le cimetiere01
    &{zone01_cim01} =  Create Dictionary
    ...  cimetiere=${cimetiere01.cimetierelib}
    ...  zonetype=CARRE
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim01}
    Set Suite Variable  ${zone01_cim01}
    # On crée une voie d'une zone pour le cimetiere01
    &{voie01_zone01_cim01} =  Create Dictionary
    ...  zone=${zone01_cim01.zonetype} ${zone01_cim01.zonelib} (${zone01_cim01.cimetiere})
    ...  voietype=ALLEE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim01}
    Set Suite Variable  ${voie01_zone01_cim01}
    # On crée la concession01 pour le cimetiere01
    &{concession01} =  Create Dictionary
    ...  famille=DURAND${testid}
    ...  numero=12
    ...  cimetierelib=${cimetiere01.cimetierelib}
    ...  zonelib=${zone01_cim01.zonetype} ${zone01_cim01.zonelib}
    ...  voielib=${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    ${concession01_id} =  Ajouter la concession  ${concession01}
    Set Suite Variable  ${concession01_id}

    # On crée le cimetiere02
    &{cimetiere02} =  Create Dictionary
    ...  cimetierelib=CIMETIERE${testid}-02
    ...  adresse1=RUE DE ROME
    ...  adresse2=
    ...  cp=99607
    ...  ville=LIBREVILLE
    ...  observations=
    Ajouter le cimetière  ${cimetiere02}
    Set Suite Variable  ${cimetiere02}

    # On crée une zone pour le cimetiere02
    &{zone01_cim02} =  Create Dictionary
    ...  cimetiere=${cimetiere02.cimetierelib}
    ...  zonetype=ENCLOS
    ...  zonelib=Z${testid}-01
    Ajouter la zone  ${zone01_cim02}
    Set Suite Variable  ${zone01_cim02}
    # On crée une voie d'une zone pour le cimetiere02
    &{voie01_zone01_cim02} =  Create Dictionary
    ...  zone=${zone01_cim02.zonetype} ${zone01_cim02.zonelib} (${zone01_cim02.cimetiere})
    ...  voietype=PLACE
    ...  voielib=V${testid}-01
    Ajouter la voie  ${voie01_zone01_cim02}
    Set Suite Variable  ${voie01_zone01_cim02}
    # On crée la concession02 pour le cimetiere02
    &{concession02} =  Create Dictionary
    ...  famille=PHILIPPE${testid}
    ...  numero=21
    ...  cimetierelib=${cimetiere02.cimetierelib}
    ...  zonelib=${zone01_cim02.zonetype} ${zone01_cim02.zonelib}
    ...  voielib=${voie01_zone01_cim02.voietype} ${voie01_zone01_cim02.voielib}
    ${concession02_id} =  Ajouter la concession  ${concession02}
    Set Suite Variable  ${concession02_id}

    # On crée le defunt1 dans la concession01
    &{defunt1} =  Create Dictionary
    ...  titre=Monsieur
    ...  nom=HAIE
    ...  marital=
    ...  prenom=HENRI
    ...  observation=
    ...  nature=cercueil
    ${defunt1_id} =  Ajouter le defunt dans le contexte de la concession  ${defunt1}  ${concession01_id}
    Set Suite Variable  ${defunt1}
    # On crée le defunt2 dans la concession01
    &{defunt2} =  Create Dictionary
    ...  titre=Madame
    ...  nom=HAIE
    ...  marital=BRIGITTE
    ...  prenom=GERMAINE
    ...  observation=
    ${defunt2_id} =  Ajouter le defunt dans le contexte de la concession  ${defunt2}  ${concession01_id}
    Set Suite Variable  ${defunt2}

    # On crée le defunt3 dans la concession02
    &{defunt3} =  Create Dictionary
    ...  titre=Madame
    ...  nom=YEAL
    ...  marital=
    ...  prenom=BEATRICE
    ...  observation=
    ...  nature=boite
    ${defunt3_id} =  Ajouter le defunt dans le contexte de la concession  ${defunt3}  ${concession02_id}
    Set Suite Variable  ${defunt3}

    # On vérifie l'export afficher dans un tableau
    Depuis l'écran principal du module 'Reqmo'
    Click Element  css=#action-reqmo-defunt-exporter
    Select From List By Label  sortie  Tableau - Affichage a l'écran
    Click Button  name=reqmo-form-valid

    # On vérifie la présence des défunts lors de l'import
    # defunt1
    Element Should Contain  css=.tab-tab  ${cimetiere01.cimetierelib}
    Element Should Contain  css=.tab-tab  ${defunt1.titre} ${defunt1.nom} ${defunt1.prenom}
    Element Should Contain  css=.tab-tab  ${zone01_cim01.zonetype} ${zone01_cim01.zonelib} ${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    Element Should Contain  css=.tab-tab  ${concession01.numero}
    Element Should Contain  css=.tab-tab  concession
    Element Should Contain  css=.tab-tab  ${concession01.famille}

    # defunt2
    Element Should Contain  css=.tab-tab  ${cimetiere01.cimetierelib}
    Element Should Contain  css=.tab-tab  ${defunt2.titre} ${defunt2.nom} ${defunt2.marital} ${defunt2.prenom}
    Element Should Contain  css=.tab-tab  ${zone01_cim01.zonetype} ${zone01_cim01.zonelib} ${voie01_zone01_cim01.voietype} ${voie01_zone01_cim01.voielib}
    Element Should Contain  css=.tab-tab  ${concession01.numero}
    Element Should Contain  css=.tab-tab  concession
    Element Should Contain  css=.tab-tab  ${concession01.famille}

    # defunt3
    Element Should Contain  css=.tab-tab  ${cimetiere02.cimetierelib}
    Element Should Contain  css=.tab-tab  ${defunt3.titre} ${defunt3.nom} ${defunt3.prenom}
    Element Should Contain  css=.tab-tab  ${zone01_cim02.zonetype} ${zone01_cim02.zonelib} ${voie01_zone01_cim02.voietype} ${voie01_zone01_cim02.voielib}
    Element Should Contain  css=.tab-tab  ${concession02.numero}
    Element Should Contain  css=.tab-tab  concession
    Element Should Contain  css=.tab-tab  ${concession02.famille}
