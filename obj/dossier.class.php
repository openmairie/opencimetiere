<?php
/**
 * Ce script définit la classe 'dossier'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier.class.php";

/**
 * Définition de la classe 'dossier' (om_dbform).
 */
class dossier extends dossier_gen {

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier",
            "emplacement",
            "fichier",
            "typedossier",
            "datedossier",
            "observation",
        );
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $form->setType('emplacement','hidden');
        //
        if ($maj==0 || $maj == 1) {
                $form->setType('typedossier','select');
        }else{ 
                $form->setType('typedossier','selectstatic');
        }
        // Gestion du champ fichier 
        if ($maj == 0 || $maj == 1) {
            $form->setType('fichier', 'upload2');
        } elseif ($maj == 3) {
            $form->setType('fichier', 'file');
        } else {
            $form->setType('fichier', 'filestatic');
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //parent::setSelect($form, $maj);
        //
        $contenu=array();
        $contenu[0]=array('dossier','photo');
        $contenu[1]=array(__('dossier'),__('photo'));
        $form->setSelect("typedossier",$contenu);
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('dossier', "[...]");
                $form->setVal('emplacement', $idxformulaire);
            }
        }
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $bloc_global_width_full = "span12";
        $bloc_global_width_half = "span6";

        //// CONTAINER GLOBAL - START
        $this->form->setBloc("dossier", "D", "", "emplacement-form dossier-form form-action-".$maj);

        //// LIGNE ID - START
        $this->form->setBloc("dossier", "D", "", "row-fluid");
            //// BLOC ID - START
            $this->form->setBloc("dossier", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-identification ");
                //// BLOC ID - START
                $this->form->setBloc("dossier", "D", "", "span12");
                $this->form->setBloc("emplacement", "F");
                //// BLOC ID - END
            $this->form->setBloc("emplacement", "F");
            //// BLOC ID - END
        $this->form->setBloc("emplacement", "F");
        //// LIGNE ID - END

        //// LIGNE DOSSIER - START
        $this->form->setBloc("fichier", "D", "", "row-fluid");
            //// BLOC DOSSIER - START
            $this->form->setBloc("fichier", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-infos ");
                //// LIGNE DOSSIER - START
                $this->form->setBloc("fichier", "D", "", "row-fluid");
                    //// BLOC DOSSIER - START
                    $this->form->setBloc("fichier", "D", "", $bloc_global_width_half);
                        //// LIGNE DOSSIER - START
                        $this->form->setBloc("fichier", "D", "", "row-fluid");
                            //// BLOC DOSSIER - START
                            $this->form->setBloc("fichier", "D", __("dossier"), " emplacement-bloc-personne span12");
                                $this->form->setBloc("fichier", "D", "", "span12");
                                $this->form->setBloc("datedossier", "F");
                            $this->form->setBloc("datedossier", "F");
                            //// BLOC DOSSIER - END
                        $this->form->setBloc("datedossier", "F");
                        //// LIGNE DOSSIER - END
                    $this->form->setBloc("datedossier", "F");
                    //// BLOC DOSSIER - END
                $this->form->setBloc("datedossier", "F");
                //// LIGNE DOSSIER - END
            $this->form->setBloc("datedossier", "F");
            //// BLOC DOSSIER - END
        $this->form->setBloc("datedossier", "F");
        //// LIGNE DOSSIER- END

        //// LIGNE OBSERVATION - START
        $this->form->setBloc("observation", "D", "", "row-fluid");
            //// BLOC OBSERVATION - START
            $this->form->setBloc("observation", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-observation ");
                $this->form->setBloc("observation", "DF", "", "group span12");
            $this->form->setBloc("observation", "F");
            // BLOC OBSERVATION - END
        $this->form->setBloc("observation", "F");
        //// LIGNE OBSERVATION - START

        //// CONTAINER GLOBAL - END
        $this->form->setBloc("observation", "F");
    }
}
