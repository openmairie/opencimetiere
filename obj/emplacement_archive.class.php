<?php
/**
 * Ce script définit la classe 'emplacement_archive'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/emplacement_archive.class.php";

/**
 * Définition de la classe 'emplacement_archive' (om_dbform).
 */
class emplacement_archive extends emplacement_archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 001 - modifier
        // -> modification impossible
        unset($this->class_actions[1]);
        // ACTION - 002 - supprimer
        // -> suppression impossible
        unset($this->class_actions[2]);
    }
}
