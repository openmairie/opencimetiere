<?php
/**
 * Ce script définit la classe 'inhumation'.
 *
 * @package opencimetiere
 * @version SVN : $Id: inhumation.class.php 812 2018-10-21 18:25:18Z fmichon $
 */

require_once "../obj/inhumation.class.php";

/**
 * Définition de la classe 'inhumation_concession' (om_dbform).
 *
 * Surcharge de la classe 'inhumation'.
 */
class inhumation_concession extends inhumation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "inhumation_concession";

    /**
     * @var string
     */
    var $emplacement_nature = 'concession';

}
