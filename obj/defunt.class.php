<?php
/**
 * Ce script définit la classe 'defunt'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/defunt.class.php";

/**
 * Définition de la classe 'defunt' (om_dbform).
 */
class defunt extends defunt_gen {

    // Variables de classe définie à la fin de la méthode init_parameters()
    // Permet de gérer la traduction et les tailles des différentes
    // natures d'enveloppe du défunt
    var $mapping_nature_defunt = array();

    /**
     *
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        // Récupération des paramètres nécessaires aux calculs de taille et de
        // durée
        $this->init_parameters();
    }

    function init_parameters() {
        //
        $default_taille_cercueil = 1;
        $this->taille_cercueil = $this->f->getParameter("taille_cercueil");
        if (is_null($this->taille_cercueil)) {
            $this->taille_cercueil = $default_taille_cercueil;
        }
        //
        $default_taille_urne = 0.1;
        $this->taille_urne = $this->f->getParameter("taille_urne");
        if (is_null($this->taille_urne)) {
            $this->taille_urne = $default_taille_urne;
        }

        $default_taille_urne_simple = 0.1;
        $this->taille_urne_simple = $this->f->getParameter("taille_urne_simple");
        if (is_null($this->taille_urne_simple)) {
            $this->taille_urne_simple = $default_taille_urne_simple;
        }

        $default_taille_urne_partage = 0.1;
        $this->taille_urne_partage = $this->f->getParameter("taille_urne_partage");
        if (is_null($this->taille_urne_partage)) {
            $this->taille_urne_partage = $default_taille_urne_partage;
        }

        $default_taille_cercueil_simple = 2;
        $this->taille_cercueil_simple = $this->f->getParameter("taille_cercueil_simple");
        if (is_null($this->taille_cercueil_simple)) {
            $this->taille_cercueil_simple = $default_taille_cercueil_simple;
        }

        $default_taille_cercueil_partage = 2;
        $this->taille_cercueil_partage = $this->f->getParameter("taille_cercueil_partage");
        if (is_null($this->taille_cercueil_partage)) {
            $this->taille_cercueil_partage = $default_taille_cercueil_partage;
        }

        $default_taille_cercueil_herm = 1;
        $this->taille_cercueil_herm = $this->f->getParameter("taille_cercueil_herm");
        if (is_null($this->taille_cercueil_herm)) {
            $this->taille_cercueil_herm = $default_taille_cercueil_herm;
        }

        $default_taille_reliquaire = 1;
        $this->taille_reliquaire = $this->f->getParameter("taille_reliquaire");
        if (is_null($this->taille_reliquaire)) {
            $this->taille_reliquaire = $default_taille_reliquaire;
        }

        $default_taille_reliquaire_partage = 2;
        $this->taille_reliquaire_partage = $this->f->getParameter("taille_reliquaire_partage");
        if (is_null($this->taille_reliquaire_partage)) {
            $this->taille_reliquaire_partage = $default_taille_reliquaire_partage;
        }

        $default_taille_non_renseigne = 1;
        $this->taille_non_renseigne = $this->f->getParameter("taille_non_renseigne");
        if (is_null($this->taille_non_renseigne)) {
            $this->taille_non_renseigne = $default_taille_non_renseigne;
        }

        //
        $default_taille_reduction = 0.5;
        $this->taille_reduction = $this->f->getParameter("taille_reduction");
        if (is_null($this->taille_reduction)) {
            $this->taille_reduction = $default_taille_reduction;
        }
        //
        $default_temps_reduction = 5;
        $this->temps_reduction = $this->f->getParameter("temps_reduction");
        if (is_null($this->temps_reduction)) {
            $this->temps_reduction = $default_temps_reduction;
        }

        // Mise à jour de la variable de classe avec les informations paramétrées
        $this->mapping_nature_defunt = array(
            'cercueil' => array("trad" => "cercueil", "taille" => $this->taille_cercueil),
            'zinc' => array("trad" => "cercueil en zinc", "taille" => $this->taille_cercueil),
            'urne' => array("trad" => "urne", "taille" => $this->taille_urne),
            'boite' => array("trad" => "boite", "taille" => $this->taille_reduction),
            'cercueil_simp' => array("trad" => "cercueil simple", "taille" => $this->taille_cercueil_simple),
            'cercueil_herm' => array("trad" => "cercueil hermétique", "taille" => $this->taille_cercueil_herm),
            'cercueil_partage' => array("trad" => "cercueil partagé", "taille" => $this->taille_cercueil_partage),
            'reliquaire' => array("trad" => "reliquaire simple", "taille" => $this->taille_reliquaire),
            'reliquaire_partage' => array("trad" => "reliquaire partagé", "taille" => $this->taille_reliquaire_partage),
            'urne_simple' => array("trad" => "urne_simple", "taille" => $this->taille_urne_simple),
            'urne_partage' => array("trad" => "urne partagée", "taille" => $this->taille_urne_partage),
            'non_renseigne' => array("trad" => "non renseignée", "taille" => $this->taille_non_renseigne),
        );
    }

    function compose_nature_defunt_trad() {
        $select_composed = "(";
        foreach ($this->mapping_nature_defunt as $nature_key => $nature_trad) {
            $select_composed .= sprintf(
                '%1$s WHEN 
                    defunt.nature = \'%2$s\'
                THEN
                    \'%3$s\'
                %4$s
                ',
                $nature_key == array_keys($this->mapping_nature_defunt)[0] ? 'CASE' : '',
                $nature_key,
                __($nature_trad['trad']),
                $nature_key == array_keys($this->mapping_nature_defunt)[count($this->mapping_nature_defunt)-1] ? 'END' : ''
            );
        }
        return $select_composed.")";
    }


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        
        // ACTION - 001 - modifier
        $this->class_actions[1]['condition'][] = "is_not_locked";
        
        // ACTION - 002 - supprimer
        $this->class_actions[2]['condition'][] = "is_not_locked";

        // ACTION - 101 - edition
        //
        $this->class_actions[101] = array(
            "identifier" => "pdf-edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("récapitulatif"),
                "description" => __('télécharger le récapitulatif au format pdf'),
                "class" => "pdf-16",
                "order" => 30,
            ),
            "permission_suffix" => "edition",
            "view" => "view_pdf_edition_etat",
            "condition" => array(
                "exists",
            ),
        );

        $this->class_actions[11] = array(
            "identifier" => "defunt-positionnement",
            // "method" => "modifier",
            "view" => "view_defunt_position",
            "permission_suffix" => "positionner",
            "condition" => array(
                "exists",
            ),
        );
    }

    /**
     * @return string
     */
    function get_om_etat_id() {
        return $this->get_absolute_class_name();
    }

    /**
     * CONDITION - is_locked
     * 
     * Permet de vérifier si un défunt est verrouillé.
     * 
     * @return boolean
     */
    function is_locked() {
        if ($this->getVal('verrou') === "Oui") {
            return true;
        }

        return false;
    }

    /**
     * CONDITION - is_not_locked
     * 
     * Permet de vérifier si un défunt n'est pas verrouillé.
     * 
     * @return boolean
     */
    function is_not_locked() {
        return ! $this->is_locked();
    }

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "defunt",
            "emplacement",
            "verrou",
            "titre",
            "nom",
            "marital",
            "prenom",
            "datenaissance",
            "lieunaissance",
            "datedeces",
            "lieudeces",
            "nature",
            "taille",
            "dateinhumation",
            "reduction",
            "datereduction",
            "exhumation",
            "dateexhumation",
            "historique",
            "parente",
            "genealogie",
            "observation",
            "taille as archivetaille",
            "x",
            "y",
            "id_temp",
        );
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        //parent::verifier($val);
        // obligatoire  [Compulsory]
        if ($this->valF['nom'] == "") {
            $this->correct = false;
            $this->addToMessage(" ".__("nom")." ".__("obligatoire"));
        }
        // verification de date   [Check of date]
        // coherence des actions   [coherence of the actions]
        if ($val['datenaissance']!=""   and $val['datedeces']!="") {
            if ($this->valF['datenaissance'] > $this->valF['datedeces']) {
                $this->correct=false;
                $this->addToMessage(__("datenaissance")." > ".__("datedeces")." [".
                $val['datenaissance'].
                " > ".$val['datedeces']."]");
            }
        }
        //  suppression des controle AC2i version 1.07
        if ($val['dateinhumation'] != ""
            and $val['datedeces'] != ""
            and $val['dateinhumation'] != "00/00/0000"
            and $val['datedeces'] != "00/00/0000") {
            if ($this->valF['datedeces'] > $this->valF['dateinhumation']) {
                $this->correct=false;
                $this->addToMessage(__("datedeces")." > ".__("dateinhumation")." [".
                $val['datedeces']." > ".$val['dateinhumation']."]");
            }
        }
        // *** 1.07 test null /defaut
        if (($val['dateinhumation']!="" and $val['dateinhumation']!="00/00/0000")
            and ($val['dateexhumation']!="" and $val['dateexhumation']!="00/00/0000")) {
            if ($this->valF['dateinhumation']>$this->valF['dateexhumation']) {
                $this->correct=false;
                $this->addToMessage(__("dateinhumation")." > ".__("dateexhumation")." [".
                $val['dateinhumation']." > ".$val['dateexhumation']."]");
            }
        }
        // regle de date de reduction = $this->temps_reduction  [Rule of date of reduction]
        // exception colombarium  et terraincommunal et depositoire
        // [exception Columbarium and municipal ground and depository]
        if (($val['dateinhumation']!="" and $val['dateinhumation']!="00/00/0000")
            and ($val['datereduction']!="" and $val['datereduction']!="00/00/0000")
            and ($this->retourformulaire != "colombarium"
            and $this->retourformulaire != "terraincommunal"
            and $this->retourformulaire != "depositoire"
            and $this->retourformulaire != "ossuaire")) {
            //
            $temp =explode("-", $this->valF['dateinhumation']);
            $temp[0] = $temp[0]+$this->temps_reduction;
            $datebutoir= $temp[0]."-".$temp[1]."-".$temp[2];
            if ($this->valF['datereduction'] < $datebutoir) {
                $this->correct=false;
                $this->addToMessage(__("datereduction")." [".
                $val['datereduction']." < ".$this->temps_reduction.
                " ".__("ans")." ".$val['dateinhumation']."]");
            }
        }
      
        if ($val['verrou']=="Oui") {
            $this->correct=false;
            $this->addToMessage(__("operation")." ".__("active"));
        }
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $form->setType('emplacement', 'hidden');
        $form->setType('genealogie', 'hidden');
        $form->setType('x', 'hidden');
        $form->setType('y', 'hidden');
        $form->setType('id_temp', 'hidden');
        if ($maj == 11) {
            foreach ($this->champs as $champ) {
                $form->setType($champ, 'hidden');
            }
            $form->setType('x', 'text');
            $form->setType('y', 'text');
        }
        //
        $form->setType('defunt', 'hiddenstatic');
        //
        $form->setType('archivetaille', 'hidden');
        //
        if ($maj == 0 || $maj == 1) {
            $form->setType('taille', 'hiddenstatic');
            $form->setType('exhumation', 'select');
            //
            $form->setType('datedeces', 'date2');
            $form->setType('dateexhumation', 'date2');
            $form->setType('datenaissance','date2');
            $form->setType('dateinhumation','date2');
            //
            if ($this->retourformulaire=='colombarium' or $this->retourformulaire=='terraincommunal'
            or $this->retourformulaire=='ossuaire' or $this->retourformulaire=='depositoire'){
               $form->setType('datereduction','hidden');
               $form->setType('reduction', 'hidden');
            }else{
               $form->setType('datereduction','date2');
               $form->setType('reduction', 'select');
            }
            //
            $form->setType('nature', 'select');
            $form->setType('historique', 'textareahiddenstatic');
            $form->setType('verrou', 'hiddenstatic');
        }

        if ($maj == 3) {
            $form->setType('nature', 'selectstatic');
            $form->setType('genealogie', 'static');
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj < 2) {
            // exhumation inhumation
            $contenu=array();
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(__('Non'),__('Oui'));
            $form->setSelect("exhumation", $contenu);
            $form->setSelect("reduction", $contenu);
        }
        //nature
        $contenu=array();
        if ($this->retourformulaire == 'colombarium') {
            $contenu[0]=array('urne');
            $contenu[1]=array(__("urne"));
        } else {
            if ($this->retourformulaire == 'terraincommunal') {
                $contenu[0]=array('cercueil','zinc');
                $contenu[1]=array(__('cercueil'),__("cercueil_zinc"));
            } else {
                $contenu[0]=array('cercueil','zinc','urne','boite','cercueil_simp', 'cercueil_herm','cercueil_partage','reliquaire','reliquaire_partage','urne_simple','urne_partage','non_renseigne',);
                $contenu[1]=array(__('cercueil'),__("cercueil_zinc"),__("urne"),__("boite"),__("cercueil simple"),__("cercueil hermétique"),__("cercueil partagé"),__("reliquaire partagé"),__("reliquaire simple"),__("urne_simple"),__("urne partagée"),__("non renseignée"));
            }
        }
        $form->setSelect("nature", $contenu);
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib('nom', __("Nom de naissance"));
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        // * mise en majuscule
        // On liste les champs sur lesquels on veut forcer les majuscules automatiques
        $fields_to_upper_case = array('nom', 'prenom', 'marital', 'lieudeces', 'lieunaissance');
        // On récupère les valeurs de l'option, si il y en a pas retourne un tableau vide
        $option_casse_force_majuscule = $this->f->get_option_casse_force_majuscule();
        // On boucle sur les champs à mettre en majuscule auto
        foreach ($fields_to_upper_case as $field) {
            // Si la clé existe dans l'option et que la valeur est true ou que la clé n'existe pas
            if (array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule)
                && $option_casse_force_majuscule[$this->clePrimaire.'.'.$field] === true
                || array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule) == false) {
                // On force la majuscule sur le champ
                $form->setOnchange($field, "this.value=this.value.toUpperCase()");
            }
        }
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('defunt', "[...]");
                $form->setVal('emplacement', $idxformulaire);
                $form->setVal('verrou', "Non");
                if ($retourformulaire == 'colombarium') {
                    $form->setVal('taille', $this->taille_urne);
                } else {
                    $form->setVal('taille', $this->taille_cercueil);
                }
            }
        }
        // En mode consultation on récupère les lien de parenté du défunt sur lequel on est
        if ($maj == 3) {
            $form->setVal('genealogie', $this->f->generate_text_genealogie('defunt', $this->getVal($this->clePrimaire)));
        }
    }

    /**
     * SETTER_TREATMENT - setValF (setValF).
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // calcul de la taille occupee par le defunt
        if ($this->valF['exhumation']=="Oui") {
            $this->valF['taille'] = 0;
        } else {
            foreach ($this->mapping_nature_defunt as $nature_key => $nature_info) {
                if ($this->valF['nature'] == $nature_key) {
                    $this->valF['taille'] = $nature_info['taille'];
                }
                if ($this->valF['reduction']=="Oui") {
                    $this->valF['taille'] = $this->taille_reduction;
                }
            }
        }
        $this->addToLog(__("taille")." = ".$this->valF['taille'], VERBOSE_MODE);
    }

    /**
     * TRIGGER - triggerajouter.
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Mise à jour du champ 'placeoccupe' de l'emplacement en fonction de
        // la taille du défunt
        $sql ="select placeoccupe from ".DB_PREFIXE."emplacement where emplacement ="
        .$this->valF['emplacement'] ;
        $emplacement=$this->f->db->getOne($sql);
        $cle= " emplacement = ".$this->valF['emplacement'];
        $valF['placeoccupe']= $emplacement+$this->valF['taille'];
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.'emplacement',
            $valF,
            DB_AUTOQUERY_UPDATE,
            $cle
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToLog("maj place occupe dans emplacement :".$valF['placeoccupe'], VERBOSE_MODE);
        $this->addToMessage(__("enregistrement")." ".
        $this->valF['emplacement']." ".__("table")." ".__("emplacement").
          " [".$this->f->db->affectedRows()." ".__("enregistrement")." ".
          __("mis_a_jour")."]") ;
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifier.
     *
     * @return boolean
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Mise à jour du champ 'placeoccupe' de l'emplacement en fonction de
        // la taille du défunt
        $sql ="select placeoccupe from ".DB_PREFIXE."emplacement where emplacement ="
        .$this->valF['emplacement'] ;
        $emplacement=$this->f->db->getOne($sql);
        $cle= " emplacement = ".$this->valF['emplacement'];
        $valF['placeoccupe']= $emplacement+$this->valF['taille']-$val['archivetaille'];
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.'emplacement',
            $valF,
            DB_AUTOQUERY_UPDATE,
            $cle
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToLog("maj place occupe dans emplacement :".$valF['placeoccupe'], VERBOSE_MODE);
        $this->addToMessage("Enregistrement ".$this->valF['emplacement']." de la table emplacement"
            ." [".$this->f->db->affectedRows()." ".__("enregistrement")." ".
            __("mis_a_jour")."]") ;
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggersupprimer.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Mise à jour du champ 'placeoccupe' de l'emplacement en fonction de
        // la taille du défunt
        if ($val['taille']<>""){
            $sql ="select placeoccupe from ".DB_PREFIXE."emplacement where emplacement ="
            .$val['emplacement'] ;
            $emplacement=$this->f->db->getOne($sql);
            $cle= " emplacement = ".$val['emplacement'];
            $valF['placeoccupe']= $emplacement-$val['taille'];
            $res = $this->f->db->autoExecute(
                DB_PREFIXE.'emplacement',
                $valF,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            if ($this->f->isDatabaseError($res, true) !== false) {
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToLog(__("maj place occupe dans emplacement").":".$valF['placeoccupe'], VERBOSE_MODE);
            $this->addToMessage(__("enregistrement")." ".$val['emplacement']." ".__("table")." ".__("emplacement").
                " [".$this->f->db->affectedRows()." ".__("enregistrement")." ".
                __("mis_a_jour")."]") ;
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $bloc_global_width_full = "span12";
        $bloc_global_width_half = "span6";

        //// CONTAINER GLOBAL - START
        $this->form->setBloc("defunt", "D", "", "emplacement-form defunt-form form-action-".$maj);

        //// LIGNE ID - START
        $this->form->setBloc("defunt", "D", "", "row-fluid");
            //// BLOC ID - START
            $this->form->setBloc("defunt", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-identification ");
                //// BLOC ID - START
                $this->form->setBloc("defunt", "D", "", "span6");
                $this->form->setBloc("emplacement", "F");
                //// BLOC ID - END
                //// BLOC VERROU - START
                $this->form->setBloc("verrou", "DF", "", "span6 emplacement-field-verrou");
                //// BLOC VERROU - END
            $this->form->setBloc("verrou", "F");
            //// BLOC ID - END
        $this->form->setBloc("verrou", "F");
        //// LIGNE ID - END

        //// LIGNE PERSONNE + INFOS + ADRESSE - START
        $this->form->setBloc("titre", "D", "", "row-fluid");
            //// BLOC PERSONNE + INFOS + ADRESSE - START
            $this->form->setBloc("titre", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-infos ");
                //// LIGNE PERSONNE + INFOS + ADRESSE - START
                $this->form->setBloc("titre", "D", "", "row-fluid");
                    //// BLOC PERSONNE + INFOS - START
                    $this->form->setBloc("titre", "D", "", $bloc_global_width_half);
                        //// LIGNE PERSONNE - START
                        $this->form->setBloc("titre", "D", "", "row-fluid");
                            //// BLOC PERSONNE - START
                            $this->form->setBloc("titre", "D", __("defunt"), " emplacement-bloc-personne span12");
                                $this->form->setBloc("titre", "D", "", "span12");
                                $this->form->setBloc("lieudeces", "F");
                            $this->form->setBloc("lieudeces", "F");
                            //// BLOC PERSONNE - END
                        $this->form->setBloc("lieudeces", "F");
                        //// LIGNE PERSONNE - END
                    $this->form->setBloc("lieudeces", "F");
                    //// BLOC PERSONNE + INFOS - END
                    //// BLOC ADRESSE - START
                    $this->form->setBloc("nature", "D", "", $bloc_global_width_half);
                        //// LIGNE PERSONNE - START
                        $this->form->setBloc("nature", "D", "", "row-fluid");
                            //// BLOC PERSONNE - START
                            $this->form->setBloc("nature", "D", __("nature"), " emplacement-bloc-personne span12");
                                $this->form->setBloc("nature", "D", "", "span12");
                                $this->form->setBloc("taille", "F");
                            $this->form->setBloc("taille", "F");
                            //// BLOC PERSONNE - END
                        $this->form->setBloc("taille", "F");
                        //// LIGNE PERSONNE - END
                        //// LIGNE ADRESSE - START
                        $this->form->setBloc("dateinhumation", "D", "", "row-fluid");
                            //// BLOC ADRESSE - START
                            $this->form->setBloc("dateinhumation", "D", __("Informations funeraires"), "emplacement-bloc-adresse span12");
                                $this->form->setBloc("dateinhumation", "D", "", "span12");
                                $this->form->setBloc("dateexhumation", "F");
                            $this->form->setBloc("dateexhumation", "F");
                            //// BLOC ADRESSE - END
                        $this->form->setBloc("dateexhumation", "F");
                        //// LIGNE ADRESSE - END
                    $this->form->setBloc("dateexhumation", "F");
                    //// BLOC ADRESSE - END
                $this->form->setBloc("dateexhumation", "F");
                //// LIGNE PERSONNE + INFOS + ADRESSE - END
            $this->form->setBloc("dateexhumation", "F");
            //// BLOC PERSONNE + INFOS + ADRESSE - END
        $this->form->setBloc("dateexhumation", "F");
        //// LIGNE PERSONNE + INFOS + ADRESSE - END

        //// LIGNE HISTORIQUE - START
        $this->form->setBloc("historique", "D", "", "row-fluid");
            //// BLOC HISTORIQUE - START
            $this->form->setBloc("historique", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-historique ");
                $this->form->setBloc("historique", "DF", "", "group span12");
            $this->form->setBloc("historique", "F");
            // BLOC HISTORIQUE - END
        $this->form->setBloc("historique", "F");
        //// LIGNE HISTORIQUE - START

        //// LIGNE parente - START
        $this->form->setBloc("parente", "D", "", "row-fluid");
            //// BLOC genealogie - START
            $this->form->setBloc("parente", "D", __("Parenté"), $bloc_global_width_full." emplacement-bloc emplacement-bloc-parente ");
                $this->form->setBloc("parente", "D", "", "span6");
                $this->form->setBloc("genealogie", "F", "", "");
            $this->form->setBloc("genealogie", "F");
            // BLOC parente - END
        $this->form->setBloc("genealogie", "F");
        //// LIGNE parente - START

        //// LIGNE OBSERVATION - START
        $this->form->setBloc("observation", "D", "", "row-fluid");
            //// BLOC OBSERVATION - START
            $this->form->setBloc("observation", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-observation ");
                $this->form->setBloc("observation", "DF", "", "group span12");
            $this->form->setBloc("observation", "F");
            // BLOC OBSERVATION - END
        $this->form->setBloc("observation", "F");
        //// LIGNE OBSERVATION - START


        //// CONTAINER GLOBAL - END
        $this->form->setBloc("observation", "F");
    }

    /**
     * MERGE_FIELDS - get_all_merge_fields.
     *
     * @return array
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "emplacement" => "emplacement",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = $this->get_values_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => $this->getVal($field),
                    ));
                    $elem_values = $elem->get_merge_fields($type);
                    $values = array_merge($values, $elem_values);
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = $this->get_labels_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => 0,
                    ));
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    /**
     * MERGE_FIELDS - get_values_legacy_merge_fields.
     *
     * @return array
     */
    function get_values_legacy_merge_fields() {
        $query = sprintf(
            'SELECT
                defunt.defunt as defunt,
                tdcd.libelle as dtitre,
                defunt.nom as dnom,
                defunt.prenom as dprenom,
                to_char(defunt.datenaissance,\'DD/MM/YYYY\') as datenaissance,
                to_char(defunt.datedeces,\'DD/MM/YYYY\') as datedeces,
                defunt.lieudeces as lieudeces,
                tdca.libelle as ctitre,
                autorisation.nom as cnom,
                autorisation.prenom as cprenom,
                emplacement.emplacement as emplacement,
                emplacement.numero as numero,
                cimetierelib,
                emplacement.complement as complement,
                emplacement.typeconcession as typeconcession,
                voie_type.libelle as type_de_voie,
                voielib,
                zone_type.libelle as type_de_zone,
                zonelib

            FROM
                %1$sdefunt 
                LEFT JOIN %1$stitre_de_civilite as tdcd on defunt.titre = tdcd.titre_de_civilite
                LEFT JOIN %1$semplacement on defunt.emplacement = emplacement.emplacement
                LEFT JOIN %1$sautorisation 
                on (defunt.emplacement = autorisation.emplacement AND autorisation.nature=\'concessionnaire\')
                LEFT JOIN %1$stitre_de_civilite as tdca on autorisation.titre = tdca.titre_de_civilite
                LEFT JOIN %1$svoie on emplacement.voie = voie.voie
                LEFT JOIN %1$svoie_type on voie.voietype = voie_type.voie_type
                LEFT JOIN %1$szone on voie.zone = zone.zone
                LEFT JOIN %1$szone_type on zone.zonetype = zone_type.zone_type
                LEFT JOIN %1$scimetiere on zone.cimetiere = cimetiere.cimetiere

            WHERE
                defunt.defunt=%2$s
            ORDER BY autorisation.autorisation
            LIMIT 1',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            return $row;
        }
        return array();
    }

    /**
     * MERGE_FIELDS - get_labels_legacy_merge_fields.
     *
     * @return array
     */
    function get_labels_legacy_merge_fields() {
        return array(
            "legacy" => array(
                // Défunt
                "defunt" => "",
                "dtitre" => "",
                "dnom" => "",
                "dprenom" => "",
                "datenaissance" => "",
                "atedeces" => "",
                "lieudeces" => "",
                // Autorisation
                "ctitre" => "",
                "cnom" => "",
                "cprenom" => "",
                // Emplacement
                "emplacement" => "",
                "numero" => "",
                "complement" => "",
                "typeconcession" => "",
                // Voie
                "type_de_voie" => "",
                "voielib" => "",
                // Zone
                "type_de_zone" => "",
                "zonelib" => "",
                // Cimetière
                "cimetierelib" => "",
            ),
        );
    }

    /**
     * MERGE_FIELDS - get_labels_merge_fields.
     *
     * @return array
     */
    function get_labels_merge_fields() {
        $labels = parent::get_labels_merge_fields();
        //
        $labels[_("defunt")]["defunt.titre"] = __("titre de civilité (libellé)");
        $labels[_("defunt")]["defunt.titre_code"] = __("titre de civilité (code)");
        $labels[_("defunt")]["defunt.titre_description"] = __("titre de civilité (description)");
        $labels[_("defunt")]["defunt.premier_prenom"] = __("Premier prénom du défunt. (Ne fonctionne que si les prénoms sont séparés par des virgules)");
        //
        return $labels;
    }

    /**
     * MERGE_FIELDS - get_values_merge_fields.
     *
     * @return array
     */
    function get_values_merge_fields() {
        $values = parent::get_values_merge_fields();
        //
        $inst_titre = $this->f->get_inst__om_dbform(array(
            "obj" => "titre_de_civilite",
            "idx" => $this->getVal("titre"),
        ));
        $values["defunt.titre"] = $inst_titre->getVal("libelle");
        $values["defunt.titre_code"] = $inst_titre->getVal("code");
        $values["defunt.titre_description"] = $inst_titre->getVal("description");
        // Récupère le premier prénom du défunt
        $values["defunt.premier_prenom"] = trim(explode(',', $values['defunt.prenom'])[0]);
        //
        $values["defunt.observation"] = str_replace("\n", "<br/>", $values["defunt.observation"]);
        return $values;
    }

    function view_defunt_position() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        
        $inst_defunt = $this->f->get_inst__om_dbform(array(
            "obj" => "defunt",
            "idx" => $this->getVal($this->clePrimaire),
        ));
        /**
         * TREATMENT
         */
        // Traitement si validation du formulaire
        if ($this->getParameter('validation') > 1 || $this->f->get_submitted_post_value('position_defunt') !== null) {
            // On vérifier que le x et le y sont cohérents avec ce qui est disponible dans le paramétrage de l'emplacement
            $inst_emplacement = $this->f->get_inst__om_dbform(array(
                "obj" => "emplacement",
                "idx" => $this->getVal('emplacement'),
            ));

            $inst_sepulturetype = $this->f->get_inst__om_dbform(array(
                "obj" => "sepulture_type",
                "idx" => $inst_emplacement->getVal('sepulturetype'),
            ));

            $nb_ligne = $inst_sepulturetype->getVal('ligne');
            if ($inst_emplacement->getVal('videsanitaire') == 'Oui') {
                $nb_ligne = $nb_ligne + 1;
            }
            
            // Si les coordonnées ont été modifiées
            if ($inst_defunt->getVal('x') != $this->f->get_submitted_get_value("x")
                || $inst_defunt->getVal('y') != $this->f->get_submitted_get_value("y")) {
                // Si x et y sont NULL ou que leur valeur ne dépassent pas la limite de la grille
                // Alors on met à jour les coordonnées du défunt
                if (($this->f->get_submitted_get_value("x") == 'NULL'
                    && $this->f->get_submitted_get_value("y") == 'NULL')
                    || ($this->f->get_submitted_get_value("x") != 'NULL'
                    && $this->f->get_submitted_get_value("y") != 'NULL')
                    && ($this->f->get_submitted_get_value("x") <= $inst_sepulturetype->getVal('colonne')
                    && $this->f->get_submitted_get_value("y") <= $nb_ligne)) {

                    $valF = array( 
                        'x' => $this->f->get_submitted_get_value("x") == 'NULL' ? NULL : $this->f->get_submitted_get_value("x"), 
                        'y' => $this->f->get_submitted_get_value("y") == 'NULL' ? NULL : $this->f->get_submitted_get_value("y"),
                    );
                    $cle = " defunt = ".$this->getVal($this->clePrimaire);
                    $res = $this->f->db->autoExecute(
                        DB_PREFIXE.'defunt',
                        $valF,
                        DB_AUTOQUERY_UPDATE,
                        $cle
                    );
                    // On log l'autoExecute
                    $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE."defunt\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");", VERBOSE_MODE);
                }
            }
        }

        $this->f->layout->display__form_container__begin(array(
            "id" => "position_defunt",
            "action" => $this->getDataSubmit(),
        ));

        $champs = array('x', 'y', );

        $this->form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => $this->getParameter("maj"),
            "champs" => $champs,
        ));

        foreach ($this->champs as $champ) {
            $this->form->setType($champ, 'hidden');
        }
        // Paramétrage des champs du formulaire
        // address
        $this->form->setLib("x", __("positionx"));
        $this->form->setLib("y", __("positiony"));
        $this->form->setType("x", "text");
        $this->form->setType("y", "text");
        $this->form->setTaille("x", 60);
        $this->form->setTaille("y", 60);
        $this->form->setMax("x", 255);
        $this->form->setMax("y", 255);
        $this->form->setVal("x", $inst_defunt->getVal('x'));
        $this->form->setVal("y", $inst_defunt->getVal('y'));
        // Ouverture du conteneur de formulaire
        $this->form->entete();
        $this->form->afficher($champs, 0, false, false);
        $this->form->enpied();
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "position_defunt",
            "value" => __("Valider"),
            "class" => "boutonFormulaire",
        ));
        $this->f->layout->display__form_controls_container__end();
        //
        $this->f->layout->display__form_container__end();
    }
}
