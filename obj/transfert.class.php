<?php
/**
 * Ce script définit la classe 'transfert'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/operation.class.php";

/**
 * Définition de la classe 'transfert' (om_dbform).
 *
 * Surcharge de la classe 'operation'.
 */
class transfert extends operation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "transfert";

    /**
     *
     */
    var $categorie = "transfert";

    /**
     * @var array
     */
    var $required_field = array(
        "operation",
        "numdossier",
        "date",
        "heure",
        "etat",
        "categorie",
        "emplacement",
        "emplacement_transfert",
    );

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) { //ajouter et modifier
            $form->setType('emplacement_transfert', 'autocomplete');
        }
        if ($maj == 3) {
            $form->setType('emplacement_transfert', 'link');
        }
    }

    /**
     * Configuration du widget de formulaire autocomplete du champ 'emplacement_transfert'.
     *
     * @return array
     */
    function get_widget_config__emplacement_transfert__autocomplete() {
        return array(
            // Surcharge visée pour l'ajout
            'obj' => "emplacement_transfert",
            // Table de l'objet
            'table' => "emplacement",
            // Permission d'ajouter
            'droit_ajout' => false,
            // Critères de recherche
            'criteres' => array(
                "'('||emplacement.emplacement||')'" => __("identifiant de l'emplacement (ex : (12))"),
                "emplacement.famille" => __("famille (ex : DUPONT)"),
                "'n°'||emplacement.numeroacte" => __("n° d'acte (ex : n°12512)"),
            ),
            // Tables liées
            'jointures' => array(
                'voie
                    ON emplacement.voie=voie.voie',
                'voie_type
                    ON voie.voietype=voie_type.voie_type',
                'zone
                    ON voie.zone=zone.zone',
                'zone_type
                    ON zone.zonetype=zone_type.zone_type',
                'cimetiere
                    ON zone.cimetiere=cimetiere.cimetiere',
            ),
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            'identifiant' => "emplacement.emplacement",
            //
            'libelle' => array(
                "concat(
                    emplacement.nature,
                    ' (',
                    emplacement.emplacement,
                    ') ',
                    ' - Famille : ',
                    emplacement.famille,
                    ' - acte n°',
                    emplacement.numeroacte,
                    '<br/><i>',
                    emplacement.numero,
                    ' ',
                    emplacement.complement,
                    ' ',
                    voie_type.libelle,
                    ' ',
                    voie.voielib,
                    ' [',
                    zone_type.libelle,
                    ' ',
                    zone.zonelib,
                    '][',
                    cimetiere.cimetierelib,
                    ']</i>'
                )",
            ),
            'group_by' => array(
                'emplacement.emplacement',
                'voie_type.libelle',
                'voie.voielib',
                'zone_type.libelle',
                'zone.zonelib',
                'cimetiere.cimetierelib',
            ),
            //
            'link_selection' => false,
        );
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj < 2) {
            // AUTOCOMPLETE emplacement
            $form->setSelect(
                "emplacement_transfert",
                $this->get_widget_config__emplacement_transfert__autocomplete()
            );
        }
        // MODE CONSULTER
        if ($maj == 3) {
            $inst_emplacement = $this->f->get_inst__om_dbform(array(
                "obj" => "emplacement",
                "idx" => $this->getVal("emplacement_transfert"),
            ));
            $params = array();
            $params['obj'] = $inst_emplacement->getVal('nature');
            $params['libelle'] = $inst_emplacement->getVal('nature').' ('.$inst_emplacement->getVal('emplacement').') - Famille : '.$inst_emplacement->getval('famille').' - acte n°'.$inst_emplacement->getVal('numeroacte')."<br/><i>".$inst_emplacement->getval('adresse')."</i>";
            $params['title'] = "Consulter l'emplacement";
            $params['idx'] = $this->getVal("emplacement_transfert");
            $form->setSelect("emplacement_transfert", $params);
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib('emplacement', __("origine"));
        $form->setLib('emplacement_transfert', __("destination"));
    }
}
