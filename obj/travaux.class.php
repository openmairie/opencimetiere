<?php
/**
 * Ce script définit la classe 'travaux'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/travaux.class.php";

/**
 * Définition de la classe 'travaux' (om_dbform).
 */
class travaux extends travaux_gen {

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "travaux",
            "emplacement",
            "entreprise",
            "datedebinter",
            "datefininter",
            "naturedemandeur",
            "naturetravaux",
            "observation",
        );
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Affichage de l'id de la table dans tous les cas
        $form->setType('travaux', "hiddenstatic");
        // On cache l'id de l'emplacement dans tous les cas
        $form->setType('emplacement', 'hidden');
        if ($maj == 0 || $maj == 1) {
            //
            $form->setType('naturedemandeur', 'select');
        }
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation == 0) {
            //
            if ($maj == 0) {
                // Affichage de l'id de la table avec la valeur [...]
                $form->setVal('travaux', "[...]");
                //
                $form->setVal('emplacement', $idxformulaire);
            }
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if($maj<2){
            $contenu=array();
            $contenu[0]=array('Concessionnaire','Ayant droit','Autre');
            $contenu[1]=array(__('concessionnaire'),
                              __('ayantdroit'),
                              __('autre'));
            $form->setSelect("naturedemandeur",$contenu);
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $bloc_global_width_full = "span12";
        $bloc_global_width_half = "span6";

        //// CONTAINER GLOBAL - START
        $this->form->setBloc("travaux", "D", "", "emplacement-form travaux-form form-action-".$maj);

        //// LIGNE ID - START
        $this->form->setBloc("travaux", "D", "", "row-fluid");
            //// BLOC ID - START
            $this->form->setBloc("travaux", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-identification ");
                //// BLOC ID - START
                $this->form->setBloc("travaux", "D", "", "span12");
                $this->form->setBloc("emplacement", "F");
                //// BLOC ID - END
            $this->form->setBloc("emplacement", "F");
            //// BLOC ID - END
        $this->form->setBloc("emplacement", "F");
        //// LIGNE ID - END

        //// LIGNE TRAVAUX - START
        $this->form->setBloc("entreprise", "D", "", "row-fluid");
            //// BLOC TRAVAUX - START
            $this->form->setBloc("entreprise", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-infos ");
                //// LIGNE TRAVAUX - START
                $this->form->setBloc("entreprise", "D", "", "row-fluid");
                    //// BLOC TRAVAUX - START
                    $this->form->setBloc("entreprise", "D", "", $bloc_global_width_half);
                        //// LIGNE TRAVAUX - START
                        $this->form->setBloc("entreprise", "D", "", "row-fluid");
                            //// BLOC TRAVAUX - START
                            $this->form->setBloc("entreprise", "D", __("travaux"), " emplacement-bloc-personne span12");
                                $this->form->setBloc("entreprise", "D", "", "span12");
                                $this->form->setBloc("naturetravaux", "F");
                            $this->form->setBloc("naturetravaux", "F");
                            //// BLOC TRAVAUX - END
                        $this->form->setBloc("naturetravaux", "F");
                        //// LIGNE TRAVAUX - END
                    $this->form->setBloc("naturetravaux", "F");
                    //// BLOC TRAVAUX - END
                $this->form->setBloc("naturetravaux", "F");
                //// LIGNE TRAVAUX - END
            $this->form->setBloc("naturetravaux", "F");
            //// BLOC TRAVAUX - END
        $this->form->setBloc("naturetravaux", "F");
        //// LIGNE TRAVAUX- END

        //// LIGNE OBSERVATION - START
        $this->form->setBloc("observation", "D", "", "row-fluid");
            //// BLOC OBSERVATION - START
            $this->form->setBloc("observation", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-observation ");
                $this->form->setBloc("observation", "DF", "", "group span12");
            $this->form->setBloc("observation", "F");
            // BLOC OBSERVATION - END
        $this->form->setBloc("observation", "F");
        //// LIGNE OBSERVATION - START

        //// CONTAINER GLOBAL - END
        $this->form->setBloc("observation", "F");
    }
}
