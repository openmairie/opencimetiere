<?php
/**
 * Ce script redirige vers le script index.php à la racine de l'application.
 *
 * @package opencimetiere
 * @version SVN : $Id: index.php 2261 2013-04-17 09:38:29Z fmichon $
 */

header("Location: ../index.php");
exit();
