<?php
/**
 * Ce script définit la classe 'concession'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/emplacement.class.php";

/**
 * Définition de la classe 'concession' (om_dbform).
 *
 * Surcharge de la classe 'emplacement'.
 */
class concession extends emplacement {

    /**
     * @var string
     */
    protected $_absolute_class_name = "concession";

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "concession"
     */
    var $nature = "concession";

    /**
     * Option pour la gestion ou non du type de concession
     * @var boolean Prend la valeur true or false
     */
    var $option_typeconcession = false;

    /**
     * Valeurs pour la sélection du type de concession
     * @var array
     */
    var $select_typeconcession = array ();

    /**
     * Option pour la gestion d'un lien SIG externe (id sig)
     * @var boolean Prend ka valeur true or false
    */
    var $option_externesig = false;

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // verification donnees real non vide (GI/compatibilite postgresql)
        // Check data real not empty (compatibility postgresql)
        if ($val['duree']=="" or is_null($val['duree']))
           $this->valF['duree'] = 0;
        if ($val['placeoccupe']=="" or is_null($val['placeoccupe']))
           $this->valF['placeoccupe'] = 0;
        if ($val['nombreplace']=="" or is_null($val['nombreplace']))
           $this->valF['nombreplace'] = 0;
        if ($val['superficie']=="" or is_null($val['superficie']))
           $this->valF['superficie'] = 0;
        if ($val['placeconstat']=="" or is_null($val['placeconstat']))
           $this->valF['placeconstat'] = 0;
        if ( ($val['datevente']=="" or is_null($val['datevente'])) && isset($_GET['idx']) ){
            $val['datevente'] = NULL;
            $this->valF['datevente'] = NULL;
        }
        // verif voie [verify street]
        if($this->correct)
            $this->verifierVoie($val);
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Localisation
        $form->setType('geom', 'hidden');
        
        $form->setType('plans', 'hidden');
        $form->setType('positionx', 'hidden');
        $form->setType('positiony', 'hidden');
        // Champs paramétrables
        $form->setType('temp1', 'hidden');
        $form->setType('temp2', 'hidden');
        $form->setType('temp3', 'hidden');
        $form->setType('temp4', 'hidden');
        $form->setType('temp5', 'hidden');
        // XXX Ce champ est à supprimer de l'application
        $form->setType('photo','hidden');
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Id
            $form->setType('emplacement', 'hiddenstatic');
            $form->setType('nature', 'hiddenstatic');
            // Libre
            $form->setType('libre', 'select');
            // Adresse
            $form->setType('complement', 'select');
            $form->setType('voie', 'autocomplete');
            // Localisation
            if ($this->f->getParameter("option_localisation") == "plan") {
                $form->setType('plans', 'select');
                $form->setType('positionx', 'text');
                $form->setType('positiony', 'localisation_plan');
            } elseif ($this->f->getParameter("option_localisation") == "sig_interne") {
                $form->setType('geom', 'geom');
            } elseif ($this->f->getParameter("option_localisation") == "sig_externe") {
                
            }

            $form->setType('dateconstat','date');
            $form->setType('placeoccupe','hiddenstatic');

            $form->setType('videsanitaire','select');
            // type de concession
            $form->setType('typeconcession', 'select');

            $form->setType('etatsemelle','select');
            $form->setType('etatmonument','select');
            // Abandon
            $form->setType('abandon','select');
        }
        //
        if ($maj == 3) {
            // Localisation
            if ($this->f->getParameter("option_localisation") == "plan") {
                $form->setType('plans', 'localisation_plan_static');
                $form->setType('positionx', 'hidden');
                $form->setType('positiony', 'hidden');
            } elseif ($this->f->getParameter("option_localisation") == "sig_interne") {
                $form->setType('geom', 'geom');
            } elseif ($this->f->getParameter("option_localisation") == "sig_externe") {
			}     
		}
		$form->setType('pgeom', 'hidden');
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Localisation
            $contenu = array();
            $contenu[0] = array('plans', 'positionx');
            $form->setSelect("positiony", $contenu);
            // Libre
            $contenu = array();
            $contenu[0] = array('Non', 'Oui');
            $contenu[1] = array(__("Non"), __("Oui"));
            $form->setSelect("libre", $contenu);
            // abandon
            $form->setSelect("abandon",$contenu);
            // terme
            $contenu = array ();
            $contenu[0]=array('',"perpetuite",'temporaire');
            $contenu[1]=array('',__("perpetuite"),__("temporaire"));
            $form->setSelect("terme",$contenu);
            //vide sanitaire
           $contenu=array();
            $contenu[0]=array('','Non','Oui');
            $contenu[1]=array('',__("Non"),__("Oui"));
            $form->setSelect("videsanitaire",$contenu);
            //etat semelle
            $contenu=array();
            $contenu[0]=array('','Bonne','Mauvaise','Passable');
            $contenu[1]=array('',__("Bonne"),__("Mauvaise"),__("Passable"));
            $form->setSelect("etatsemelle",$contenu);
            //etat monument
            $contenu=array();
            $contenu[0]=array('','Bon','Mauvais','Passable');
            $contenu[1]=array('',__("Bon"),__("Mauvais"),__("Passable"));
            $form->setSelect("etatmonument",$contenu);

            // type de concession
            $contenu = array();
            $contenu [0] = array ("", "Familiale", "Individuelle", "Collective");
            $contenu [1] = array ("", __("Familiale"), __("Individuelle"), __("Collective"));
            $form->setSelect ('typeconcession', $contenu);            
        }
    }
}
