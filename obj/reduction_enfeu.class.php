<?php
/**
 * Ce script définit la classe 'reduction_enfeu'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/reduction.class.php";

/**
 * Définition de la classe 'reduction_enfeu' (om_dbform).
 *
 * Surcharge de la classe 'reduction'.
 */
class reduction_enfeu extends reduction {

    /**
     * @var string
     */
    protected $_absolute_class_name = "reduction_enfeu";

    /**
     * @var string
     */
    var $emplacement_nature = 'enfeu';

}
