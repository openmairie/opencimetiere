<?php
/**
 * Ce script définit la classe 'ossuaire'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/emplacement.class.php";

/**
 * Définition de la classe 'ossuaire' (om_dbform).
 *
 * Surcharge de la classe 'emplacement'.
 */
class ossuaire extends emplacement {

    /**
     * @var string
     */
    protected $_absolute_class_name = "ossuaire";

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "ossuaire"
     */
    var $nature = "ossuaire";

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Localisation
        $form->setType('geom', 'hidden');
        $form->setType('pgeom', 'hidden');
        $form->setType('plans', 'hidden');
        $form->setType('positionx', 'hidden');
        $form->setType('positiony', 'hidden');
        // Champs paramétrables
        $form->setType('temp1', 'hidden');
        $form->setType('temp2', 'hidden');
        $form->setType('temp3', 'hidden');
        $form->setType('temp4', 'hidden');
        $form->setType('temp5', 'hidden');
        //// Champs de l'emplacement qui ne concernent pas un ossuaire 
        // Libre
        $form->setType('libre', 'hidden');
        // Acte
        $form->setType('typeconcession', 'hidden');
        $form->setType('numerocadastre', 'hidden');
        $form->setType('numeroacte', 'hidden');
        $form->setType('dateacte','hidden');
        $form->setType('terme', 'hidden');
        $form->setType('duree', 'hidden');
        $form->setType('datevente','hidden');
        $form->setType('daterenouvellement', 'hidden');
        $form->setType('dateterme', 'hidden');
        // Bati
        $form->setType('sepulturetype', 'hidden');
        $form->setType('videsanitaire', 'hidden');
        $form->setType('semelle', 'hidden');
        $form->setType('etatsemelle', 'hidden');
        $form->setType('monument', 'hidden');
        $form->setType('etatmonument', 'hidden');
        $form->setType('largeur', 'hidden');
        $form->setType('profondeur', 'hidden');
        // Place
        $form->setType('nombreplace', 'hidden');
        $form->setType('placeoccupe','hidden');
        $form->setType('superficie', 'hidden');
        $form->setType('placeconstat', 'hidden');
        $form->setType('dateconstat', 'hidden');
        // Abandon
        $form->setType('abandon', 'hidden');
        $form->setType('date_abandon', 'hidden');
        // XXX Ce champ est à supprimer de l'application
        $form->setType('photo','hidden');
        ////////
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Id
            $form->setType('emplacement', 'hiddenstatic');
            $form->setType('nature', 'hiddenstatic');
            // Adresse
            $form->setType('complement', 'select');
            $form->setType('voie','autocomplete');
        }
    }
}
