<?php
/**
 * Ce script définit la classe 'inhumation_colombarium'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/inhumation.class.php";

/**
 * Définition de la classe 'inhumation_colombarium' (om_dbform).
 *
 * Surcharge de la classe 'inhumation'.
 */
class inhumation_colombarium extends inhumation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "inhumation_colombarium";

    /**
     * @var string
     */
    var $emplacement_nature = 'colombarium';

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if($maj<2){
            //nature
            $contenu=array();
            $contenu[0]=array('urne');
            $contenu[1]=array(__("urne"));
            $form->setSelect("defunt_nature",$contenu);
        }
    }
}
