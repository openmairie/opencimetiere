<?php
/**
 * Ce script permet de ...
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

/**
 * Objet Module de recherche - LAUGIER JeanYves (salamandre) - 20100707
 *
 *   Le module de recherche est totalement parametrable depuis le fichier sql/../[obj].search.inc.php ou
 *   par passage d'arguments ($configTypeRecherche) dans : new search($[class utils],$configTypeRecherche)
 *   
 *      -----> ($configTypeRecherche) ------>
 *      // definitions des types de recherche
 *      $conf = array(
 *      "libelle" => __("Recherche globale"),
 *      "champAffiche" => array(), /// liste des champs a afficher
 *      "champRecherche" => array(), /// champs de recherche 
 *      "table" => "", // tables
 *      "serie" => 0, // limit (facultatif - par defaut 100 )
 *      "selection" => "", // where
 *      "tri" => "", // order by (facultatif)
 *     "href" => "" // lien (facultatif)
 *      );
 *      array_push( $configTypeRecherche, $conf);
 *
 *  Le module possede une fonction searchInclude permettant d'implementer les resultats de la recherche dans
 *  la meme page courante ou autre selon le lien indique dans $include
 *
 *  Vous pouvez cacher le formulaire de recherche en ajoutant $objRech->showForm(true)
 *
 *
 *  Methode d'implementation sur le tableau de bord avec formulaire et l'affichage des resultats
 *  (exemple tableau de bord ( fichier tdb.php ) ):
 *          require_once "../obj/search.class.php";
 *          $objRech = new search($f);   // $f = class utils
 *          $objRech->searchInclude("tdb.php?obj=[obj]"); // lien de la page du tableau de bord (envoi formulaire)
 *          $objRech->showForm();
 *          $objRech->showResults();
 *          
 *  Methode d'implementation sur une page quelconque, formulaire seulement
 *  (exemple):
 *          require_once "../obj/search.class.php";
 *          $objRech = new search($f);  /// $f = class utils
 *          $objRech->searchInclude("search.php?obj=[obj]"); // lien de la page du tableau de bord (envoi formulaire)
 *          $objRech->showForm();
 * 
 **/



class search {
    
    //// mode DEBUG /////
    private $DEBUG = 0;
    
    private $langue; // langue
    private $typeRecherche; // configuration des types de recherche
    private $f; // class utils
    private $js; // verification si javascript deja implente
    private $formulaire; // verification si formulaire deja affiche
    private $affichage; // verification si resultats deja affiche
    private $include; // page d'appel du formaulaire (action=)

    /** fonction configure
    *
    * Recuperation du fichier de configuration des types de recherche
    * selon le type de connexion (mysql,pgsql) dans sql/../recherche.inc
    *
    **/
    private function configure($obj = ""){
        if( file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".search.inc.php") ){
            require_once("../sql/".OM_DB_PHPTYPE."/".$obj.".search.inc.php");
        }
        if( isset($configTypeRecherche) ){
            $this->typeRecherche = $configTypeRecherche;
            if( $this->DEBUG ){
                echo("<br/>-----configTypeRecherche-------<br/>");
                print_r($configTypeRecherche);
                echo("<br/>-------------------------------<br/>");
            }
            return true;
        }else{
            echo("<div class='erreur'>Class search : \$configTypeRecherche non configure par defaut pour obj=".$obj.".</div>");
            $this->typeRecherche = array();
            return false;
        }
    }

    /**
    * Module de recherche
    *
    *   Permet d'effectuer une recherche selon un type predefini dans $configTypeRecherche de
    *    sql/../recherche.inc
    *
    * @param $utils Objet  Class utils.class.php (Connexion base de donnees)
    * @param $configTypeRecherche Array(Array()) - facultatif, Remplace la configuration des types
    *                                  de recherche par defaut dans le fichier sql/../recherche.inc par
    *                                  les nouvelles valeurs dans $configTypeRecherche
    * @param $langue string langue (falcultatif, par defaut "francais")
    **/
    public function __construct($utils = "", $configTypeRecherche = "", $langue = "francais"){
        $this->include = "";
        $this->js = false;
        $this->formulaire = false;
        $this->affichage = false;
        $obj = "";
        if( isset($_GET['obj']) ) $obj = $_GET['obj'];
        if( !$langue ){ $langue = "francais"; }
        if( !($utils->db) ){
            echo("<div class='erreur'>Class search : Impossible d'appeler la Fonction 'db' depuis \$utils.</div>");
            return false;
        }else{
            $this->f = $utils;
        }
        if( !$configTypeRecherche ){
            if( !$this->configure($obj) ){
                return false;
            }else{
                $this->include = "../app/search.php?obj=".$obj;
            }
         }   
        
        if( $configTypeRecherche ) $this->typeRecherche = $configTypeRecherche;
        $this->langue = $langue;
    }

    /** Fonction searchInclude
    *
    * Fonction permettant d'inclure la recherche dans la page courante ou autre, (lien href)
    * $objRech -> showRecherche() doit etre present sur cette page
    *
    * @param $include string Lien href
    **/
    public function searchInclude($include = ""){
        $this->include = $include;
    }

    /**
    * Fonction detecteDate
    *
    * Detecte si format date anglais et effectue un formatage selon la langue
    *
    * @param date $date date format 0000-00-00
    * 
    **/
    private function detecteDate($date){
        if( substr($date,4,1) == "-" && substr($date,7,1) == "-" ){
            $split = preg_split("/-/",$date); 
            $annee = $split[0]; 
            $mois = $split[1]; 
            $jour = $split[2]; 
            switch($this->langue){
                case "francais":
                    return $jour."/".$mois."/".$annee; 
                    break;
                default:
                    return $date;
                    break;
            }
        }else{
            return $date;
        }
    }

    /**
    * Fonction reverseDate
    *
    * convertit la date 00/00/0000 en 0000-00-00 (compatibilite requete SQL)
    *
    * @param date $date date format 00/00/0000
    * 
    **/
    private function reverseDate($date){
        $value = $date;
        $date = str_replace("/","-",$date);
        $dateFormat = preg_split("/-/",$date);
        $nbcaract =  strlen($date);
        if( $this->langue == "francais" ){
            if( count($dateFormat) > 1 && strpos($date,"-") == 2  ){
                switch($nbcaract){
                    case "10": // 00/00/0000
                        $split = preg_split("/-/",$date); 
                        $jour = $split[0]; 
                        $mois = $split[1];
                        $annee = $split[2];
                        $value = $annee."-".$mois."-".$jour; 
                        break;
                    case "7": // 00/0000
                        $split = preg_split("/-/",$date); 
                        $mois = $split[0]; 
                        $annee = $split[1]; 
                        $value = $annee."-".$mois."-"; 
                        break;
                    case "5": // 00/00
                        $split = preg_split("/-/",$date);
                        $jour = $split[0];
                        $mois = $split[1];
                        $value = '-'.$mois.'-'.$jour;
                        break;

                }
            }
        }
        return $value;
    }

    /**
     * Méthode permettant de faire les traitements d'échappements
     * et de normalisation sur une chaîne destinée à la recherche
     *
     * @param string $value Chaîne recherchée à normaliser.
     *
     * @return string
     */
    function normalizeSearchValue($value){
        //
        $value = html_entity_decode($value, ENT_QUOTES);
        // échappement des caractères spéciaux
        $value=pg_escape_string($value);
        // encodage
        if (DBCHARSET != 'UTF8' and HTTPCHARSET == 'UTF-8') {
            $value = utf8_decode($value);
        }
        //
        return $value;
    }

    /**
     * Fonction showForm
     *
     *  Affiche le formulaire de recherche
     *
     *  @param $hidden bool cacher le formulaire (facultatif)
     *
     **/
    public function showForm( $hidden = false ){
        if( $this->formulaire ) return false;
        $recherche = "";
        $searchType = 0;
        $validation = 0;
        $largeSearch = "off";
        $temp = "";
        if( isset($_POST['recherche']) ) $recherche = $_POST['recherche'];
        $recherche = $this->f->clean_break($recherche);
        if( isset($_POST['searchType']) ) $searchType = $_POST['searchType'];
        if( isset($_POST['validation']) ) $validation = $_POST['validation'];
        if( isset($_POST['largeSearch']) ) $largeSearch = $_POST['largeSearch'];
        $cptsel = 0;
        foreach( $_POST as $POST => $POSTNAME ){
            if( substr($POST,0,4) == "temp" ){
                $temp[$cptsel] = $_POST[$POST];
                $cptsel++;
            }
        }
        // =============================================================================
        //// cacher le formulaire
        $style = "";
        if( $hidden ) $style = " style='display:none;' ";
        //// formulaire de recherche
        $this->formulaire = true;
        echo("<a name='tabRech'></a>");
        if( !$this->include) {
            echo("<form action='../app/recherche_obj.php#tabRech'  method='post'  id='rechercheFormModule' ".$style." >");
        }else{
            echo("<form action='".$this->include."#tabRech'  method='post'  id='rechercheFormModule' ".$style." >");
        }
        echo("<input type='hidden' name='validation' value='1' />");
        //////////////////////
        if( $validation ){
            echo("<input type='hidden' id='modRechOrder' name='order' value='' />");
            echo("<input type='hidden' id='modRechCurrentType' name='currentType' value='' />");
            echo("<input type='hidden' id='modRechOrderType' name='orderType' value='' />");
        }
        //////////////////////
        echo("<table  border='0' cellpadding='1'  cellspacing='1'>");
        echo("<tr valign='bottom'>");
        echo("<td><img src='../app/img/recherche_globale.png'  border='0' alt='".__("recherche_globale")."'  title='".__("recherche_globale")."' hspace='4'style='vertical-align:middle' /></td>");
        $selected = "";
        if( $largeSearch == "on") $selected = "checked";
        echo("<td><input name='largeSearch' type='checkbox' ".$selected." />".__("recherche_elargie")."<br/>");
        echo("<input type='text' name='recherche' value='".$recherche."' class='om-autofocus champFormulaire' /></td>");

         
        //// creation select depuis $configTypeRecherche si existant
        $cptsel = 0;
        foreach( $this->typeRecherche as $tRecherche){
                if( !empty($tRecherche['type']) && $tRecherche['type'] == "select" ){
                    echo("<td>&nbsp;".__($tRecherche['libelle'])."<br/><select name='temp".$cptsel."' class='champFormulaire' >");
                    $resultats = array();
                    $res = $this -> f -> db -> query($tRecherche['requete']);
                    if ($this->f->isDatabaseError($res, true) !== false) {
                        echo("Erreur SQL : ".$tRecherche['requete']."<br /><br /><br />");
                        $correct = false;
                    }else{
                        while($row=& $res->fetchRow())
                            array_push($resultats, $row);  
                    }
                    echo("<option value='*' >*</option>");
                    foreach( $resultats as $data ){
                        $selected = "";
                        if( isset($temp[$cptsel]) && $temp[$cptsel] == $data[$tRecherche['value']] ) {
                            $selected = " selected=\"selected\"";
                        }
                        echo("<option value='".$data[$tRecherche['value']]."' ".$selected."  >".$data[$tRecherche['list']]."</option>");
                    }
                    echo("</select></td>");
                    $cptsel++;
            }
        }

        echo("<td>&nbsp;".__('type_de_recherche')."<br/><select name='searchType' class='champFormulaire' >");
        $compteur = 0;
        /// select type de recherche
        foreach( $this->typeRecherche as $tRecherche){
            if( empty($tRecherche['type']) ){
                $selected = "";
                if( $compteur == $searchType ) {
                    $selected = " selected=\"selected\"";
                }
                echo("<option value='".$compteur."' ".$selected." >".$tRecherche['libelle']."</option>");
                $compteur++;
            }
        }
        echo("</select></td>");
        echo("<td>&nbsp;<br/><button class='boutonFormulaire' type='submit'>".__("Recherche")."</button></td>");
        echo("</tr></table></form><br/><br/>");
        // =============================================================================
    }

    /**
     * Fonction showForm
     *
     *  Affiche le formulaire de recherche
     *
     *  @param $hidden bool cacher le formulaire (facultatif)
     *
     **/
    public function showWidgetForm( $hidden = false ){
        //
        if( $this->formulaire ) return false;
        $recherche = "";
        $searchType = 0;
        $validation = 0;
        $largeSearch = "off";
        $temp = "";
        if( isset($_POST['recherche']) ) $recherche = $_POST['recherche'];
        if( isset($_POST['searchType']) ) $searchType = $_POST['searchType'];
        if( isset($_POST['validation']) ) $validation = $_POST['validation'];
        if( isset($_POST['largeSearch']) ) $largeSearch = $_POST['largeSearch'];
        $cptsel = 0;
        foreach( $_POST as $POST => $POSTNAME ){
            if( substr($POST,0,4) == "temp" ){
                $temp[$cptsel] = $_POST[$POST];
                $cptsel++;
            }
        }
        // =============================================================================
        //// cacher le formulaire
        $style = "";
        if( $hidden ) $style = " style='display:none;' ";
        //// formulaire de recherche
        $this->formulaire = true;
        echo("<a name='tabRech'></a>");
        if( !$this->include) {
            echo("<form action='../app/recherche_obj.php#tabRech'  method='post'  id='rechercheFormModule' ".$style." >");
        }else{
            echo("<form action='".$this->include."#tabRech'  method='post'  id='rechercheFormModule' ".$style." >");
        }
        echo("<input type='hidden' name='validation' value='1' />");
        //////////////////////
        if( $validation ){
            echo("<input type='hidden' id='modRechOrder' name='order' value='' >");
            echo("<input type='hidden' id='modRechCurrentType' name='currentType' value='' >");
            echo("<input type='hidden' id='modRechOrderType' name='orderType' value='' >");
        }
        //////////////////////

        echo "<div class=\"search-primary-field\">";
        //
        $selected = "";
        if( $largeSearch == "on") $selected = "checked";
        echo "<input name='largeSearch' type='checkbox' ".$selected." />".__("recherche_elargie");
        
        //
        echo "<br/>";
        //
        echo "<input type='text' name='recherche' value='".$recherche."' class='om-autofocus champFormulaire' />";

        //
        echo "</div>";

        //
        echo "<br/>";

        //// creation select depuis $configTypeRecherche si existant
        $cptsel = 0;
        foreach( $this->typeRecherche as $tRecherche){
                if( !empty($tRecherche['type']) && $tRecherche['type'] == "select" ){
                    echo("&nbsp;".__($tRecherche['libelle'])."<br/><select name='temp".$cptsel."' class='champFormulaire' >");
                    $resultats = array();
                    $res = $this -> f -> db -> query($tRecherche['requete']);
                    if ($this->f->isDatabaseError($res, true) !== false) {
                        echo("Erreur SQL : ".$tRecherche['requete']."<br /><br /><br />");
                        $correct = false;
                    }else{
                        while($row=& $res->fetchRow())
                            array_push($resultats, $row);  
                    }
                    echo("<option value='*' >*</option>");
                    foreach( $resultats as $data ){
                        $selected = "";
                        if(isset($temp[$cptsel]) && $temp[$cptsel] == $data[$tRecherche['value']]) {
                            $selected = " selected=\"selected\"";
                        }
                        echo("<option value=\"".$data[$tRecherche['value']]."\"".$selected.">".$data[$tRecherche['list']]."</option>");
                    }
                    echo("</select>");
                    $cptsel++;
            }
        }

        //
        echo "<br/>";

        //
        echo __('type_de_recherche')."<br/><select name='searchType' class='champFormulaire' >";
        $compteur = 0;
        /// select type de recherche
        foreach( $this->typeRecherche as $tRecherche){
            if( empty($tRecherche['type']) ){
                $selected = "";
                if( $compteur == $searchType ) {
                    $selected = " selected=\"selected\"";
                }
                echo("<option value=\"".$compteur."\"".$selected.">".$tRecherche['libelle']."</option>");
                $compteur++;
            }
        }
        echo("</select>");
        
        //
        echo "<br/>";
        echo "<br/>";
        echo "<button class='boutonFormulaire' type='submit'>".__("Recherche")."</button>";
        
        echo("</form>");
        // =============================================================================
    }
    
    /**
     * Fonction d'implementation du javascript
     *
     * Javascript : Renvoi le formulaire avec en argument le nom du champ a trier
     * 
     **/
    private function includeJS(){
        echo("
        <script type='text/javascript'>
            //////////// Recharge la page avec tri adequat (clic sur l'entete de la colonne)
            function rechercheReload(_arg1,_arg2,_arg3){
                document.getElementById('modRechOrder').value = 'ORDER BY ' + _arg1;
                document.getElementById('modRechCurrentType').value = _arg2;
                document.getElementById('modRechOrderType').value = _arg3;
        ");
        if( !$this->include ){
            echo("document.getElementById('rechercheFormModule').action = '../app/search.php#tab' + _arg2;");
        }else{
            echo("document.getElementById('rechercheFormModule').action = '".$this->include."#tab' + _arg2;");           
        }
        echo("
                document.getElementById('rechercheFormModule').submit();
            }
        </script>
        ");
    }

    /**
     * Fonction d'affichage des resultats
     *
     * Traitement de recherche et affichage des resultats
     *
     * Possibilite d'afficher une recherche par passage d'arguments sinon laisser vide
     * pour une recherche par le formulaire
     *
     * @param $recherche string Mot a rechercher (facultatif si automatique)
     * @param $searchType integer Type de recherche (facultatif si automatique)
     * @param $largeSearch off/on Active la recherche elargie
     * @param $orderby string Tri sur clic de l'entete de la colonne (facultatif si automatique)
     * @param $currentType integer (Si recherche global, tri sur clic de
     *                                  l'entete de la table adequat selon $orderby ) (facultatif si automatique)
     * @param $validation integer Active la recherche si = 1 et $recherche <> NULL (facultatif si automatique)
     **/
    public function showResults( $recherche = "", $searchType = 0, $largeSearch="off", $orderby = "", $currentType = 0, $validation = 0 ){
        if( $this-> affichage ) return false;
        $recherche = "";
        if (isset($_POST['recherche'])) {
            $recherche = preg_split("/;/", $_POST['recherche']);
            if ($recherche === false
                || (isset($recherche[0]) && $recherche[0] == "")) {
                //
                $this->f->displayMessage("error", __("Vous devez saisir au moins une valeur dans le champ recherche."));
            }
        }
        if( isset($_POST['validation']) ) $validation = $_POST['validation'];
        if ($recherche === ""
            || $recherche === false
            || (isset($recherche[0]) && $recherche[0] == "")
            || !$validation) {
            //
            return false;
        }
        if( isset($_POST['searchType']) ) $searchType = $_POST['searchType'];       
        if( isset($_POST['order']) ) $orderby = $_POST['order'];        
        if( isset($_POST['currentType']) ) $currentType = $_POST['currentType'];
        $orderType = "0";
        if( isset($_POST['orderType']) ) $orderType = $_POST['orderType'];
        if( isset($_POST['largeSearch']) ) $largeSearch = $_POST['largeSearch'];
        $temp = "";
        
        $cptsel = 0;
        foreach( $_POST as $POST => $POSTNAME ){
            if( substr($POST,0,4) == "temp" ){
                $temp[$cptsel] = $_POST[$POST];
                $cptsel++;
            }
        }
        //if( isset($_POST['temp1']) ) $temp = $_POST['temp1'];
        
        ///// MODE DEBUG
        if( $this->DEBUG ){
            echo("<br/>-----POST Variables-------<br/>");
            print_r($_POST);
            echo("<br/>--------------------------<br/>");
        }
        
        //// Si formulaire non existant, creation d'un formulaire cacher
        if( !$this->formulaire ){
            $this->showForm(true);
            $this->formulaire = true;
        }
        ///// insertion d'une fonction javascript pour triage par colonne (refresh)
        if( !$this->js ){
            $this->includeJS();
            $this->js = true; 
        }
        
        ///// Definition ordre de triage
        $orderTri = " asc ";
        if( $orderType ) $orderTri = " desc ";
        $this->affichage = true;
        ///// Recuperation des mots-clefs de recherche
        $num = 0;
        foreach( $recherche as $rech ){
           $recherche[$num] = $this->reverseDate($rech);
           $num++;
        }

        $num = 0;
        foreach ($this->typeRecherche as $tRecherche) {
            if( empty($tRecherche['type']) ){
                /////// Recuperation des champs et des variables pour la requete sql
                $correct = true;
                $champs = $tRecherche['champAffiche'];
                $table = $tRecherche['table'];
                $where = $tRecherche['selection'];
                $lien = strtolower($tRecherche['href']);
                $nomType = $tRecherche['libelle'];
                $tri = $tRecherche['tri'];
                $serie = $tRecherche['serie'];
                $champsRecherche = $tRecherche['champRecherche'];
                
                ///// methode recherche entre 2 dates
                $entreDate = false;
                if( isset($tRecherche['modeDate']) ){
                    $entreDate = $tRecherche['modeDate'];
                }
                if( count($recherche)<=1 ) $entreDate = false;
                
                ///// MODE DEBUG
                if( $this->DEBUG ){
                    echo("<br/>-----ConfigType Variables-------<br/>");
                    echo("\$correct = ".$correct."<br/>");
                    echo("\$champs = ".$champs."<br/>");
                    echo("\$table = ".$table."<br/>");
                    echo("\$where = ".$where."<br/>");
                    echo("\$lien = ".$lien."<br/>");
                    echo("\$nomType = ".$nomType."<br/>");
                    echo("\$tri = ".$tri."<br/>");
                    echo("\$serie = ".$serie."<br/>");
                    echo("\$champsRecherche = ".$champsRecherche."<br/>");
                    echo("\$entreDate = ".$entreDate."<br/>");
                    echo("count(\$recherche) = ".count($recherche)."<br/>");
                    echo("--------------------------<br/>");
                }
                
                ///// initialisation requete sql
                $requete = "";
                if( $searchType == $num || $this->typeRecherche[$searchType]['champAffiche'] == "#" ) {
                     if( $champs != "#" ){
                           ////// Construction de la requete (SELECT)
                           $requete = "SELECT ".implode(",",$champs)." ";
                           ////// Construction de la requete (FROM)
                           $requete .= " FROM ".$table;
                           if( $where ) $requete .= " ".$where;
                           ////// Construction de la requete (WHERE)
                           $cpt = 0;
                           foreach( $champsRecherche as $rech ){
                               if(! $cpt ){
                                   if( $where ){
                                       $requete .= " AND (";
                                   }else{
                                       $requete .= " WHERE (";
                                   }
                               }else{
                                   $requete .= " OR ";
                               }
                               $cpt2 = 0;
                               foreach( $recherche as $arRech ){
                                    if( $cpt2 ){
                                        if($entreDate){
                                            $requete .= " AND ";
                                        }else{
                                            $requete .= " OR ";                                            
                                        }
                                    }else{
                                        $requete .= " ( ";
                                    }
                                    ////// Si modeDate = true (Recherche entre 2 dates)
                                    if (!$entreDate) {
                                        //// mysql
                                        if( OM_DB_PHPTYPE == "mysql" )
                                            $requete .= " CAST(".$rech." AS CHAR(50)) £largeSearch '£recherche[".$cpt2."]' "; // CAST int/date
                                        //// pgsql
                                        if( OM_DB_PHPTYPE == "pgsql" ){
                                            if(CHARSET=='UTF8')
                                            $requete .= " LOWER(CAST(".$rech." AS CHAR(50))) £largeSearch LOWER('£recherche[".utf8_decode($cpt2)."]') "; // Translate accent / Cast
                                            else
                                            $requete .= " TRANSLATE(LOWER(CAST(".$rech." AS CHAR(50))),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY') £largeSearch LOWER('£recherche[".$cpt2."]') "; // Translate accent / Cast
                                        }
                                    }else{
                                        if( $cpt2 <= 1){
                                            $requete .= " ".$rech." £largeSearch".$cpt2." '£recherche[".$cpt2."]' ";
                                        }else{
                                            $requete .= " CAST(".$rech." AS CHAR(50)) £largeSearch '£recherche[".$cpt2."]' ";                                            
                                        }
                                    }
                                    $cpt2++;
                                }
                                $requete .= " ) ";
                                $cpt++;
                            }
                            if($cpt) $requete.= " ) ";
                            ////// Si modeDate = true (Recherche entre 2 dates)
                            if($entreDate){
                                $requete = str_replace('£largeSearch0',">=",$requete);
                                $requete = str_replace('£largeSearch1',"<=",$requete);
                            }
                            ////// Remplacement de £recherche par $search et £largeSearch 
                            $cpt2 = 0;
                            foreach( $recherche as $search){
                                $search = $this->f->clean_break($search);
                                $search = $this->normalizeSearchValue($search);
                                if($search == "*" && $entreDate) $search="0000-00-00";
                                if($search == "*" && !$entreDate){
                                     $requete = str_replace('£recherche['.$cpt2.']',"%",$requete);
                                     $requete = str_replace('£largeSearch',"like",$requete);
                                }else{             
                                    if($largeSearch=="on" && !$entreDate){
                                        $requete = str_replace('£recherche['.$cpt2.']',"%".$search."%",$requete);
                                        $requete = str_replace('£largeSearch',"like",$requete);
                                    }else{
                                        ///// mysql
                                        if( OM_DB_PHPTYPE == "mysql" )
                                            $requete = str_replace('£recherche['.$cpt2.']',$search,$requete);
                                        //// pgsql
                                        if( OM_DB_PHPTYPE == "pgsql" && $entreDate ){
                                            //// si date
                                            $dateFormat = preg_split("/-/",$search);
                                            if( (count($dateFormat) == 3 ) && strlen($search) == 10 ){
                                                $requete = str_replace("'£recherche[".$cpt2."]'","to_date('".$search."','YYYY-MM-DD')",$requete);
                                            }else{
                                                $requete = str_replace("'£recherche[".$cpt2."]'","NULL",$requete);                                                
                                            }
                                        }
                                        if( OM_DB_PHPTYPE == "pgsql" && !$entreDate )
                                            $requete = str_replace('£recherche['.$cpt2.']',$search,$requete);
                                        //// commun
                                        $requete = str_replace('£largeSearch',"=",$requete);
                                    }
                                }
                                $cpt2++;
                            }             
                          
                            /////////// Ajout de la condition du select depuis $configTypeRecherche si existant
                            $cptsel = 0;
                            foreach( $this->typeRecherche as $tSelect){
                                if( !empty($tSelect['type']) && $tSelect['type'] == "select" ){
                                    if( $temp[$cptsel]!="*" ) $requete .= " AND (CAST(".$tSelect['selection']." AS CHAR(50))='".$temp[$cptsel]."') ";
                                    $cptsel++;
                                }
                            }
                            
                            //////// VERIFICATION TRIAGE
                            ////// Construction de la requete - ORDER BY/GROUP BY
                            if($orderby && ($searchType==$num || $currentType==$num)){
                               $order = $orderby;
                            }else{
                               $order = "";
                            }
                            if( !$order ) $order = $tRecherche['tri'];
                            if( $order ) $requete .= " ".$order." ".$orderTri;
                          
                            ////// Construction de la requete - LIMIT
                            if( $serie ){
                                if( $serie != "*" ){
                                    $requete .= " LIMIT ".$serie." ";
                                }
                            }else{
                                $requete .= " LIMIT 100 ";
                            }

                            /////// creation des colonnes de la tables
                            $colonnes = $champs;
                            $nbcolonnes = count($colonnes);
                          
                            //// DEBUG /////
                                            
                            if($this->DEBUG){
                                echo("<br/>-----Requete SQL-------<br/>");
                                echo($requete."<br/>");
                            }
                                                      
                            ////// moteur de recherche et affichage des resultats
                            ///////// Execution de la requete
                            $resultats = array();
                            $res = $this -> f -> db -> query($requete);
                            if ($this->f->isDatabaseError($res, true) !== false) {
                                echo("Erreur SQL : ".$requete."<br /><br /><br />");
                                $correct = false;
                            }else{
                                while ($row=& $res->fetchRow())
                                    array_push($resultats, $row);  
                            }
                            if( $correct && count($resultats)>=1 ){
                                //{{{ affichages des resultats
                                $tableau = "<a name='tab".$num."'></a>\n";
                                $tableau .= "\n<table class='tabEntetenbr' border='0'>\n<tr><td><center>".$nomType." (".count($resultats).")</center></td></tr>\n</table>\n";
                                $tableau .= "\n<table class ='tab-tab'  >\n";
                                $tableau .= "<tr class ='ui-tabs-nav ui-accordion ui-state-default tab-title' >";
                                foreach( $colonnes as $c ){
                                    /////// Renommage du champs pour l'affichage dans l'entete de la colonne
                                    /////// si detection de AS dans la requete SQL
                                    $as = strpos(strtolower($c),' as ');
                                    $champ1 = $c; 
                                    $champ2 = $c;
                                    if($as){
                                        $split = preg_split("/ as /",strtolower($c)); 
                                        $champ1 = str_replace(' ','',$split[0]); 
                                        $champ2 = str_replace(' ','',$split[1]);
                                    }
                                    $tableau .= "<th class='title'>";
                                    if( strtolower($order) ==  "order by ".strtolower($champ1) ||  strtolower($order) ==  "order by ".strtolower($champ2) ||
                                        strtolower($order) ==  "group by ".strtolower($champ1) ||  strtolower($order) ==  "group by ".strtolower($champ2) ){
                                        if( !$orderType ){
                                            $tableau .= "<img alt=\"^\" vspace='2' hspace='3' border='0' src='../app/img/fleche_tri.png' />";
                                            $tableau .= "<a href=\"javascript:rechercheReload('".$champ2."',".$num.",1);\">".__($champ2)."</a></th>";
                                        }else{
                                            $tableau .= "<img alt=\"v\" vspace='2' hspace='3' border='0' src='../app/img/fleche_tri_reverse.png' />";
                                            $tableau .= "<a href=\"javascript:rechercheReload('".$champ2."',".$num.",0);\">".__($champ2)."</a></th>";    
                                        }
                                    }else{
                                        $tableau .= "<img alt=\">\" vspace='2' hspace='3' border='0' src='../app/img/fleche_nontri.png' />";
                                        $tableau .= "<a href=\"javascript:rechercheReload('".$champ2."',".$num.",0);\">".__($champ2)."</a></th>";
                                    }
                                }
                                $tableau .= "</tr>";
                                ////// TRAITEMENT DES RESULTATS
                                $numRow=0;
                                foreach( $resultats as $r ){
                                    $numRow++;
                                    if ($numRow&1){
                                        $tableau .= "<tr class ='tab-data odd' >";
                                    }else{
                                        $tableau .= "<tr class ='tab-data even' >";
                                    }
                                    
                                    $numCol = 0;
                                    $rlien = $lien;
                                    ////// MISE A JOUR DU LIEN HREF 
                                    if($lien){
                                        foreach( $colonnes as $c ){
                                            $as = strpos(strtolower($c),' as ');
                                            $champ1 = $c;
                                            $champ2 = "";
                                            if($as){
                                                $split = preg_split("/ as /",strtolower($c)); 
                                                $champ1 = str_replace(' ','',$split[0]);
                                                $champ2 = str_replace(' ','',$split[1]);
                                                $rlien = str_replace("£".$champ1,$r[$numCol],$rlien);
                                                $rlien = str_replace("£".$champ2,$r[$numCol],$rlien);
                                            }else{
                                                $rlien = str_replace("£".$champ1,$r[$numCol],$rlien);
                                            }
                                            $numCol++;
                                        }
                                    }
                                    ///////////// AFFICHAGE DES DONNEES
                                    $numCol = 0;     
                                    foreach( $colonnes as $c ){
                                        $r[$numCol] = $this -> detecteDate($r[$numCol]);
                                        if ($rlien) {
                                            $tableau .= "<td><a href=\"".$rlien."\">".$r[$numCol]."</a></td>";
                                        }else{
                                            $tableau .= "<td>".$r[$numCol]."</td>";              
                                        }
                                        $numCol++;
                                    }
                                    $tableau .= "</tr>";
                                }
                                $tableau .= "\n</table>\n<br /><br />";
                                echo($tableau);
                                // }}}
                            }else{
                                echo("\n<table class='tabEntetenbr' border='0' ><tr><td><center>".__("aucun_resultat")." ".$nomType.".</center></td></tr>\n</table>\n<br/><br/>");
                        }
                    }
                }
                $num++;
            }
        }
    }
}
