<?php
/**
 * Ce script définit la classe 'om_dbform'.
 *
 * @package opencimetiere
 * @version SVN : $Id: om_dbform.class.php 2045 2013-01-22 14:03:35Z jbastide $
 */

require_once PATH_OPENMAIRIE."om_dbform.class.php";

/**
 * Définition de la classe 'om_dbform' (om_dbform).
 *
 * Cette classe permet la surcharge de certaines méthodes de
 * la classe om_dbform du framework pour des besoins spécifiques
 * de l'application.
 */
class om_dbform extends dbForm {

    /**
     *
     */
    function get_values_substitution_vars($om_collectivite_idx = null) {
        //
        $values = parent::get_values_substitution_vars($om_collectivite_idx);
        //
        $values["aujourdhui"] = date("d/m/Y");
        $values["aujourdhui_lettre"] = strftime("%d %B %Y"); 
        //
        return $values;
    }

    /**
     *
     */
    function get_labels_substitution_vars($om_collectivite_idx = null) {
        //
        $labels = parent::get_labels_substitution_vars($om_collectivite_idx);
        //
        $labels["specifique"]["logo(id=identifiant-logo)"] = __("Intégration d'une image disponible dans le paramétrage des logos à partir de son identifiant");
        $labels["divers"]["aujourdhui"] = __("Date du jour (Format : 14/01/1978)");
        $labels["divers"]["aujourdhui_lettre"] = __("Date du jour (Format : 14 janvier 1978)");
        //
        return $labels;
    }

    /**
     * @return string
     */
    function get_om_etat_id() {
        return "cette_edition_n_existe_pas";
    }

    /**
     * VIEW - view_pdf_edition.
     *
     * @return void
     */
    function view_pdf_edition_etat() {
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "etat",
            $this->get_om_etat_id(),
            null,
            $this->getVal($this->clePrimaire)
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
        return;
    }
}
