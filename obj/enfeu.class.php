<?php
/**
 * Ce script définit la classe 'enfeu'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/concession.class.php";

/**
 * Définition de la classe 'enfeu' (om_dbform).
 *
 * Surcharge de la classe 'concession'.
 */
class enfeu extends concession {

    /**
     * @var string
     */
    protected $_absolute_class_name = "enfeu";

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "enfeu"
     */
    var $nature = "enfeu";

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //// Champs de la concession qui ne concernent pas un enfeu
        // Acte
        $form->setType('typeconcession', 'hidden');
        $form->setType('numerocadastre', 'hidden');
        // Bati
        $form->setType('sepulturetype', 'hidden');
        $form->setType('videsanitaire', 'hidden');
        $form->setType('semelle', 'hidden');
        $form->setType('etatsemelle', 'hidden');
        $form->setType('monument', 'hidden');
        $form->setType('etatmonument', 'hidden');
        $form->setType('largeur', 'hidden');
        $form->setType('profondeur', 'hidden');
        // Place
        $form->setType('nombreplace', 'hidden');
        $form->setType('superficie', 'hidden');
        $form->setType('placeconstat', 'hidden');
        $form->setType('dateconstat', 'hidden');
        // Abandon
        $form->setType('abandon', 'hidden');
        $form->setType('date_abandon', 'hidden');
    }

    /**
     * SETTER_FORM - setVal.
     *
     * @return void
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //parent::setVal($form, $maj, $validation);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('emplacement', "[...]");
                $form->setVal('terme', 'perpetuite');
                $form->setVal('duree', '0');
               $form->setVal('nature', $this->nature);
               $form->setVal('positionx', 10); // compatibilite localisation mozilla  [compatibility mozilla]
               $form->setVal('positiony', 10); // compatibilite localisation mozilla  [compatibility mozilla]
               $form->setVal('placeoccupe', 0);
            }
        }
    }
}
