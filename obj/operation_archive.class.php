<?php
/**
 * Ce script définit la classe 'operation_archive'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/operation_archive.class.php";

/**
 * Définition de la classe 'operation_archive' (om_dbform).
 */
class operation_archive extends operation_archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 001 - modifier
        // -> modification impossible
        unset($this->class_actions[1]);
        // ACTION - 002 - supprimer
        // -> suppression impossible
        unset($this->class_actions[2]);
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "operation_archive.operation",
            "numdossier",
            "date",
            "heure",
            "emplacement_archive.famille as emplacement",
            "societe_coordonnee",
            "pf_coordonnee",
            "etat",
            "categorie",
            "particulier",
            'CASE WHEN emplacement_transfert_archive.famille IS NOT NULL THEN emplacement_transfert_archive.famille ELSE emplacement_transfert.famille END  as "'.__("emplacement_transfert").'"',
            "operation_archive.observation",
            "consigne_acces",
            "prive",
            "consigne_acces_transfert",
            "edition_operation",
        );
    }


    /**
     * GETTER_FORMINC - tableSelect.
     *
     * @return string
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
                '%1$soperation_archive 
                    INNER JOIN %1$semplacement_archive on operation_archive.emplacement = emplacement_archive.emplacement
                    INNER JOIN %1$soperation_defunt_archive on operation_archive.operation = operation_defunt_archive.operation 
                    LEFT JOIN %1$semplacement as emplacement_transfert on operation_archive.emplacement_transfert = emplacement_transfert.emplacement
                    LEFT JOIN %1$semplacement_archive as emplacement_transfert_archive on operation_archive.emplacement_transfert = emplacement_transfert_archive.emplacement',
                DB_PREFIXE);
    }
}
