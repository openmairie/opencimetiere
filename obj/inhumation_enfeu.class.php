<?php
/**
 * Ce script définit la classe 'inhumation_enfeu'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/inhumation.class.php";

/**
 * Définition de la classe 'inhumation_enfeu' (om_dbform).
 *
 * Surcharge de la classe 'inhumation'.
 */
class inhumation_enfeu extends inhumation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "inhumation_enfeu";

    /**
     * @var string
     */
    var $emplacement_nature = 'enfeu';
}
