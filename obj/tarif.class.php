<?php
/**
 * Ce script définit la classe 'tarif'.
 *
 * @package opencimetiere
 * @version SVN : $Id: tarif.class.php 812 2018-10-21 18:25:18Z fmichon $
 */

require_once "../gen/obj/tarif.class.php";

/**
 * Définition de la classe 'tarif' (om_dbform).
 */
class tarif extends tarif_gen {

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj == 0) {
            $form->setType('origine', 'select');
        } else {
            $form->setType('origine', 'hiddenstatic');
        }
        if ($maj == 0 || $maj == 1) {
            $form->setType('monnaie', 'select');
            $form->setType('terme', 'select');
            $form->setType('nature', 'select');
        } else {
            $form->setType('terme', 'selecthiddenstatic');
            $form->setType('monnaie', 'selecthiddenstatic');
            $form->setType('nature', 'selecthiddenstatic');
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // MODE AJOUTER & MODE MODIFIER
        // nature
        $contenu = array();
        $contenu [0] = array ("", "concession", "colombarium", "enfeu", "terraincommunal", );
        $contenu [1] = array ("", "concession", "colombarium", "enfeu", "terraincommunal", );
        $form->setSelect("nature", $contenu);
        // origine
        $contenu = array();
        $contenu [0] = array ("", "achat", "renouvellement",);
        $contenu [1] = array (__("Choisir origine"), __("Achat"), __("Renouvellement"));
        $form->setSelect("origine", $contenu);
        // Terme
        $contenu = array ();
        $contenu[0]=array('',"perpetuite",'temporaire');
        $contenu[1]=array('',__("perpetuite"),__("temporaire"));
        $form->setSelect("terme", $contenu);
        // monnaie
        $contenu = array ();
        $contenu[0]=array("euro", "franc", "ancien_franc");
        $contenu[1]=array("Euros", "Francs", "Anciens Francs");
        $form->setSelect("monnaie", $contenu);
    }

    /**
     * SETTER_FORM - setMax.
     *
     * @return void
     */
    function setMax(&$form, $maj) {
        parent::setMax($form, $maj);
        //
        $form->setMax("montant", 12);
    }
}
