<?php
/**
 * Ce script définit la classe 'dossier_archive'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier_archive.class.php";

/**
 * Définition de la classe 'dossier_archive' (om_dbform).
 */
class dossier_archive extends dossier_archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 001 - modifier
        // -> modification impossible
        unset($this->class_actions[1]);
        // ACTION - 002 - supprimer
        // -> suppression impossible
        unset($this->class_actions[2]);
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $form->setType('emplacement', 'hidden');
        if ($maj == 3) {
            $form->setType('fichier', 'file');
        } else {
            $form->setType('fichier', 'filestatic');
        }
    }
}
