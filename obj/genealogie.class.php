<?php
//$Id$ 
//gen openMairie le 15/09/2022 17:21

require_once "../gen/obj/genealogie.class.php";

class genealogie extends genealogie_gen {
    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "genealogie",
            "emplacement",
            "autorisation_p1",
            "autorisation_p2",
            "defunt_p1",
            "defunt_p2",
            "personne_1",
            "lien_parente",
            "personne_2",
            "commentaire",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt() {
        // On veut sélectionner seulement les défunts de l'emplacement sur lequel on se trouve, on récupère donc l'idxformulaire
        return "
            SELECT 
                defunt.defunt, CONCAT_WS(' ', defunt.defunt, defunt.nom, defunt.marital, defunt.prenom, 'defunt')
            FROM 
                ".DB_PREFIXE."defunt 
                    INNER JOIN ".DB_PREFIXE."emplacement 
                        ON defunt.emplacement = emplacement.emplacement 
                        AND emplacement.emplacement = ".intval($this->f->get_submitted_get_value('idxformulaire'))." 
            ORDER BY defunt.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_by_id() {
        return "
            SELECT
                defunt.defunt, CONCAT_WS(' ', defunt.defunt, defunt.nom, defunt.marital, defunt.prenom, 'defunt') as libelle 
            FROM
                ".DB_PREFIXE."defunt
            WHERE 
                defunt = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation() {
        return "
            SELECT 
                autorisation.autorisation, CONCAT_WS(' ', autorisation.autorisation, autorisation.nom, autorisation.marital, autorisation.prenom, 'contact') 
            FROM 
                ".DB_PREFIXE."autorisation 
                    INNER JOIN ".DB_PREFIXE."emplacement 
                        ON autorisation.emplacement = emplacement.emplacement 
                        AND emplacement.emplacement = ".intval($this->f->get_submitted_get_value('idxformulaire'))." 
            ORDER BY autorisation.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_by_id() {
        return "
            SELECT 
                autorisation.autorisation, CONCAT_WS(' ', autorisation.autorisation, autorisation.nom, autorisation.marital, autorisation.prenom, 'contact') as libelle 
            FROM 
                ".DB_PREFIXE."autorisation 
            WHERE 
                autorisation = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p1() {
        return $this->get_var_sql_forminc__sql_autorisation();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p1_by_id() {
        return $this->get_var_sql_forminc__sql_autorisation_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p2() {
        return $this->get_var_sql_forminc__sql_autorisation();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p2_by_id() {
        return $this->get_var_sql_forminc__sql_autorisation_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p1() {
        return $this->get_var_sql_forminc__sql_defunt();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p1_by_id() {
        return $this->get_var_sql_forminc__sql_defunt_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p2() {
        return $this->get_var_sql_forminc__sql_defunt();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p2_by_id() {
        return $this->get_var_sql_forminc__sql_defunt_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_lien_parente() {
        return "SELECT lien_parente.lien_parente, CONCAT_WS(' -> ', lien_parente.libelle, lien_parente.lien_inverse) as libelle FROM ".DB_PREFIXE."lien_parente ORDER BY lien_parente.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_lien_parente_by_id() {
        return "SELECT lien_parente.lien_parente, CONCAT_WS(' -> ', lien_parente.libelle, lien_parente.lien_inverse) as libelle FROM ".DB_PREFIXE."lien_parente WHERE lien_parente = <idx>";
    }


    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        
        // Les champ defunt_p1 et autorisation_p1 sont saisis en fonction de
        // la valeur choisie dans le champ personne_1. 
        // Si les deux champs sont vides alors on renvoie une erreur
        if ($val['defunt_p1'] == '' && $val['autorisation_p1'] == '' ) {
            $this->correct = false;
            $this->addToMessage(__("Le champ personne 1 est obligatoire"));
        }

        // Les champ defunt_p2 et autorisation_p2 sont saisis en fonction de
        // la valeur choisie dans le champ personne_2. 
        // Si les deux champs sont vides alors on renvoie une erreur.
        if ($val['defunt_p2'] == '' && $val['autorisation_p2'] == '' ) {
            $this->correct = false;
            $this->addToMessage(__("Le champ personne 2 est obligatoire"));
        }
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        
        $form->setType('emplacement', 'hidden');
        // En consultation et suppression
        if ($maj >= 2) {
            $form->setType('personne_1', 'selectstatic');
            $form->setType('personne_2', 'selectstatic');
            // On masque les champs autorisation et defunt p1/p2
            $form->setType('autorisation_p1', 'hidden');
            $form->setType('autorisation_p2', 'hidden');
            $form->setType('defunt_p1', 'hidden');
            $form->setType('defunt_p2', 'hidden');

        }
        // Ajout et modification
        if ($maj < 2) {
            $form->setType('personne_1', 'select');
            $form->setType('personne_2', 'select');
            // Les champs défunt et autorisation sont masqués en js lors de l'ajout et de la modification
        }
    }

    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        
        // En ajout et modification le champ sélect est modifié
        // avec du js pour récupérer les défunts et les contacts disponibles
        if ($maj <= 1) {
            $contenu=array();
            $contenu[0]=array('');
            $contenu[1]=array('choisir personne_1');
            $form->setSelect("personne_1", $contenu);
            $contenu[0]=array('');
            $contenu[1]=array('choisir personne_2');
            $form->setSelect("personne_2", $contenu);
        }
        // En consultation et suppression
        if ($maj >= 2) {
            // En fonction de la valeur d'autorisation_p1 et defunt_p1
            // on récupère la requête sql à lancer pour récupérer la personne
            if ($this->getVal('autorisation_p1') !== '') {
                $table = 'autorisation';
                $libelle_from_select = str_replace(
                    '<idx>',
                    $this->getVal('autorisation_p1'),
                    $this->get_var_sql_forminc__sql_autorisation_p1_by_id()
                );
            }
            if ($this->getVal('defunt_p1') !== '') {
                $table = 'defunt';
                $libelle_from_select = str_replace(
                    '<idx>',
                    $this->getVal('defunt_p1'),
                    $this->get_var_sql_forminc__sql_defunt_p1_by_id()
                ); 
            }
            // On lance la requête sql get_all_results qui permet
            // de récupérer l'identifiant et le libelle de la personne sélectionnée
            $resq = $this->f->get_all_results_from_db_query(
                $libelle_from_select
            );
            // On construit le tableau et on l'ajoute
            // au champ personne_1
            $contenu=array();
            $contenu[0][]=$resq['result'][0][$table];
            $contenu[1][]=$resq['result'][0]['libelle'];
            $form->setSelect("personne_1", $contenu);

            // De même pour personne_2
            if ($this->getVal('autorisation_p2') !== '') {
                $table = 'autorisation';
                $libelle_from_select = str_replace(
                    '<idx>',
                    $this->getVal('autorisation_p2'),
                    $this->get_var_sql_forminc__sql_autorisation_p2_by_id()
                );
            }
            if ($this->getVal('defunt_p2') !== '') {
                $table = 'defunt';
                $libelle_from_select = str_replace(
                    '<idx>',
                    $this->getVal('defunt_p2'),
                    $this->get_var_sql_forminc__sql_defunt_p2_by_id()
                ); 
            }
            $resq = $this->f->get_all_results_from_db_query(
                $libelle_from_select
            );
            $contenu=array();
            $contenu[0][]=$resq['result'][0][$table];
            $contenu[1][]=$resq['result'][0]['libelle'];
            $form->setSelect("personne_2", $contenu);
        }
    }

    function setOnchange(&$form, $maj) {
        $form->setOnchange('genealogie','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('autorisation_p1','VerifNum(this)');
        $form->setOnchange('defunt_p1','VerifNum(this)');
        // Pas de besoin de VérifNum pour personne_1 et personne_2
        // On ajoute la fonctionn permettant de gérer les valeurs de personne 1/2
        $form->setOnchange('personne_1','manage_personne()');
        $form->setOnchange('personne_2','manage_personne()');
        $form->setOnchange('lien_parente','VerifNum(this)');
        $form->setOnchange('autorisation_p2','VerifNum(this)');
        $form->setOnchange('defunt_p2','VerifNum(this)');
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('genealogie', "[...]");
                $form->setVal('emplacement', $idxformulaire);
            }
        }
        // En consultation et suppression
        if ($maj>=2) {
            if ($this->getVal('autorisation_p1') !== '') {
                $form->setVal('personne_1', $this->getVal('autorisation_p1'));
            }
            if ($this->getVal('defunt_p1') !== '') {
                $form->setVal('personne_1', $this->getVal('defunt_p1'));
            }

            if ($this->getVal('autorisation_p2') !== '') {
                $form->setVal('personne_2', $this->getVal('autorisation_p2'));
            }
            if ($this->getVal('defunt_p2') !== '') {
                $form->setVal('personne_2', $this->getVal('defunt_p2'));
            }
        }
    }
}
