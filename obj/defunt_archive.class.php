<?php
/**
 * Ce script définit la classe 'defunt_archive'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/defunt_archive.class.php";

/**
 * Définition de la classe 'defunt_archive' (om_dbform).
 */
class defunt_archive extends defunt_archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 001 - modifier
        // -> modification impossible
        unset($this->class_actions[1]);
        // ACTION - 002 - supprimer
        // -> suppression impossible
        unset($this->class_actions[2]);
    }
}
