<?php
/**
 * Ce script définit la classe 'zone'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/zone.class.php";

/**
 * Définition de la classe 'zone' (om_dbform).
 */
class zone extends zone_gen {

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib("zonelib", __("libelle"));
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Gestion du champs geom
        $form->setType('geom', 'hidden');
        if ($this->f->getParameter("option_localisation") == "sig_interne") {
            if ($maj == 0 || $maj == 1 || $maj == 3) {
                $form->setType('geom', 'geom');
            }
        }
    }
}
