<?php
/**
 * Ce script définit la classe 'terraincommunal'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/emplacement.class.php";

/**
 * Définition de la classe 'terraincommunal' (om_dbform).
 *
 * Surcharge de la classe 'emplacement'.
 */
class terraincommunal extends emplacement {

    /**
     * @var string
     */
    protected $_absolute_class_name = "terraincommunal";

    /**
     *
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        // Récupération des paramètres nécessaires aux calculs de taille et de
        // durée
        $this->init_parameters();
    }

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "terraincommunal"
     */
    var $nature = "terraincommunal";

    function init_parameters() {
        //
        $default_duree_terraincommunal = 5;
        $this->duree_terraincommunal = $this->f->getParameter("duree_terraincommunal");
        if (is_null($this->duree_terraincommunal)) {
            $this->duree_terraincommunal = $default_duree_terraincommunal;
        }
        //
        $default_superficie_terraincommunal = 2;
        $this->superficie_terraincommunal = $this->f->getParameter("superficie_terraincommunal");
        if (is_null($this->superficie_terraincommunal)) {
            $this->superficie_terraincommunal = $default_superficie_terraincommunal;
        }
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Localisation
        $form->setType('geom', 'hidden');
        $form->setType('pgeom', 'hidden');
        $form->setType('plans', 'hidden');
        $form->setType('positionx', 'hidden');
        $form->setType('positiony', 'hidden');
        // Champs paramétrables
        $form->setType('temp1', 'hidden');
        $form->setType('temp2', 'hidden');
        $form->setType('temp3', 'hidden');
        $form->setType('temp4', 'hidden');
        $form->setType('temp5', 'hidden');
        //// Champs de l'emplacement qui ne concernent pas un terrain communal
        // Acte
        $form->setType('typeconcession', 'hidden');
        $form->setType('numerocadastre', 'hidden');
        $form->setType('numeroacte', 'hidden');
        $form->setType('daterenouvellement', 'hidden');
        // Bati
        $form->setType('sepulturetype', 'hidden');
        $form->setType('videsanitaire', 'hidden');
        $form->setType('semelle', 'hidden');
        $form->setType('etatsemelle', 'hidden');
        $form->setType('monument', 'hidden');
        $form->setType('etatmonument', 'hidden');
        $form->setType('largeur', 'hidden');
        $form->setType('profondeur', 'hidden');
        // Abandon
        $form->setType('abandon', 'hidden');
        $form->setType('date_abandon', 'hidden');
        // XXX Ce champ est à supprimer de l'application
        $form->setType('photo','hidden');
        ////////
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Id
            $form->setType('emplacement', 'hiddenstatic');
            $form->setType('nature', 'hiddenstatic');
            // Libre
            $form->setType('libre', 'select');
            // Adresse
            $form->setType('complement', 'select');
            $form->setType('voie', 'autocomplete');
            // Localisation
            if ($this->f->getParameter("option_localisation") == "plan") {
                $form->setType('plans', 'select');
                $form->setType('positionx', 'text');
                $form->setType('positiony', 'localisation_plan');
            } elseif ($this->f->getParameter("option_localisation") == "sig_interne") {
                $form->setType('geom', 'geom');
            } elseif ($this->f->getParameter("option_localisation") == "sig_externe") {
                
            }
            // Acte
            $form->setType('terme', 'hiddenstatic');
            $form->setType('duree', 'hiddenstatic');
            // Place
            $form->setType('nombreplace', 'hidden');
            $form->setType('placeoccupe','static');
            $form->setType('superficie','text');
            $form->setType('placeconstat', 'hidden');
            $form->setType('dateconstat', 'hidden');
        }
        //
        if ($maj == 3) {
            // Localisation
            if ($this->f->getParameter("option_localisation") == "plan") {
                $form->setType('plans', 'localisation_plan_static');
                $form->setType('positionx', 'hidden');
                $form->setType('positiony', 'hidden');
            } elseif ($this->f->getParameter("option_localisation") == "sig_interne") {
                
            } elseif ($this->f->getParameter("option_localisation") == "sig_externe") {
                
            }
        }
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // verification donnees real non vide (GI/compatibilite postgresql)
        // Check data real not empty (compatibility postgresql)
        if ($val['duree']=="" or is_null($val['duree']))
            $this->valF['duree'] = 0;
        if ($val['placeoccupe']=="" or is_null($val['placeoccupe']))
            $this->valF['placeoccupe'] = 0;
        if ($val['nombreplace']=="" or is_null($val['nombreplace']))
            $this->valF['nombreplace'] = 0;
        if ($val['superficie']=="" or is_null($val['superficie']))
            $this->valF['superficie'] = 0;
        if ($val['placeconstat']=="" or is_null($val['placeconstat']))
            $this->valF['placeconstat'] = 0;
        if($this->correct)
            $this->verifierVoie($val);
    }

    /**
     * SETTER_FORM - setVal.
     *
     * @return void
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('terme', 'temporaire');
                $form->setVal('duree', $this->duree_terraincommunal);
                $form->setVal('superficie', $this->superficie_terraincommunal);
            }
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Localisation
            $contenu = array();
            $contenu[0] = array('plans', 'positionx');
            $form->setSelect("positiony", $contenu);
            // Libre
            $contenu = array();
            $contenu[0] = array('Non', 'Oui');
            $contenu[1] = array(__("Non"), __("Oui"));
            $form->setSelect("libre", $contenu);
        }
    }
}
