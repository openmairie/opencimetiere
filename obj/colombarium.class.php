<?php
/**
 * Ce script définit la classe 'colombarium'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/concession.class.php";

/**
 * Définition de la classe 'colombarium' (om_dbform).
 *
 * Surcharge de la classe 'concession'.
 */
class colombarium extends concession {

    /**
     * @var string
     */
    protected $_absolute_class_name = "colombarium";

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "colombarium"
     */
    var $nature = "colombarium";

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //// Champs de la concession qui ne concernent pas un colombarium
        // Bati
        $form->setType('sepulturetype', 'hidden');
        $form->setType('videsanitaire', 'hidden');
        $form->setType('semelle', 'hidden');
        $form->setType('etatsemelle', 'hidden');
        $form->setType('monument', 'hidden');
        $form->setType('etatmonument', 'hidden');
        $form->setType('largeur', 'hidden');
        $form->setType('profondeur', 'hidden');
        // Abandon
        if ($maj == 0 || $maj == 1) {
            $form->setType('abandon', 'select');
            $form->setType('date_abandon', 'date');
        }
        ////////
    }


    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        if ($maj == 0 || $maj == 1) {
            // Abandon
            $contenu = array();
            $contenu[0] = array('Non', 'Oui');
            $contenu[1] = array(__("Non"), __("Oui"));
            
            $form->setSelect("abandon",$contenu);
        }
    }
}
