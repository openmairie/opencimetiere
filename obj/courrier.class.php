<?php
/**
 * Ce script définit la classe 'courrier'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/courrier.class.php";

/**
 * Définition de la classe 'courrier' (om_dbform).
 */
class courrier extends courrier_gen {

    /**
     *
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        // XXX Modifier la base de données en conséquence ?
        $this->required_field[] = "lettretype";
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 031 - edition pdf
        // Retourne le pdf généré
        $this->class_actions[31] = array(
            "identifier" => "pdf-edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("édition pdf"),
                "order" => 120,
                "class" => "pdf-16",
            ),
            "view" => "view_pdf_edition",
            "permission_suffix" => "pdf_edition",
        );
    }

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "courrier",
            "emplacement",
            "destinataire",
            "datecourrier",
            "lettretype",
            "complement",
            "contrat",
        );
    }

    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return _("id")." ".$this->getVal($this->clePrimaire)." - ".$this->getVal("lettretype")." du ".$this->dateDBToForm($this->getVal("datecourrier"));
    }

    /**
     * GETTER_FORMINC - sql_contrat.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrat() {
        // Filtre des destinataires par emplacement
        $filter = "";
        if ($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire)) {
            $filter = " WHERE emplacement=".intval($this->getParameter("idxformulaire"))." ";
        }
        if($this->is_in_context_of_foreign_key('contrat', $this->retourformulaire)) {
            $filter = " WHERE contrat=".intval($this->getParameter("idxformulaire"))." ";
        }
        return "SELECT contrat.contrat, contrat.origine||' du '||to_char(contrat.datedemande, 'DD/MM/YYYY') as lib FROM ".DB_PREFIXE."contrat ".$filter." ORDER BY contrat";
    }

    /**
     * GETTER_FORMINC - sql_contrat_by_id.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrat_by_id() {
        //
        return "SELECT contrat.contrat, contrat.origine||' du '||to_char(contrat.datedemande, 'DD/MM/YYYY') as lib FROM ".DB_PREFIXE."contrat WHERE contrat = <idx>";
    }

    /**
     * GETTER_FORMINC - sql_destinataire.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_destinataire() {
        // Filtre des destinataires par emplacement
        $filter = "";
        if ($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire)) {
            $filter = " WHERE emplacement=".intval($this->getParameter("idxformulaire"))." ";
        }
        if($this->is_in_context_of_foreign_key('contrat', $this->retourformulaire)) {
            $inst_contrat = $this->f->get_inst__om_dbform(array(
                "obj" => "contrat",
                "idx" => $this->getParameter("idxformulaire"),
            ));
            if ($inst_contrat->exists() === true) {
                $filter = " WHERE emplacement=".intval($inst_contrat->getVal("emplacement"))." ";
            }
        }
        return sprintf(
            'SELECT
                autorisation.autorisation,
                \'(\'||autorisation.nature||\') \'||coalesce(nom, \'-\')||\' \'||coalesce(prenom, \'-\') as nom
            FROM
                %1$sautorisation
            %2$s
            ORDER BY
                nom',
            DB_PREFIXE,
            $filter
        );
    }

    /**
     * GETTER_FORMINC - sql_om_lettretype.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_lettretype() {
        return "SELECT om_lettretype.id, (om_lettretype.id||' '||om_lettretype.libelle) as libelle FROM ".DB_PREFIXE."om_lettretype WHERE om_lettretype.actif is TRUE ORDER BY id";
    }

    /**
     * GETTER_FORMINC - sql_om_lettretype_by_id.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_lettretype_by_id() {
        return "SELECT om_lettretype.id, (om_lettretype.id||' '||om_lettretype.libelle) as libelle FROM ".DB_PREFIXE."om_lettretype WHERE om_lettretype.id='<idx>'";
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Affichage de l'id de la table dans tous les cas
        $form->setType('courrier', 'hiddenstatic');
        // On cache l'id de l'emplacement dans tous les cas
        $form->setType('emplacement', 'hidden');
        //
        if ($maj == 0 || $maj == 1) {
            $form->setType('lettretype', 'select');
            $form->setType('complement', 'html');
            
        }
        //
        if ($maj == 3) {
            $form->setType('lettretype', 'selectstatic');
        }
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation == 0) {
            //
            if ($maj == 0) {
                // Affichage de l'id de la table avec la valeur [...]
                $form->setVal('courrier', "[...]");
                //
                $form->setVal('datecourrier', date('Y-m-d'));
            }
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "lettretype",
            $this->get_var_sql_forminc__sql("om_lettretype"),
            $this->get_var_sql_forminc__sql("om_lettretype_by_id"),
            false
        );
    }

    /**
     * SETTER_FORM - setValF.
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // Si on est dans le context d'un contrat alors on récupère l'emplacemente associé à ce contrat
        if($this->is_in_context_of_foreign_key('contrat', $this->retourformulaire)) {
            $inst_contrat = $this->f->get_inst__om_dbform(array(
                "obj" => "contrat",
                "idx" => $this->getParameter("idxformulaire"),
            ));
            $this->valF["emplacement"] = $inst_contrat->getVal("emplacement");
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $bloc_global_width_full = "span12";
        $bloc_global_width_half = "span6";

        //// CONTAINER GLOBAL - START
        $this->form->setBloc("courrier", "D", "", "emplacement-form travaux-form form-action-".$maj);

        //// LIGNE ID - START
        $this->form->setBloc("courrier", "D", "", "row-fluid");
            //// BLOC ID - START
            $this->form->setBloc("courrier", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-identification ");
                //// BLOC ID - START
                $this->form->setBloc("courrier", "D", "", "span12");
                $this->form->setBloc("emplacement", "F");
                //// BLOC ID - END
            $this->form->setBloc("emplacement", "F");
            //// BLOC ID - END
        $this->form->setBloc("emplacement", "F");
        //// LIGNE ID - END

        //// CONTAINER GLOBAL - END
        $this->form->setBloc("complement", "F");
    }

    /**
     * VIEW - view_pdf_edition.
     *
     * @return void
     */
    function view_pdf_edition() {
        $this->checkAccessibility();
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output(
            "lettretype",
            $this->getVal("lettretype"),
            null,
            $this->getVal($this->clePrimaire)
        );
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition['pdf_output'],
            $pdfedition['filename']
        );
    }

    /**
     * MERGE_FIELDS - merge_fields_to_avoid_obj.
     * @var array
     */
    var $merge_fields_to_avoid_obj = array(
        "emplacement",
    );

    /**
     * MERGE_FIELDS - get_labels_merge_fields.
     *
     * @return array
     */
    function get_labels_merge_fields() {
        $labels = parent::get_labels_merge_fields();
        //
        $date_fields_to_format = array("datecourrier", );
        foreach ($date_fields_to_format as $date_field_to_format) {
            $labels[$this->table][$this->table.".".$date_field_to_format."_format_jj_mois_aaaa"] = $labels[$this->table][$this->table.".".$date_field_to_format]." (Format : 14 janvier 1978)";
        }
        //
        return $labels;
    }

    /**
     * MERGE_FIELDS - get_values_merge_fields.
     *
     * @return array
     */
    function get_values_merge_fields() {
        $values = parent::get_values_merge_fields();
        //
        $date_fields_to_format = array("datecourrier", );
        foreach ($date_fields_to_format as $date_field_to_format) {
            $values[$this->table.".".$date_field_to_format."_format_jj_mois_aaaa"] = '';
            $date_value_as_string = $this->getVal($date_field_to_format);
            if (!empty($date_value_as_string)
                && preg_match('|^[0-9]{4}-[0-9]{2}-[0-9]{2}$|', $date_value_as_string)) {
                //
                $date_value_as_object = DateTime::createFromFormat('Y-m-d', $date_value_as_string);
                $values[$this->table.".".$date_field_to_format."_format_jj_mois_aaaa"] = strftime('%d %B %Y', $date_value_as_object->getTimestamp());
            }
        }
        //
        return $values;
    }

    /**
     * MERGE_FIELDS - get_all_merge_fields.
     *
     * @return array
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "emplacement" => "emplacement",
            "autorisation" => "destinataire",
            "contrat" => "contrat",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = $this->get_values_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => $this->getVal($field),
                    ));
                    $elem_values = $elem->get_merge_fields($type);
                    $values = array_merge($values, $elem_values);
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = $this->get_labels_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => 0,
                    ));
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    /**
     * MERGE_FIELDS - get_values_legacy_merge_fields.
     *
     * @return array
     */
    function get_values_legacy_merge_fields() {
        $query = sprintf(
            'SELECT
                to_char(courrier.datecourrier, \'DD/MM/YYYY\') as datecourrier,
                courrier.complement,
                titre_de_civilite.code as titre,
                autorisation.nom, 
                autorisation.prenom, 
                autorisation.marital,
                autorisation.adresse1,
                autorisation.adresse2,
                autorisation.cp,
                autorisation.ville,
                autorisation.emplacement,
                autorisation.nature as autorisation_nature,
                emplacement.nature,
                emplacement.superficie,
                emplacement.numero as emplacement_adresse_numero,
                emplacement.complement as emplacement_adresse_complement,
                emplacement.typeconcession as typeconcession,
                voie_type.libelle as type_de_voie,
                voielib,
                zone_type.libelle as type_de_zone,
                zonelib,
                emplacement.terme, 
                emplacement.numeroacte,
                to_char(emplacement.dateterme,\'DD/MM/YYYY\') as dateterme,
                to_char(emplacement.datevente,\'DD/MM/YYYY\') as datevente,
                to_char(emplacement.daterenouvellement,\'DD/MM/YYYY\') as daterenouvellement,
                emplacement.famille,
                emplacement.duree,
                emplacement.placeconstat,
                cimetierelib

            FROM
                %1$scourrier
                LEFT JOIN %1$sautorisation on courrier.destinataire = autorisation.autorisation
                LEFT JOIN %1$stitre_de_civilite on autorisation.titre = titre_de_civilite.titre_de_civilite
                LEFT JOIN %1$semplacement on courrier.emplacement = emplacement.emplacement
                LEFT JOIN %1$svoie on emplacement.voie = voie.voie
                LEFT JOIN %1$svoie_type on voie.voietype = voie_type.voie_type
                LEFT JOIN %1$szone on voie.zone = zone.zone
                LEFT JOIN %1$szone_type on zone.zonetype = zone_type.zone_type
                LEFT JOIN %1$scimetiere on zone.cimetiere = cimetiere.cimetiere

            WHERE
                courrier.courrier = %2$s',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $res = $this->f->db->query($query);
        $this->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            return $row;
        }
        return array();
    }

    /**
     * MERGE_FIELDS - get_labels_legacy_merge_fields.
     *
     * @return array
     */
    function get_labels_legacy_merge_fields() {
        return array(
            "legacy" => array(
                //
                "datecourrier" => "",
                "complement" => "",
                //
                "titre" => "",
                "nom" => "",
                "prenom" => "",
                "marital" => "",
                "adresse1" => "",
                "adresse2" => "",
                "cp" => "",
                "ville" => "",
                "autorisation_nature" => "",
                //
                "emplacement" => "",
                "nature" => "",
                "superficie" => "",
                "emplacement_adresse_numero" => "",
                "emplacement_adresse_complement" => "",
                "typeconcession" => "",
                "terme" => "",
                "numeroacte" => "",
                "dateterme" => "",
                "datevente" => "",
                "daterenouvellement" => "",
                "famille" => "",
                "duree" => "",
                "placeconstat" => "",
                //
                "type_de_voie" => "",
                "voielib" => "",
                //
                "type_de_zone" => "",
                "zonelib" => "",
                //
                "cimetierelib" => "",
            ),
        );
    }
}
