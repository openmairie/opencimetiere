<?php
/**
 * Ce script définit la classe 'reduction_concession'.
 *
 * @package opencimetiere
 * @version SVN : $Id: reduction.class.php 787 2018-10-15 08:21:45Z fmichon $
 */

require_once "../obj/reduction.class.php";

/**
 * Définition de la classe 'reduction_concession' (om_dbform).
 *
 * Surcharge de la classe 'reduction'.
 */
class reduction_concession extends reduction {

    /**
     * @var string
     */
    protected $_absolute_class_name = "reduction_concession";

    /**
     * @var string
     */
    var $emplacement_nature = 'concession';

}
