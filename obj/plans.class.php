<?php
/**
 * Ce script définit la classe 'plans'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/plans.class.php";

/**
 * Définition de la classe 'plans' (om_dbform).
 */
class plans extends plans_gen {

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj == 0 || $maj == 1) {
            $form->setType('fichier', 'upload');
        } elseif ($maj == 3) {
            $form->setType('fichier', 'file');
        } else {
            $form->setType('fichier', 'filestatic');
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange('planslib','this.value=this.value.toUpperCase()');
    }
}
