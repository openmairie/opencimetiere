<?php
/**
 * Ce script définit la classe 'cimetiere'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/cimetiere.class.php";

/**
 * Définition de la classe 'cimetiere' (om_dbform).
 */
class cimetiere extends cimetiere_gen {

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib('information_generale', __("informations générales"));
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Gestion du champs geom
        $form->setType('geom','hidden');
        if ($this->f->getParameter("option_localisation") == "sig_interne") {
            if ($maj == 0 || $maj == 1 || $maj == 3) {
                $form->setType('geom', 'geom');
            }
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        // * mise en majuscule
        // On liste les champs sur lesquels on veut forcer les majuscules automatiques
        $fields_to_upper_case = array('cimetierelib', 'adresse1', 'adresse2', 'ville');
        // On récupère les valeurs de l'option, si il y en a pas retourne un tableau vide
        $option_casse_force_majuscule = $this->f->get_option_casse_force_majuscule();
        // On boucle sur les champs à mettre en majuscule auto
        foreach ($fields_to_upper_case as $field) {
            // Si la clé existe dans l'option et que la valeur est true ou que la clé n'existe pas
            if (array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule)
                && $option_casse_force_majuscule[$this->clePrimaire.'.'.$field] === true
                || array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule) == false) {
                // On force la majuscule sur le champ
                $form->setOnchange($field, "this.value=this.value.toUpperCase()");
            }
        }
    }
}
