<?php
//$Id$ 
//gen openMairie le 17/02/2020 15:31

require_once "../gen/obj/contrat_archive.class.php";

class contrat_archive extends contrat_archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 001 - modifier
        // -> modification impossible
        unset($this->class_actions[1]);
        // ACTION - 002 - supprimer
        // -> suppression impossible
        unset($this->class_actions[2]);
    }
}
