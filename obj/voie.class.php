<?php
/**
 * Ce script définit la classe 'voie'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/voie.class.php";

/**
 * Définition de la classe 'voie' (om_dbform).
 */
class voie extends voie_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 101 - edition
        //
        $this->class_actions[101] = array(
            "identifier" => "pdf-edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("récapitulatif"),
                "description" => __('télécharger le récapitulatif au format pdf'),
                "class" => "pdf-16",
                "order" => 30,
            ),
            "permission_suffix" => "edition",
            "view" => "view_pdf_edition_etat",
            "condition" => array(
                "exists",
            ),
        );
    }

    /**
     * @return string
     */
    function get_om_etat_id() {
        return $this->get_absolute_class_name();
    }

    /**
     * GETTER_FORMINC - sql_zone.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zone() {
        //
        $sql_zone_common = "SELECT zone.zone, (zone_type.libelle||' '||zone.zonelib||' ('||cimetiere.cimetierelib||')') as lib " ;
        $sql_zone_common .= " FROM ".DB_PREFIXE."zone ";
        $sql_zone_common .= " LEFT JOIN ".DB_PREFIXE."zone_type ";
        $sql_zone_common .= " ON zone.zonetype = zone_type.zone_type ";
        $sql_zone_common .= " LEFT JOIN ".DB_PREFIXE."cimetiere ";
        $sql_zone_common .= " ON zone.cimetiere = cimetiere.cimetiere ";
        //
        return $sql_zone_common." ORDER BY cimetiere.cimetierelib, (zone_type.libelle||' '||zone.zonelib) ";
    }

    /**
     * GETTER_FORMINC - sql_zone_by_id.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zone_by_id() {
        //
        $sql_zone_common = "SELECT zone.zone, (zone_type.libelle||' '||zone.zonelib||' ('||cimetiere.cimetierelib||')') as lib " ;
        $sql_zone_common .= " FROM ".DB_PREFIXE."zone ";
        $sql_zone_common .= " LEFT JOIN ".DB_PREFIXE."zone_type ";
        $sql_zone_common .= " ON zone.zonetype = zone_type.zone_type ";
        $sql_zone_common .= " LEFT JOIN ".DB_PREFIXE."cimetiere ";
        $sql_zone_common .= " ON zone.cimetiere = cimetiere.cimetiere ";
        //
        return $sql_zone_common." WHERE zone = <idx> ";
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib("voielib", __("libelle"));
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Gestion du champs geom
        $form->setType('geom','hidden');
        if ($this->f->getParameter("option_localisation") == "sig_interne") {
            if ($maj == 0 || $maj == 1 || $maj == 3) {
                $form->setType('geom', 'geom');
            }
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("voielib","this.value=this.value.toUpperCase()");
    }

    /**
     * MERGE_FIELDS - get_all_merge_fields.
     *
     * @return array
     */
    function get_all_merge_fields($type) {
        //
        $all = array();
        //
        switch ($type) {
            case 'values':
                //
                $values = $this->get_values_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => $this->getVal($field),
                    ));
                    $elem_values = $elem->get_merge_fields($type);
                    $values = array_merge($values, $elem_values);
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = $this->get_labels_legacy_merge_fields();
                foreach ($all as $table => $field) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $table,
                        "idx" => 0,
                    ));
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    /**
     * MERGE_FIELDS - get_all_merge_fields.
     *
     * @return array
     */
    function get_values_legacy_merge_fields() {
        $sql = sprintf(
            'SELECT
                voie,
                voie_type.libelle as type_de_voie,
                voielib,
                zone_type.libelle as type_de_zone,
                zonelib,
                cimetierelib

            FROM
                %1$svoie
                LEFT JOIN %1$svoie_type on voie.voietype = voie_type.voie_type
                LEFT JOIN %1$szone on voie.zone = zone.zone
                LEFT JOIN %1$szone_type on zone.zonetype = zone_type.zone_type
                LEFT JOIN %1$scimetiere on  zone.cimetiere = cimetiere.cimetiere

            WHERE
                voie=%2$s',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            return $row;
        }
        return array();
    }

    /**
     * MERGE_FIELDS - get_all_merge_fields.
     *
     * @return array
     */
    function get_labels_legacy_merge_fields() {
        return array(
            "legacy" => array(
                // Voie
                "voie" => "",
                "type_de_voie" => "",
                "voielib" => "",
                // Zone
                "type_de_zone" => "",
                "zonelib" => "",
                // Cimetière
                "cimetierelib" => "",
            ),
        );
    }
}
