<?php
//$Id$ 
//gen openMairie le 29/09/2022 11:25

require_once "../gen/obj/sepulture_type.class.php";

class sepulture_type extends sepulture_type_gen {
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        # On récupère les emplacements impactés par la modification
        $emplacement_q = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT
                    emplacement
                 FROM 
                    %1$semplacement
                 WHERE
                    sepulturetype = %2$s
                ',
                DB_PREFIXE,
                $this->getVal('sepulture_type')
            )
        );
        // Si on a des emplacements avec ce type de sépulture
        if (! empty($emplacement_q['result'])) {
            $emplacement_ids = array();
            // On ajoute les identifiants dans un tableau
            foreach ($emplacement_q['result'] as $emplacement) {
                $emplacement_ids[] = $emplacement['emplacement'];
            }
        }

        // Si on supprime des colonnes ou des lignes il faut vérifier qu'il n'y ait pas de défunts placé dans les emplacements
        if ($this->getVal('colonne') > $this->valF['colonne']|| $this->getVal('ligne') > $this->valF['ligne']) {
            // Si des défunts sont positionnés sur la ou les colonnes supprimées
            // alors on bloque la modification
            $defunt_pos_q = $this->f->get_all_results_from_db_query(
                sprintf(
                    'SELECT
                        defunt
                     FROM 
                        %1$sdefunt
                        INNER JOIN %1$semplacement ON defunt.emplacement = emplacement.emplacement
                     WHERE
                        CASE 
                            WHEN emplacement.videsanitaire = \'Non\' AND x > %2$s OR y > %3$s
                            THEN 
                                true
                            WHEN emplacement.videsanitaire = \'Oui\' AND x > %2$s OR y > %3$s+1
                            THEN
                                true
                            ELSE 
                                false
                        END = true
                        AND defunt.emplacement IN (%4$s)
                    ',
                    DB_PREFIXE,
                    $this->valF['colonne'],
                    $this->valF['ligne'],
                    implode(',', $emplacement_ids)
                )
            );
            // Bloquage de la modification avec message d'erreur
            if (! empty($defunt_pos_q['result'])) {
                $this->addToMessage('Le type de sépulture ne peut pas être modifié car des défunts
                    sont placés dans les emplacements.');
                $this->addToLog(__METHOD__."(): Le type de sépulture ne peut pas être modifié car des défunts
                    sont placés dans les emplacements.", DEBUG_MODE);
                $this->correct = false;
                return false;
            }
        }
    }
}
