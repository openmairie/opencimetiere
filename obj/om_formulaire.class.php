<?php
/**
 * Ce script définit la classe 'om_formulaire'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 * Définition de la classe 'om_formulaire' (om_formulaire).
 *
 * Cette classe permet la surcharge de certaines méthodes de
 * la classe 'om_formulaire' du framawork pour des besoins spécifiques de
 * l'application.
 */
class om_formulaire extends formulaire {

    /**
     * NOUVELLE METHODE pour la localisation sur plan
     * La modification para rapport à la méthode originale est l'appel à la
     * fonction javascript localisation_plan()
     * 
     * localisation
     * - $select['positiony'][0]="plan";// zone plan
     * - $select['positiony'][1]="positionx"; // zone coordonnees X
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function localisation_plan($champ, $validation, $DEBUG = false) {

        //
        echo "<input";
        echo " type=\"text\"";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$this->val[$champ]."\"";
        echo " size=\"".$this->taille[$champ]."\"";
        echo " maxlength=\"".$this->max[$champ]."\"";
        echo " class=\"champFormulaire localisation\"";
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                echo " onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                echo " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                echo " onclick=\"".$this->onclick[$champ]."\"";
            }
        } else {
            echo " disabled=\"disabled\"";
        }
        echo " />\n";

        //
        if (!$this->correct) {
            // zone libelle
            $plan = $this->select[$champ][0][0];  // plan
            $positionx = $this->select[$champ][0][1];
            //          
            echo "<a class=\"localisation ui-state-default ui-corner-all\" href=\"javascript:localisation_plan('".$champ."','".$plan."','".$positionx."');\">";
            echo "<span class=\"ui-icon ui-icon-pin-s\" ";
            echo "title=\"".__("Cliquer ici pour positionner l'element")."\">";
            echo __("Localisation");
            echo "</span>";
            echo "</a>";
        }

    }

    /**
     * NOUVELLE METHODE pour la localisation sur plan
     * La modification para rapport à la méthode originale est l'appel à la
     * fonction javascript localisation_plan()
     * 
     * localisation
     * - $select['positiony'][0]="plan";// zone plan
     * - $select['positiony'][1]="positionx"; // zone coordonnees X
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function localisation_plan_static($champ, $validation, $DEBUG = false) {
        //
        $this->selectstatic($champ, $validation, $DEBUG);
        //
        echo "<a href=\"../app/localisation_plan_view.php?idx=".$this->val["emplacement"]."&amp;obj=".$this->val["nature"]."&amp;retour=form#camap0\"><span title=\"Visualiser sur plan\" class=\"om-icon om-icon-16 om-icon-fix sig-16\">Plan</span></a>";
    }

    /**
     * WIDGET_FORM - autocomplete_link_selection
     *
     * Cette méthode permet d'ajouter un élément dans le widget de formulaire autocomplete.
     * Elle est à surcharger dans chaque application selon les besoins.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function autocomplete_link_selection($champ, $validation, $DEBUG = false) {
        if (isset($this->select[$champ])
            && isset($this->select[$champ]['table'])
            && $this->select[$champ]['table'] === "voie") {
            //
            echo sprintf(
                '<a id="autocomplete-voie-link-selection" 
                    class="autocomplete autocomplete-empty ui-state-default ui-corner-all"
                    onclick="overlay_select_voie_by_cimetiere_zone();">
                    <span title="%1$s" class="ui-icon ui-icon-extlink">
                        %1$s
                    </span>
                </a>',
                __("Sélection de voie par cimetière/zone")
            );
        }
    }

    /**
     * WIDGET_FORM - contrat_montant
     *
     * Cette méthode permet ...
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function contrat_montant($champ, $validation, $DEBUG = false) {
        $this->text($champ, $validation, $DEBUG);
        //
        echo sprintf(
            '<a id="contrat-link-montant"
                class="autocomplete autocomplete-empty ui-state-default ui-corner-all"
                onclick="overlay_contrat_montant();">
                <span title="%1$s" class="ui-icon ui-icon-extlink">
                    %1$s
                </span>
            </a>',
            __("Calcul automatique du montant du contrat")
        );
    }

    /**
     * WIDGET_FORM - link.
     * 
     * Champ lien vers un enregistrement lié.
     *
     * Configuration du widget via setSelect : array(
     *     "obj" => "objet du formulaire lié",
     *     "idx" => (optionnel) "Identifiant de l'enregistrement lié (si aucune
     *              valeur n'est fournie c'est la valeur du champ dans le
     *              formulaire qui est utilisée)",
     *     "libelle" => (optionnel) "Libellé de la valeur du champ (si aucune
     *                  valeur n'est fourni c'est idx qui est utilisé)",
     *     "right" => (optionnel) "Permissions pour avoir accès au lien",
     * );
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function link($champ, $validation, $DEBUG = false) {
        /**
         * Configuration
         */
        $obj = "";
        $idx = "";
        $libelle = "";
        $right = null;
        // Récupération de la configuration du setSelect
        if (is_array($this->select) === true
            && array_key_exists($champ, $this->select) === true
            && is_array($this->select[$champ]) === true) {
            //
            if (array_key_exists("obj", $this->select[$champ]) === true) {
                $obj = $this->select[$champ]["obj"];
            }
            if (array_key_exists("idx", $this->select[$champ]) === true) {
                $idx = $this->select[$champ]["idx"];
            }
            if (array_key_exists("libelle", $this->select[$champ]) === true) {
                $libelle = $this->select[$champ]["libelle"];
            }
            if (array_key_exists("right", $this->select[$champ]) === true) {
                $right = $this->select[$champ]["right"];
            }
            if (array_key_exists("title", $this->select[$champ]) === true) {
                $title = $this->select[$champ]["title"];
            }
        }
        // Aucun identifiant fourni dans le setSelect donc on ressaye de
        // récupérer la valeur du champ présente dans le formulaire
        if ($idx === ""
            && is_array($this->val) === true
            && array_key_exists($champ, $this->val) === true) {
            //
            $idx = $this->val[$champ];
        }
        // Aucun libellé fourni dans le setSelect donc on y affecte la
        // valeur de l'identifiant de l'élément lié
        if ($libelle === "") {
            $libelle = $idx;
        }
        /**
         * Affiche la value dans un champ hidden pour être postée par le form
         */
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
        /**
         * Affichage soit du lien soit du libellé soit rien :
         * - le lien est affiché si tous les paramètres sont correctement
         *   configurés et que l'utilisateur a les permissions,
         * - sinon si un libellé est configuré c'est le libellé qui est affiché,
         * - sinon on affiche rien.
         */
        if ($idx !== ""
            && $obj !== ""
            && $libelle !== ""
            && ($right == null
                || $this->f->isAccredited($right) === true)) {
            //
            printf(
                '<a id="link_%1$s" class="lienFormulaire" title="%4$s" href="%2$s">%3$s</a>',
                $champ,
                sprintf(
                    '%1$s&obj=%2$s&action=3&idx=%3$s',
                    OM_ROUTE_FORM,
                    $obj,
                    $idx
                ),
                $libelle,
                $title

            );
        } elseif ($libelle !== "") {
            printf(
                '<span class="field_value">%1$s</span>',
                $libelle
            );
        }
    }
}
