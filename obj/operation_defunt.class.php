<?php
/**
 * Ce script définit la classe 'operation_defunt'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/operation_defunt.class.php";

/**
 * Définition de la classe 'operation_defunt' (om_dbform).
 */
class operation_defunt extends operation_defunt_gen {

    var $idxformulaire;
    var $validation;
    var $maj;

    /**
     * GETTER_FORMINC - sql_defunt.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt() {
        return "SELECT defunt, nom, prenom FROM ".DB_PREFIXE."defunt";
    }

    /**
     * SETTER_TREATMENT - setValF (setValF).
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        //
        if($this->maj==0){
            unset($this->valF['defunt_titre']);
            unset($this->valF['defunt_nom']);
            unset($this->valF['defunt_marital']);
            unset($this->valF['defunt_prenom']);
            unset($this->valF['defunt_datenaissance']);
            unset($this->valF['defunt_datedeces']);
            unset($this->valF['defunt_lieudeces']);
            unset($this->valF['defunt_lieunaissance']);
            unset($this->valF['defunt_parente']);
        }
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // On verifie si le champ n'est pas vide
        if ($this->valF['defunt'] == "") {
            $this->correct = false;
            $this->addToMessage(__("Le champ")." ".__("defunt")." ".__("est obligatoire"));
        }
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) { //ajouter et modifier
            if($maj==0){
                echo $maj;
                $form->setType('defunt', 'select');
                $form->setType('defunt_titre','hidden');
                $form->setType('defunt_nom','hidden');
                $form->setType('defunt_marital','hidden');
                $form->setType('defunt_prenom','hidden');
                $form->setType('defunt_datenaissance','hidden');
                $form->setType('defunt_datedeces','hidden');
                $form->setType('defunt_lieudeces','hidden');
                $form->setType('defunt_lieunaissance','hidden');
                $form->setType('defunt_parente','hidden');
                $form->setType('defunt_nature','hidden');
            }else{
                $form->setType('defunt', 'hidden');
            }
            $form->setType('operation', 'hiddenstatic');
        }
         if($maj==2){
             $form->setType('defunt', 'hiddenstatic');
         }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("defunt_nom","this.value=this.value.toUpperCase();");
        $form->setOnchange("defunt_prenom","this.value=this.value.toUpperCase();");
        $form->setOnchange("defunt_lieudeces","this.value=this.value.toUpperCase();");
        $form->setOnchange("defunt_marital","this.value=this.value.toUpperCase();");
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if($maj<2){
             // defunt
            $k=1;
            $contenu = array(
                0 => array(
                    0 => "",
                ),
                1 => array(
                    0 => __("choix_defunt"),
                ),
            );
            // construction de la requete suivant le type d operation
            $sql_defunt = $this->get_var_sql_forminc__sql("defunt");
            if( ($this->retourformulaire =='reduction_concession')
                || $this->retourformulaire =='reduction_enfeu' 
                && $this->validation == '0'){     
                $temp = "select emplacement from ".DB_PREFIXE."operation where operation =".$this->idxformulaire;
                $emplacement = $this->f->db->getOne($temp);
                $sql_defunt.=" where emplacement =".$emplacement;
                $sql_defunt.= " and reduction = 'Non'";
                $sql_defunt.= " and exhumation = 'Non'";
                $sql_defunt.= " and verrou = 'Non'";
                $sql_defunt.= " and nature != 'urne'";
            }
            if($this->retourformulaire =='transfert' and $this->validation == '0'){
                $temp = "select emplacement from ".DB_PREFIXE."operation where operation =".$this->idxformulaire;
                $emplacement = $this->f->db->getOne($temp);
                $sql_defunt.=" where emplacement =".$emplacement;
                $sql_defunt.= " and exhumation = 'Non'";
                $sql_defunt.= " and verrou = 'Non'";            
            }
            $res = $this->f->db->query($sql_defunt);
            $this->addToLog(__METHOD__."(): db->query(\"".$sql_defunt."\");", VERBOSE_MODE);
            if ($this->f->isDatabaseError($res, true) !== false) {
                 die($res->getMessage().$sql_defunt);
            }else{              
                 //$k=1;
                 while ($row=& $res->fetchRow()){
                     $contenu[0][$k]=$row[0];
                     $contenu[1][$k]=$row[1]." ".$row[2];
                     $k++;
                 }
                 $form->setSelect("defunt",$contenu);
            }
        }  
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if($validation==0 and isset($retourformulaire) and $retourformulaire != '') {
            $form->setVal('operation', $idxformulaire);
        }
        $this->validation=$validation;
        $this->idxformulaire=$idxformulaire;
        $this->retourformulaire=$retourformulaire;
        $this->maj=$maj;
    }

    /**
     * TRIGGER - triggerajouter.
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // creation du defunt
        $sql="select * from ".DB_PREFIXE."defunt where defunt=".$this->valF['defunt'];
        $res= $this->f->db->query($sql);
        // a voir type de cercueil
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $this->valF['defunt_nom'] = $row['nom'];
            $this->valF['defunt_prenom'] = $row['prenom'];
            $this->valF['defunt_marital'] = $row['marital'];
            if($row['titre']!='')
                $this->valF['defunt_titre'] = $row['titre'];
            $this->valF['defunt_nature'] = $row['nature'];
            if($row['datenaissance']!='')
                $this->valF['defunt_datenaissance'] = $row['datenaissance'];
            if($row['datedeces'])
                $this->valF['defunt_datedeces'] = $row['datedeces'];
            $this->valF['defunt_lieudeces'] = $row['lieudeces'];
            $this->valF['defunt_lieunaissance'] = $row['lieunaissance'];
            $this->valF['defunt_parente'] = $row['parente'];
        }
        // controle de date : 5 ans inhumation
        // envoi d un message
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //verrou defunt
        $sql="update ".DB_PREFIXE."defunt set verrou ='Oui' where defunt=".$this->valF['defunt'];
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggersupprimer.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //verrou defunt
        $sql="update ".DB_PREFIXE."defunt set verrou ='Non' where defunt=".$val['defunt'];
        $res= $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) !== false) {
            $message = __("impossible de supprimer le verrou")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            return $this->end_treatment(__METHOD__, false);
        }
        $message = __("Enregistrement")."&nbsp;".$val['defunt']."&nbsp;";
        $message .= __("de la table")."&nbsp;\"defunt\"&nbsp;";
        $message .= "[&nbsp;".$this->f->db->affectedRows()."&nbsp;";
        $message .= __("enregistrement modifie")."&nbsp;]";
        $this->addToLog($message, VERBOSE_MODE);
        return $this->end_treatment(__METHOD__, true);
    }
}
