<?php
/**
 * Ce script définit la classe 'opencimetiere'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

/**
 *
 */
if (file_exists("../dyn/locales.inc.php") === true) {
    require_once "../dyn/locales.inc.php";
}

/**
 * Définition de la constante représentant le chemin d'accès au framework
 */
define("PATH_OPENMAIRIE", getcwd()."/../core/");

/**
 * TCPDF specific config
 */
define("K_TCPDF_EXTERNAL_CONFIG", true);
define("K_TCPDF_CALLS_IN_HTML", true);

/**
 * Dépendances PHP du framework
 * On modifie la valeur de la directive de configuration include_path en
 * fonction pour y ajouter les chemins vers les librairies dont le framework
 * dépend.
 */
set_include_path(
    get_include_path().PATH_SEPARATOR.implode(
        PATH_SEPARATOR,
        array(
            getcwd()."/../php/pear",
            getcwd()."/../php/db",
            getcwd()."/../php/fpdf",
            getcwd()."/../php/phpmailer",
            getcwd()."/../php/tcpdf",
        )
    )
);

/**
 *
 */
if (file_exists("../dyn/debug.inc.php") === true) {
    require_once "../dyn/debug.inc.php";
}

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 * Surcharges du framework à réintégrer
 */
require_once "../obj/om_application_override.class.php";

/**
 * Définition de la classe 'opencimetiere' (om_application).
 *
 * Cette classe permet la surcharge de certaines méthodes de
 * la classe om_application pour des besoins spécifiques de l'application.
 */
class opencimetiere extends om_application_override {

    /**
     * Gestion du nom de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_application_name = "openCimetière";

    /**
     * Titre HTML.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_title = ":: openMairie :: openCimetière";

    /**
     * Gestion du nom de la session.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_session_name = "1bb484de79f96a6dab00aaf4463c18f8bf";

    /**
     * Gestion du mode de gestion des permissions.
     *
     * @var mixed Configuration niveau application.
     */
    protected $config__permission_by_hierarchical_profile = false;

    /**
     *
     * @return void
     */
    function setDefaultValues() {
        $this->config['upload_extension'] = ".geojson;.gif;.jpg;.jpeg;.png;.txt;.pdf;.csv;";
        $this->addHTMLHeadCss(
            array(
                "../lib/om-theme/jquery-ui-theme/jquery-ui.custom.css",
                "../lib/om-theme/om.css",
            ),
            21
        );
    }

    /**
     * Surcharge - getParameter().
     *
     * Force les paramètres suivants aux valeurs suivantes :
     * - is_settings_view_enabled = true
     *
     * @return mixed
     */
    function getParameter($param = null) {
        // openCimetière est fait pour fonctionner avec la view 'settings'
        // pour gérer le menu 'Administration & Paramétrage'. On force donc le
        // paramètre.
        if ($param == "is_settings_view_enabled") {
            return true;
        }
        return parent::getParameter($param);
    }

    /**
     * get_option_casse_force_majuscule
     * 
     * Permet de recupérer les valeurs de l'option option_casse_force_majuscule.
     * 
     * @return array
     */
    function get_option_casse_force_majuscule() {
        return $this->get_parametrer_with_multiple_values('option_casse_force_majuscule', "\n", '=');
    }

    /**
     * get_parametrer_with_multiple_values
     * 
     * Permet de retourner un tableau contenant plusieurs valeur pour une option 
     * 
     * @param $option_libelle Le nom de l'option que l'on veut récupérer
     * @param $separator Le séparateur de chaque valeur de l'option (par défaut c'est un saut de ligne "\n")
     * @param $value_separator Si il y a un indicateur dans la valeur (exemple defunt.nom=true le séparateur est =)
     * par défaut à null
     * 
     * @return array
     */
    function get_parametrer_with_multiple_values($option_libelle, $separator = "\n", $value_separator = null) {
        $param_tab = array();
        if (! empty($this->getParameter($option_libelle))) {
            foreach (explode($separator, $this->getParameter($option_libelle)) as $option_value) {
                if ($value_separator != null) {
                    $option_value_exploded = explode($value_separator, $option_value);
                    if (is_array($option_value_exploded)) {
                        $param_tab[$option_value_exploded[0]] = trim($option_value_exploded[1]) == 'true' ? true : false;
                    }
                } else {
                    $param_tab[] = $option_value;
                }
            }
        }
        return $param_tab;
    }

    /**
     * Surcharge - view_main().
     *
     * Ajoute la vue 'search' comme un nouveau module.
     *
     * @return void
     */
    function view_main() {
        if ($this->get_submitted_get_value("module") === "search") {
            $this->view_module_search();
            return;
        }
        parent::view_main();
    }

    /**
     * Cette vue permet d'afficher un formulaire de recherche globale ainsi que
     * les résultats de recherche associés.
     *
     * @return void
     */
    function view_module_search() {
        $this->isAuthorized("recherche");
        $this->setTitle(__("recherche")." -> ".__("recherche globale"));
        $this->setFlag(null);
        $this->display();
        //
        set_time_limit(1800);
        //
        (isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");
        // Description de la page
        $description = __("Cette page vous permet de rechercher dans toutes les tables ".
        "de l'application.");
        $this->displayDescription($description);
        //
        echo "\n<div id=\"edition\">\n";
        //
        echo "<fieldset class=\"cadre ui-corner-all ui-widget-content\">\n";
        echo "\t<legend class=\"ui-corner-all ui-widget-content ui-state-active\">";
        echo __("recherche");
        echo "</legend>\n";
        //
        require_once "../sql/".OM_DB_PHPTYPE."/".$obj.".search.inc.php";
        require_once "../obj/search.class.php";
        $objRech = new search($this, $configTypeRecherche);
        $objRech->searchInclude("../app/index.php?module=search&obj=concession");
        $objRech->showForm();
        $objRech->showResults();
        //
        echo "</fieldset>\n";
        //
        echo "</div>";
    }

    /**
     * Surcharge - set_config__footer().
     *
     * @return void
     */
    protected function set_config__footer() {
        $footer = array();
        // Documentation du site
        $footer[] = array(
            "title" => __("Documentation"),
            "description" => __("Acceder a l'espace documentation de l'application"),
            "href" => "http://docs.openmairie.org/?project=opencimetiere&version=4.2&format=html&path=manuel_utilisateur",
            "target" => "_blank",
            "class" => "footer-documentation",
        );
        // Forum openMairie
        $footer[] = array(
            "title" => __("Forum"),
            "description" => __("Espace d'échange ouvert du projet openMairie"),
            "href" => "https://communaute.openmairie.org/c/opencimetiere",
            "target" => "_blank",
            "class" => "footer-forum",
        );
        // Portail openMairie
        $footer[] = array(
            "title" => __("openMairie.org"),
            "description" => __("Site officiel du projet openMairie"),
            "href" => "http://www.openmairie.org/catalogue/opencimetiere",
            "target" => "_blank",
            "class" => "footer-openmairie",
        );
        //
        $this->config__footer = $footer;
    }

    /**
     * Surcharge - set_config__menu().
     *
     * @return void
     */
    protected function set_config__menu() {
        parent::set_config__menu();
        $parent_menu = $this->config__menu;
        //
        $menu = array();
        // {{{ Rubrique EMPLACEMENT
        //
        $rubrik = array(
            "title" => __("emplacement"),
            "class" => "emplacement",
            "open" => array(
                "emplacement.php|",
            )
        );
        //
        $links = array();
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=concession",
            "class" => "concession",
            "title" => __("concession"),
            "right" => array(
                "concession",
                "concession_tab",
            ),
            "open" => array(
                "index.php|concession[module=tab]",
                "index.php|concession[module=form][action=0]",
                "index.php|concession[module=form][action=1]",
                "index.php|concession[module=form][action=2]",
                "index.php|concession[module=form][action=3]",
                "index.php|concession[module=map][mode=tab_sig]",
                "localisation_plan_view.php|concession",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=colombarium",
            "class" => "colombarium",
            "title" => __("colombarium"),
            "right" => array(
                "colombarium",
                "colombarium_tab",
            ),
            "open" => array(
                "index.php|colombarium[module=tab]",
                "index.php|colombarium[module=form][action=0]",
                "index.php|colombarium[module=form][action=1]",
                "index.php|colombarium[module=form][action=2]",
                "index.php|colombarium[module=form][action=3]",
                "index.php|colombarium[module=map][mode=tab_sig]",
                "localisation_plan_view.php|colombarium",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=enfeu",
            "class" => "enfeu",
            "title" => __("enfeu"),
            "right" => array(
                "enfeu",
                "enfeu_tab",
            ),
            "open" => array(
                "index.php|enfeu[module=tab]",
                "index.php|enfeu[module=form][action=0]",
                "index.php|enfeu[module=form][action=1]",
                "index.php|enfeu[module=form][action=2]",
                "index.php|enfeu[module=form][action=3]",
                "index.php|enfeu[module=map][mode=tab_sig]",
                "localisation_plan_view.php|enfeu",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=ossuaire",
            "class" => "ossuaire",
            "title" => __("ossuaire"),
            "right" => array(
                "ossuaire",
                "ossuaire_tab",
            ),
            "open" => array(
                "index.php|ossuaire[module=tab]",
                "index.php|ossuaire[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=depositoire",
            "class" => "depositoire",
            "title" => __("depositoire"),
            "right" => array(
                "depositoire",
                "depositoire_tab",
            ),
            "open" => array(
                "index.php|depositoire[module=tab]",
                "index.php|depositoire[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=terraincommunal",
            "class" => "terraincommunal",
            "title" => __("terraincommunal"),
            "right" => array(
                "terraincommunal",
                "terraincommunal_tab",
            ),
            "open" => array(
                "index.php|terraincommunal[module=tab]",
                "index.php|terraincommunal[module=form][action=0]",
                "index.php|terraincommunal[module=form][action=1]",
                "index.php|terraincommunal[module=form][action=2]",
                "index.php|terraincommunal[module=form][action=3]",
                "index.php|terraincommunal[module=map][mode=tab_sig]",
                "localisation_plan_view.php|terraincommunal",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "contrat",
                "contrat_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contrat",
            "class" => "contrat",
            "title" => __("Contrats"),
            "right" => array(
                "contrat",
                "contrat_tab",
            ),
            "open" => array(
                "index.php|contrat[module=tab]",
                "index.php|contrat[module=form]",
            ),
        );
        //
        $rubrik['links'] = $links;
        //
        $menu["app-menu-rubrik-emplacement"] = $rubrik;
        // }}}

        // {{{ Rubrique RECHERCHE
        //
        $rubrik = array(
            "title" => __("recherche"),
            "class" => "recherche",
        );
        //
        $links = array();
        //
        $links[] = array(
            "href" => "../app/index.php?module=search&obj=concession",
            "class" => "recherche",
            "title" => __("recherche").' '.__("globale"),
            "right" => array(
                "recherche",
            ),
            "open" => array(
                "index.php|concession[module=search]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "terme",
                "terme_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=terme",
            "class" => "concession_terme",
            "title" => __("concession_terme"),
            "right" => array(
                "terme",
                "terme_tab",
            ),
            "open" => array(
                "index.php|terme[module=tab]",
                "index.php|terme[module=form]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "concessionlibre",
                "concessionlibre_tab",
                "colombariumlibre",
                "colombariumlibre_tab",
                "terraincommunallibre",
                "terraincommunallibre_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=concessionlibre",
            "class" => "concession_libre",
            "title" => __("concession_libre"),
            "right" => array(
                "concessionlibre",
                "concessionlibre_tab",
            ),
            "open" => array(
                "index.php|concessionlibre[module=tab]",
                "index.php|concessionlibre[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=colombariumlibre",
            "class" => "colombarium_libre",
            "title" => __("colombarium_libre"),
            "right" => array(
                "colombariumlibre",
                "colombariumlibre_tab",
            ),
            "open" => array(
                "index.php|colombariumlibre[module=tab]",
                "index.php|colombariumlibre[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=terraincommunallibre",
            "class" => "terraincommunal_libre",
            "title" => __("terraincommunal_libre"),
            "right" => array(
                "terraincommunallibre",
                "terraincommunallibre_tab",
            ),
            "open" => array(
                "index.php|terraincommunallibre[module=tab]",
                "index.php|terraincommunallibre[module=form]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "operation_en_cours_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=operation_en_cours",
            "class" => "operation_en_cours",
            "title" => __("opérations en cours"),
            "right" => array(
                "operation_en_cours_tab",
            ),
            "open" => array(
                "index.php|operation_en_cours[module=tab]",
            ),
        );
        //
        $rubrik['links'] = $links;
        //
        $menu["app-menu-rubrik-recherche"] = $rubrik;
        // }}}

        // {{{ Rubrique OPERATIONS
        //
        $rubrik = array(
            "title" => __("operations"),
            "class" => "operation",
        );
        //
        $links = array();
        //
        $links[] = array(
            "class" => "category",
            "title" => __("inhumations"),
            "right" => array(
                "inhumation_concession",
                "inhumation_concession_tab",
                "inhumation_terraincommunal",
                "inhumation_terraincommunal_tab",
                "inhumation_enfeu",
                "inhumation_enfeu_tab",
                "inhumation_colombarium",
                "inhumation_colombarium_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "inhumation_concession",
                "inhumation_concession_tab",
                "inhumation_terraincommunal",
                "inhumation_terraincommunal_tab",
                "inhumation_enfeu",
                "inhumation_enfeu_tab",
                "inhumation_colombarium",
                "inhumation_colombarium_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=inhumation_concession",
            "class" => "operation",
            "title" => __("inhumation")." ".__("concession"),
            "right" => array(
                "inhumation_concession",
                "inhumation_concession_tab",
            ),
            "open" => array(
                "index.php|inhumation_concession[module=tab]",
                "index.php|inhumation_concession[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=inhumation_terraincommunal",
            "class" => "operation",
            "title" => __("inhumation")." ".__("terraincommunal"),
            "right" => array(
                "inhumation_terraincommunal",
                "inhumation_terraincommunal_tab",
            ),
            "open" => array(
                "index.php|inhumation_terraincommunal[module=tab]",
                "index.php|inhumation_terraincommunal[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=inhumation_enfeu",
            "class" => "operation",
            "title" => __("inhumation")." ".__("enfeu"),
            "right" => array(
                "inhumation_enfeu",
                "inhumation_enfeu_tab",
            ),
            "open" => array(
                "index.php|inhumation_enfeu[module=tab]",
                "index.php|inhumation_enfeu[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=inhumation_colombarium",
            "class" => "operation",
            "title" => __("inhumation")." ".__("colombarium"),
            "right" => array(
                "inhumation_colombarium",
                "inhumation_colombarium_tab",
            ),
            "open" => array(
                "index.php|inhumation_colombarium[module=tab]",
                "index.php|inhumation_colombarium[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("reductions"),
            "right" => array(
                "reduction_concession",
                "reduction_concession_tab",
                "reduction_enfeu",
                "reduction_enfeu_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "reduction_concession",
                "reduction_concession_tab",
                "reduction_enfeu",
                "reduction_enfeu_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reduction_concession",
            "class" => "operation",
            "title" => __("reduction")." ".__("concession"),
            "right" => array(
                "reduction_concession",
                "reduction_concession_tab",
            ),
            "open" => array(
                "index.php|reduction_concession[module=tab]",
                "index.php|reduction_concession[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reduction_enfeu",
            "class" => "operation",
            "title" => __("reduction")." ".__("enfeu"),
            "right" => array(
                "reduction_enfeu",
                "reduction_enfeu_tab",
            ),
            "open" => array(
                "index.php|reduction_enfeu[module=tab]",
                "index.php|reduction_enfeu[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("transferts"),
            "right" => array(
                "transfert",
                "transfert_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "transfert",
                "transfert_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=transfert",
            "class" => "operation",
            "title" => __("transfert")." ".__("emplacement"),
            "right" => array(
                "transfert",
                "transfert_tab",
            ),
            "open" => array(
                "index.php|transfert[module=tab]",
                "index.php|transfert[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("autres"),
            "right" => array(
                "operation_trt",
                "operation_trt_tab",
                "concessionplace",
                "numdossier",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "operation_trt",
                "operation_trt_tab",
                "edition",
                "operation_en_cours_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=operation_trt",
            "class" => "operation",
            "title" => __("operation(s) traitee(s)"),
            "right" => array(
                "operation_trt",
                "operation_trt_tab",
            ),
            "open" => array(
                "index.php|operation_trt[module=tab]",
                "index.php|operation_trt[module=form]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "emplacement_calculer_place",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=emplacement&action=91",
            "class" => "concessionplace",
            "title" => __("calcul")." ".__("place"),
            "right" => array(
                "emplacement_calculer_place",
            ),
            "open" => array(
                "index.php|emplacement[action=91]"
            ),
        );
        //
        $rubrik['links'] = $links;
        //
        $menu["app-menu-rubrik-operation"] = $rubrik;
        // }}}

        // {{{ RUBRIQUE ARCHIVE
        //
        $rubrik = array(
            "title" => __("archive"),
            "class" => "archive",
        );
        //
        $links = array();
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=emplacement_archive",
            "class" => "emplacement_archive",
            "title" => __("emplacement"),
            "right" => array(
                "emplacement_archive",
                "emplacement_archive_tab",
            ),
            "open" => array(
                "index.php|emplacement_archive[module=tab]",
                "index.php|emplacement_archive[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=defunt_archive",
            "class" => "defunt_archive",
            "title" => __("defunt"),
            "right" => array(
                "defunt_archive",
                "defunt_archive_tab",
            ),
            "open" => array(
                "index.php|defunt_archive[module=tab]",
                "index.php|defunt_archive[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorisation_archive",
            "class" => "autorisation_archive",
            "title" => __("contact"),
            "right" => array(
                "autorisation_archive",
                "autorisation_archive_tab",
            ),
            "open" => array(
                "index.php|autorisation_archive[module=tab]",
                "index.php|autorisation_archive[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contrat_archive",
            "class" => "contrat_archive",
            "title" => __("contrat"),
            "right" => array(
                "contrat_archive",
                "contrat_archive_tab",
            ),
            "open" => array(
                "index.php|contrat_archive[module=tab]",
                "index.php|contrat_archive[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=operation_archive",
            "class" => "operation_archive",
            "title" => __("operation"),
            "right" => array(
                "operation_archive",
                "operation_archive_tab",
            ),
            "open" => array(
                "index.php|operation_archive[module=tab]",
                "index.php|operation_archive[module=form]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "concessionfin",
                "concessionfin_tab",
                "colombariumfin",
                "colombariumfin_tab",
                "terraincommunalfin",
                "terraincommunalfin_tab",
                "enfeufin",
                "enfeufin_tab",
            ),
        );
        //
        $links[] = array(
            "title" => __("fin_concession"),
            "href" => OM_ROUTE_TAB."&obj=concessionfin",
            "class" => "fin_concession",
            "right" => array(
                "concessionfin",
                "concessionfin_tab",
            ),
            "open" => array(
                "index.php|concessionfin[module=tab]",
                "index.php|concession[module=form][action=31]",
            ),
        );
        //
        $links[] = array(
            "title" => __("fin_colombarium"),
            "href" => OM_ROUTE_TAB."&obj=colombariumfin",
            "class" => "fin_colombarium",
            "right" => array(
                "colombariumfin",
                "colombariumfin_tab",
            ),
            "open" => array(
                "index.php|colombariumfin[module=tab]",
                "index.php|colombarium[module=form][action=31]",
            ),
        );
        //
        $links[] = array(
            "title" => __("fin_terrain_communal"),
            "href" => OM_ROUTE_TAB."&obj=terraincommunalfin",
            "class" => "fin_terrain_communal",
            "right" => array(
                "terraincommunalfin",
                "terraincommunalfin_tab",
            ),
            "open" => array(
                "index.php|terraincommunalfin[module=tab]",
                "index.php|terraincommunal[module=form][action=31]",
            ),
        );
        //
        $links[] = array(
            "title" => __("fin_enfeu"),
            "href" => OM_ROUTE_TAB."&obj=enfeufin",
            "class" => "fin_enfeu",
            "right" => array(
                "enfeufin",
                "enfeufin_tab",
            ),
            "open" => array(
                "index.php|enfeufin[module=tab]",
                "index.php|enfeu[module=form][action=31]",
            ),
        );
        //
        $rubrik['links'] = $links;
        //
        $menu["app-menu-rubrik-archive"] = $rubrik;
        // }}}
        //
        // PARAMETRAGE METIER
        //
        $links = array();
        //
        $links[] = array(
            "class" => "category",
            "title" => __("divers"),
            "right" => array(
                "entreprise",
                "entreprise_tab",
                "titre_de_civilite",
                "titre_de_civilite_tab",
                "travaux_nature",
                "travaux_nature_tab",
                "sepulture_type",
                "sepulture_type_tab",
                "zone_type",
                "zone_type_tab",
                "voie_type",
                "voie_type_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "entreprise",
                "entreprise_tab",
                "tarif",
                "tarif_tab",
                "titre_de_civilite",
                "titre_de_civilite_tab",
                "travaux_nature",
                "travaux_nature_tab",
                "sepulture_type",
                "sepulture_type_tab",
                "zone_type",
                "zone_type_tab",
                "voie_type",
                "voie_type_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=entreprise",
            "class" => "entreprise",
            "title" => __("entreprise"),
            "description" => __("Une entreprise habilitée à venir effectuer des travaux sur l'emplacement."),
            "right" => array(
                "entreprise",
                "entreprise_tab",
            ),
            "open" => array(
                "index.php|entreprise[module=tab]",
                "index.php|entreprise[module=form]",
            ),
        );
        // Ajout de l'entrée de menu lien_parente
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=lien_parente",
            "class" => "lien_parente",
            "title" => __("lien_parente"),
            "description" => __("Les liens de parenté permettent de caractériser les enregistrements de généalogie."),
            "right" => array(
                "lien_parente",
                "lien_parente_tab",
            ),
            "open" => array(
                "index.php|lien_parente[module=tab]",
                "index.php|lien_parente[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=tarif",
            "class" => "tarif",
            "title" => __("tarif"),
            "description" => __("Les tarifs correspondent aux montants des contrats d’achat et de renouvellement d’un emplacement."),
            "right" => array(
                "tarif",
                "tarif_tab",
            ),
            "open" => array(
                "index.php|tarif[module=tab]",
                "index.php|tarif[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=titre_de_civilite",
            "class" => "titre-de-civilite",
            "title" => __("titre_de_civilite"),
            "description" => __("Le titre de civilité est le plus fréquemment utilisé pour identifier la civilité d’une personne (Monsieur, Madame ou Mademoiselle). Il est également utilisé pour le titre ou le rang d’une personne."),
            "right" => array(
                "titre_de_civilite",
                "titre_de_civilite_tab",
            ),
            "open" => array(
                "index.php|titre_de_civilite[module=tab]",
                "index.php|titre_de_civilite[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=sepulture_type",
            "class" => "sepulture_type",
            "title" => __("sepulture_type"),
            "description" => __("Le type de sépulture est utilisé pour décrire une concession. Exemples : “Fosse maçonnée haute”, “Cavurne”, “Pierre tombale”, “Caveau T2 haut”, “Caveau T2 bas”, …"),
            "right" => array(
                "sepulture_type",
                "sepulture_type_tab",
            ),
            "open" => array(
                "index.php|sepulture_type[module=tab]",
                "index.php|sepulture_type[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=travaux_nature",
            "class" => "travaux_nature",
            "title" => __("travaux_nature"),
            "description" => __("La nature des travaux est utilisée pour décrire les travaux sur un emplacement. Exemples : “Enlèvement porte”, “Démolition-Reconstruction à l’identique”, “Creusement”, “Surélévation”, …"),
            "right" => array(
                "travaux_nature",
                "travaux_nature_tab",
            ),
            "open" => array(
                "index.php|travaux_nature[module=tab]",
                "index.php|travaux_nature[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=zone_type",
            "class" => "zone_type",
            "title" => __("zone_type"),
            "description" => __("Le type de zone est utilisé pour catégoriser une zone dans le système de localisation. Exemples : “Carré”, “Extension”, “Section”, …"),
            "right" => array(
                "zone_type",
                "zone_type_tab",
            ),
            "open" => array(
                "index.php|zone_type[module=tab]",
                "index.php|zone_type[module=form]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=voie_type",
            "class" => "voie_type",
            "title" => __("voie_type"),
            "description" => __("Le type de voie est utilisé pour catégoriser une voie dans le système de localisation. Exemples : “Allée”, “Place”, “Rangée”, …"),
            "right" => array(
                "voie_type",
                "voie_type_tab",
            ),
            "open" => array(
                "index.php|voie_type[module=tab]",
                "index.php|voie_type[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("localisation"),
            "right" => array(
                "plans",
                "plans_tab",
                "cimetiere",
                "cimetiere_tab",
                "zone",
                "zone_tab",
                "voie",
                "voie_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "cimetiere",
                "cimetiere_tab",
                "zone",
                "zone_tab",
                "voie",
                "voie_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=cimetiere",
            "class" => "cimetiere",
            "title" => __("cimetiere"),
            "description" => __("Paramétrage des cimetières."),
            "right" => array(
                "cimetiere",
                "cimetiere_tab",
            ),
            "open" => array(
                "index.php|cimetiere[module=tab]",
                "index.php|cimetiere[module=form]",
                "index.php|cimetiere[module=map][mode=tab_sig]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=zone",
            "class" => "zone",
            "title" => __("zone"),
            "description" => __("Paramétrage des zones. Un cimetière se compose de plusieurs zones (comme des carrés ou des sections)."),
            "right" => array(
                "zone",
                "zone_tab",
            ),
            "open" => array(
                "index.php|zone[module=tab]",
                "index.php|zone[module=form]",
                "index.php|zone[module=map][mode=tab_sig]",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=voie",
            "class" => "voie",
            "title" => __("voie"),
            "description" => __("Paramétrage des voies. Un emplacement est adressé par un numéro, éventullement un complément de numéro et une voie. Une zone se compose de plusieurs voies (comme des allées ou des rangées)."),
            "right" => array(
                "voie",
                "voie_tab",
            ),
            "open" => array(
                "index.php|voie[module=tab]",
                "index.php|voie[module=form]",
                "index.php|voie[module=map][mode=tab_sig]",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "plans",
                "plans_tab",
            ),
            "parameters" => array("option_localisation" => "plan", ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=plans",
            "class" => "plans",
            "title" => __("plans"),
            "description" => __("Paramétrage des plans. Un plan correspond à un croquis d’un cimetière ou d’une partie d’un cimetière. Un emplacement se localise sur un plan grâce à un point."),
            "right" => array(
                "plans",
                "plans_tab",
            ),
            "open" => array(
                "index.php|plans[module=tab]",
                "index.php|plans[module=form]",
            ),
            "parameters" => array("option_localisation" => "plan", ),
        );
        //
        $parent_menu["om-menu-rubrik-parametrage"]["links"] = array_merge(
            $links,
            $parent_menu["om-menu-rubrik-parametrage"]["links"]
        );
        //
        $this->config__menu = array_merge(
            $menu,
            $parent_menu
        );
    }

    /**
     * Instanciation de la classe *om_edition*.
     *
     * @param array $args Arguments à passer au constructeur.
     *
     * @return om_edition
     */
    function get_inst__om_edition($args = array()) {
        if (file_exists("../obj/app_om_edition.class.php")) {
            require_once "../obj/app_om_edition.class.php";
            $class_name = "app_om_edition";
        } else {
            require_once PATH_OPENMAIRIE."om_edition.class.php";
            $class_name = "edition";
        }
        return new $class_name();
    }

    /**
     * Instanciation de la classe *om_import*.
     *
     * @param array $args Arguments à passer au constructeur.
     *
     * @return om_import
     */
    function get_inst__om_import($args = array()) {
        if (file_exists("../obj/app_om_import.class.php")) {
            require_once "../obj/app_om_import.class.php";
            $class_name = "app_om_import";
        } else {
            require_once PATH_OPENMAIRIE."om_import.class.php";
            $class_name = "import";
        }
        return new $class_name();
    }

    /**
     * Récupère tous les résultats d'une requête SQL.
     *
     * @param string  $query        Requête SQL
     * @param boolean $force_return Force le retour d'un boolean en cas d'erreur
     *
     * @return array
     */
    function get_all_results_from_db_query($query = "", $force_return = false) {
        $res = $this->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $result = array(
            'code' => '',
            'message' => '',
            'result' => array(),
        );
        if ($this->isDatabaseError($res, $force_return) !== false) {
            $result['code'] = 'KO';
            $result['message'] = __("Erreur de base de donnees. Contactez votre administrateur.");
            return $result;
        }
        $result['code'] = 'OK';
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($result['result'], $row);
        }
        $res->free();
        return $result;
    }

    /**
     * Récupère un résultat d'une requête SQL.
     *
     * @param string  $query        Requête SQL
     * @param boolean $force_return Force le retour d'un boolean en cas d'erreur
     *
     * @return array
     */
    function get_one_result_from_db_query($query = "", $force_return = false) {
        $res = $this->db->getone($query);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$query."\");",
            VERBOSE_MODE
        );
        $result = array(
            'code' => '',
            'message' => '',
            'result' => array(),
        );
        if ($this->isDatabaseError($res, $force_return) !== false) {
            $result['code'] = 'KO';
            $result['message'] = __("Erreur de base de donnees. Contactez votre administrateur.");
            return $result;
        }
        $result['code'] = 'OK';
        $result['result'] = $res;
        return $result;
    }

    /**
     * Permet de générer la liste des généalogie pour un défunt ou pour un contact.
     * 
     * @param $defunt_or_autorisation Il faut spécifier si c'est le texte d'un défunt ou d'un contact
     * @param $id Identifiant du défunt ou du contact
     * 
     * @return string $text_genealogie | null
     */
    function generate_text_genealogie($defunt_or_autorisation, $id) {
        
        // Si le paramètre n'est pas spécifié
        if (($defunt_or_autorisation != 'defunt' && $defunt_or_autorisation != 'autorisation') || is_int(intval($id)) == false) {
            return null;
        }
        // Si le paramètre est à défunt on change la clause where de la requête sql
        if ($defunt_or_autorisation == 'defunt') {
            $whereclause = sprintf('(defunt_p1 = %1$d OR defunt_p2 = %1$d)', $id);
        }

        // Si le paramètre est à autorisation on change la clause where de la requête sql
        if ($defunt_or_autorisation == 'autorisation') {
            $whereclause = sprintf('(autorisation_p1 = %1$d OR autorisation_p2 = %1$d)', $id);
        }

        // Il est possible que la requête soit lancé deux si jamais un lien de parenté
        // de type même personne est identifié
        $template_sql_lien_parente = '
                SELECT
                    defunt_p1,
                    defunt_p2,
                    autorisation_p1, 
                    autorisation_p2,
                    lien_parente.libelle as lien_parente_libelle,
                    lien_parente.lien_inverse as lien_inverse,
                    (SELECT 
                        CONCAT_WS(\' \', defunt.nom, defunt.marital, defunt.prenom) as defunt_p1_nom 
                    FROM 
                        %1$sdefunt 
                    WHERE 
                        defunt = genealogie.defunt_p1),

                    (SELECT 
                        CONCAT_WS(\' \', defunt.nom, defunt.marital, defunt.prenom) as defunt_p2_nom
                    FROM 
                        %1$sdefunt
                    WHERE 
                        defunt = genealogie.defunt_p2),

                    (SELECT
                        CONCAT_WS(\' \', autorisation.nom, autorisation.marital, autorisation.prenom) as autorisation_p1_nom
                    FROM
                        %1$sautorisation
                    WHERE
                        autorisation = genealogie.autorisation_p1
                    ),

                    (SELECT
                        CONCAT_WS(\' \', autorisation.nom, autorisation.marital, autorisation.prenom) as autorisation_p2_nom
                    FROM
                        %1$sautorisation
                    WHERE
                        autorisation = genealogie.autorisation_p2
                    ),

                    lien_parente.meme_personne
                FROM
                    %1$sgenealogie
                    INNER JOIN %1$slien_parente ON genealogie.lien_parente=lien_parente.lien_parente
                WHERE
                    %2$s
                ';

        $liens_genealogie = $this->get_all_results_from_db_query(
            sprintf(
                $template_sql_lien_parente,
                DB_PREFIXE,
                $whereclause
            )
        );

        $lien_meme_personne = array();
        $text_genealogie = '<ul class="liste_genealogie">';
        $texte_template = "<li> %s de %s%s </li>";
        // Génération du texte de la personne dont l'id est passé en paramètre
        foreach ($liens_genealogie['result'] as $lien_genealogie) {
            // Soit notre personne est en première position
            // on prend donc la deuxième
            if ($lien_genealogie[$defunt_or_autorisation.'_p1'] == $id) {
                $lien_parente = $lien_genealogie['lien_parente_libelle'];
                $defunt = $lien_genealogie['defunt_p2_nom'];
                $autorisation = $lien_genealogie['autorisation_p2_nom'];
            }

            // Soit notre personne est en deuxième position
            // on prend donc la première
            if ($lien_genealogie[$defunt_or_autorisation.'_p2'] == $id) {
                $lien_parente = $lien_genealogie['lien_inverse'];
                $defunt = $lien_genealogie['defunt_p1_nom'];
                $autorisation = $lien_genealogie['autorisation_p1_nom'];
            }

            // Si le lien indique la même personne
            if ($lien_genealogie['meme_personne'] == 't') {
                // Récupération du lien même personne afin de récupérer les liens de l'autre personne
                if ($lien_genealogie[$defunt_or_autorisation.'_p1'] == $id) {
                    $lien_parente = $lien_genealogie['lien_inverse'];
                }
                if ($lien_genealogie[$defunt_or_autorisation.'_p2'] == $id) {
                    $lien_parente = $lien_genealogie['lien_parente_libelle'];
                }
                $texte_template = "<li> Même personne que le %s %s%s </li>";
                $lien_meme_personne[] = $lien_genealogie;
            }

            // Ajout du texte de généalogie
            $text_genealogie .= sprintf(
                $texte_template,
                $lien_parente,
                $defunt,
                $autorisation
            );
        }

        // Gestion des liens même personne
        $id_meme_personne = 0;
        $d_or_c_meme_personne = 'defunt';
        // Si il y a un lien même personne
        if (! empty($lien_meme_personne)) {
            
            // Si c'est un défunt qui est passé en paramètre alors on vérifie sont identifiant
            // et on récupère la deuxième personne lié indiqué comme "même personne"
            if ($defunt_or_autorisation == 'defunt') {
                if ($lien_meme_personne[0]['defunt_p1'] == $id) {
                    if (!empty($lien_meme_personne[0]['defunt_p2'])) {
                        $id_meme_personne = $lien_meme_personne[0]['defunt_p2'];
                    }
                    if (!empty($lien_meme_personne[0]['autorisation_p2'])) {
                        $id_meme_personne = $lien_meme_personne[0]['autorisation_p2'];
                        $d_or_c_meme_personne = 'autorisation';
                    }
                }
                if ($lien_meme_personne[0]['defunt_p2'] == $id) {
                    if (!empty($lien_meme_personne[0]['defunt_p1'])) {
                        $id_meme_personne = $lien_meme_personne[0]['defunt_p1'];
                    }
                    if (!empty($lien_meme_personne[0]['autorisation_p1'])) {
                        $id_meme_personne = $lien_meme_personne[0]['autorisation_p1'];
                        $d_or_c_meme_personne = 'autorisation';
                    }
                }
            }

            // Si c'est une autorisation qui est passé en paramètre alors on vérifie sont identifiant
            // et on récupère la deuxième personne lié indiqué comme "même personne"
            if ($defunt_or_autorisation == 'autorisation') {
                if ($lien_meme_personne[0]['autorisation_p1'] == $id) {
                    if (!empty($lien_meme_personne[0]['defunt_p2'])) {
                        $id_meme_personne = $lien_meme_personne[0]['defunt_p2'];
                    }
                    if (!empty($lien_meme_personne[0]['autorisation_p2'])) {
                        $id_meme_personne = $lien_meme_personne[0]['autorisation_p2'];
                        $d_or_c_meme_personne = 'autorisation';
                    }
                }
                if ($lien_meme_personne[0]['autorisation_p2'] == $id) {
                    if (!empty($lien_meme_personne[0]['defunt_p1'])) {
                        $id_meme_personne = $lien_meme_personne[0]['defunt_p1'];
                    }
                    if (!empty($lien_meme_personne[0]['autorisation_p1'])) {
                        $id_meme_personne = $lien_meme_personne[0]['autorisation_p1'];
                        $d_or_c_meme_personne = 'autorisation';
                    }
                }
            }

            // Construction de la clause where en fonction du type de la 2ème personne indiqué comme "même personne"
            if ($d_or_c_meme_personne == 'defunt') {
                $whereclause = sprintf('(defunt_p1 = %1$d OR defunt_p2 = %1$d)', $this->db->escapeSimple(intval($id_meme_personne)));
            }
            if ($d_or_c_meme_personne == 'autorisation') {
                $whereclause = sprintf('(autorisation_p1 = %1$d OR autorisation_p2 = %1$d)', $this->db->escapeSimple(intval($id_meme_personne)));
            }

            $qliens_meme_personne = $this->get_all_results_from_db_query(
                sprintf(
                    $template_sql_lien_parente,
                    DB_PREFIXE,
                    $whereclause
                )
            );
            
            $texte_template = "<li> %s de %s%s </li>";
            foreach ($qliens_meme_personne['result'] as $lien_genealogie) {
                // Si le lien est même personne alors on ne le récupère pas
                if ($lien_genealogie['meme_personne'] == 't') {
                    continue;
                }

                // Soit la personne indiqué comme "même personne" est en première position
                // on prend donc la deuxième
                if ($lien_genealogie[$d_or_c_meme_personne.'_p1'] == $id_meme_personne) {
                    $lien_parente = $lien_genealogie['lien_parente_libelle'];
                    $defunt = $lien_genealogie['defunt_p2_nom'];
                    $autorisation = $lien_genealogie['autorisation_p2_nom'];
                }

                // Soit la personne indiqué comme "même personne" est en deuxième position
                // on prend donc la première
                if ($lien_genealogie[$d_or_c_meme_personne.'_p2'] == $id_meme_personne) {
                    $lien_parente = $lien_genealogie['lien_inverse'];
                    $defunt = $lien_genealogie['defunt_p1_nom'];
                    $autorisation = $lien_genealogie['autorisation_p1_nom'];
                }

                // Ajout du texte de généalogie
                $text_genealogie .= sprintf(
                    $texte_template,
                    $lien_parente,
                    $defunt,
                    $autorisation
                );
            }
        }
        // Fermeture de la liste des liens généalogiques
        $text_genealogie .= "</ul>";

        return $text_genealogie;
    }
}
