<?php
/**
 * Ce script définit la classe 'inhumation'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/operation.class.php";

/**
 * Définition de la classe 'inhumation' (om_dbform).
 *
 * Surcharge de la classe 'operation'.
 */
class inhumation extends operation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "inhumation";

    /**
     * @var string
     */
    var $categorie = "inhumation";

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) { //ajouter et modifier
            $form->setType('defunt_titre','select');
            $form->setType('defunt_nom','text');
            $form->setType('defunt_prenom','text');
            $form->setType('defunt_marital','text');
            $form->setType('defunt_datenaissance','date');
            $form->setType('defunt_lieunaissance','text');
            $form->setType('defunt_datedeces','date');
            $form->setType('defunt_lieudeces','text');
            $form->setType('defunt_parente','text');
            $form->setType('defunt_nature','select');
        }
        if ($maj == 3) {
            $form->setType('defunt_titre', 'selectstatic');
            $form->setType('defunt_nom', 'hiddenstatic');
            $form->setType('defunt_marital', 'hiddenstatic');
            $form->setType('defunt_prenom', 'hiddenstatic');
            $form->setType('defunt_datenaissance', 'hiddenstaticdate');
            $form->setType('defunt_lieunaissance','hiddenstatic');
            $form->setType('defunt_datedeces', 'hiddenstaticdate');
            $form->setType('defunt_lieudeces', 'hiddenstatic');
            $form->setType('defunt_parente','hiddenstatic');
            $form->setType('defunt_nature', 'hiddenstatic');
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "defunt_titre",
            $this->get_var_sql_forminc__sql("defunt_titre"),
            $this->get_var_sql_forminc__sql("defunt_titre_by_id"),
            true
        );
        //
        if($maj<2){
            //nature
            $contenu=array();
            $contenu[0]=array('cercueil','zinc','urne','boite','cercueil_simp', 'cercueil_herm','cercueil_partage','reliquaire','reliquaire_partage','urne_simple','urne_partage','non_renseigne',);
            $contenu[1]=array(__('cercueil'),__("cercueil_zinc"),__("urne"),__("boite"),__("cercueil simple"),__("cercueil hermétique"),__("cercueil partagé"),__("reliquaire partagé"),__("reliquaire simple"),__("urne_simple"),__("urne partagée"),__("non renseignée"));
            $form->setSelect("defunt_nature",$contenu);
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //

        // * mise en majuscule
        // On liste les champs sur lesquels on veut forcer les majuscules automatiques
        $fields_to_upper_case = array('defunt_nom', 'defunt_prenom', 'defunt_lieudeces', 'defunt_marital', 'defunt_lieunaissance');
        // On récupère les valeurs de l'option, si il y en a pas retourne un tableau vide
        $option_casse_force_majuscule = $this->f->get_option_casse_force_majuscule();
        // On boucle sur les champs à mettre en majuscule auto
        foreach ($fields_to_upper_case as $field) {
            // Si la clé existe dans l'option et que la valeur est true ou que la clé n'existe pas
            if (array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule)
                && $option_casse_force_majuscule[$this->clePrimaire.'.'.$field] === true
                || array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule) == false) {
                // On force la majuscule sur le champ
                $form->setOnchange($field, "this.value=this.value.toUpperCase()");
            }
        }

        $form->setOnchange('defunt_datenaissance','fdate(this)');
        $form->setOnchange('defunt_datedeces','fdate(this)');
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib('defunt_titre',"");
        $form->setLib('defunt_nom',__("nom de naissance"));
        $form->setLib('defunt_marital',__("nom d'usage"));
        $form->setLib('defunt_prenom',__("prenom"));
        $form->setLib('defunt_datenaissance',__("ne(e) le"));
        $form->setLib('defunt_lieunaissance',__("a"));
        $form->setLib('defunt_datedeces',__("decede le"));
        $form->setLib('defunt_lieudeces',__("a"));
        $form->setLib('defunt_parente',__("parente"));
        $form->setLib('defunt_nature',"");
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // creation du defunt
        $valF["operation_defunt"]=$this->f->db->nextId(DB_PREFIXE."operation_defunt");
        $valF['operation'] = $this->valF["operation"];
        $valF['defunt_titre'] = $val['defunt_titre'];
        if ($valF['defunt_titre'] == "") {
            $valF['defunt_titre'] = null;
        }
        $valF['defunt_nom'] = $val['defunt_nom'];
        $valF['defunt_marital'] = $val['defunt_marital'];
        $valF['defunt_prenom'] = $val['defunt_prenom'];
        //
        if ($val['defunt_datenaissance'] != "") {
            $valF['defunt_datenaissance'] = $this->dateDB($val['defunt_datenaissance']);
        } else {
            $valF['defunt_datenaissance'] = NULL;
        }
        //
        if ($val['defunt_datedeces'] != "") {
            $valF['defunt_datedeces'] = $this->dateDB($val['defunt_datedeces']);
        } else {
            $valF['defunt_datedeces'] = NULL;
        }
        $valF['defunt_lieudeces'] = $val['defunt_lieudeces'];
        $valF['defunt_lieunaissance'] = $val['defunt_lieunaissance'];
        $valF['defunt_parente'] = $val['defunt_parente'];
        $valF['defunt_nature'] = $val['defunt_nature'];
        $res= $this->f->db->autoExecute(DB_PREFIXE."operation_defunt",$valF,DB_AUTOQUERY_INSERT);
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Enregistrement ").
                   $valF["operation_defunt"]." de la table operation_defunt [".$this->f->db->affectedRows().
            " ".__("ajoute")."]") ;

        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // modification du defunt
        $valF['defunt_titre'] = $val['defunt_titre'];
        if ($valF['defunt_titre'] == "") {
            $valF['defunt_titre'] = null;
        }
        $valF['defunt_nom'] = $val['defunt_nom'];
        $valF['defunt_marital'] = $val['defunt_marital'];
        $valF['defunt_prenom'] = $val['defunt_prenom'];
        //
        if ($val['defunt_datenaissance'] != "") {
            $valF['defunt_datenaissance'] = $this->dateDB($val['defunt_datenaissance']);
        } else {
            $valF['defunt_datenaissance'] = NULL;
        }
        //
        if ($val['defunt_datedeces'] != "") {
            $valF['defunt_datedeces'] = $this->dateDB($val['defunt_datedeces']);
        } else {
            $valF['defunt_datedeces'] = NULL;
        }
        $valF['defunt_lieudeces'] = $val['defunt_lieudeces'];
        $valF['defunt_lieunaissance'] = $val['defunt_lieunaissance'];
        $valF['defunt_parente'] = $val['defunt_parente'];
        $valF['defunt_nature'] = $val['defunt_nature'];
        // a voir type de cercueil
        $cle= "operation =".$this->valF["operation"];
        $res= $this->f->db->autoExecute(DB_PREFIXE."operation_defunt",$valF,DB_AUTOQUERY_UPDATE,$cle);
        if ($this->f->isDatabaseError($res, true) !== false) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Enregistrement ").
                   $id." de la table operation_defunt [".$this->f->db->affectedRows().
            " ".__("modifie")."]") ;

        return $this->end_treatment(__METHOD__, true);
    }
}
