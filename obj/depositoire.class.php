<?php
/**
 * Ce script définit la classe 'depositoire'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/ossuaire.class.php";

/**
 * Définition de la classe 'depositoire' (om_dbform).
 *
 * Surcharge de la classe 'ossuaire'.
 */
class depositoire extends ossuaire {

    /**
     * @var string
     */
    protected $_absolute_class_name = "depositoire";

    /**
     * Attribut de nature d'emplacement
     * @var string Prend la valeur "depositoire"
     */
    var $nature = "depositoire";
}
