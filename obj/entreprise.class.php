<?php
/**
 * Ce script définit la classe 'entreprise'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/entreprise.class.php";

/**
 * Définition de la classe 'entreprise' (om_dbform).
 */
class entreprise extends entreprise_gen {

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        // * mise en majuscule
        // On liste les champs sur lesquels on veut forcer les majuscules automatiques
        $fields_to_upper_case = array('nomentreprise', 'adresse1', 'adresse2', 'ville');
        // On récupère les valeurs de l'option, si il y en a pas retourne un tableau vide
        $option_casse_force_majuscule = $this->f->get_option_casse_force_majuscule();
        // On boucle sur les champs à mettre en majuscule auto
        foreach ($fields_to_upper_case as $field) {
            // Si la clé existe dans l'option et que la valeur est true ou que la clé n'existe pas
            if (array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule)
                && $option_casse_force_majuscule[$this->clePrimaire.'.'.$field] === true
                || array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule) == false) {
                // On force la majuscule sur le champ
                $form->setOnchange($field, "this.value=this.value.toUpperCase()");
            }
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj < 2) {
            //pompes funebres
            $contenu=array();
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(__("Non"),__("Oui"));
            $form->setSelect("pf",$contenu);
        }
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) {
            $form->setType('pf','select');
        }
    }
}
