<?php
/**
 * Ce script définit la classe 'reduction'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/operation.class.php";

/**
 * Définition de la classe 'reduction' (om_dbform).
 *
 * Surcharge de la classe 'operation'.
 */
class reduction extends operation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "reduction";

    /**
     * @var string
     */
    var $categorie = "reduction";

}
