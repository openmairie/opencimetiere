<?php
/**
 * Ce script définit la classe 'autorisation'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../gen/obj/autorisation.class.php";

/**
 * Définition de la classe 'autorisation' (om_dbform).
 */
class autorisation extends autorisation_gen {

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "autorisation",
            "emplacement",
            "nature",
            "titre",
            "nom",
            "marital",
            "prenom",
            "datenaissance",
            "dcd",
            "adresse1",
            "adresse2",
            "cp",
            "ville",
            "telephone1",
            "telephone2",
            "courriel",
            "parente",
            "genealogie",
            "observation",
        );
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        $form->setType('emplacement', 'hidden');
        $form->setType('genealogie', 'hidden');

        if ($maj == 0) {
            $form->setType('nature', 'select');
        } else {
            $form->setType('nature', 'hiddenstatic');
        }
        //
        if ($maj < 2) {
            $form->setType('autorisation', 'hiddenstatic');
        }

        if ($maj == 3) {
            $form->setType('genealogie', 'static');
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // type de contact
            $contenu = array();
            $contenu [0] = array ("", "concessionnaire", "ayantdroit", "autre");
            $contenu [1] = array (__("Choisir nature"), __("Concessionnaire"), __("Ayant-droit"), __("Autre"));
            $form->setSelect("nature", $contenu);
        }
    }

    /**
     * SETTER_FORM - setValsousformulaire (setVal).
     *
     * @return void
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal('autorisation', "[...]");
                $form->setVal('emplacement', $idxformulaire);
            }
        }
        // En mode consultation on récupère les lien de parenté du contact sur lequel on est 
        if ($maj == 3) {
            $form->setVal('genealogie', $this->f->generate_text_genealogie('autorisation', $this->getVal($this->clePrimaire)));
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib('nom', __("Nom de naissance"));
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        // * mise en majuscule
        // On liste les champs sur lesquels on veut forcer les majuscules automatiques
        $fields_to_upper_case = array('nom', 'prenom', 'marital', 'adresse1', 'adresse2', 'ville');
        // On récupère les valeurs de l'option, si il y en a pas retourne un tableau vide
        $option_casse_force_majuscule = $this->f->get_option_casse_force_majuscule();
        // On boucle sur les champs à mettre en majuscule auto
        foreach ($fields_to_upper_case as $field) {
            // Si la clé existe dans l'option et que la valeur est true ou que la clé n'existe pas
            if (array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule)
                && $option_casse_force_majuscule[$this->clePrimaire.'.'.$field] === true
                || array_key_exists($this->clePrimaire.'.'.$field, $option_casse_force_majuscule) == false) {
                // On force la majuscule sur le champ
                $form->setOnchange($field, "this.value=this.value.toUpperCase()");
            }
        }
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $bloc_global_width_full = "span12";
        $bloc_global_width_half = "span6";

        //// CONTAINER GLOBAL - START
        $this->form->setBloc("autorisation", "D", "", "emplacement-form autorisation-form form-action-".$maj);

        //// LIGNE ID - START
        $this->form->setBloc("autorisation", "D", "", "row-fluid");
            //// BLOC ID - START
            $this->form->setBloc("autorisation", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-identification ");
                //// BLOC ID - START
                $this->form->setBloc("autorisation", "D", "", "span12");
                $this->form->setBloc("nature", "F");
                //// BLOC ID - END
            $this->form->setBloc("nature", "F");
            //// BLOC ID - END
        $this->form->setBloc("nature", "F");
        //// LIGNE ID - END

        //// LIGNE PERSONNE + INFOS + ADRESSE - START
        $this->form->setBloc("titre", "D", "", "row-fluid");
            //// BLOC PERSONNE + INFOS + ADRESSE - START
            $this->form->setBloc("titre", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-infos ");
                //// LIGNE PERSONNE + INFOS + ADRESSE - START
                $this->form->setBloc("titre", "D", "", "row-fluid");
                    //// BLOC PERSONNE + INFOS - START
                    $this->form->setBloc("titre", "D", "", $bloc_global_width_half);
                        //// LIGNE PERSONNE - START
                        $this->form->setBloc("titre", "D", "", "row-fluid");
                            //// BLOC PERSONNE - START
                            $this->form->setBloc("titre", "D", __("Contact"), " emplacement-bloc-personne span12");
                                $this->form->setBloc("titre", "D", "", "span12");
                                $this->form->setBloc("datenaissance", "F");
                            $this->form->setBloc("datenaissance", "F");
                            //// BLOC PERSONNE - END
                        $this->form->setBloc("datenaissance", "F");
                        //// LIGNE PERSONNE - END
                        //// LIGNE INFOS - START
                        $this->form->setBloc("dcd", "D", "", "row-fluid");
                            //// BLOC INFOS - START
                            $this->form->setBloc("dcd", "D", __("Informations"), "emplacement-bloc-personnne-infos span12 ");
                                $this->form->setBloc("dcd", "DF", "", "span12");
                            $this->form->setBloc("dcd", "F");
                            //// BLOC INFOS - END
                        $this->form->setBloc("dcd", "F");
                        //// LIGNE INFOS - END
                    $this->form->setBloc("dcd", "F");
                    //// BLOC PERSONNE + INFOS - END
                    //// BLOC ADRESSE - START
                    $this->form->setBloc("adresse1", "D", "", $bloc_global_width_half);
                        //// LIGNE ADRESSE - START
                        $this->form->setBloc("adresse1", "D", "", "row-fluid");
                            //// BLOC ADRESSE - START
                            $this->form->setBloc("adresse1", "D", __("Adresse"), "emplacement-bloc-adresse span12");
                                $this->form->setBloc("adresse1", "D", "", "span12");
                                $this->form->setBloc("courriel", "F");
                            $this->form->setBloc("courriel", "F");
                            //// BLOC ADRESSE - END
                        $this->form->setBloc("courriel", "F");
                        //// LIGNE ADRESSE - END
                    $this->form->setBloc("courriel", "F");
                    //// BLOC ADRESSE - END
                $this->form->setBloc("courriel", "F");
                //// LIGNE PERSONNE + INFOS + ADRESSE - END
            $this->form->setBloc("courriel", "F");
            //// BLOC PERSONNE + INFOS + ADRESSE - END
        $this->form->setBloc("courriel", "F");
        //// LIGNE PERSONNE + INFOS + ADRESSE - END
        
        //// LIGNE parente - START
        $this->form->setBloc("parente", "D", "", "row-fluid");
            //// BLOC genealogie - START
            $this->form->setBloc("parente", "D", __("Parenté"), $bloc_global_width_full." emplacement-bloc emplacement-bloc-parente ");
                $this->form->setBloc("parente", "D", "", "span6");
                $this->form->setBloc("genealogie", "F", "", "");
            $this->form->setBloc("genealogie", "F");
            // BLOC parente - END
        $this->form->setBloc("genealogie", "F");
        //// LIGNE parente - START

        //// LIGNE OBSERVATION - START
        $this->form->setBloc("observation", "D", "", "row-fluid");
            //// BLOC OBSERVATION - START
            $this->form->setBloc("observation", "D", "", $bloc_global_width_full." emplacement-bloc emplacement-bloc-observation ");
                $this->form->setBloc("observation", "DF", "", "group span12");
            $this->form->setBloc("observation", "F");
            // BLOC OBSERVATION - END
        $this->form->setBloc("observation", "F");
        //// LIGNE OBSERVATION - START


        //// CONTAINER GLOBAL - END
        $this->form->setBloc("observation", "F");


    }

    /**
     * MERGE_FIELDS - get_labels_merge_fields.
     *
     * @return array
     */
    function get_labels_merge_fields() {
        $labels = parent::get_labels_merge_fields();
        //
        $labels["autorisation"]["autorisation.titre"] = __("titre de civilité (libellé)");
        $labels["autorisation"]["autorisation.titre_code"] = __("titre de civilité (code)");
        $labels["autorisation"]["autorisation.titre_description"] = __("titre de civilité (description)");
        $labels["autorisation"]["autorisation.premier_prenom"] = __("Premier prénom du contact. (Ne fonctionne que si les prénoms sont séparés par des virgules)");
        //
        return $labels;
    }

    /**
     * MERGE_FIELDS - get_values_merge_fields.
     *
     * @return array
     */
    function get_values_merge_fields() {
        $values = parent::get_values_merge_fields();
        //
        $inst_titre = $this->f->get_inst__om_dbform(array(
            "obj" => "titre_de_civilite",
            "idx" => $this->getVal("titre"),
        ));
        $values["autorisation.titre"] = $inst_titre->getVal("libelle");
        $values["autorisation.titre_code"] = $inst_titre->getVal("code");
        $values["autorisation.titre_description"] = $inst_titre->getVal("description");
        // Récupère le premier prénom du contact
        $values["autorisation.premier_prenom"] = trim(explode(',', $values['autorisation.prenom'])[0]);
        //
        $values["autorisation.observation"] = str_replace("\n", "<br/>", $values["autorisation.observation"]);
        return $values;
    }
}
