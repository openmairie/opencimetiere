<?php
/**
 * Ce script définit la classe 'inhumation_terraincommunal'.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/inhumation.class.php";

/**
 * Définition de la classe 'inhumation_terraincommunal' (om_dbform).
 *
 * Surcharge de la classe 'inhumation'.
 */
class inhumation_terraincommunal extends inhumation {

    /**
     * @var string
     */
    protected $_absolute_class_name = "inhumation_terraincommunal";

    /**
     * @var string
     */
    var $emplacement_nature = 'terraincommunal';

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if($maj<2){
            //nature
            $contenu=array();
            $contenu[0]=array('cercueil','zinc');
            $contenu[1]=array(__('cercueil'),__("cercueil_zinc"));
            $form->setSelect("defunt_nature",$contenu);
        }
    }
}
