<?php
/**
 * Widget - Concession(s) à terme
 *
 * Ce widget permet d'afficher la première  page du listing des concessions qui
 * sont à terme ainsi qu'un lien permettant d'accéder au listing complet.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/opencimetiere.class.php";
if (!isset($f)) {
    $f = new opencimetiere(null, null, __("Widget - Contrat(s) à Valider"));
}

// Nom de l'objet métier
$obj = "contrat";

// si l'utilisateur a les permissions pour accéder à l'élément
if ($f->isAccredited(array($obj."_tab", $obj), "OR") == true) {

    /**
     * Initialisation des variables nécessaires à l'utilisation du tableau
     */
    // Premier enregistrement a afficher
    $premier = 0;
    // Colonne choisie pour le tri
    $tricol = "";
    // Id unique de la recherche avancee
    $advs_id = "";
    // Valilite des objets a afficher
    $valide = "";
    //
    $options = array();
    //
    $extra_parameters = array();
    //
    $om_validite = false;
    //
    $href = array();
    // Declaration du dictionnaire
    $tab_actions = array('corner' => array(),
                         'left' => array(),
                         'content' => array(),
                         'specific_content' => array(),);

    // /**
    //  *
    //  */
    // Surcharge spécifique des objets
    if (file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php")) {
        require_once "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
    }

    /**
     *
     */
    $href = array();
    // Declaration du dictionnaire
    $tab_actions = array('corner' => array(),
                         'left' => array(),
                         'content' => array(),
                         'specific_content' => array(),);
    
    $tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj=contrat&amp;action=3&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
    
    // On cache la recherche
    $options[] = array(
        "type" => "search",
        "display" => false,
    );
    // On cache la pagination
    $options[] = array(
        "type" => "pagination_select",
        "display" => false,
    );
    $field_contrat_emplacement_famille = 'emplacement.famille as "'.__("famille").'"';
    $champAffiche_widget = array(
      'contrat.contrat as "'.__("id").'"',
      $field_contrat_emplacement_famille,
      'to_char(contrat.datedemande ,\'DD/MM/YYYY\') as "'.__("datedemande").'"',
      'contrat.origine as "'.__("origine").'"',
    );

    $serie_widget=15;

    $edition ="";

    $selection_widget=" where valide is not true";

    $search = array(
        "valide" => "false",
        "advanced-search-submit" => "",
    );
    $advs_id = str_replace(array('.',','), '', microtime(true));
    $_SESSION["advs_ids"][$advs_id] = serialize($search);
    /**
     * Instanciation de la classe et affichage du tableau
     */
    //
    $tb = $f->get_inst__om_table(array(
        "aff" => OM_ROUTE_TAB,
        "table" => $table,
        "serie" => $serie_widget,
        "champAffiche" => $champAffiche_widget,
        "champRecherche" => $champRecherche,
        "tri" => $tri,
        "selection" => $selection_widget,
        "edition" => $edition,
        "options" => $options,
        "advs_id" => $advs_id,
    ));
    //
    $params = array(
        "obj" => $obj,
        "premier" => $premier,
        "tricol" => $tricol,
        "valide" => $valide,
    );
    // Ajout de paramètre spécifique
    $params = array_merge($params,$extra_parameters);
    //
    $tb->display($params, $tab_actions, $f->db, "tab", false);
    
    /**
     * Affichage du lien vers le tableau standard
     */
    $footer = OM_ROUTE_TAB."&obj=contrat&advs_id=".$advs_id;
    $footer_title = __("Voir tous les contrats a valider");

}