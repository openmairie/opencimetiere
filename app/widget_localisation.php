<?php
/**
 * Widget - Localisation
 *
 * Ce widget permet d'afficher des accès directs aux écrand le localisation quel
 * que soit le système de localisation utilisé. Pour le système de localisation
 * sur plans, ce sont des liens vers chacun des plans qui sont présentés à
 * l'utilisateur. Pour le système de localisation SIG interne, le widget est à
 * composer.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/opencimetiere.class.php";
if (!isset($f)) {
    $f = new opencimetiere(null, null, __("Widget - Localisation"));
}

// On vérifie que l'utilisateur a la permission d'accéder aux emplacements
// en visualisation pour afficher les informations du widget
$permissions = array(
    "concession_tab", "concession",
    "colombarium_tab", "colombarium",
    "enfeu_tab", "enfeu",
    "ossuaire_tab", "ossuaire",
    "depositoire_tab", "depositoire",
    "terraincommunal_tab", "terraincommunal",
);
if ($f->isAccredited($permissions, "OR") == true) {

    // On teste quelle option de localisation est activée
    if ($f->getParameter("option_localisation") == "plan") {

        // Lorsque l'option de localisation est positionnée sur "plan" alors on
        // affiche la liste des plans avec un lien vers l'écran de visualisation
        // du plan et de tous les emplacements qui sont sur ce dernier

        // On exécute une requête de récupération de la liste des plans
        $sql = "select * from ".DB_PREFIXE."plans order by planslib";
        $res =& $f->db->query($sql);
        $f->addToLog("app/widget_localisation.php: db->query(\"".$sql."\");", VERBOSE_MODE);
        $f->isDatabaseError($res);

        // On teste le nombre de résultats de la requête
        if ($res->numRows() == 0) {
            // Si aucun résultat
            echo __("Aucun plan n'est configuré. Contactez votre administrateur.");
        } else {
            // Si on a au moins un résultat
            echo "<ul>";
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                echo "<li>";
                echo "<a href=\"";
                echo "../app/localisation_plan_view.php?plan=".$row["plans"];
                echo "\">";
                echo "<span class=\"om-icon om-icon-16 om-icon-fix sig-16\" title=\"".__("Visualiser le plan")."\">".__("Plan")."</span>";
                echo "</a>&nbsp;";
                echo "<a href=\"";
                echo "../app/localisation_plan_view.php?plan=".$row["plans"];
                echo "\">";
                echo $row["planslib"];
                echo "</a>";
                echo "</li>";
            }
            echo "</ul>";
        }

    } elseif ($f->getParameter("option_localisation") == "sig_interne") {

        // Lorsque l'option de localisation est positionnée sur "sig_interne"
        // on affiche les plans de cimetiere

        // On exécute une requête de récupération de la liste des plans
        $sql = "select * from ".DB_PREFIXE."cimetiere where geom is not null order by cimetierelib";
        $res =& $f->db->query($sql);
        $f->addToLog("app/widget_localisation.php: db->query(\"".$sql."\");", VERBOSE_MODE);
        $f->isDatabaseError($res);

        // On teste le nombre de résultats de la requête
        if ($res->numRows() == 0) {
            // Si aucun résultat
            echo __("Aucun plan n'est configuré. Contactez votre administrateur.");
        } else {
            // Si on a au moins un résultat
            echo "<ul>";
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                echo "<li>";
                echo "<a href=\"";
                echo OM_ROUTE_MAP."&mode=tab_sig&obj=carte_cimetiere&idx=".$row["cimetiere"];
                echo "\">";
                echo "<span class=\"om-icon om-icon-16 om-icon-fix sig-16\" title=\"".__("Visualiser le plan")."\">".__("Plan")."</span>";
                echo "</a>&nbsp;";
                echo "<a href=\"";
                echo OM_ROUTE_MAP."&mode=tab_sig&obj=carte_cimetiere&idx=".$row["cimetiere"];
                echo "\">";
                echo $row["cimetierelib"];
                echo "</a>";
                echo "</li>";
            }
            echo "</ul>";
        }


    } elseif ($f->getParameter("option_localisation") == "sig_externe") {

        // Lorsque l'option de localisation est positionnée sur "sig_externe"
        // XXX Compléter ici le contenu du widget pour l'option sig_externe

    } else {

        // Lorsque l'option de localisation n'est pas définie alors on affiche
        // un message clair à l'utilisateur
        echo __("Aucune option de localisation n'est configurée. Contactez votre administrateur.");

    }
}
