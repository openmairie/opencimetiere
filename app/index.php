<?php
/**
 * Ce script permet d'interfacer l'application.
 *
 * @package opencimetiere
 * @version SVN : $Id: index.php 4099 2017-12-19 15:18:16Z fmichon $
 */

require_once "../obj/opencimetiere.class.php";
$flag = filter_input(INPUT_GET, 'module');
if (in_array($flag, array("login", "logout", )) === false) {
    $flag = "nohtml";
}
$f = new opencimetiere($flag);
$f->view_main();
