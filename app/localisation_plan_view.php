<?php
/**
 * Ce script permet de ...
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/opencimetiere.class.php";
$f = new opencimetiere("nohtml");

// Cette fonction permet de calculer le lien retour en fonction des paramètres
// récupérés en $_GET.
function calculate_back_link($get, $emplacements) {
    //
    if (isset($get["retour"]) && $get["retour"] == "tab") {
        //
        $link = OM_ROUTE_TAB;
        $link .= "&";
        //
        if (isset($get["specific_origin"])) {
            $get["obj"] = $get["specific_origin"];
        }
        //
        foreach ($get as $key => $value) {
            $link .= "&amp;".$key."=".$value;
        }
    } elseif (isset($get["retour"]) && $get["retour"] == "form" && isset($emplacements[0])) {
        //
        $emplacement = $emplacements[0];
        //
        $link = OM_ROUTE_TAB;
        $link .= "&";
        //
        $link .= "obj=".$emplacement['nature']."&amp;idx=".$emplacement['emplacement']."&amp;action=3";
    } else {
        //
        $link = OM_ROUTE_DASHBOARD;
    }
    //
    return $link;
}

// Cette fonction permet d'afficher le bouton retour
function display_button_back($link = "") {
    //
    echo "<div class=\"formControls\">\n";
    //
    echo "<a class=\"retour\" ";
    echo "href=\"";
    //
    echo $link;
    //
    echo "\"";
    echo ">";
    //
    echo __("Retour");
    //
    echo "</a>\n";
    //
    echo "</div>\n";
}

//
$emplacements = array();

//
$mode = 0;

// MODE 1 - Visualisation d'un seul objet sur le plan
//
$obj = "";
if (isset($_GET['obj'])) {
    $obj = $_GET['obj'];
}
//
$idx = "";
if (isset($_GET['idx'])) {
    $idx = $_GET['idx'];
}
//
if ($obj != "" && $idx != "") {
    $mode = 1;
}
//
if ($mode == 1 && (strpos($obj, "/") !== false
    || !file_exists("../obj/".$obj.".class.php"))) {
    $class = "error";
    $message = __("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

// MODE 2 - Visualisation de tous les objets sur le plan
//
$plan = "";
if (isset($_GET['plan'])) {
    $plan = $_GET['plan'];
}
//
$nature = "";
if (isset($_GET['nature'])) {
    $nature = $_GET['nature'];
}
//
if ($mode == 0 && $plan != "") {
    $mode = 2;
}

//
if ($mode == 1) {
    //
    require "../obj/".$obj.".class.php";
    $emplacement = new $obj($idx, $f->db, 0);
    //
    $plan = $emplacement->getVal("plans");
    //
    $emplacements[] = array(
        "emplacement" => $emplacement->getVal("emplacement"),
        "positionx" => $emplacement->getVal("positionx"),
        "positiony" => $emplacement->getVal("positiony"),
        "nature" => $emplacement->getVal("nature"),
        "famille" => $emplacement->getVal("famille"),
        "libre" => $emplacement->getVal("libre"),
        "terme" => $emplacement->getVal("terme"),
    );
}

//
if ($mode == 2) {
    //
    $sql = "select * from ".DB_PREFIXE."emplacement where plans='".$plan."'";
    if ($nature != "") {
        $sql .= " and nature='".$nature."'";
    }
    $res = $f->db->query($sql);
    $f->addToLog("visualisationplan.php: db->query(\"".$sql."\");", VERBOSE_MODE);
    $f->isDatabaseError($res);
    while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
        $emplacements[] = $row;
    }
}

//
require_once "../obj/plans.class.php";
$plans = new plans($plan, $f->db, NULL);
//
$imageplan = $f->storage->getPath($plans->getVal("fichier"));

//
if (!file_exists($imageplan)) {
    $class = "error";
    $message = __("L'objet est invalide car il n y a pas de plan selectionné pour cet enregistrement.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

//
$sql = "select planslib from ".DB_PREFIXE."plans where plans='".$plan."'";
$plan_libelle = $f->db->getone($sql);
$f->addToLog("app/widget_localisation.php: db->getone(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($plan_libelle);

//
$html_body = "<body class=\"localisation\">";
$f->setHTMLBody($html_body);

//
$f->setFlag("htmlonly");
$f->display();

//
$f->displayStartContent();
//
$f->setTitle(__("Localisation")." -> ".$plan_libelle." (".$plan.")");
$f->displayTitle();

/**
 *
 */
//
display_button_back(calculate_back_link($_GET, $emplacements));

/**
 *
 */
//
$size = getimagesize($imageplan);
//
echo "<div";
echo " id=\"localisation-wrapper\"";
echo " style=\"";
//
echo "border: 1px solid #cdcdcd; ";
echo "position:relative; ";
echo "width:".$size[0]."px; ";
echo "height:".$size[1]."px; ";
echo "background-image: url('".OM_ROUTE_FORM."&snippet=file&obj=plans&champ=fichier&id=".$plan."'); ";
//
echo "\"";
echo ">\n";
//
foreach($emplacements as $i => $emplacement) {
    //
    echo "<div id=\"camap".$i."\"";
    echo " style=\"";
    echo "position:absolute; ";
    echo "left:".($emplacement['positionx'])."px;";
    echo "top:".($emplacement['positiony'])."px";
    echo "\">\n";
    //
    echo "\t<a href=\"".OM_ROUTE_FORM."&obj=".$emplacement['nature']."&amp;idx=".$emplacement['emplacement']."&amp;action=3\">";
    //
    echo "<img name='image$i' src='../app/img/";
    //
    if ($emplacement['libre'] == 'Oui') {
        echo "point_emplacement_libre.png";
    } else {
        //
        if ($emplacement['terme']=='temporaire') {
            //
            echo "point_emplacement_temporaire.png";
        } elseif ($emplacement['terme']=='perpetuite') {
            //
            echo "point_emplacement_perpetuite.png";
        } else {
            //
            echo "point_emplacement.png";
        }
    }
    //
    echo "' title=\"".$emplacement['famille']."\" alt=\".\" />";
    //
    echo "</a>\n";
    //
    echo "</div>\n";
}
//
echo "\n</div>\n";
//
echo "<div class=\"visualClear\"><!-- --></div>\n";

/**
 *
 */
//
display_button_back(calculate_back_link($_GET, $emplacements));

/**
 *
 */
//
$f->displayEndContent();
