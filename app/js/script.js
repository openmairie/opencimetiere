/**
 * Script JS spécifique à l'applicatif, ce script est destiné à être
 * appelé en dernier dans la pile des fichiers JS.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

/**
 * Surcharge autocomplete framework
 * 
 * La surcharge porte sur deux points : 
 * - on passe le champ en readonly lorsque une valeur est sélectionnée (évite
 *   que le champ soit éditable après que la liaison ait été effectuée et que
 *   l'utilisateur puisse saisir une valeur qu'il croit à tort valide),
 * - on positionne le focus sur le champ de recherche lorsque une valeur est
 *   déselectionnée (ceci afin d'éviter un clic à l'utilisateur).
 */
function handle_autocomplete_readonly() {
    $("input.autocomplete").each (function(){
        var obj = $(this).parents().attr("id").replace("autocomplete-","");
        var autocomplete_id = "autocomplete-"+obj;
        if ($("#"+autocomplete_id+"-id").val() == "") {
            $("#"+autocomplete_id+"-search").prop('readonly', false);
        } else {
            $("#"+autocomplete_id+"-search").prop('readonly', true);
        }
        $("#"+autocomplete_id+"-id").change(function(){
            if ($("#"+autocomplete_id+"-id").val() == "") {
                $("#"+autocomplete_id+"-search").prop('readonly', false);
                $("#"+autocomplete_id+"-search").focus();
            } else {
                $("#"+autocomplete_id+"-search").prop('readonly', true);
            }
        });
    });
}

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

/**
 * Donne le focus aux champs <input /> spécifiés avec la classe 'om-autofocus'.
 */
function handle_input_autofocus() {
  $('input.om-autofocus').focus();
}

/**
 * Au chargement de la page
 */
$(function() {
    // Gère le comportement de la page settings (administration et paramétrage).
    handle_settings_page();
    // Donne le focus aux champs <input /> spécifiés avec la classe 'om-autofocus'.
    handle_input_autofocus();
});


function app_initialize_content(tinymce_load) {
    handle_autocomplete_readonly();
    form_helper__field_date_today_with_space();
    form_helper__field_montant();
    form_helper__fields_terme_duree_and_date("contrat");
    form_helper__fields_terme_duree_and_date("emplacement");
    if ($("#form-content .contrat-form #terme").val() == "perpetuite" || $("#form-content .emplacement-form #terme").val() == "perpetuite" ) {
        $("#form-content .contrat-form #duree").prop('readonly', true);
        $("#form-content .emplacement-form #duree").prop('readonly', true);
    }
    merge_select_personne_genealogie();
    if ($('.overlay_defunt #action-sousform-defunt-modifier').length != 0) {
        $('.ui-dialog .formControls').hide();
    }
    if ($('.overlay_defunt .tab-tab').length > 0) {
        $('.soustab-container').hide();
    }

    if ($('#plan_en_coupe').length != 0) {
        make_fields_lockable();
        $('button').each(function(){lock_unlock_field(this)});
    }
}

/**
 * FORM_HELPER - form_helper__field_date_today_with_space.
 *
 * Aide à la saisie sur tous les champs date : la saisie de la touche espace
 * insère la date du jour si le champ est vide.
 */
function form_helper__field_date_today_with_space() {
    $("input.datepicker").keypress(function(event) {
        if (event.which == 32) {
            if ($(this).val() == "") {
                var today = new Date().toLocaleDateString("fr");
                $(this).val(today);
            }
        }
    });
}

/**
 * FORM_HELPER - form_helper__montant_max.
 */
function form_helper__montant_max() {
    montant = $("#form-content #montant").val();
    if (montant > 999999999.99) {
        window.alert("Le montant maximum est 999999999.99");
        $("#form-content #montant").val('');
    }
}

/**
 * FORM_HELPER - form_helper__calculate_dateterme.
 */
function form_helper__calculate_dateterme(form_context) {
    if ($("#form-content ."+form_context+"-form #terme").val() != "temporaire") {
        return;
    }
    datevente = $("#form-content ."+form_context+"-form #datevente").val();
    daterenouvellement = $("#form-content ."+form_context+"-form #daterenouvellement").val();
    duree = parseInt($("#form-content ."+form_context+"-form #duree").val());
    datereference = "";
    if (daterenouvellement != undefined && daterenouvellement != "") {
        datereference = daterenouvellement;
    } else if (datevente != undefined && datevente != "") {
        datereference = datevente;
    }
    if (datereference != ""
        && duree > 0) {
        var jour = datereference.split('/')[0];
        var mois = parseInt(datereference.split('/')[1])-1;
        var annee = parseInt(datereference.split('/')[2]);
        dateterme_obj = new Date(annee+duree, mois, jour);
        dateterme = dateterme_obj.toLocaleDateString('fr-FR');
        $("#form-content ."+form_context+"-form #dateterme").val(dateterme);
    }
}

/**
 * FORM_HELPER - form_helper__field_montant.
 */
function form_helper__field_montant() {
    $("#form-content #montant").on("blur", function(event) {
        form_helper__montant_max();
    });
}

/**
 * FORM_HELPER - form_helper__fields_terme_duree_and_date.
 */
function form_helper__fields_terme_duree_and_date(form_context) {
    if ($("#form-content ."+form_context+"-form").length == 0) {
        return;
    }
    $("#form-content ."+form_context+"-form #datevente").on("blur", function(event) {
        if ($(this).val() == ""
            && $("#form-content ."+form_context+"-form #daterenouvellement").val() == "") {
            $("#form-content ."+form_context+"-form #dateterme").val("");
        } else {
            form_helper__calculate_dateterme(form_context);
        }
    });
    $("#form-content ."+form_context+"-form #terme").on("blur", function(event) {
        if ($(this).val() == "perpetuite") {
            $("#form-content ."+form_context+"-form #dateterme").val("");
            $("#form-content ."+form_context+"-form #duree").val("0");
            $("#form-content ."+form_context+"-form #duree").prop('readonly', true);
        }
        if ($(this).val() == "temporaire") {
            form_helper__calculate_dateterme(form_context);
            $("#form-content ."+form_context+"-form #duree").val("");
            $("#form-content ."+form_context+"-form #duree").prop('readonly', false);
        }
        if ($(this).val() == "") {
            $("#form-content ."+form_context+"-form #duree").val("");
            $("#form-content ."+form_context+"-form #dateterme").val("");
            $("#form-content ."+form_context+"-form #duree").prop('readonly', false);
        }
    });
    $("#form-content ."+form_context+"-form #duree").on("blur", function(event) {
        form_helper__calculate_dateterme(form_context);
    });
    $("#form-content ."+form_context+"-form #daterenouvellement").on("blur", function(event) {
        if ($(this).val() == ""
            && $("#form-content ."+form_context+"-form #datevente").val() == "") {
            $("#form-content ."+form_context+"-form #dateterme").val("");
        } else {
            form_helper__calculate_dateterme(form_context);
        }
    });
}

/**
 * Aide à la saisie d'une voie sur un emplacement
 */
function overlay_contrat_montant() {
    contrat = $("#form-content .contrat-form #contrat").val();
    emplacement = $("#form-content .contrat-form #emplacement").val();
    datedemande = $("#form-content .contrat-form #datedemande").val();
    origine = $("#form-content .contrat-form #origine").val();
    terme = $("#form-content .contrat-form #terme").val();
    duree = $("#form-content .contrat-form #duree").val();
    datevente = $("#form-content .contrat-form #datevente").val();
    //
    var dialog = $('<div id="overlay-contrat-montant" role="dialog"></div>').insertAfter('#tabs-1 .formControls-bottom');

    $(dialog).dialog({
        //OnClose suppression du contenu
        close: function(ev, ui) {
            $(this).remove();
        },
        title: 'Calcul automatique du montant du contrat',
        resizable: true,
        modal: true,
        width: 'auto',
        height: 'auto',
        position: 'top-left',
    });

    link = "../app/index.php?module=form&obj=contrat&idx=0&action=21&emplacement="+emplacement+"&contrat="+contrat+"&datedemande="+datedemande+"&origine="+origine+"&terme="+terme+"&duree="+duree+"&datevente="+datevente

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Ajout du contenu recupere
            dialog.html(html);
            // Initialisation du theme OM
            om_initialize_content();
        },
        async : false
    });
    // Fermeture du dialog lors d'un clic sur le bouton retour
    $('#overlay-contrat-montant').on("click",'#close-button',function() {
        $(dialog).dialog('close').remove();
        return;
    });
    return false;
}

/**
 * Fonction de récupération des paramètres GET de la page
 * @return Array Tableau associatif contenant les paramètres GET
 */
function extractUrlParams(){    
    var t = location.search.substring(1).split('&');
    var f = [];
    for (var i=0; i<t.length; i++){
        var x = t[ i ].split('=');
        f[x[0]]=x[1];
    }
    return f;
}


/**
 * Permet d'afficher l'overlay contenant les informations du défunt
 */
function overlay_defunt(id_defunt) {
    var dialog = $('<div id="sousform-defunt" class="overlay_defunt" role="dialog"></div>').insertAfter('#plancoupe-href-base');
    (dialog).dialog({
        //OnClose suppression du contenu
        close: function(ev, ui) {
            $(this).remove();
            location.reload();
        },
        title: 'Informations du défunt',
        resizable: true,
        modal: true,
        width: '70%',
        height: 'auto',
        position: 'top-left',
    });

    urlParams = extractUrlParams()
    var idx_formulaire = 'idx' in urlParams ? urlParams['idx'] : null;
    
    link = "../app/index.php?module=sousform&obj=defunt&idx="+id_defunt+"&action=3&retourformulaire=concession&idxformulaire="+idx_formulaire;

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Ajout du contenu recupere
            dialog.html(html);
            // Initialisation du theme OM
            om_initialize_content();
        },
        async : false
    });

    // Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-defunt .overlay_defunt').on("click",'#close-button',function() {
        $(dialog).dialog('close').remove();
        return;
    });
    return false;

    
}

/**
 * Permet de mettre à jour la position du défunt
 */
function maj_pos_defunt(card) {
    // On doit attendre que la carte soit placé au bon endroit avant de mettre à jour
    setTimeout(() => {
        // L'identifiant de la carte est widget_<identifiant du défunt>
        id_defunt = card.id.slice(7);
        // Par défaut on met les coordonnées à la valeur NULL
        col = "NULL";
        line = "NULL";
        // L'identifiant column est réservé à la liste des défunts à la position NULL; NULL
        if (card.parentElement.id != "column") {
            // On récupère les classes de la case sur laquelle on a déposé la carte
            classes = card.parentElement.getAttribute('class');
            classes_tab = classes.split(' ');
            // Les classes sont dans cette ordre :
            // column_%s col%s line%s grid_defunt column ui-sortable
            col = classes_tab[1].slice(3);
            line = classes_tab[2].slice(4);
        }
        // Composition de l'url permettant de mettre à jour la position du défunt
        link = "../app/index.php?module=form&obj=defunt&idx="+id_defunt+"&action=11&x="+col+"&y="+line+"&validation=1";
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            success: function(html){
                return true;
            },
            async : false
        });
        make_fields_lockable();
    }, "250");
}

function lock_unlock_field($this) {
    parent = $this.parentNode;
    parent_class = parent.getAttribute('class')
    is_locked = parent_class.includes('cellLocked')
    is_locked_by_right = parent_class.includes('column_locked')

    if (is_locked == true || is_locked_by_right == true) {
        parent.classList.remove('column');
    }

    if (is_locked == false && is_locked_by_right == false) {
        parent.classList.add('column');
    }
}

/**
 * Permet de vérifier si il y a au moins une carte dans une case
 * et de masquer le bouton si c'est le cas
 */
function make_fields_lockable() {
    // console.log($('grid_defunt'));
    $('.grid_defunt').each(function(){
        children = this.children;
        is_not_lockable = false;
        substring = 'widget';
        for (let node of children) {
            id = node.getAttribute('id');
            //
            if (id != undefined && id.includes(substring)) {
                is_not_lockable = true;
            }
        }

        classes = this.getAttribute('class');
        classes_tab = classes.split(' ');
        // Les classes sont dans cette ordre :
        // column_%s col%s line%s grid_defunt column ui-sortable
        col = classes_tab[1].slice(3);
        line = classes_tab[2].slice(4);

        // Si l'utilisateur n'a pas les droits
        // pour verrouiller/déverrouiller des cases
        // alors on enlève les boutons
        if (classes.includes('hide_button')) {
            is_not_lockable = true;
        }

        if (is_not_lockable == true) {
            $('#but_case_'+col+'_'+line).hide();
        }

        if (is_not_lockable == false) {
            $('#but_case_'+col+'_'+line).show();
        }
    })
}

/**
 * Permet de mettre à jour la position du défunt
 */
function maj_etat_case($this, $coordonnees) {
    emplacement = $("#form-content #emplacement").val();
    coord = $coordonnees.split('_');
    coordonnees = coord[0]+";"+coord[1];

    parent_class = $this.parentNode.getAttribute('class')
    is_locked = parent_class.includes('cellLocked')
    iconClass = is_locked ? "./img/lock/lock.svg#ri-lock-fill" : "./img/lock/unlock.svg#ri-lock-unlock-fill";

    $this.setAttribute('href', iconClass);

    urlParams = extractUrlParams()
    var idx_formulaire = 'idx' in urlParams ? urlParams['idx'] : null;
    
    // Composition de l'url permettant de mettre à jour la position du défunt
    link = "../app/index.php?module=form&obj=emplacement&idx="+idx_formulaire+"&action=42&coordonnees="+$coordonnees+"&validation=1";
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        success: function(html){
            location.reload();
            return true;
        },
        async : false
    });
}


/**
 * Aide à la saisie d'une voie sur un emplacement
 */
function overlay_select_voie_by_cimetiere_zone() {
    //
    voie = $("#autocomplete-voie-id").val();
    //
    var dialog = $('<div id="overlay-selection-voie-by-cimetiere-zone" role="dialog"></div>').insertAfter('#tabs-1 .formControls-bottom');

    $(dialog).dialog({
        //OnClose suppression du contenu
        close: function(ev, ui) {
            $(this).remove();
        },
        title: 'Sélection de voie par cimetière/zone',
        resizable: true,
        modal: true,
        width: 'auto',
        height: 'auto',
        position: 'top-left',
    });

    link = "../app/index.php?module=form&obj=emplacement&idx=0&action=21&val="+voie

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Ajout du contenu recupere
            dialog.html(html);
            // Initialisation du theme OM
            om_initialize_content();
        },
        async : false
    });
    // Fermeture du dialog lors d'un clic sur le bouton retour
    $('#overlay-selection-voie-by-cimetiere-zone').on("click",'#close-button',function() {
        $(dialog).dialog('close').remove();
        return;
    });
    return false;
}

// a verifier ?
function changetaille()
{
if(document.f2.exhumation.value=="Oui")
        document.f1.taille.value=0.00;
}
//
function trt_confirm() {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        return true;
    } else {
        //
        return false;
    }
}

//
function trt_form_submit(id, link, formulaire) {
    // Si le formulaire n'a pas une classe no_confirmation
    if (!$(formulaire).hasClass("no_confirmation")) {
        // Alors on execute la fonction trt_confirm qui permet de demander
        // a l'utilisateur si il est sur de vouloir continuer
        if (trt_confirm() == false) {
            // Si l'utilisateur n'est pas sur alors on sort de la fonction
            return false;
        }
    }
    //
    return true;
}

/**
 * Les fonctions suivantes sont similaires aux fonctions qui permettent de gérer
 * la localisation dans les formulaires de paramétrage des éditions du framework
 * mais modifiées pour coller au besoin de la localisation sur plan
 * d'opencimetiere.
 */

//
$(function() {
   localisation_plan_bind_draggable();
});
//
function localisation_plan_bind_draggable() {
    $("#plan-draggable").draggable({ containment: "parent" });
    $("#plan-draggable").dblclick(function() {
        infos = $(this).attr("class");
        infos = infos.split(" ");
        infos = infos[0].split(";");
        // on enlève un à l'abscisse et à l'ordonnée pour la bordure de l'image
        // qui décale d'un pixel
        position = $(this).position();
        x = parseInt(position.left);
        y = parseInt(position.top);
        localisation_plan_return(infos[0], infos[1], (x-1), infos[2], (y-1));
        return true;
    });
}
// Cette fonction permet de retourner les informations sur le fichier téléchargé
// du formulaire d'upload vers le formulaire d'origine
function localisation_plan_return(form, champ_x, value_x, champ_y, value_y) {
    $("form[name|="+form+"] #"+champ_x, window.opener.document).attr('value', value_x);
    $("form[name|="+form+"] #"+champ_y, window.opener.document).attr('value', value_y);
    window.close();
}


/**
 * FORM WIDGET - LOCALISATION PLAN
 */
//
function localisation_plan(champ, chplan, positionx) {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var plan = document.f1.elements[chplan].value;
    var x = document.f1.elements[positionx].value;
    var y = document.f1.elements[champ].value;
    //
    if (plan == "") {
        //
        alert("Vous devez d'abord selectionner un plan pour pouvoir localiser l'emplacement.");
    } else {
        //
        link = "../app/localisation_plan_edit.php?positiony="+champ+"&positionx="+positionx+"&plan="+plan+"&form=f1"+"&x="+x+"&y="+y+"#draggable";
        //
        pfenetre = window.open(link,"localisation","toolbar=no,scrollbars=yes,width="+screen.availWidth+",height="+screen.availHeight+",top=0,left=0");
        //
        fenetreouverte = true;
    }
}

/**
 * Défini l'url pour charger le contenu de l'onglet de lde l'emplacement.
 * (uniquement dans le cas d'un clic depuis un autre onglet
 * et si l'url n'est pas déjà défini).
 * Cela permet de forcer le (re)chargement du contenu de l'onglet de l'emplacement
 * à chaque clic sur l'onglet de l'emplacement.
 *
 * @return  boolean  'true' si clique sur l'onglet de l'emplacement à partir d'un autre onglet, sinon 'false'
 */
function set_emplacement_href_in_data_load_tabs(evt, ui) {
    var ret = false;

    // recherche l'index de l'onglet de l'emplacement et son noeud DOM
    var emplacement_tab_title_node = null;
    var emplacement_tab_index = -1;
    var emplacement_tab_node = null;
    var list_of_tabs = $('.ui-tabs-nav li');
    for (var i = 0; i < list_of_tabs.length; i++) {
        var tab = list_of_tabs.get(i);
        var tab_title_node = $(tab).find('> a');
        if(tab_title_node && tab_title_node.length == 1) {
            tab_title_node.contents().filter(function() {
                return this.nodeType == Node.TEXT_NODE;
            });
            var tab_title = null;
            if (tab_title_node) {
                tab_title = $(tab_title_node).text().trim();
            }
            if (tab_title == 'concession' || tab_title == 'colombarium' || tab_title == 'enfeu') {
                emplacement_tab_title_node = tab_title_node.get(0);
                emplacement_tab_index = i;
                break;
            }
        }
    }

    // si l'onglet de l'emplacement existe (son index a été trouvé)
    if (emplacement_tab_index != -1 && emplacement_tab_title_node) {
        // récupère l'id de l'onglet actuellement sélectionné
        var tabIdSelectedFrom = $("#formulaire").tabs('class', 'selected');
        // si on clic sur l'onglet de l'emplacement à partir d'un autre onglet
        if (ui.tab == emplacement_tab_title_node && tabIdSelectedFrom != emplacement_tab_index) {
            ret = true;
            // récupère la source (href) de l'onglet de l'emplacement actuellement définie
            var tab_href = $.data(emplacement_tab_title_node, 'load.tabs');
            // si l'onglet n'a aucune source distante définie
            if (!tab_href) {
                // récupère l'URL de la page courante
                // (sans le hash car celui-ci est utilisé pour afficher un autre onglet)
                var tab_href = window.location.href;
                if (tab_href.indexOf('#') != -1) {
                    tab_href = window.location.href.split('#')[0];
                }
            }
            // une recherche dynamique est concessionsponible
            // défini la source (href) de l'onglet concession (ce qui provoquera son rechargement)
            $.data(emplacement_tab_title_node, 'load.tabs', tab_href);
        }
    }
    return ret;
}

/**
 * Fonction utilisée dans l'onglet Généalogie en mode ajout ou modificaion 
 * Permet de fusionner les options des champs défunt et autorisation dans
 * les champs personne 1 et personne 2
 * 
 * @return void
 */
function merge_select_personne_genealogie() {
    // On sélectionne toutes les options de défunt et autorisation
    // Les deux sélect ont les mêmes option donc on prend p1
    defunt = $('.field-type-select #defunt_p1 option');
    autorisation = $('.field-type-select #autorisation_p1 option');

    // On copie chaque option du champ défunt dans les champs personne 1 et personne 2
    defunt.each(function(){
        if (this.value != '') {
            cloned_node_personne1 = this.cloneNode(true);
            $('.field-type-select #personne_1').append(cloned_node_personne1);

            cloned_node_personne2 = this.cloneNode(true);
            $('.field-type-select #personne_2').append(cloned_node_personne2);
        }
    });

    // On copie chaque option du champ autorisation dans les champs personne 1 et personne 2
    autorisation.each(function(){
        if (this.value != '') {
            cloned_node_personne1 = this.cloneNode(true);
            $('.field-type-select #personne_1').append(cloned_node_personne1);
            cloned_node_personne2 = this.cloneNode(true);
            $('.field-type-select #personne_2').append(cloned_node_personne2);
        }
    });

    // Pour chaque option de personne 1 on modifie la valeur pour préciser
    // si c'est un défunt ou un contact
    $('.field-type-select #personne_1 option').each(function() {
        if (this.value != '') {
            type_defunt = this.text.search('defunt');
            type_contact = this.text.search('contact');
            if (type_defunt != -1) {
                type_personne = this.text.substring(type_defunt);
            } else {
                type_personne = this.text.substring(type_contact);
            }
            this.value = this.value+'_'+type_personne
        }
    });
    // Pour chaque option de personne 2 on modifie la valeur pour préciser
    // si c'est un défunt ou un contact
    // identifiant_<défunt/contact>
    $('.field-type-select #personne_2 option').each(function() {
        if (this.value != '') {
            type_defunt = this.text.search('defunt');
            type_contact = this.text.search('contact');
            if (type_defunt != -1) {
                type_personne = this.text.substring(type_defunt);
            } else {
                type_personne = this.text.substring(type_contact);
            }
            this.value = this.value+'_'+type_personne
        }
    });

    // Si on est en modification, alors on récupère l'identifient et le type de personne pour mettre à jour les champs
    // personne 1 et personne 2
    // opérateur ternaire : si la valeur de defunt_p1 n'est pas vide alors on récupère la valeur sinon en récupère celle de contact
    value_select_p1 = $('.field-type-select #defunt_p1').val() != "" ? 
        $('.field-type-select #defunt_p1').val()+'_defunt' : $('.field-type-select #autorisation_p1').val()+'_contact'
    value_select_p2 = $('.field-type-select #defunt_p2').val() != "" ? 
        $('.field-type-select #defunt_p2').val()+'_defunt' : $('.field-type-select #autorisation_p2').val()+'_contact'
    
    // Mise à jour de personne 1 et personne 2 avec les identifiants récupérés
    $('.field-type-select #personne_1').val(value_select_p1);
    $('.field-type-select #personne_2').val(value_select_p2);

    // On cache les champs autorisation et défunt
    $('.field-type-select #defunt_p1').parent().parent().hide();
    $('.field-type-select #defunt_p2').parent().parent().hide();
    $('.field-type-select #autorisation_p1').parent().parent().hide();
    $('.field-type-select #autorisation_p2').parent().parent().hide();
}

/**
 * Fonction utilisée dans le onchange des champs personne 1 et personne 2.
 * Permet de mettre à jour les champs autorisation et défunt p1/p2 avec les options
 * sélectionnées dans les champs personne 1 et personne 2
 * 
 * TODO Refactoriser pour avoir qu'une seule méthode qui gère le champ personne_1 ou personne_2
 * 
 * @return void
 */
function manage_personne() {
    // On détermine si la personne sélectionné est un défunt ou un contact
    // La valeur de chaque option est dans le format <identifiant>_<defunt/contact>
    type_defunt_p1 = $('.field-type-select #personne_1').val().search('_defunt');
    type_contact_p1 = $('.field-type-select #personne_1').val().search('_contact');
        
    // Si il n'y a pas de valeurs saisies
    if (type_defunt_p1 == -1 && type_contact_p1 == -1) {
        // On remet le champ autorisation à 0
        $('.field-type-select #autorisation_p1').val(0);
         // On remet le champ defunt à 0
        $('.field-type-select #defunt_p1').val(0);
    }

    // Si c'est un défunt
    if (type_defunt_p1 != -1) {
        // On remet le champ autorisation à 0
        $('.field-type-select #autorisation_p1').val(0);
        // On sélectionne le défunt en fonction de son identifiant
        $('.field-type-select #defunt_p1').val($('.field-type-select #personne_1').val().substring(-1,type_defunt_p1))
    }
    // Si c'est un contact
    if (type_contact_p1 != -1) {
        // On remet le champ defunt à 0
        $('.field-type-select #defunt_p1').val(0);
        // On sélection le contact en fonction de son identifiant
        $('.field-type-select #autorisation_p1').val($('.field-type-select #personne_1').val().substring(-1,type_contact_p1))
    }

    // On détermine si la personne sélectionné est un défunt ou un contact
    // La valeur de chaque option est dans le format <identifiant>_<defunt/contact>
    type_defunt_p2 = $('.field-type-select #personne_2').val().search('_defunt');
    type_contact_p2 = $('.field-type-select #personne_2').val().search('_contact');

    // Si il n'y a pas de valeurs saisies
    if (type_defunt_p2 == -1 && type_contact_p2 == -1) {
        // On remet le champ autorisation à 0
        $('.field-type-select #autorisation_p2').val(0);
         // On remet le champ defunt à 0
        $('.field-type-select #defunt_p2').val(0);
    }

    // Si c'est un défunt
    if (type_defunt_p2 != -1) {
        // On remet le champ autorisation à 0
        $('.field-type-select #autorisation_p2').val(0);
        // On sélectionne le défunt en fonction de son identifiant
        $('.field-type-select #defunt_p2').val($('.field-type-select #personne_2').val().substring(-1,type_defunt_p2))
    }
    // Si c'est un contact
    if (type_contact_p2 != -1) {
        // On remet le champ defunt à 0
        $('.field-type-select #defunt_p2').val(0);
        // On sélection le contact en fonction de son identifiant
        $('.field-type-select #autorisation_p2').val($('.field-type-select #personne_2').val().substring(-1,type_contact_p2))
    }
}

/**
 *
 */
//
$(function() {
    $( "#accordion" ).accordion({
        heightStyle: "fill"
    });

    // surcharge la fonction de sélection des onglets
    // pour déclencher le (re)chargement de l'onglet de l'empalcement
    // à chaque futur clic provenant d'un autre onglet
    var tabs_select_function = $("#formulaire").tabs('option', 'select');
    $("#formulaire").tabs('option', 'select', function(event, ui) {
        var clicked_on_emplacement_from_another_tab = set_emplacement_href_in_data_load_tabs(event, ui);
        tabs_select_function(event, ui);
    });
});

// Mise en forme dynamique Plan en coupe

$(() => {
    let planCoupeGrid = document.getElementById('grid_defunt_container');
    if (planCoupeGrid != null) {
        let planCoupeColonnes = planCoupeGrid.getAttribute('data-plan-coupe-colonnes');
        let planCoupeLignes = planCoupeGrid.getAttribute('data-plan-coupe-lignes');
        planCoupeGrid.style.gridTemplateColumns = "repeat(" + planCoupeColonnes + ", 1fr)";
        planCoupeGrid.style.gridTemplateRows = "repeat(" + planCoupeLignes + ", 1fr)";
    }
})
