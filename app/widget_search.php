<?php
/**
 * Widget - 
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

require_once "../obj/opencimetiere.class.php";
if (!isset($f)) {
    $f = new opencimetiere(null, null, __("Widget - Localisation"));
}

if ($f->isAccredited("recherche") === true) {
    $obj="concession";
    require_once "../sql/".OM_DB_PHPTYPE."/".$obj.".search.inc.php";        
    require_once "../obj/search.class.php";
    $objRech = new search($f, $configTypeRecherche);
    $objRech->searchInclude("../app/index.php?module=search&obj=concession");
    $objRech->showWidgetForm();
}
