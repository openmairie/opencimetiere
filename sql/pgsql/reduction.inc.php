<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/operation.inc.php";

$ent = __("operations")." -> ".__("reduction");

//
$tab_actions['left']['validation-operation'] = array(
    'lien' => OM_ROUTE_FORM."&obj=".$obj."&action=21&amp;idx=",
    'id' => "&retour=tab",
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix operation-valider-16\" title=\"".__("tooltip_ValiderOperation")."\">".__("Valider")."</span>",
    'ajax' => false,
    'ordre' => 110,
);
//
$tab_actions['left']['edition-operation'] = array(
    'lien' => OM_ROUTE_FORM."&obj=".$obj."&action=11&amp;idx=",
    'id' => "&retour=tab",
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Edition")."\">".__("Edition")."</span>",
    'ajax' => false,
    'ordre' => 120,
);

//
$table=DB_PREFIXE."operation inner join ".DB_PREFIXE."emplacement on operation.emplacement = emplacement.emplacement";

$edition="";
$champAffiche = array(
    'operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    'famille as "'.__("famille").'"',
    'to_char (date,\'DD/MM/YYYY\') as "'.__("date").'"',
);
$champRecherche = array(
    'operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    "famille",
);
$tri= "order by date";
$selection = " where categorie = 'reduction' and etat = 'actif'"; // and etat = 'actif'

$sousformulaire=array("operation_defunt");
