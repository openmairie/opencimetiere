<?php
//$Id$ 
//gen openMairie le 16/03/2012 23:17 
$import= "Insertion dans la table emplacement voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."emplacement";
$id='emplacement'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
$obligatoire['emplacement']=1;// obligatoire = 1
//* cle secondaire=voie
$exist['voie']=1;//  0=non / 1=oui
$sql_exist['voie']= "select * from ".DB_PREFIXE."voie where voie = '";
//* cle secondaire=plans
$exist['plans']=1;//  0=non / 1=oui
$sql_exist['plans']= "select * from ".DB_PREFIXE."plans where plans = '";
// * champ = emplacement
$zone['emplacement']='0';
// $defaut['emplacement']='***'; // *** par defaut si non renseigne
// * champ = nature
$zone['nature']='1';
// $defaut['nature']='***'; // *** par defaut si non renseigne
// * champ = numero
$zone['numero']='2';
// $defaut['numero']='***'; // *** par defaut si non renseigne
// * champ = complement
$zone['complement']='3';
// $defaut['complement']='***'; // *** par defaut si non renseigne
// * champ = voie
$zone['voie']='4';
// $defaut['voie']='***'; // *** par defaut si non renseigne
// * champ = numerocadastre
$zone['numerocadastre']='5';
// $defaut['numerocadastre']='***'; // *** par defaut si non renseigne
// * champ = famille
$zone['famille']='6';
// $defaut['famille']='***'; // *** par defaut si non renseigne
// * champ = numeroacte
$zone['numeroacte']='7';
// $defaut['numeroacte']='***'; // *** par defaut si non renseigne
// * champ = datevente
$zone['datevente']='8';
// $defaut['datevente']='***'; // *** par defaut si non renseigne
// * champ = terme
$zone['terme']='9';
// $defaut['terme']='***'; // *** par defaut si non renseigne
// * champ = duree
$zone['duree']='10';
// $defaut['duree']='***'; // *** par defaut si non renseigne
// * champ = dateterme
$zone['dateterme']='11';
// $defaut['dateterme']='***'; // *** par defaut si non renseigne
// * champ = nombreplace
$zone['nombreplace']='12';
// $defaut['nombreplace']='***'; // *** par defaut si non renseigne
// * champ = placeoccupe
$zone['placeoccupe']='13';
// $defaut['placeoccupe']='***'; // *** par defaut si non renseigne
// * champ = superficie
$zone['superficie']='14';
// $defaut['superficie']='***'; // *** par defaut si non renseigne
// * champ = placeconstat
$zone['placeconstat']='15';
// $defaut['placeconstat']='***'; // *** par defaut si non renseigne
// * champ = dateconstat
$zone['dateconstat']='16';
// $defaut['dateconstat']='***'; // *** par defaut si non renseigne
// * champ = observation
$zone['observation']='17';
// $defaut['observation']='***'; // *** par defaut si non renseigne
// * champ = plans
$zone['plans']='18';
// $defaut['plans']='***'; // *** par defaut si non renseigne
// * champ = positionx
$zone['positionx']='19';
// $defaut['positionx']='***'; // *** par defaut si non renseigne
// * champ = positiony
$zone['positiony']='20';
// $defaut['positiony']='***'; // *** par defaut si non renseigne
// * champ = photo
$zone['photo']='21';
// $defaut['photo']='***'; // *** par defaut si non renseigne
// * champ = libre
$zone['libre']='22';
// $defaut['libre']='***'; // *** par defaut si non renseigne
// * champ = sepulturetype
$zone['sepulturetype']='23';
// $defaut['sepulturetype']='***'; // *** par defaut si non renseigne
// * champ = daterenouvellement
$zone['daterenouvellement']='24';
// $defaut['daterenouvellement']='***'; // *** par defaut si non renseigne
// * champ = typeconcession
$zone['typeconcession']='26';
// $defaut['typeconcession']='***'; // *** par defaut si non renseigne
// * champ = temp1
$zone['temp1']='27';
// $defaut['temp1']='***'; // *** par defaut si non renseigne
// * champ = temp2
$zone['temp2']='28';
// $defaut['temp2']='***'; // *** par defaut si non renseigne
// * champ = temp3
$zone['temp3']='29';
// $defaut['temp3']='***'; // *** par defaut si non renseigne
// * champ = temp4
$zone['temp4']='30';
// $defaut['temp4']='***'; // *** par defaut si non renseigne
// * champ = temp5
$zone['temp5']='31';
// $defaut['temp5']='***'; // *** par defaut si non renseigne
