<?php
/**
 * 
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier.inc.php";

//
$ent = " -> ".__($obj);

// SELECT 
$champAffiche = array(
    $obj.'.'.$obj.' as "'.__("id").'"',
    'to_char(dossier.datedossier ,\'DD/MM/YYYY\') as "'.__("datedossier").'"',
    'dossier.typedossier as "'.__("typedossier").'"',
    'dossier.observation as "'.__("observation").'"'
);
//
$champRecherche = array(
    $obj.'.'.$obj.' as "'.__("id").'"',
    'dossier.typedossier as "'.__("typedossier").'"',
);

//
if ($retourformulaire == 'concession'
    || $retourformulaire == 'colombarium'
    || $retourformulaire == 'enfeu'
    || $retourformulaire == 'ossuaire'
    || $retourformulaire == 'depositoire'
    || $retourformulaire == 'terraincommunal') {
    $selection = " WHERE (".$obj.".emplacement=".$idx.") ";
}

// Actions a gauche : télécharger
$tab_actions["left"]["download"] = array(
    'lien' => OM_ROUTE_FORM.'&snippet=file&obj='.$obj.'&champ=fichier&id=',
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix download-16" title="'.__('Télécharger').'">'.__('Télécharger').'</span>',
    'rights' => array('list' => array($obj, $obj.'_fichier_telecharger'), 'operator' => 'OR'),
    'ordre' => 15,
    'target' => '_blank',
);
