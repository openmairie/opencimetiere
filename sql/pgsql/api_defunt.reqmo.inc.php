<?php
// Libellé de la requête
$reqmo['libelle']=__('reqmo-libelle-api_defunt');
$reqmo['reqmo_libelle']=__('reqmo-libelle-api_defunt');
$ent=__('api_defunt');

// Requête à effectuer
$reqmo['sql'] = "
SELECT
  [defunt],
  [emplacement.emplacement as emplacement],
  [titre_de_civilite.libelle as titre],
  [nom],
  [marital],
  [prenom],
  [datenaissance],
  [lieunaissance],
  [datedeces],
  [lieudeces],
  [defunt.observation as observation]
FROM
  ".DB_PREFIXE."defunt 
  LEFT JOIN ".DB_PREFIXE."titre_de_civilite
    ON defunt.titre = titre_de_civilite.titre_de_civilite
  LEFT JOIN ".DB_PREFIXE."emplacement
    ON defunt.emplacement = emplacement.emplacement
";

$reqmo['defunt'] = "checked";
$reqmo['emplacement'] = "checked";
$reqmo['titre'] = "checked";
$reqmo['nom'] = "checked";
$reqmo['marital'] = "checked";
$reqmo['prenom'] = "checked";
$reqmo['datenaissance'] = "checked";
$reqmo['lieunaissance'] = "checked";
$reqmo['datedeces'] = "checked";
$reqmo['lieudeces'] = "checked";
$reqmo['observation'] = "checked";