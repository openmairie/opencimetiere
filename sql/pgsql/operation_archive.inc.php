<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/operation_archive.inc.php";

if (isset($idx)) {
    $selection = sprintf(
        ' WHERE operation_archive.emplacement=%s',
        $idx
    );
}

// Actions
if (isset($tab_actions)) {
    unset($tab_actions['corner']['ajouter']);
}

$sousformulaire = array("operation_defunt_archive");

$table = sprintf(
        '%1$soperation_archive 
            INNER JOIN %1$semplacement_archive on operation_archive.emplacement = emplacement_archive.emplacement
            INNER JOIN %1$soperation_defunt_archive on operation_archive.operation = operation_defunt_archive.operation 
            LEFT JOIN %1$semplacement as emplacement_transfert on operation_archive.emplacement_transfert = emplacement_transfert.emplacement
            LEFT JOIN %1$semplacement_archive as emplacement_transfert_archive on operation_archive.emplacement_transfert = emplacement_transfert_archive.emplacement',
        DB_PREFIXE);

$champAffiche = array(
    'operation_archive.operation as "'.__("id").'"',
    'operation_archive.numdossier as "'.__("numdossier").'"' ,
    'operation_archive.categorie as "'.__("categorie").'"',
    'emplacement_archive.famille as "'.__("emplacement").'"',
    'operation_defunt_archive.defunt_nom as "'.__("nom de naissance").'"',
    'operation_defunt_archive.defunt_prenom as "'.__("prenom").'"',
    'operation_defunt_archive.defunt_marital as "'.__("nom d'usage").'"',
    'to_char(operation_archive.date,\'DD/MM/YYYY\') as "'.__("date").'"',
    'operation_archive.consigne_acces as "'.__("consigne d'accès (origine)").'"',
    'CASE WHEN emplacement_transfert_archive.famille IS NOT NULL THEN emplacement_transfert_archive.famille ELSE emplacement_transfert.famille END  as "'.__("emplacement transfert").'"',
    'operation_archive.consigne_acces_transfert as "'.__("consigne d'accès (destination)").'"',
    "CASE operation_archive.prive WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("Privé").'"',
);

// Si on est sur l'emplacement_archive
if ($retourformulaire== 'emplacement_archive'){
    // Pas besoin d'afficher l'emplacement lié
    $champAffiche = array(
        'operation_archive.operation as "'.__("id").'"',
        'operation_archive.numdossier as "'.__("numdossier").'"' ,
        'operation_archive.categorie as "'.__("categorie").'"',
        'operation_defunt_archive.defunt_nom as "'.__("nom de naissance").'"',
        'operation_defunt_archive.defunt_prenom as "'.__("prenom").'"',
        'operation_defunt_archive.defunt_marital as "'.__("nom d'usage").'"',
        'to_char(operation_archive.date,\'DD/MM/YYYY\') as "'.__("date").'"',
        'operation_archive.consigne_acces as "'.__("consigne d'accès (origine)").'"',
        'CASE WHEN emplacement_transfert_archive.famille IS NOT NULL THEN emplacement_transfert_archive.famille ELSE emplacement_transfert.famille END  as "'.__("emplacement_transfert").'"',
        'operation_archive.consigne_acces_transfert as "'.__("consigne d'accès (destination)").'"',
        "CASE operation_archive.prive WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("Privé").'"',
    );
}