<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/operation_defunt.inc.php";

$tri= " order by defunt_nom, defunt_prenom, defunt_marital ";


$table=DB_PREFIXE."operation_defunt";

$champAffiche=array('operation_defunt as "'.__('operation_defunt').'"',
                    'defunt_titre as "'.__('defunt_titre').'"',
                    'defunt_nom as "'.__('defunt_nom').'"',
                    'defunt_marital as "'.__('defunt_marital').'"',
                    'defunt_prenom as "'.__('defunt_prenom').'"',
                    'to_char (defunt_datenaissance,\'DD/MM/YYYY\') as "'.__("defunt_datenaissance").'"',
                    'to_char (defunt_datedeces,\'DD/MM/YYYY\') as "'.__("defunt_datedeces").'"',
                   'defunt_lieudeces as "'.__('defunt_lieudeces').'"');

$champRecherche=array("defunt_nom","defunt_prenom","defunt_marital");

if( isset($idx) ) $selection=" where operation = ".$idx;
