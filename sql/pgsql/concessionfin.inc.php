<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

// !!! On positionne $obj à 'concession' pour éviter des surcharges
$obj = "concession";

//
include "../sql/pgsql/concession.inc.php";

// !!! On repositionne $obj à 'concessionfin'
$obj = "concessionfin";

//
$ent = __("archives")." -> ".__("concessionfin");

//
$selection .= " and emplacement.libre<>'Oui' ";

//
unset($tab_actions['left']['localiser-sig-interne']);
unset($tab_actions['left']['localiser-sig-externe']);
unset($tab_actions['left']['localiser-plan']);
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);

//
$tab_actions['content'] = array(
    "lien" => OM_ROUTE_FORM."&obj=concession&action=31&idx=",
    "id" => "&specific_origin=concessionfin&amp;advs_id=".$advs_id."&amp;premier=".$premier."&amp;tricol=".$tricol."&amp;valide=".$valide."&amp;retour=tab",
    "lib" => "",
);
