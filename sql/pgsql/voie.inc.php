<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/voie.inc.php";

// FROM
$table = DB_PREFIXE."voie
    LEFT JOIN ".DB_PREFIXE."voie_type 
        ON voie.voietype=voie_type.voie_type 
    LEFT JOIN ".DB_PREFIXE."zone 
        ON voie.zone=zone.zone
    LEFT JOIN ".DB_PREFIXE."zone_type
        ON zone.zonetype=zone_type.zone_type
    LEFT JOIN ".DB_PREFIXE."cimetiere
        ON zone.cimetiere=cimetiere.cimetiere";

// SELECT
$champAffiche = array(
    'voie.voie as "'.__("id").'"',
    'voie_type.libelle as "'.__("voietype").'"',
    'voie.voielib as "'.__("libelle").'"',
    "(zone_type.libelle||' '||zone.zonelib||' ('||cimetiere.cimetierelib||')') as \"".__("zone")."\"",
);
$champRecherche = $champAffiche;
$tri = " ORDER BY voie.zone, voie.voielib ";

// LOCALISATION
// XXX Pourquoi être obligé de fixer dans l'url la géométrie et le zoom ?
$tab_actions['left']['localisation'] = array(
    'lien' => OM_ROUTE_MAP.'&mode=tab_sig&obj='.$obj.'&amp;idx=',
    'id' => '&amp;geometrie=line&amp;zoom=11',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 100,
    "parameters" => array("option_localisation" => "sig_interne", ),
);
// EDITION PDF
$tab_actions['left']['etat-voie'] = array(
    'lien' => OM_ROUTE_FORM."&obj=".$obj."&action=101&idx=",
    'id' => '',
    'target' => '_blank',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix pdf-16" title="'.__('télécharger le récapitulatif au format pdf').'">'.__('récapitulatif').'</span>',
    'ordre' => 110,
);
