<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */
include "../sql/pgsql/operation.inc.php";
//
$DEBUG=0;
$serie=15;
$ent = __("Recherche")." -> ".__("Opération en cours");
$edition="";

$obj = "operation";
// CONSULTER
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=30&amp;specific_origin=operation_en_cours&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];

unset($tab_actions['corner']['ajouter']);

$table=DB_PREFIXE."operation inner join ".DB_PREFIXE."emplacement on operation.emplacement = emplacement.emplacement";
//
$champAffiche = array(
    'operation.operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    "emplacement.famille",
    'categorie as "'.__("categorie").'"',
    "to_char (date,'DD/MM/YYYY') as date",
    'operation.consigne_acces as "'.__("consigne d'accès (origine)").'"',
    'operation.consigne_acces_transfert as "'.__("consigne d'accès (destination)").'"',
    "CASE operation.prive WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("Privé").'"',
);
$champRecherche = array(
    'operation.operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    "emplacement.famille",
    'categorie as "'.__("categorie").'"',
);
$tri= "order by date";
$selection=" where etat != 'trt'";
