<?php
//$Id$ 
//gen openMairie le 15/09/2022 17:21

include "../gen/sql/pgsql/genealogie.inc.php";

$champAffiche = array(
    'genealogie.genealogie as "'.__("Généalogie").'"',
    // Affichage de la personne sélectionnée en fonction de defunt_p1 et autorisation_p1
    'CASE WHEN 
        genealogie.defunt_p1 IS NOT NULL 
            THEN 
                (SELECT CONCAT_WS(\' \', defunt.defunt, defunt.nom, defunt.marital, defunt.prenom, \'defunt\') as libelle FROM '.DB_PREFIXE.'defunt WHERE defunt = genealogie.defunt_p1)
            ELSE 
                (SELECT CONCAT_WS(\' \', autorisation.autorisation, autorisation.nom, autorisation.marital, autorisation.prenom, \'contact\') as libelle FROM '.DB_PREFIXE.'autorisation WHERE autorisation = genealogie.autorisation_p1)
     END as"'.__("personne_1").'"',
    'CONCAT_WS(\' -> \', lien_parente.libelle, lien_parente.lien_inverse) as "'.__("Lien de parenté").'"',
    // Affichage de la personne sélectionnée en fonction de defunt_p2 et autorisation_p2
    'CASE WHEN 
        genealogie.defunt_p2 IS NOT NULL 
            THEN 
                (SELECT CONCAT_WS(\' \', defunt.defunt, defunt.nom, defunt.marital, defunt.prenom, \'defunt\') as libelle FROM '.DB_PREFIXE.'defunt WHERE defunt = genealogie.defunt_p2)
            ELSE 
                (SELECT CONCAT_WS(\' \', autorisation.autorisation, autorisation.nom, autorisation.marital, autorisation.prenom, \'contact\') as libelle FROM '.DB_PREFIXE.'autorisation WHERE autorisation = genealogie.autorisation_p2)
     END as "'.__("personne_2").'"',
);

