<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
$DEBUG=0;
$serie=15;
$ent = __("Recherche")." -> ".__("concession_terme");
$edition="";

// !!! On positionne $obj à 'concession' pour éviter des surcharges
$obj = "concession";

// CONSULTER
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=3&amp;specific_origin=terme&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];

// LOCALISATION
$tab_actions['left']['localiser-sig-interne'] = array(
    'lien' => OM_ROUTE_MAP."&mode=tab_sig&idx=",
    'id' => '&amp;obj='.$obj.'&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "sig_interne", ),
);
$tab_actions['left']['localiser-sig-externe'] = array(
    'lien' => "../app/localisation_sig_externe.php?idx=",
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "sig_externe", ),
);
$tab_actions['left']['localiser-plan'] = array(
    'lien' => "../app/localisation_plan_view.php?idx=",
    'id' => '&amp;obj='.$obj.'&amp;specific_origin=terme&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Visualiser sur plan').'">'.__('Plan').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "plan", ),
);

// !!! On repositionne $obj à 'terme'
$obj = "terme";



// Actions a gauche : consulter
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);



$table=DB_PREFIXE."emplacement";
//
$droitTable="";
//
$champAffiche=array(
                    'emplacement as "'.__("id").'"',
                    'famille as "'.__("famille").'"',
                    'to_char(dateterme,\'DD/MM/YYYY\') as "'.__("dateterme").'"'
                    );
$champRecherche=array("emplacement","famille");
$tri= "order by dateterme";
$selection=" where extract(YEAR FROM dateterme)<=extract(YEAR FROM current_date) and terme='temporaire'";
