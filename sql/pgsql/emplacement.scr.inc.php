<?php
//$Id: dossier.scr.inc,v 1.3 2010-08-22 21:40:12 fraynaud Exp $
$sql_emplacement="select emplacement.emplacement, famille, nature,
                  numero,complement,voietype,voielib,
                  zonetype,zonelib,cimetierelib,
                  placeconstat, to_char(dateconstat,'DD/MM/YYYY') as dateconstat,
                  nombreplace,placeoccupe,observation
                  from ".DB_PREFIXE."emplacement
                  inner join ".DB_PREFIXE."voie on emplacement.voie=voie.voie
                  inner join ".DB_PREFIXE."zone on voie.zone=zone.zone
                  inner join ".DB_PREFIXE."cimetiere on zone.cimetiere=cimetiere.cimetiere
                  where emplacement =".$idx;
$sql_defunt = "select nom, prenom, marital,
                to_char(datenaissance,'DD/MM/YYYY') as datenaissance,
                to_char(dateinhumation,'DD/MM/YYYY') as dateinhumation,
                to_char(dateexhumation,'DD/MM/YYYY') as dateexhumation,
                nature, taille
                from ".DB_PREFIXE."defunt where emplacement =".$idx;
$sql_courrier = "select courrier as id,
                    (nom||' '||prenom) as nom,
                    lettretype,
                    to_char(datecourrier,'DD/MM/YYYY') as date
                    from ".DB_PREFIXE."courrier left join ".DB_PREFIXE."autorisation on courrier.destinataire=autorisation.autorisation
                    where courrier.emplacement =".$idx;
$sql_concessionnaire = "select autorisation,
                    nom, marital, prenom,
                    to_char(datenaissance,'DD/MM/YYYY') as datenaissance,
                    case autorisation.dcd when 't' then 'Oui' else 'Non' end as dcd
                    from ".DB_PREFIXE."autorisation
                    where emplacement =".$idx." and nature ='concessionnaire'";
$sql_ayantdroit = "select autorisation,
                    nom, marital, prenom,
                    to_char(datenaissance,'DD/MM/YYYY') as datenaissance,
                    case autorisation.dcd when 't' then 'Oui' else 'Non' end as dcd, parente
                    from ".DB_PREFIXE."autorisation
                    where emplacement =".$idx." and nature ='ayantdroit'";
$sql_operation = "select operation.operation,  numdossier,
                        operation_defunt.defunt_nom as nom,
                        operation_defunt.defunt_prenom as prenom,
                        operation_defunt.defunt_marital as marital,
                        operation.categorie as categorie,
                        to_char (date,'DD/MM/YYYY') as date,
                        etat
                    from ".DB_PREFIXE."operation inner join ".DB_PREFIXE."operation_defunt on operation.operation = operation_defunt.operation
                    where emplacement =".$idx;

$sql_dossier = "select fichier, to_char(datedossier ,'DD/MM/YYYY') as date, typedossier
                    from ".DB_PREFIXE."dossier
                    where emplacement =".$idx;                 

$sql_travaux = "select nomentreprise as entreprise,
                    to_char(datedebinter ,'DD/MM/YYYY') as debut,
                    to_char(datefininter ,'DD/MM/YYYY') as fin,
                    naturedemandeur as demandeur, naturetravaux as nature
                    from ".DB_PREFIXE."travaux inner join ".DB_PREFIXE."entreprise on entreprise.entreprise = travaux.entreprise
                    where emplacement =".$idx; 
