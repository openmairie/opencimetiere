<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/plans.inc.php";

// SELECT
// Remplacement du libellé de la première colonne
$champAffiche[0] = 'plans.plans as "'.__("id").'"';
$champRecherche[0] = $champAffiche[0];

// LOCALISATION
$tab_actions['left']['localiser-plan'] = array(
    'lien' => "../app/localisation_plan_view.php?plan=",
    'id' => '&amp;obj='.$obj.'&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Visualiser le plan').'">'.__('Plan').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "plan", ),
);
