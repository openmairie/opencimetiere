<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/autorisation.inc.php";

//
$ent = " -> ".__("contact");

//
$field_autorisation_emplacement = 'emplacement.emplacement as "'.__("emplacement").'"';
$champAffiche = array(
    'autorisation.autorisation as "'.__("id").'"',
    'autorisation.nature as "'.__("nature").'"',
    'autorisation.nom as "'.__("nom de naissance").'"',
    'autorisation.marital as "'.__("nom d'usage").'"',
    'autorisation.prenom as "'.__("prenom").'"',
    'to_char(autorisation.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    "case autorisation.dcd when 't' then 'Oui' else 'Non' end as \"".__("dcd")."\"",
    $field_autorisation_emplacement,
);
$champRecherche = array(
    'autorisation.nom as "'.__("nom de naissance").'"',
    'autorisation.marital as "'.__("nom d'usage").'"',
    'autorisation.prenom as "'.__("prenom").'"',
);
$tri = " ORDER BY autorisation.nom, autorisation.prenom ";
//
$fields_to_hide = array();
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $fields_to_hide[] = $field_autorisation_emplacement;
}
$champAffiche = array_diff($champAffiche, $fields_to_hide);
