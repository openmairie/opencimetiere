<?php
// Libellé de la requête
$reqmo['libelle']=__("Liste des défunts triée par libellé du cimetière, nom et prénom du défunt");

$reqmo['reqmo_libelle']=__('defunt');

// Requête à effectuer
$reqmo['sql'] = "
SELECT
  cimetiere.cimetierelib,
  titre_de_civilite.libelle as civilite,
  defunt.nom,
  defunt.marital,
  defunt.prenom,
  CONCAT_WS(' ', zone_type.libelle, zone.zonelib) as zone,
  CONCAT_WS(' ', voie_type.libelle, voie.voielib) as voie,
  emplacement.numero,
  emplacement.complement,
  emplacement.nature,
  emplacement.famille
FROM
  opencimetiere.defunt 
  LEFT JOIN opencimetiere.titre_de_civilite
    ON defunt.titre = titre_de_civilite.titre_de_civilite
  LEFT JOIN opencimetiere.emplacement
    ON defunt.emplacement = emplacement.emplacement
  LEFT JOIN opencimetiere.voie
    ON emplacement.voie = voie.voie
  LEFT JOIN opencimetiere.voie_type
    ON voie.voietype = voie_type.voie_type
  LEFT JOIN opencimetiere.zone
    ON voie.zone = zone.zone
  LEFT JOIN opencimetiere.zone_type
    ON zone.zonetype = zone_type.zone_type
  LEFT JOIN opencimetiere.cimetiere
    ON zone.cimetiere = cimetiere.cimetiere
ORDER BY
 cimetiere.cimetierelib,
 defunt.nom,
 defunt.prenom
";
