<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

// !!! On positionne $obj à 'colombarium' pour éviter des surcharges
$obj = "colombarium";

//
include "../sql/pgsql/colombarium.inc.php";

// !!! On repositionne $obj à 'colombariumfin'
$obj = "colombariumfin";

//
$ent = __("archives")." -> ".__("colombariumfin");

//
$selection .= " and emplacement.libre<>'Oui' ";

//
unset($tab_actions['left']['localiser-sig-interne']);
unset($tab_actions['left']['localiser-sig-externe']);
unset($tab_actions['left']['localiser-plan']);
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);

//
$tab_actions['content'] = array(
    "lien" => OM_ROUTE_FORM."&obj=colombarium&action=31&idx=",
    "id" => "&specific_origin=colombariumfin&amp;advs_id=".$advs_id."&amp;premier=".$premier."&amp;tricol=".$tricol."&amp;valide=".$valide."&amp;retour=tab",
    "lib" => "",
);
