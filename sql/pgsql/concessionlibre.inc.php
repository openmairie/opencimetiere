<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

// !!! On positionne $obj à 'concession' pour éviter des surcharges
$obj = "concession";

//
include "../sql/pgsql/concession.inc.php";

// CONSULTER
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=3&amp;specific_origin=concessionlibre&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];

// LOCALISATION
$tab_actions['left']['localiser-plan'] = array(
    'lien' => "../app/localisation_plan_view.php?idx=",
    'id' => '&amp;obj='.$obj.'&amp;specific_origin=concessionlibre&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Visualiser sur plan').'">'.__('Plan').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "plan", ),
);

// !!! On repositionne $obj à 'concessionlibre'
$obj = "concessionlibre";

// Suppression de la dernière colonne affichée : libre
array_pop($champAffiche);

//
$ent = __("recherche")." -> ".__("concessionlibre");

//
$selection .= " and emplacement.libre='Oui' ";

// AJOUTER
// Suppression de l'action ajouter
unset($tab_actions['corner']['ajouter']);

// EDITION PDF
$tab_actions['left']['etat-emplacement'] = array(
    'lien' => OM_ROUTE_FORM."&obj=concession&action=102&idx=",
    'id' => '',
    'target' => '_blank',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix pdf-16" title="'.__('télécharger le récapitulatif (libre) au format pdf').'">'.__('récapitulatif (libre)').'</span>',
    'ordre' => 110,
);
