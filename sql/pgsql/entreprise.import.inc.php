<?php
//$Id$ 
//gen openMairie le 19/04/2012 10:13 
$import= "Insertion dans la table entreprise voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."entreprise";
$id='entreprise'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$DEBUG=0; // =0 pas d affichage messages / =1 affichage detail enregistrement
$fic_erreur=1; // =0 pas de fichier d erreur / =1  fichier erreur
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
$obligatoire['entreprise']=1;// obligatoire = 1
// * champ = entreprise
$zone['entreprise']='0';
// $defaut['entreprise']='***'; // *** par defaut si non renseigne
// * champ = nomentreprise
$zone['nomentreprise']='1';
// $defaut['nomentreprise']='***'; // *** par defaut si non renseigne
// * champ = adresse1
$zone['adresse1']='2';
// $defaut['adresse1']='***'; // *** par defaut si non renseigne
// * champ = adresse2
$zone['adresse2']='3';
// $defaut['adresse2']='***'; // *** par defaut si non renseigne
// * champ = cp
$zone['cp']='4';
// $defaut['cp']='***'; // *** par defaut si non renseigne
// * champ = ville
$zone['ville']='5';
// $defaut['ville']='***'; // *** par defaut si non renseigne
// * champ = telephone
$zone['telephone']='6';
// $defaut['telephone']='***'; // *** par defaut si non renseigne
// * champ = pf
$zone['pf']='7';
// $defaut['pf']='***'; // *** par defaut si non renseigne
