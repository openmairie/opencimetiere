<?php
/**
 * 
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/courrier.inc.php";

//
$ent = " -> ".__($obj);

// Modification du libellé du chap destinataire + id
$champAffiche = array(
    'courrier.courrier as "'.__("id").'"',
    "coalesce(autorisation.nom, '')||' '||coalesce(autorisation.prenom, '') as \"".__("destinataire")."\"",
    'to_char(courrier.datecourrier ,\'DD/MM/YYYY\') as "'.__("datecourrier").'"',
    'courrier.lettretype as "'.__("lettretype").'"',
    );
$champRecherche = array(
    'courrier.courrier as "'.__("id").'"',
    "coalesce(autorisation.nom, '')||' '||coalesce(autorisation.prenom, '') as \"".__("destinataire")."\"",
    'courrier.lettretype as "'.__("lettretype").'"',
    );

// EDITION
$tab_actions['left']['edition'] = array(
    'lien' => OM_ROUTE_FORM."&obj=courrier&action=31&idx=",
    'id' => '',
    'target' => '_blank',
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Edition")."\">".__("Edition")."</span>",
    'ordre' => 110,
);
