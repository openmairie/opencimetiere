<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/emplacement.inc.php";

//
$sousformulaire = array(
    "defunt",
    "autorisation",
    "genealogie",
    "contrat",
    "courrier",
    "operation_trt",
    "travaux",
    "dossier",
);
$sousformulaire_parameters = array(
    "autorisation" => array(
        "title" => _("Contacts"),
    ),
    "contrat" => array(
        "title" => _("Contrats"),
    ),
);
