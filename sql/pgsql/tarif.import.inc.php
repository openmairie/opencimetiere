<?php
//$Id$ 
//gen openMairie le 07/02/2020 11:27

$import= "Insertion dans la table tarif voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."tarif";
$id='tarif'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "tarif" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "annee" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "origine" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "50",
    ),
    "terme" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "15",
    ),
    "duree" => array(
        "notnull" => "",
        "type" => "float",
        "len" => "20",
    ),
    "nature" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "15",
    ),
    "sepulture_type" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "sepulture_type",
            "foreign_column_name" => "sepulture_type",
            "sql_exist" => "select * from ".DB_PREFIXE."sepulture_type where sepulture_type = '",
                "foreign_key_alias" => array(
                    "query" => "select sepulture_type from ".DB_PREFIXE."sepulture_type where code = '<SEARCH>'",
                    "fields_list" => array("code", ),
                ),
        ),
    ),
    "montant" => array(
        "notnull" => "1",
        "type" => "float",
        "len" => "20",
    ),
    "monnaie" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "30",
    ),
);
