<?php
//$Id$ 
//gen openMairie le 11/02/2016 11:31

$reqmo['libelle']=__('reqmo-libelle-voie');
$reqmo['reqmo_libelle']=__('reqmo-libelle-voie');
$ent=__('voies par zone');
$reqmo['sql']="select  [voie], [voietype], [voielib] from ".DB_PREFIXE."voie where zone = '[zone]' order by [tri]";
$reqmo['voie']='checked';
$reqmo['zone']="select zone, concat_ws(' ', zone_type.libelle, zone.zonelib) as lib
from ".DB_PREFIXE."zone
left join ".DB_PREFIXE."zone_type on zone.zonetype = zone_type.zone_type
order by zone_type.libelle, zone.zonelib";
$reqmo['voietype']='checked';
$reqmo['voielib']='checked';
$reqmo['tri']=array('voie','voietype','voielib');
