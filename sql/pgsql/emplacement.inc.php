<?php
/**
 * 
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/emplacement.inc.php";

/*
 * Surcharge réalisée pour le retour sur les écrans 'concessionlibre',
 * 'terraincommunallibre', 'colombariumlibre' et 'terme'
 */
if ($obj == "concession" || $obj == "colombarium" || $obj == "terraincommunal") {
    $specific_origin = "";
    if (isset($_GET['specific_origin'])) {
        $specific_origin = $_GET['specific_origin'];
    }
    $extra_parameters["specific_origin"] = $specific_origin;
}

//
$ent = __("emplacements")." -> ".__($obj);

if (!isset($premier)) 
    $premier="";
if (!isset($advs_id)) 
    $advs_id="";
if (!isset($tricol)) 
    $tricol="";
if (!isset($valide)) 
    $valide="";
$edition = "";

//
$table = DB_PREFIXE."emplacement
    LEFT JOIN ".DB_PREFIXE."voie
        ON emplacement.voie=voie.voie
    LEFT JOIN ".DB_PREFIXE."voie_type
        ON voie.voietype=voie_type.voie_type
    LEFT JOIN ".DB_PREFIXE."zone
        ON voie.zone=zone.zone
    LEFT JOIN ".DB_PREFIXE."zone_type
        ON zone.zonetype=zone_type.zone_type
    LEFT JOIN ".DB_PREFIXE."cimetiere
        ON zone.cimetiere=cimetiere.cimetiere
    LEFT JOIN ".DB_PREFIXE."sepulture_type
        ON emplacement.sepulturetype=sepulture_type.sepulture_type
    ";
        

//
$champAffiche=array(
    'emplacement as "'.__("id").'"',
    'emplacement.geom as "geom_picto1"',
    'emplacement.pgeom as "geom_picto2"',
    'famille as "'.__("famille").'"',
    "numero::text||' '||COALESCE(complement, '') as \"".__("numero")."\"",
    "(voie_type.libelle||' '||voielib) as \"".__("voie")."\"",
    "(zone_type.libelle||' '||zonelib) as \"".__("zone")."\"",
    'cimetierelib as "'.__("cimetierelib").'"',
    'emplacement.numeroacte as "'.__("numeroacte").'"',
    'libre as "'.__("libre").'"'
);

//
$champRecherche=array(
    'emplacement as "'.__("id").'"',
    //"(cast(numero as text)||' '||COALESCE(complement, '') ) as \"".__("numero")."\"",
    "(voie_type.libelle||' '||voielib) as \"".__("voie")."\"",
    "(zone_type.libelle||' '||zonelib) as \"".__("zone")."\"",
    'cimetierelib as "'.__("cimetierelib").'"',
    'nature as "'.__("nature").'"',
    'numerocadastre as "'.__("numerocadastre").'"',
    'famille as "'.__("famille").'"',
    'emplacement.numeroacte as "'.__("numeroacte").'"',
    'terme as "'.__("terme").'"',
    'sepulture_type.libelle as "'.__("sepulturetype").'"',
    'typeconcession as "'.__("typeconcession").'"',
    'videsanitaire as "'.__("videsanitaire").'"',
    'semelle as "'.__("semelle").'"',
    'etatsemelle as "'.__("etatsemelle").'"',
    'monument as "'.__("monument").'"',
    'etatmonument as "'.__("etatmonument").'"',
    'abandon as "'.__("abandon").'"'
    );

//
$selection = " where emplacement.nature='".$obj."' ";


//
$tri = " order by famille ";

//
$sousformulaire = array();

// EDITION
$tab_actions['left']['etat-emplacement'] = array(
    'lien' => OM_ROUTE_FORM."&obj=".$obj."&action=101&idx=",
    'id' => '',
    'target' => "_blank",
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix pdf-16" title="'.__('télécharger le récapitulatif au format pdf').'">'.__('récapitulatif').'</span>',
    'ordre' => 110,
);

// LOCALISATION
$tab_actions['left']['localiser-sig-interne'] = array(
    'lien' => OM_ROUTE_MAP."&mode=tab_sig&idx=",
    'id' => '&amp;obj=carte_emplacement',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "sig_interne", ),
);
$tab_actions['left']['localiser-sig-externe'] = array(
    'lien' => "../app/localisation_sig_externe.php?idx=",
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "sig_externe", ),
);
$tab_actions['left']['localiser-plan'] = array(
    'lien' => "../app/localisation_plan_view.php?idx=",
    'id' => '&amp;obj='.$obj.'&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab#camap0',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Visualiser sur plan').'">'.__('Plan').'</span>',
    'ordre' => 150,
    'parameters' => array("option_localisation" => "plan", ),
);

/**
* Options
*/
// Marque la ligne si le dossier n'a pas été géolocalisé.
// Nécessite que la seconde colonne des tableaux soit le geom.
// Dans la variable $champAffiche en seconde position mettre 'dossier.geom as "geom_picto"',
$options[] = array(
    "type" => "condition",
    "field" => 'emplacement.geom',
    "case" => array(
        1 => array( // column key for dossier.geom (geom_picto)
            "values" => array(''), // style only empty values
            "style" => "no-geoloc1"
        ),
    ),
);
$options[] = array(
    "type" => "condition",
    "field" => 'emplacement.pgeom',
    "case" => array(
        1 => array( // column key for dossier.geom (geom_picto)
            "values" => array(''), // style only empty values
            "style" => "no-geoloc2"
        ),
    ),
);

