<?php
/**
 * 
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/travaux.inc.php";

//
$ent = " -> ".__($obj);

// Remplacement du libellé de la première colonne
$champAffiche[0] = $obj.'.'.$obj.' as "'.__("id").'"';
$champRecherche[0] = $champAffiche[0];

//
if ($retourformulaire == 'concession'
    || $retourformulaire == 'colombarium'
    || $retourformulaire == 'enfeu'
    || $retourformulaire == 'ossuaire'
    || $retourformulaire == 'depositoire'
    || $retourformulaire == 'terraincommunal') {
    $selection = " WHERE (".$obj.".emplacement=".$idx.") ";
}
