<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/operation.inc.php";

$label=__("operation_trt");
$ent = __("Operations")." -> ".__("traitees");
// Actions a gauche : consulter
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);
$tab_actions['content'] = NULL;

//
$tab_actions['left']['edition-operation'] = array(
    'lien' => OM_ROUTE_FORM."&obj=operation&action=12&amp;idx=",
    'id' => "&retour=tab",
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Edition")."\">".__("Edition")."</span>",
    'ajax' => false,
    'ordre' => 120,
);

$table=DB_PREFIXE."operation inner join ".DB_PREFIXE."emplacement on operation.emplacement = emplacement.emplacement";
$edition="";
$champAffiche = array(
    'operation.operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    "famille",
    'categorie as "'.__("categorie").'"',
    "to_char (date,'DD/MM/YYYY') as date",
    'operation.consigne_acces as "'.__("consigne d'accès (origine)").'"',
    'operation.consigne_acces_transfert as "'.__("consigne d'accès (destination)").'"',
    "CASE operation.prive WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("Privé").'"',
);
$champRecherche = array(
    'operation.operation as "'.__("id").'"',
    'numdossier as "'.__("numdossier").'"',
    "famille",
    'categorie as "'.__("categorie").'"',
);
$tri= "order by date";
$selection = " where etat = 'trt'";
if ($retourformulaire== 'concession'
    or $retourformulaire== 'colombarium'
    or $retourformulaire== 'terraincommunal'
    or $retourformulaire== 'enfeu'
    ){
    $selection.=" and operation.emplacement =".$idx;
    $table=DB_PREFIXE."operation inner join ".DB_PREFIXE."operation_defunt on operation.operation = operation_defunt.operation";
    $champAffiche = array(
        'operation.operation as "'.__("id").'"',
        'operation.numdossier as "'.__("numdossier").'"' ,
        'operation.categorie as "'.__("categorie").'"',
        'operation_defunt.defunt_nom as "'.__("nom de naissance").'"',
        'operation_defunt.defunt_prenom as "'.__("prenom").'"',
        'operation_defunt.defunt_marital as "'.__("nom d'usage").'"',
        'to_char(operation.date,\'DD/MM/YYYY\') as "'.__("date").'"',
        'operation.consigne_acces as "'.__("consigne d'accès (origine)").'"',
        'operation.consigne_acces_transfert as "'.__("consigne d'accès (destination)").'"',
        "CASE operation.prive WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("Privé").'"',
    );
}
