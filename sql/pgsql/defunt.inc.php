<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/defunt.inc.php";

//
$ent = " -> ".__($obj);

require_once "../obj/defunt.class.php";
$defunt = new defunt(0);
$nature_defunt_trad = $defunt->compose_nature_defunt_trad();
//
$champAffiche = array(
    'defunt.defunt as "'.__("id").'"',
    'defunt.nom as "'.__("nom de naissance").'"',
    'defunt.marital as "'.__("nom d'usage").'"',
    'defunt.prenom as "'.__("prenom").'"',
    'to_char(defunt.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    'to_char(defunt.datedeces ,\'DD/MM/YYYY\') as "'.__("datedeces").'"',
    'to_char(defunt.dateinhumation ,\'DD/MM/YYYY\') as "'.__("dateinhumation").'"',
    $nature_defunt_trad.' as "'.__("nature").'"',
    'defunt.taille as "'.__("taille").'"',
    'defunt.verrou as "'.__("verrou").'"',
);
$champRecherche = array(
    'defunt.nom as "'.__("nom de naissance").'"',
    'defunt.prenom as "'.__("prenom").'"',
    'defunt.marital as "'.__("nom d'usage").'"',
);
$tri = " ORDER BY nom, prenom ";

// EDITION PDF
$tab_actions['left']['etat-defunt'] = array(
    'lien' => OM_ROUTE_FORM."&obj=".$obj."&action=101&idx=",
    'id' => '',
    'target' => "_blank",
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix pdf-16" title="'.__('télécharger le récapitulatif au format pdf').'">'.__('récapitulatif').'</span>',
    'ordre' => 110,
);

//
$options = array();
$options[] = array(
    "type" => "condition",
    "field" => "defunt.verrou",
    "case" => array(
        array(
            "values" => array("Oui", ),
            "style" => "radiation-encours",
            //
            "href" => array(
                0 => array("lien" => "", "id" => "", "lib" => ""),
                1 => array("lien" => "", "id" => "", "lib" => ""),
                2 => array("lien" => "", "id" => "", "lib" => ""),
            ),
        ),
    ),
);
