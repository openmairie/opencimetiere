<?php
/**
 * Fichier de configuration pour le module recherche (obj/search.class.php)
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

///// definitions des types de recherche
// $conf = array(
//    "libelle" => __("Recherche globale"),
//    "champAffiche" => array(), /// liste des champs a afficher
//    "champRecherche" => array(), /// champs de recherche 
//    "table" => "", // tables
//    "serie" => 0, // limit (facultatif - par defaut 100 )
//    "selection" => "", // where
//    "tri" => "", // order by (facultatif)
//    "href" => "" // lien (facultatif)
// );
// array_push( $configTypeRecherche, $conf);

$configTypeRecherche = array();


$conf = array(
    "libelle" => __("Recherche globale"),
    "champAffiche" => "#",
    "champRecherche" => "",
    "table" => "",
    "serie" => 0,
    "selection" => "",
    "tri" => "",
    "href" => ""
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Numero de concession"),
    "champAffiche" => array(
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("e.numero","(e.numero||' '||e.complement)","e.emplacement"),
    "table" => DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere",
    "tri" => "ORDER BY numero",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Famille de la concession"),
    "champAffiche" => array(
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("e.famille"),
    "table" => DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere",
    "tri" => "ORDER BY famille",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Numéro d'acte"),
    "champAffiche" => array(
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("e.numeroacte"),
    "table" => DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere",
    "tri" => "ORDER BY numeroacte",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);

$conf = array(
    "libelle" => __("Nom de naissance du défunt"),
    "champAffiche" => array(
        "d.titre AS titre",
        "d.nom AS nomdenaissance",
        "d.prenom AS prenom",
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib AS voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("d.nom"),
    "table" => DB_PREFIXE."defunt d,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND d.emplacement = e.emplacement",
    "tri" => "ORDER BY nom",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Date de décès du défunt"),
    "champAffiche" => array(
        "d.titre AS titre",
        "d.nom AS nomdenaissance",
        "d.prenom AS prenom",
        "d.datedeces AS dcd",
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib AS voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("d.datedeces"),
    "table" => DB_PREFIXE."defunt d,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND d.emplacement = e.emplacement",
    "tri" => "ORDER BY dcd",
    "modeDate" => "true",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Nom d'usage du défunt"),
    "champAffiche" => array(
        "d.titre AS titre",
        "d.nom AS nomdenaissance",
        "d.prenom AS prenom",
        'd.marital AS nomdusage',
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib AS voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("marital"),
    "table" => DB_PREFIXE."defunt d,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND d.emplacement = e.emplacement AND d.marital<>''",
    "tri" => "ORDER BY marital",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Nom de naissance du concessionnaire"),
    "champAffiche" => array(
        "a.titre AS titre",
        "a.nom AS nomdenaissance",
        "a.prenom AS prenom",
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("nom"),
    "table" => DB_PREFIXE."autorisation a,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND a.nature='concessionnaire' AND a.emplacement = e.emplacement",
    "tri" => "ORDER BY nom",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Nom d'usage du concessionnaire"),
    "champAffiche" => array(
        "a.titre AS titre",
        "a.nom AS nomdenaissance",
        "a.prenom AS prenom",
        'a.marital AS nomdusage',
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("marital"),
    "table" => DB_PREFIXE."autorisation a,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND a.nature='concessionnaire' AND a.emplacement = e.emplacement AND a.marital<>''",
    "tri" => "ORDER BY marital",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Nom de naissance de l'ayant droit"),
    "champAffiche" => array(
        "a.titre AS titre",
        "a.nom AS nomdenaissance",
        "a.prenom AS prenom",
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("nom"),
    "table" => DB_PREFIXE."autorisation a,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND a.nature='ayantdroit' AND a.emplacement = e.emplacement",
    "tri" => "ORDER BY nom",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


$conf = array(
    "libelle" => __("Nom d'usage de l'ayant droit"),
    "champAffiche" => array(
        "a.titre AS titre",
        "a.nom AS nomdenaissance",
        "a.prenom AS prenom",
        'a.marital AS nomdusage',
        "e.emplacement AS emplacement",
        "e.nature AS nature",
        "e.famille AS famille",
        "e.numero || ' ' || COALESCE(e.complement, '') AS numero",
        "v.voielib as voielib",
        "z.zonelib AS zonelib",
        "c.cimetierelib AS cimetierelib",
        "e.numeroacte AS numeroacte",
    ),
    "champRecherche" => array("marital"),
    "table" => DB_PREFIXE."autorisation a,".DB_PREFIXE."emplacement e, ".DB_PREFIXE."voie v, ".DB_PREFIXE."zone z, ".DB_PREFIXE."cimetiere c",
    "serie" => 100,
    "selection" => "WHERE e.voie = v.voie AND v.zone = z.zone AND z.cimetiere = c.cimetiere AND a.nature='ayantdroit' AND a.emplacement = e.emplacement AND a.marital<>''",
    "tri" => "ORDER BY marital",
    "href" => OM_ROUTE_FORM."&obj=£nature&amp;action=3&amp;idx=£emplacement"
);
array_push( $configTypeRecherche, $conf);


////// SELECTION DU CIMETIERE
$conf = array(
    "type" => "select",
    "libelle" => __("cimetiere"),
    "requete" => "select cimetiere, cimetierelib from ".DB_PREFIXE."cimetiere",
    "list" => 1,
    "value" => 0,
    "selection" => "c.cimetiere"
);
array_push( $configTypeRecherche, $conf);
