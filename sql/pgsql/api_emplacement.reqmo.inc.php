<?php
//$Id$ 
//gen openMairie le 11/02/2016 11:30

$reqmo['libelle']=__('reqmo-libelle-api_emplacement');
$reqmo['reqmo_libelle']=__('reqmo-libelle-api_emplacement');
$ent=__('api_emplacement');
$reqmo['sql']="
SELECT  
    [emplacement], [nature], [numero], [complement], [CONCAT_WS(' ', zone_type.libelle, zone.zonelib) as zone], [CONCAT_WS(' ', voie_type.libelle, voie.voielib) as voie], [cimetiere.cimetiere as cimetiere], [cimetierelib], [famille], [datevente], [terme], [duree], [dateterme], [observation], [positionx], [positiony], [sepulturetype], [daterenouvellement], [videsanitaire], [abandon], [date_abandon], [emplacement.geom as geom], [emplacement.pgeom as pgeom]
FROM 
  ".DB_PREFIXE."emplacement
 LEFT JOIN ".DB_PREFIXE."voie
   ON emplacement.voie = voie.voie
 LEFT JOIN ".DB_PREFIXE."voie_type
   ON voie.voietype = voie_type.voie_type
 LEFT JOIN ".DB_PREFIXE."zone
   ON voie.zone = zone.zone
 LEFT JOIN ".DB_PREFIXE."zone_type
   ON zone.zonetype = zone_type.zone_type
 LEFT JOIN ".DB_PREFIXE."cimetiere
   ON zone.cimetiere = cimetiere.cimetiere
GROUP BY emplacement.emplacement, zone_type.libelle, zone.zonelib, voie_type.libelle, voie.voielib, cimetiere.cimetiere, cimetiere.cimetierelib
ORDER BY 
    [tri]";
$reqmo['emplacement']='checked';
$reqmo['nature']='checked';
$reqmo['numero']='checked';
$reqmo['zone']='checked';
$reqmo['voie']='checked';
$reqmo['complement']='checked';
$reqmo['cimetiere']='checked';
$reqmo['cimetierelib']='checked';
$reqmo['famille']='checked';
$reqmo['datevente']='checked';
$reqmo['terme']='checked';
$reqmo['duree']='checked';
$reqmo['dateterme']='checked';
$reqmo['observation']='checked';
$reqmo['positionx']='checked';
$reqmo['positiony']='checked';
$reqmo['sepulturetype']='checked';
$reqmo['daterenouvellement']='checked';
$reqmo['videsanitaire']='checked';
$reqmo['abandon']='checked';
$reqmo['date_abandon']='checked';
$reqmo['geom']='checked';
$reqmo['pgeom']='checked';


$reqmo['tri']=array(
  "emplacement",
  "nature",
  "numero",
  "complement",
  "cimetiere",
  "cimetierelib",
  "famille",
  "datevente",
  "terme",
  "duree",
  "dateterme",
  "daterenouvellement",
  "observation",
  "position_x",
  "position_y",
  "spulture_type",
  "videsanitaire",
  "abandon",
  "date_abandon",
  "geom",
  "pgeom",);
