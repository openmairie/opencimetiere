<?php
// Libellé de la requête
$reqmo['libelle']=__('reqmo-libelle-api_operation');
$reqmo['reqmo_libelle']=__('reqmo-libelle-api_operation');
$ent=__('api_operation');

// Requête à effectuer
$reqmo['sql'] = '
SELECT
  [operation],
  [emplacement.emplacement as emplacement],
  [cimetiere.cimetiere as cimetiere],
  [numdossier],
  [operation.date as date],
  [heure],
  [societe_coordonnee],
  [pf_coordonnee],
  [categorie],
  [operation.observation as observation]
FROM
  '.DB_PREFIXE.'operation 
  LEFT JOIN '.DB_PREFIXE.'emplacement
    ON operation.emplacement = emplacement.emplacement
   LEFT JOIN '.DB_PREFIXE.'voie
    ON emplacement.voie = voie.voie
  LEFT JOIN '.DB_PREFIXE.'zone
   ON voie.zone = zone.zone
  LEFT JOIN '.DB_PREFIXE.'cimetiere
   ON zone.cimetiere = cimetiere.cimetiere
';

$reqmo['operation'] = "checked";
$reqmo['emplacement'] = "checked";
$reqmo['cimetiere'] = "checked";
$reqmo['numdossier'] = "checked";
$reqmo['date'] = "checked";
$reqmo['heure'] = "checked";
$reqmo['societe_coordonnee'] = "checked";
$reqmo['pf_coordonnee'] = "checked";
$reqmo['categorie'] = "checked";
$reqmo['observation'] = "checked";