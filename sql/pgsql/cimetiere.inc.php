<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/cimetiere.inc.php";

// SELECT
// Remplacement du libellé de la première colonne
$champAffiche[0] = 'cimetiere.cimetiere as "'.__("id").'"';
$champRecherche[0] = $champAffiche[0];

// LOCALISATION
// XXX Pourquoi être obligé de fixer dans l'url la géométrie et le zoom ?
$tab_actions['left']['localisation'] = array(
    'lien' => OM_ROUTE_MAP.'&mode=tab_sig&obj='.$obj.'&amp;idx=',
    'id' => '&amp;geometrie=polygon&amp;zoom=17',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 100,
    "parameters" => array("option_localisation" => "sig_interne", ),
);
