<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/defunt_archive.inc.php";

$champAffiche = array(
    'defunt_archive.defunt as "'.__("id").'"',
    'nom as "'.__("nom de naissance").'"',
    'marital as "'.__("nom d'usage").'"',
    'prenom as "'.__("prenom").'"',
    "to_char(datenaissance,'DD/MM/YYYY') as datenaissance",
    "to_char(dateinhumation,'DD/MM/YYYY') as dateinhumation",
    "defunt_archive.nature",
);

$champRecherche = array(
    'nom as "'.__("nom de naissance").'"',
    'marital as "'.__("nom d'usage").'"',
    'prenom as "'.__("prenom").'"',
);

$tri = " order by nom, prenom";

if (isset($idx)) $selection=" where defunt_archive.emplacement=".$idx;
// Actions a gauche : consulter
// Actions a gauche : consulter
if(isset($tab_actions)) {
    unset($tab_actions['corner']['ajouter']);
}
