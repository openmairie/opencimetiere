<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/contrat.inc.php";

//
$ent = " -> ".__("contrats");

//
$field_contrat_emplacement = '(emplacement.nature||\' - n°\'||emplacement.emplacement||\' - Famille : \'||emplacement.famille) as "'.__("emplacement").'"';
$champAffiche = array(
    'contrat.contrat as "'.__("id").'"',
    $field_contrat_emplacement,
    'to_char(contrat.datedemande ,\'DD/MM/YYYY\') as "'.__("datedemande").'"',
    'contrat.origine as "'.__("origine").'"',
    'contrat.terme as "'.__("terme").'"',
    'contrat.duree as "'.__("duree").'"',
    'to_char(contrat.datevente ,\'DD/MM/YYYY\') as "'.__("datevente").'"',
    'to_char(contrat.dateterme ,\'DD/MM/YYYY\') as "'.__("dateterme").'"',
    'contrat.montant as "'.__("montant").'"',
    'contrat.monnaie as "'.__("monnaie").'"',
    "case contrat.valide when 't' then 'Oui' else 'Non' end as \"".__("valide")."\"",
);
//
$fields_to_hide = array();
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $fields_to_hide[] = $field_contrat_emplacement;
}
$champAffiche = array_diff($champAffiche, $fields_to_hide);
$tri = " ORDER BY contrat.datevente ASC, contrat.dateterme ASC ";


/**
 * Options du LISTING
 */
$options = array();
// ADVS
$champs = array();
$champs['id'] = array(
    'colonne' => 'contrat',
    'table' => 'contrat',
    'type' => 'text',
    'libelle' => __('id'),
    'taille' => 8,
    'max' => '',
);
$champs['emplacement'] = array(
    'colonne' => array('nature', 'emplacement','famille'),
    'table' => 'emplacement',
    'type' => 'text',
    'libelle' => __('emplacement'),
    'help' => __("identifiant de l'emplacement (ex : 12) ou famille (ex : DUPONT) ou nature de l'emplacement (ex : concession)"),
    'taille' => 8,
    'max' => '',
);
$champs['datedemande'] = array(
    'colonne' => 'datedemande',
    'table' => 'contrat',
    'type' => 'date',
    'libelle' => __('datedemande'),
    'lib1'=> __("début"),
    'lib2' => __("fin"),
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['datevente'] = array(
    'colonne' => 'datevente',
    'table' => 'contrat',
    'type' => 'date',
    'libelle' => __('date de vente'),
    'lib1'=> __("début"),
    'lib2' => __("fin"),
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['dateterme'] = array(
    'colonne' => 'dateterme',
    'table' => 'contrat',
    'type' => 'date',
    'libelle' => __('date de terme'),
    'lib1'=> __("début"),
    'lib2' => __("fin"),
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['origine'] = array(
    'colonne' => 'origine',
    'table' => 'contrat',
    'type' => 'select',
    'libelle' => __('origine'),
    'taille' => 8,
);
$champs['terme'] = array(
    'colonne' => 'terme',
    'table' => 'contrat',
    'type' => 'select',
    'libelle' => __('terme'),
    'taille' => 8,
);
$champs['duree'] = array(
    'colonne' => 'duree',
    'table' => 'contrat',
    'type' => 'text',
    'libelle' => __('durée'),
    'taille' => 8,
    'max' => '',
);
$args = array();
$args[0] = array('', 'true', 'false');
$args[1] = array(_('Tous'), _('Oui'), _('Non'));
$champs['valide'] = array(
    'colonne' => 'valide',
    'table' => 'contrat',
    'type' => 'select',
    'libelle' => __('valide'),
    'subtype' => 'manualselect',
    'args' => $args,
);
$options[] =  array(
    'type' => 'search',
    'display' => true,
    'advanced' => $champs,
    'default_form' => 'advanced',
    'absolute_object' => 'contrat'
);
