<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

// !!! On positionne $obj à 'enfeu' pour éviter des surcharges
$obj = "enfeu";

//
include "../sql/pgsql/enfeu.inc.php";

// !!! On repositionne $obj à 'enfeufin'
$obj = "enfeufin";

//
$ent = __("archives")." -> ".__("enfeufin");

//
$selection .= " and emplacement.libre<>'Oui' ";

//
unset($tab_actions['left']['localiser-sig-interne']);
unset($tab_actions['left']['localiser-sig-externe']);
unset($tab_actions['left']['localiser-plan']);
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);

//
$tab_actions['content'] = array(
    "lien" => OM_ROUTE_FORM."&obj=enfeu&action=31&idx=",
    "id" => "&specific_origin=enfeufin&amp;advs_id=".$advs_id."&amp;premier=".$premier."&amp;tricol=".$tricol."&amp;valide=".$valide."&amp;retour=tab",
    "lib" => "",
);
