<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/emplacement.inc.php";

// Suppression de la dernière colonne affichée : libre
array_pop($champAffiche);

//
$sousformulaire = array(
    "defunt",
);

// XXX Pourquoi ne pas permettre de localiser le dépositoire ?
unset($tab_actions['left']['localiser-sig-interne']);
unset($tab_actions['left']['localiser-sig-externe']);
unset($tab_actions['left']['localiser-plan']);
