<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/zone.inc.php";

// SELECT
$champAffiche = array(
    'zone.zone as "'.__("id").'"',
    'zone_type.libelle as "'.__("zonetype").'"',
    'zone.zonelib as "'.__("libelle").'"',
    'cimetiere.cimetierelib as "'.__("cimetiere").'"',
);
$champRecherche = $champAffiche;
$tri = " ORDER BY zone.zonelib ";

// LOCALISATION
// XXX Pourquoi être obligé de fixer dans l'url la géométrie et le zoom ?
$tab_actions['left']['localisation'] = array(
    'lien' => OM_ROUTE_MAP.'&mode=tab_sig&obj='.$obj.'&amp;idx=',
    'id' => '&amp;geometrie=polygon&amp;zoom=11',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('SIG').'">'.__('SIG').'</span>',
    'ordre' => 100,
    "parameters" => array("option_localisation" => "sig_interne", ),
);
