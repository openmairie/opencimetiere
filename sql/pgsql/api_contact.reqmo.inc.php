<?php
// Libellé de la requête
$reqmo['libelle']=__('reqmo-libelle-api_contact');
$reqmo['reqmo_libelle']=__('reqmo-libelle-api_contact');
$ent=__('api_contact');

// Requête à effectuer
$reqmo['sql'] = "
SELECT
  [autorisation],
  [emplacement.emplacement as emplacement],
  [titre_de_civilite.libelle as titre],
  [nom],
  [marital],
  [prenom],
  [adresse1],
  [adresse2],
  [cp],
  [ville],
  [telephone1],
  [telephone2],
  [courriel],
  [autorisation.observation as observation]
FROM
  ".DB_PREFIXE."autorisation 
  LEFT JOIN ".DB_PREFIXE."titre_de_civilite
    ON autorisation.titre = titre_de_civilite.titre_de_civilite
  LEFT JOIN ".DB_PREFIXE."emplacement
    ON autorisation.emplacement = emplacement.emplacement
";

$reqmo['autorisation'] = "checked";
$reqmo['emplacement'] = "checked";
$reqmo['titre'] = "checked";
$reqmo['nom'] = "checked";
$reqmo['marital'] = "checked";
$reqmo['prenom'] = "checked";
$reqmo['adresse1'] = "checked";
$reqmo['adresse2'] = "checked";
$reqmo['cp'] = "checked";
$reqmo['ville'] = "checked";
$reqmo['telephone1'] = "checked";
$reqmo['telephone2'] = "checked";
$reqmo['courriel'] = "checked";
$reqmo['observation'] = "checked";