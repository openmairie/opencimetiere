<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/emplacement.inc.php";

//
$sousformulaire = array(
    "defunt",
    "autorisation",
    "contrat",
    "operation_trt",
    "courrier",
    "travaux",
    "dossier",
);
$sousformulaire_parameters = array(
    "autorisation" => array(
        "title" => _("Contacts"),
    ),
    "contrat" => array(
        "title" => _("Contrats"),
    ),
);
