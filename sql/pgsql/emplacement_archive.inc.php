<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/emplacement_archive.inc.php";

$table=DB_PREFIXE."emplacement_archive inner join ".DB_PREFIXE.
        "voie on emplacement_archive.voie=voie.voie inner join ".DB_PREFIXE.
        "zone on voie.zone=zone.zone inner join ".DB_PREFIXE.
        "cimetiere on zone.cimetiere=cimetiere.cimetiere ";

$champAffiche=array("emplacement as id",
                    "famille",
                    "numero",
                    "complement as cpt",
                    "voietype",
                    "voielib",
                    "zonetype",
                    "zonelib",
                    "cimetierelib");

$champRecherche=array("emplacement",
                      "famille",
                      "cimetierelib",
                      "voielib",
                      "zonelib");
$tri= " order by famille";

// Actions a gauche : consulter
if(isset($tab_actions)) {
    unset($tab_actions['corner']['ajouter']);
}


$sousformulaire=array("autorisation_archive", "dossier_archive", "contrat_archive", "courrier_archive","travaux_archive","defunt_archive", "operation_archive");
