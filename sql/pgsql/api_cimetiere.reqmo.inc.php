<?php
//$Id$ 
//gen openMairie le 11/02/2016 11:30

$reqmo['libelle']=__('reqmo-libelle-api_cimetiere');
$reqmo['reqmo_libelle']=__('reqmo-libelle-api_cimetiere');
$ent=__('api_cimetiere');
$reqmo['sql']="select  [cimetiere], [cimetierelib], [adresse1], [adresse2], [cp], [ville], [observations], [information_generale] from ".DB_PREFIXE."cimetiere  order by [tri]";
$reqmo['cimetiere']='checked';
$reqmo['cimetierelib']='checked';
$reqmo['adresse1']='checked';
$reqmo['adresse2']='checked';
$reqmo['cp']='checked';
$reqmo['ville']='checked';
$reqmo['observations']='checked';
$reqmo['information_generale']='checked';
$reqmo['tri']=array('cimetiere','cimetierelib','adresse1','adresse2','cp','ville','observations','information_generale');
