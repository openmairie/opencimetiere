<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/emplacement.inc.php";

//
$sousformulaire = array(
    "defunt",
    "courrier",
    "operation_trt",
    "travaux",
    "dossier",
);
$sousformulaire_parameters = array(
    "contrat" => array(
        "title" => _("Contrats"),
    ),
);
