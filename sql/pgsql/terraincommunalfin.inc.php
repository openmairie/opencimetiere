<?php
/**
 *
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

// !!! On positionne $obj à 'terraincommunal' pour éviter des surcharges
$obj = "terraincommunal";

//
include "../sql/pgsql/terraincommunal.inc.php";

// !!! On repositionne $obj à 'terraincommunalfin'
$obj = "terraincommunalfin";

//
$ent = __("archives")." -> ".__("terraincommunalfin");

//
$selection .= " and emplacement.libre<>'Oui' ";

//
unset($tab_actions['left']['localiser-sig-interne']);
unset($tab_actions['left']['localiser-sig-externe']);
unset($tab_actions['left']['localiser-plan']);
unset($tab_actions['left']['consulter']);
unset($tab_actions['corner']['ajouter']);

//
$tab_actions['content'] = array(
    "lien" => OM_ROUTE_FORM."&obj=terraincommunal&action=31&idx=",
    "id" => "&specific_origin=terraincommunalfin&amp;advs_id=".$advs_id."&amp;premier=".$premier."&amp;tricol=".$tricol."&amp;valide=".$valide."&amp;retour=tab",
    "lib" => "",
);
