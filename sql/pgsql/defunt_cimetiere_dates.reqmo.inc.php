<?php
//$Id$ 
//gen openMairie le 11/02/2016 11:30

$reqmo['libelle']=__('Defunt par cimetiere entre 2  dates');
$reqmo['reqmo_libelle']=__('reqmo-defunt');
$ent=__('emplacement');
$reqmo['sql']="select  [nom], [prenom], [marital], 
		[to_char(datedeces, 'DD/MM/YYYY') as date_deces], 
		[to_char(dateinhumation, 'DD/MM/YYYY') as date_inhumation], 
		[to_char(datenaissance, 'DD/MM/YYYY') as date_naissance],
		[numero], [voie_type.libelle as voie_type_lib],[voielib], [zone_type.libelle as zone_type_lib], [zonelib], 
		[cimetierelib], 
		[famille] from ".
		DB_PREFIXE."defunt left join ".
		DB_PREFIXE."emplacement on emplacement.emplacement = defunt.emplacement left join ".
		DB_PREFIXE."voie on voie.voie = emplacement.voie left join ".
		DB_PREFIXE."voie_type on voie.voietype = voie_type.voie_type left join ".
		DB_PREFIXE."zone on zone.zone = voie.zone left join ".
		DB_PREFIXE."zone_type on zone.zonetype = zone_type.zone_type left join ".
		DB_PREFIXE."cimetiere on cimetiere.cimetiere = zone.cimetiere ";
$reqmo['sql'] .= " where dateinhumation >= '[inhumation_debut_(AAAA-MM-JJ)]' " ;
$reqmo['sql'] .= " and dateinhumation <= '[inhumation_fin_(AAAA-MM-JJ)]' " ;
$reqmo['sql'] .= " and cimetiere.cimetiere = '[cimetiere_choix]' " ;
$reqmo['sql'] .= " order by [tri]";
$reqmo['nom']='checked';
$reqmo['prenom']='checked';
$reqmo['marital']='checked';
$reqmo['date_deces']='checked';
$reqmo['date_naissance']='checked';
$reqmo['date_inhumation']='checked';
$reqmo['voielib']='checked';
$reqmo['zonelib']='checked';
$reqmo['type']='checked';
$reqmo['voie_type_lib']='checked';
$reqmo['zone_type_lib']='checked';
$reqmo['cimetierelib']='checked';
$reqmo['famille']='checked';
$reqmo['numero']='checked';
$reqmo['cimetiere_choix']='select cimetiere, cimetierelib from '.
DB_PREFIXE.'cimetiere order by cimetierelib';

$reqmo['tri']=array('nom','prenom','marital','dateinhumation', 
		'zonelib', 'voielib');
