<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package opencimetiere
 * @version SVN : $Id$
 */

$files_to_avoid = array(
    "om_application_override.class.php",
    "opencimetiere.class.php",
    "search.class.php",
);

$permissions = array(
	//
	"plans_fichier_telecharger",
);
