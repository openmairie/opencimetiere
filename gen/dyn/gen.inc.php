<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package opencimetiere
 * @version SVN : $Id: gen.inc.php 2473 2013-09-11 10:45:27Z fmichon $
 */

/**
 * Surcharge applicative de la classe 'om_dbform'.
 */
$om_dbform_path_override = "../obj/om_dbform.class.php";
$om_dbform_class_override = "om_dbform";

/**
 *
 */
$administration_parametrage = "administration & paramétrage";

/**
 * Ce tableau permet de lister les tables qui ne doivent pas être prises en
 * compte dans le générateur. Elles n'apparaîtront donc pas dans l'interface
 * et ne seront pas automatiquement générées par le 'genfull'.
 */
$tables_to_avoid = array(
    "geometry_columns",
    "om_version",
    "spatial_ref_sys",
    "geo_cimetiere",
    "geo_emplacement",
    "geo_hab_lin",
    "geo_hab_point",
    "geo_hab_pol",
    "geo_hab_txt",
    "geo_zone",
    "geo_loc_emplacement",
);


/**
 * Ce tableau de configuration permet de donner des informations de surcharges
 * sur certains objets pour qu'elles soient prises en compte par le générateur.
 */
$tables_to_overload = array(
    // A&P
    "cimetiere" => array(
        "displayed_fields_in_tableinc" => array(
            "cimetierelib", "adresse1", "adresse2", "cp", "ville",
        ),
        "breadcrumb_in_page_title" => array($administration_parametrage, "localisation", ),
    ),
    // A&P
    "zone" => array(
        "displayed_fields_in_tableinc" => array(
            "zonetype", "zonelib", "cimetiere",
        ),
        "breadcrumb_in_page_title" => array($administration_parametrage, "localisation", ),
    ),
    // A&P
    "zone_type" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "om_validite" => "hidden_by_default",
    ),
    // A&P
    "entreprise" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "tabs_in_form" => false,
    ),
    // A&P
    "lien_parente" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "tabs_in_form" => false,
    ),
    // A&P
    "tarif" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "tabs_in_form" => false,
    ),
    // A&P
    "plans" => array(
        "displayed_fields_in_tableinc" => array(
            "planslib",
        ),
        "breadcrumb_in_page_title" => array($administration_parametrage, "localisation", ),
        "tabs_in_form" => false,
    ),
    // A&P
    "sepulture_type" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "om_validite" => "hidden_by_default",
        "tabs_in_form" => false,
    ),
    // A&P
    "titre_de_civilite" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "om_validite" => "hidden_by_default",
        "tabs_in_form" => false,
    ),
    // A&P
    "travaux_nature" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "om_validite" => "hidden_by_default",
        "tabs_in_form" => false,
    ),
    // A&P
    "voie" => array(
        "displayed_fields_in_tableinc" => array(
            "voietype", "voielib", "zone",
        ),
        "breadcrumb_in_page_title" => array($administration_parametrage, "localisation", ),
        "tabs_in_form" => false,
    ),
    // A&P
    "voie_type" => array(
        "breadcrumb_in_page_title" => array($administration_parametrage, "divers", ),
        "om_validite" => "hidden_by_default",
    ),
    // FMWK
    "om_collectivite" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "général", ),
    ),
    // FMWK
    "om_dashboard" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "tableaux de bord", ),
    ),
    // FMWK
    "om_droit" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_etat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_lettretype" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_logo" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_parametre" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "général", ),
    ),
    // FMWK
    "om_permission" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_profil" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
    ),
    // FMWK
    "om_requete" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_sig_extent" => array(
        "tablename_in_page_title" => "étendue",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_flux" => array(
        "tablename_in_page_title" => "flux",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map" => array(
        "tablename_in_page_title" => "carte",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map_comp" => array(
        "tablename_in_page_title" => "géométrie",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sig_map_flux" => array(
        "tablename_in_page_title" => "flux appliqué à une carte",
        "breadcrumb_in_page_title" => array($administration_parametrage, "SIG", ),
    ),
    // FMWK
    "om_sousetat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "éditions", ),
    ),
    // FMWK
    "om_utilisateur" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "gestion des utilisateurs", ),
        "displayed_fields_in_tableinc" => array(
            "nom", "email", "login", "om_type", "om_profil",
        ),
    ),
    // FMWK
    "om_widget" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "tableaux de bord", ),
        "displayed_fields_in_tableinc" => array(
            "libelle", "om_profil", "type",
        ),
    ),
    //
    "emplacement" => array(
        "extended_class" => array(
            "colombarium",
            "concession",
            "depositoire",
            "enfeu",
            "ossuaire",
            "terraincommunal",
        ),
    ),
    //
    "operation" => array(
        "extended_class" => array(
            "inhumation",
            "inhumation_colombarium",
            "inhumation_enfeu",
            "inhumation_terraincommunal",
            "reduction",
            "reduction_enfeu",
            "transfert",
        ),
    ),
    //
    "autorisation" => array(
        "displayed_fields_in_tableinc" => array(
            "nom", "marital", "prenom", "datenaissance", "dcd", "nature",
        ),
    ),
    "autorisation_archive" => array(
        "displayed_fields_in_tableinc" => array(
            "nom", "marital", "prenom", "datenaissance", "dcd", "nature",
        ),
    ),
    //
    "courrier" => array(
        "displayed_fields_in_tableinc" => array(
            "destinataire", "datecourrier", "lettretype",
        ),
    ),
    "courrier_archive" => array(
        "displayed_fields_in_tableinc" => array(
            "destinataire", "datecourrier", "lettretype",
        ),
    ),
    //
    "travaux" => array(
        "displayed_fields_in_tableinc" => array(
            "entreprise", "datedebinter", "datefininter", "naturedemandeur", "naturetravaux",
        ),
    ),
    "travaux_archive" => array(
        "displayed_fields_in_tableinc" => array(
            "entreprise", "datedebinter", "datefininter", "naturedemandeur", "naturetravaux",
        ),
    ),
    //
    "dossier" => array(
        "displayed_fields_in_tableinc" => array(
            "fichier", "datedossier", "typedossier",
        ),
    ),
    "dossier_archive" => array(
        "displayed_fields_in_tableinc" => array(
            "fichier", "datedossier", "typedossier",
        ),
    ),
);
