<?php
//$Id$ 
//gen openMairie le 13/10/2022 09:14

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("autorisation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."autorisation
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON autorisation.emplacement=emplacement.emplacement 
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite 
        ON autorisation.titre=titre_de_civilite.titre_de_civilite ";
// SELECT 
$champAffiche = array(
    'autorisation.autorisation as "'.__("autorisation").'"',
    'autorisation.nature as "'.__("nature").'"',
    'autorisation.nom as "'.__("nom").'"',
    'autorisation.marital as "'.__("marital").'"',
    'autorisation.prenom as "'.__("prenom").'"',
    'to_char(autorisation.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    "case autorisation.dcd when 't' then 'Oui' else 'Non' end as \"".__("dcd")."\"",
    );
//
$champNonAffiche = array(
    'autorisation.emplacement as "'.__("emplacement").'"',
    'autorisation.titre as "'.__("titre").'"',
    'autorisation.adresse1 as "'.__("adresse1").'"',
    'autorisation.adresse2 as "'.__("adresse2").'"',
    'autorisation.cp as "'.__("cp").'"',
    'autorisation.ville as "'.__("ville").'"',
    'autorisation.telephone1 as "'.__("telephone1").'"',
    'autorisation.parente as "'.__("parente").'"',
    'autorisation.observation as "'.__("observation").'"',
    'autorisation.telephone2 as "'.__("telephone2").'"',
    'autorisation.courriel as "'.__("courriel").'"',
    'autorisation.genealogie as "'.__("genealogie").'"',
    );
//
$champRecherche = array(
    'autorisation.autorisation as "'.__("autorisation").'"',
    'autorisation.nature as "'.__("nature").'"',
    'autorisation.nom as "'.__("nom").'"',
    'autorisation.marital as "'.__("marital").'"',
    'autorisation.prenom as "'.__("prenom").'"',
    );
$tri="ORDER BY emplacement.nature ASC NULLS LAST";
$edition="autorisation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    "titre_de_civilite" => array("titre_de_civilite", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (autorisation.emplacement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (autorisation.titre = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'courrier',
    'genealogie',
);

