<?php
//$Id$ 
//gen openMairie le 14/09/2023 16:33

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("operation_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."operation_archive";
// SELECT 
$champAffiche = array(
    'operation_archive.operation as "'.__("operation").'"',
    'operation_archive.numdossier as "'.__("numdossier").'"',
    'to_char(operation_archive.date ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'operation_archive.heure as "'.__("heure").'"',
    'operation_archive.emplacement as "'.__("emplacement").'"',
    'operation_archive.etat as "'.__("etat").'"',
    'operation_archive.categorie as "'.__("categorie").'"',
    'operation_archive.particulier as "'.__("particulier").'"',
    'operation_archive.emplacement_transfert as "'.__("emplacement_transfert").'"',
    'operation_archive.consigne_acces as "'.__("consigne_acces").'"',
    "case operation_archive.prive when 't' then 'Oui' else 'Non' end as \"".__("prive")."\"",
    'operation_archive.consigne_acces_transfert as "'.__("consigne_acces_transfert").'"',
    'operation_archive.edition_operation as "'.__("edition_operation").'"',
    );
//
$champNonAffiche = array(
    'operation_archive.societe_coordonnee as "'.__("societe_coordonnee").'"',
    'operation_archive.pf_coordonnee as "'.__("pf_coordonnee").'"',
    'operation_archive.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'operation_archive.operation as "'.__("operation").'"',
    'operation_archive.numdossier as "'.__("numdossier").'"',
    'operation_archive.emplacement as "'.__("emplacement").'"',
    'operation_archive.etat as "'.__("etat").'"',
    'operation_archive.categorie as "'.__("categorie").'"',
    'operation_archive.particulier as "'.__("particulier").'"',
    'operation_archive.emplacement_transfert as "'.__("emplacement_transfert").'"',
    'operation_archive.consigne_acces as "'.__("consigne_acces").'"',
    'operation_archive.consigne_acces_transfert as "'.__("consigne_acces_transfert").'"',
    'operation_archive.edition_operation as "'.__("edition_operation").'"',
    );
$tri="ORDER BY operation_archive.numdossier ASC NULLS LAST";
$edition="operation_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

