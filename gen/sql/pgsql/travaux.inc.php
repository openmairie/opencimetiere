<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:57

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("travaux");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."travaux
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON travaux.emplacement=emplacement.emplacement 
    LEFT JOIN ".DB_PREFIXE."entreprise 
        ON travaux.entreprise=entreprise.entreprise 
    LEFT JOIN ".DB_PREFIXE."travaux_nature 
        ON travaux.naturetravaux=travaux_nature.travaux_nature ";
// SELECT 
$champAffiche = array(
    'travaux.travaux as "'.__("travaux").'"',
    'entreprise.nomentreprise as "'.__("entreprise").'"',
    'to_char(travaux.datedebinter ,\'DD/MM/YYYY\') as "'.__("datedebinter").'"',
    'to_char(travaux.datefininter ,\'DD/MM/YYYY\') as "'.__("datefininter").'"',
    'travaux.naturedemandeur as "'.__("naturedemandeur").'"',
    'travaux_nature.libelle as "'.__("naturetravaux").'"',
    );
//
$champNonAffiche = array(
    'travaux.emplacement as "'.__("emplacement").'"',
    'travaux.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'travaux.travaux as "'.__("travaux").'"',
    'entreprise.nomentreprise as "'.__("entreprise").'"',
    'travaux.naturedemandeur as "'.__("naturedemandeur").'"',
    'travaux_nature.libelle as "'.__("naturetravaux").'"',
    );
$tri="ORDER BY entreprise.nomentreprise ASC NULLS LAST";
$edition="travaux";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    "entreprise" => array("entreprise", ),
    "travaux_nature" => array("travaux_nature", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (travaux.emplacement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - entreprise
if (in_array($retourformulaire, $foreign_keys_extended["entreprise"])) {
    $selection = " WHERE (travaux.entreprise = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - travaux_nature
if (in_array($retourformulaire, $foreign_keys_extended["travaux_nature"])) {
    $selection = " WHERE (travaux.naturetravaux = ".intval($idxformulaire).") ";
}

