<?php
//$Id$ 
//gen openMairie le 14/12/2022 16:21

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("defunt");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."defunt
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON defunt.emplacement=emplacement.emplacement 
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite 
        ON defunt.titre=titre_de_civilite.titre_de_civilite ";
// SELECT 
$champAffiche = array(
    'defunt.defunt as "'.__("defunt").'"',
    'defunt.nature as "'.__("nature").'"',
    'defunt.taille as "'.__("taille").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'titre_de_civilite.libelle as "'.__("titre").'"',
    'defunt.nom as "'.__("nom").'"',
    'defunt.prenom as "'.__("prenom").'"',
    'defunt.marital as "'.__("marital").'"',
    'to_char(defunt.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    'to_char(defunt.datedeces ,\'DD/MM/YYYY\') as "'.__("datedeces").'"',
    'defunt.lieudeces as "'.__("lieudeces").'"',
    'to_char(defunt.dateinhumation ,\'DD/MM/YYYY\') as "'.__("dateinhumation").'"',
    'defunt.exhumation as "'.__("exhumation").'"',
    'to_char(defunt.dateexhumation ,\'DD/MM/YYYY\') as "'.__("dateexhumation").'"',
    'defunt.reduction as "'.__("reduction").'"',
    'to_char(defunt.datereduction ,\'DD/MM/YYYY\') as "'.__("datereduction").'"',
    'defunt.verrou as "'.__("verrou").'"',
    'defunt.lieunaissance as "'.__("lieunaissance").'"',
    'defunt.x as "'.__("x").'"',
    'defunt.y as "'.__("y").'"',
    'defunt.id_temp as "'.__("id_temp").'"',
    );
//
$champNonAffiche = array(
    'defunt.observation as "'.__("observation").'"',
    'defunt.historique as "'.__("historique").'"',
    'defunt.parente as "'.__("parente").'"',
    'defunt.genealogie as "'.__("genealogie").'"',
    );
//
$champRecherche = array(
    'defunt.defunt as "'.__("defunt").'"',
    'defunt.nature as "'.__("nature").'"',
    'defunt.taille as "'.__("taille").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'titre_de_civilite.libelle as "'.__("titre").'"',
    'defunt.nom as "'.__("nom").'"',
    'defunt.prenom as "'.__("prenom").'"',
    'defunt.marital as "'.__("marital").'"',
    'defunt.lieudeces as "'.__("lieudeces").'"',
    'defunt.exhumation as "'.__("exhumation").'"',
    'defunt.reduction as "'.__("reduction").'"',
    'defunt.verrou as "'.__("verrou").'"',
    'defunt.lieunaissance as "'.__("lieunaissance").'"',
    'defunt.x as "'.__("x").'"',
    'defunt.y as "'.__("y").'"',
    'defunt.id_temp as "'.__("id_temp").'"',
    );
$tri="ORDER BY defunt.nature ASC NULLS LAST";
$edition="defunt";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    "titre_de_civilite" => array("titre_de_civilite", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (defunt.emplacement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (defunt.titre = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'genealogie',
    'operation_defunt',
);

