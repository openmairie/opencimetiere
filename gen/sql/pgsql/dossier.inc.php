<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:57

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("dossier");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON dossier.emplacement=emplacement.emplacement ";
// SELECT 
$champAffiche = array(
    'dossier.dossier as "'.__("dossier").'"',
    'dossier.fichier as "'.__("fichier").'"',
    'to_char(dossier.datedossier ,\'DD/MM/YYYY\') as "'.__("datedossier").'"',
    'dossier.typedossier as "'.__("typedossier").'"',
    );
//
$champNonAffiche = array(
    'dossier.emplacement as "'.__("emplacement").'"',
    'dossier.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'dossier.dossier as "'.__("dossier").'"',
    'dossier.fichier as "'.__("fichier").'"',
    'dossier.typedossier as "'.__("typedossier").'"',
    );
$tri="ORDER BY emplacement.nature ASC NULLS LAST";
$edition="dossier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (dossier.emplacement = ".intval($idxformulaire).") ";
}

