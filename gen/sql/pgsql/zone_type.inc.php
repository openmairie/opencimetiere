<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("zone_type");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."zone_type";
// SELECT 
$champAffiche = array(
    'zone_type.zone_type as "'.__("zone_type").'"',
    'zone_type.code as "'.__("code").'"',
    'zone_type.libelle as "'.__("libelle").'"',
    'zone_type.description as "'.__("description").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(zone_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(zone_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'zone_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'zone_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'zone_type.zone_type as "'.__("zone_type").'"',
    'zone_type.code as "'.__("code").'"',
    'zone_type.libelle as "'.__("libelle").'"',
    'zone_type.description as "'.__("description").'"',
    );
$tri="ORDER BY zone_type.libelle ASC NULLS LAST";
$edition="zone_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((zone_type.om_validite_debut IS NULL AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE)) OR (zone_type.om_validite_debut <= CURRENT_DATE AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((zone_type.om_validite_debut IS NULL AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE)) OR (zone_type.om_validite_debut <= CURRENT_DATE AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'zone',
);

