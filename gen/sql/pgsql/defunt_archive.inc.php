<?php
//$Id$ 
//gen openMairie le 22/01/2020 19:49

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("defunt_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."defunt_archive
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite 
        ON defunt_archive.titre=titre_de_civilite.titre_de_civilite ";
// SELECT 
$champAffiche = array(
    'defunt_archive.defunt as "'.__("defunt").'"',
    'defunt_archive.nature as "'.__("nature").'"',
    'defunt_archive.taille as "'.__("taille").'"',
    'defunt_archive.emplacement as "'.__("emplacement").'"',
    'titre_de_civilite.libelle as "'.__("titre").'"',
    'defunt_archive.nom as "'.__("nom").'"',
    'defunt_archive.prenom as "'.__("prenom").'"',
    'defunt_archive.marital as "'.__("marital").'"',
    'to_char(defunt_archive.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    'to_char(defunt_archive.datedeces ,\'DD/MM/YYYY\') as "'.__("datedeces").'"',
    'defunt_archive.lieudeces as "'.__("lieudeces").'"',
    'to_char(defunt_archive.dateinhumation ,\'DD/MM/YYYY\') as "'.__("dateinhumation").'"',
    'defunt_archive.exhumation as "'.__("exhumation").'"',
    'to_char(defunt_archive.dateexhumation ,\'DD/MM/YYYY\') as "'.__("dateexhumation").'"',
    'defunt_archive.reduction as "'.__("reduction").'"',
    'to_char(defunt_archive.datereduction ,\'DD/MM/YYYY\') as "'.__("datereduction").'"',
    'defunt_archive.verrou as "'.__("verrou").'"',
    'defunt_archive.parente as "'.__("parente").'"',
    'defunt_archive.lieunaissance as "'.__("lieunaissance").'"',
    );
//
$champNonAffiche = array(
    'defunt_archive.observation as "'.__("observation").'"',
    'defunt_archive.historique as "'.__("historique").'"',
    );
//
$champRecherche = array(
    'defunt_archive.defunt as "'.__("defunt").'"',
    'defunt_archive.nature as "'.__("nature").'"',
    'defunt_archive.taille as "'.__("taille").'"',
    'defunt_archive.emplacement as "'.__("emplacement").'"',
    'titre_de_civilite.libelle as "'.__("titre").'"',
    'defunt_archive.nom as "'.__("nom").'"',
    'defunt_archive.prenom as "'.__("prenom").'"',
    'defunt_archive.marital as "'.__("marital").'"',
    'defunt_archive.lieudeces as "'.__("lieudeces").'"',
    'defunt_archive.exhumation as "'.__("exhumation").'"',
    'defunt_archive.reduction as "'.__("reduction").'"',
    'defunt_archive.verrou as "'.__("verrou").'"',
    'defunt_archive.parente as "'.__("parente").'"',
    'defunt_archive.lieunaissance as "'.__("lieunaissance").'"',
    );
$tri="ORDER BY defunt_archive.nature ASC NULLS LAST";
$edition="defunt_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "titre_de_civilite" => array("titre_de_civilite", ),
);
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (defunt_archive.titre = ".intval($idxformulaire).") ";
}

