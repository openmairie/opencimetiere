<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:27

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("tarif");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."tarif
    LEFT JOIN ".DB_PREFIXE."sepulture_type 
        ON tarif.sepulture_type=sepulture_type.sepulture_type ";
// SELECT 
$champAffiche = array(
    'tarif.tarif as "'.__("tarif").'"',
    'tarif.annee as "'.__("annee").'"',
    'tarif.origine as "'.__("origine").'"',
    'tarif.terme as "'.__("terme").'"',
    'tarif.duree as "'.__("duree").'"',
    'tarif.nature as "'.__("nature").'"',
    'sepulture_type.libelle as "'.__("sepulture_type").'"',
    'tarif.montant as "'.__("montant").'"',
    'tarif.monnaie as "'.__("monnaie").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'tarif.tarif as "'.__("tarif").'"',
    'tarif.annee as "'.__("annee").'"',
    'tarif.origine as "'.__("origine").'"',
    'tarif.terme as "'.__("terme").'"',
    'tarif.duree as "'.__("duree").'"',
    'tarif.nature as "'.__("nature").'"',
    'sepulture_type.libelle as "'.__("sepulture_type").'"',
    'tarif.montant as "'.__("montant").'"',
    'tarif.monnaie as "'.__("monnaie").'"',
    );
$tri="ORDER BY tarif.annee ASC NULLS LAST";
$edition="tarif";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "sepulture_type" => array("sepulture_type", ),
);
// Filtre listing sous formulaire - sepulture_type
if (in_array($retourformulaire, $foreign_keys_extended["sepulture_type"])) {
    $selection = " WHERE (tarif.sepulture_type = ".intval($idxformulaire).") ";
}

