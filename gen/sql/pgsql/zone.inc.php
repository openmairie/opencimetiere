<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("localisation")." -> ".__("zone");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."zone
    LEFT JOIN ".DB_PREFIXE."cimetiere 
        ON zone.cimetiere=cimetiere.cimetiere 
    LEFT JOIN ".DB_PREFIXE."zone_type 
        ON zone.zonetype=zone_type.zone_type ";
// SELECT 
$champAffiche = array(
    'zone.zone as "'.__("zone").'"',
    'cimetiere.cimetierelib as "'.__("cimetiere").'"',
    'zone_type.libelle as "'.__("zonetype").'"',
    'zone.zonelib as "'.__("zonelib").'"',
    );
//
$champNonAffiche = array(
    'zone.geom as "'.__("geom").'"',
    );
//
$champRecherche = array(
    'zone.zone as "'.__("zone").'"',
    'cimetiere.cimetierelib as "'.__("cimetiere").'"',
    'zone_type.libelle as "'.__("zonetype").'"',
    'zone.zonelib as "'.__("zonelib").'"',
    );
$tri="ORDER BY cimetiere.cimetierelib ASC NULLS LAST";
$edition="zone";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "cimetiere" => array("cimetiere", ),
    "zone_type" => array("zone_type", ),
);
// Filtre listing sous formulaire - cimetiere
if (in_array($retourformulaire, $foreign_keys_extended["cimetiere"])) {
    $selection = " WHERE (zone.cimetiere = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - zone_type
if (in_array($retourformulaire, $foreign_keys_extended["zone_type"])) {
    $selection = " WHERE (zone.zonetype = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'voie',
);

