<?php
//$Id$ 
//gen openMairie le 12/10/2023 13:25

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("operation_defunt_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."operation_defunt_archive";
// SELECT 
$champAffiche = array(
    'operation_defunt_archive.operation_defunt as "'.__("operation_defunt").'"',
    'operation_defunt_archive.operation as "'.__("operation").'"',
    'operation_defunt_archive.defunt as "'.__("defunt").'"',
    'operation_defunt_archive.defunt_titre as "'.__("defunt_titre").'"',
    'operation_defunt_archive.defunt_nom as "'.__("defunt_nom").'"',
    'operation_defunt_archive.defunt_marital as "'.__("defunt_marital").'"',
    'operation_defunt_archive.defunt_prenom as "'.__("defunt_prenom").'"',
    'to_char(operation_defunt_archive.defunt_datenaissance ,\'DD/MM/YYYY\') as "'.__("defunt_datenaissance").'"',
    'to_char(operation_defunt_archive.defunt_datedeces ,\'DD/MM/YYYY\') as "'.__("defunt_datedeces").'"',
    'operation_defunt_archive.defunt_lieudeces as "'.__("defunt_lieudeces").'"',
    'operation_defunt_archive.defunt_nature as "'.__("defunt_nature").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'operation_defunt_archive.operation_defunt as "'.__("operation_defunt").'"',
    'operation_defunt_archive.operation as "'.__("operation").'"',
    'operation_defunt_archive.defunt as "'.__("defunt").'"',
    'operation_defunt_archive.defunt_titre as "'.__("defunt_titre").'"',
    'operation_defunt_archive.defunt_nom as "'.__("defunt_nom").'"',
    'operation_defunt_archive.defunt_marital as "'.__("defunt_marital").'"',
    'operation_defunt_archive.defunt_prenom as "'.__("defunt_prenom").'"',
    'operation_defunt_archive.defunt_lieudeces as "'.__("defunt_lieudeces").'"',
    'operation_defunt_archive.defunt_nature as "'.__("defunt_nature").'"',
    );
$tri="ORDER BY operation_defunt_archive.operation ASC NULLS LAST";
$edition="operation_defunt_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

