<?php
//$Id$ 
//gen openMairie le 22/01/2020 19:49

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("contrat");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."contrat
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON contrat.emplacement=emplacement.emplacement ";
// SELECT 
$champAffiche = array(
    'contrat.contrat as "'.__("contrat").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'to_char(contrat.datedemande ,\'DD/MM/YYYY\') as "'.__("datedemande").'"',
    'to_char(contrat.datevente ,\'DD/MM/YYYY\') as "'.__("datevente").'"',
    'contrat.origine as "'.__("origine").'"',
    'to_char(contrat.dateterme ,\'DD/MM/YYYY\') as "'.__("dateterme").'"',
    'contrat.terme as "'.__("terme").'"',
    'contrat.duree as "'.__("duree").'"',
    'contrat.montant as "'.__("montant").'"',
    'contrat.monnaie as "'.__("monnaie").'"',
    "case contrat.valide when 't' then 'Oui' else 'Non' end as \"".__("valide")."\"",
    );
//
$champNonAffiche = array(
    'contrat.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'contrat.contrat as "'.__("contrat").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'contrat.origine as "'.__("origine").'"',
    'contrat.terme as "'.__("terme").'"',
    'contrat.duree as "'.__("duree").'"',
    'contrat.montant as "'.__("montant").'"',
    'contrat.monnaie as "'.__("monnaie").'"',
    );
$tri="ORDER BY emplacement.nature ASC NULLS LAST";
$edition="contrat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (contrat.emplacement = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'courrier',
);

