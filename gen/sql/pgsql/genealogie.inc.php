<?php
//$Id$ 
//gen openMairie le 16/10/2023 10:26

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("genealogie");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."genealogie
    LEFT JOIN ".DB_PREFIXE."autorisation as autorisation0 
        ON genealogie.autorisation_p1=autorisation0.autorisation 
    LEFT JOIN ".DB_PREFIXE."autorisation as autorisation1 
        ON genealogie.autorisation_p2=autorisation1.autorisation 
    LEFT JOIN ".DB_PREFIXE."defunt as defunt2 
        ON genealogie.defunt_p1=defunt2.defunt 
    LEFT JOIN ".DB_PREFIXE."defunt as defunt3 
        ON genealogie.defunt_p2=defunt3.defunt 
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON genealogie.emplacement=emplacement.emplacement 
    LEFT JOIN ".DB_PREFIXE."lien_parente 
        ON genealogie.lien_parente=lien_parente.lien_parente ";
// SELECT 
$champAffiche = array(
    'genealogie.genealogie as "'.__("genealogie").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'autorisation0.emplacement as "'.__("autorisation_p1").'"',
    'defunt2.nature as "'.__("defunt_p1").'"',
    'genealogie.personne_1 as "'.__("personne_1").'"',
    'genealogie.personne_2 as "'.__("personne_2").'"',
    'lien_parente.libelle as "'.__("lien_parente").'"',
    'autorisation1.emplacement as "'.__("autorisation_p2").'"',
    'defunt3.nature as "'.__("defunt_p2").'"',
    );
//
$champNonAffiche = array(
    'genealogie.commentaire as "'.__("commentaire").'"',
    );
//
$champRecherche = array(
    'genealogie.genealogie as "'.__("genealogie").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'autorisation0.emplacement as "'.__("autorisation_p1").'"',
    'defunt2.nature as "'.__("defunt_p1").'"',
    'genealogie.personne_1 as "'.__("personne_1").'"',
    'genealogie.personne_2 as "'.__("personne_2").'"',
    'lien_parente.libelle as "'.__("lien_parente").'"',
    'autorisation1.emplacement as "'.__("autorisation_p2").'"',
    'defunt3.nature as "'.__("defunt_p2").'"',
    );
$tri="ORDER BY emplacement.nature ASC NULLS LAST";
$edition="genealogie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "autorisation" => array("autorisation", ),
    "defunt" => array("defunt", ),
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    "lien_parente" => array("lien_parente", ),
);
// Filtre listing sous formulaire - autorisation
if (in_array($retourformulaire, $foreign_keys_extended["autorisation"])) {
    $selection = " WHERE (genealogie.autorisation_p1 = ".intval($idxformulaire)." OR genealogie.autorisation_p2 = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - defunt
if (in_array($retourformulaire, $foreign_keys_extended["defunt"])) {
    $selection = " WHERE (genealogie.defunt_p1 = ".intval($idxformulaire)." OR genealogie.defunt_p2 = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (genealogie.emplacement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - lien_parente
if (in_array($retourformulaire, $foreign_keys_extended["lien_parente"])) {
    $selection = " WHERE (genealogie.lien_parente = ".intval($idxformulaire).") ";
}

