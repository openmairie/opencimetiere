<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:10

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("emplacement_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."emplacement_archive
    LEFT JOIN ".DB_PREFIXE."sepulture_type 
        ON emplacement_archive.sepulturetype=sepulture_type.sepulture_type ";
// SELECT 
$champAffiche = array(
    'emplacement_archive.emplacement as "'.__("emplacement").'"',
    'emplacement_archive.nature as "'.__("nature").'"',
    'emplacement_archive.numero as "'.__("numero").'"',
    'emplacement_archive.complement as "'.__("complement").'"',
    'emplacement_archive.voie as "'.__("voie").'"',
    'emplacement_archive.numerocadastre as "'.__("numerocadastre").'"',
    'emplacement_archive.famille as "'.__("famille").'"',
    'emplacement_archive.numeroacte as "'.__("numeroacte").'"',
    'to_char(emplacement_archive.datevente ,\'DD/MM/YYYY\') as "'.__("datevente").'"',
    'emplacement_archive.terme as "'.__("terme").'"',
    'emplacement_archive.duree as "'.__("duree").'"',
    'to_char(emplacement_archive.dateterme ,\'DD/MM/YYYY\') as "'.__("dateterme").'"',
    'emplacement_archive.nombreplace as "'.__("nombreplace").'"',
    'emplacement_archive.placeoccupe as "'.__("placeoccupe").'"',
    'emplacement_archive.superficie as "'.__("superficie").'"',
    'emplacement_archive.placeconstat as "'.__("placeconstat").'"',
    'to_char(emplacement_archive.dateconstat ,\'DD/MM/YYYY\') as "'.__("dateconstat").'"',
    'emplacement_archive.plans as "'.__("plans").'"',
    'emplacement_archive.positionx as "'.__("positionx").'"',
    'emplacement_archive.positiony as "'.__("positiony").'"',
    'emplacement_archive.photo as "'.__("photo").'"',
    'emplacement_archive.libre as "'.__("libre").'"',
    'sepulture_type.libelle as "'.__("sepulturetype").'"',
    );
//
$champNonAffiche = array(
    'emplacement_archive.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'emplacement_archive.emplacement as "'.__("emplacement").'"',
    'emplacement_archive.nature as "'.__("nature").'"',
    'emplacement_archive.numero as "'.__("numero").'"',
    'emplacement_archive.complement as "'.__("complement").'"',
    'emplacement_archive.voie as "'.__("voie").'"',
    'emplacement_archive.numerocadastre as "'.__("numerocadastre").'"',
    'emplacement_archive.famille as "'.__("famille").'"',
    'emplacement_archive.numeroacte as "'.__("numeroacte").'"',
    'emplacement_archive.terme as "'.__("terme").'"',
    'emplacement_archive.duree as "'.__("duree").'"',
    'emplacement_archive.nombreplace as "'.__("nombreplace").'"',
    'emplacement_archive.placeoccupe as "'.__("placeoccupe").'"',
    'emplacement_archive.superficie as "'.__("superficie").'"',
    'emplacement_archive.placeconstat as "'.__("placeconstat").'"',
    'emplacement_archive.plans as "'.__("plans").'"',
    'emplacement_archive.positionx as "'.__("positionx").'"',
    'emplacement_archive.positiony as "'.__("positiony").'"',
    'emplacement_archive.photo as "'.__("photo").'"',
    'emplacement_archive.libre as "'.__("libre").'"',
    'sepulture_type.libelle as "'.__("sepulturetype").'"',
    );
$tri="ORDER BY emplacement_archive.nature ASC NULLS LAST";
$edition="emplacement_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "sepulture_type" => array("sepulture_type", ),
);
// Filtre listing sous formulaire - sepulture_type
if (in_array($retourformulaire, $foreign_keys_extended["sepulture_type"])) {
    $selection = " WHERE (emplacement_archive.sepulturetype = ".intval($idxformulaire).") ";
}

