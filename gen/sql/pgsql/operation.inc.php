<?php
//$Id$ 
//gen openMairie le 14/09/2023 16:33

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("operation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."operation
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON operation.emplacement=emplacement.emplacement ";
// SELECT 
$champAffiche = array(
    'operation.operation as "'.__("operation").'"',
    'operation.numdossier as "'.__("numdossier").'"',
    'to_char(operation.date ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'operation.heure as "'.__("heure").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'operation.etat as "'.__("etat").'"',
    'operation.categorie as "'.__("categorie").'"',
    'operation.particulier as "'.__("particulier").'"',
    'operation.emplacement_transfert as "'.__("emplacement_transfert").'"',
    'operation.consigne_acces as "'.__("consigne_acces").'"',
    "case operation.prive when 't' then 'Oui' else 'Non' end as \"".__("prive")."\"",
    'operation.edition_operation as "'.__("edition_operation").'"',
    'operation.consigne_acces_transfert as "'.__("consigne_acces_transfert").'"',
    );
//
$champNonAffiche = array(
    'operation.societe_coordonnee as "'.__("societe_coordonnee").'"',
    'operation.pf_coordonnee as "'.__("pf_coordonnee").'"',
    'operation.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'operation.operation as "'.__("operation").'"',
    'operation.numdossier as "'.__("numdossier").'"',
    'emplacement.nature as "'.__("emplacement").'"',
    'operation.etat as "'.__("etat").'"',
    'operation.categorie as "'.__("categorie").'"',
    'operation.particulier as "'.__("particulier").'"',
    'operation.emplacement_transfert as "'.__("emplacement_transfert").'"',
    'operation.consigne_acces as "'.__("consigne_acces").'"',
    'operation.edition_operation as "'.__("edition_operation").'"',
    'operation.consigne_acces_transfert as "'.__("consigne_acces_transfert").'"',
    );
$tri="ORDER BY operation.numdossier ASC NULLS LAST";
$edition="operation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
);
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (operation.emplacement = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'operation_defunt',
);

