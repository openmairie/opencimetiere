<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("entreprise");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."entreprise";
// SELECT 
$champAffiche = array(
    'entreprise.entreprise as "'.__("entreprise").'"',
    'entreprise.nomentreprise as "'.__("nomentreprise").'"',
    'entreprise.pf as "'.__("pf").'"',
    'entreprise.adresse1 as "'.__("adresse1").'"',
    'entreprise.adresse2 as "'.__("adresse2").'"',
    'entreprise.cp as "'.__("cp").'"',
    'entreprise.ville as "'.__("ville").'"',
    'entreprise.telephone as "'.__("telephone").'"',
    'entreprise.courriel as "'.__("courriel").'"',
    'entreprise.numero_habilitation as "'.__("numero_habilitation").'"',
    'to_char(entreprise.date_fin_validite_habilitation ,\'DD/MM/YYYY\') as "'.__("date_fin_validite_habilitation").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'entreprise.entreprise as "'.__("entreprise").'"',
    'entreprise.nomentreprise as "'.__("nomentreprise").'"',
    'entreprise.pf as "'.__("pf").'"',
    'entreprise.adresse1 as "'.__("adresse1").'"',
    'entreprise.adresse2 as "'.__("adresse2").'"',
    'entreprise.cp as "'.__("cp").'"',
    'entreprise.ville as "'.__("ville").'"',
    'entreprise.telephone as "'.__("telephone").'"',
    'entreprise.courriel as "'.__("courriel").'"',
    'entreprise.numero_habilitation as "'.__("numero_habilitation").'"',
    );
$tri="ORDER BY entreprise.nomentreprise ASC NULLS LAST";
$edition="entreprise";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'travaux',
);

