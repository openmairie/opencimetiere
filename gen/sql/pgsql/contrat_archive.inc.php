<?php
//$Id$ 
//gen openMairie le 17/02/2020 15:30

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("contrat_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."contrat_archive";
// SELECT 
$champAffiche = array(
    'contrat_archive.contrat as "'.__("contrat").'"',
    'contrat_archive.emplacement as "'.__("emplacement").'"',
    'to_char(contrat_archive.datedemande ,\'DD/MM/YYYY\') as "'.__("datedemande").'"',
    'to_char(contrat_archive.datevente ,\'DD/MM/YYYY\') as "'.__("datevente").'"',
    'contrat_archive.origine as "'.__("origine").'"',
    'to_char(contrat_archive.dateterme ,\'DD/MM/YYYY\') as "'.__("dateterme").'"',
    'contrat_archive.terme as "'.__("terme").'"',
    'contrat_archive.duree as "'.__("duree").'"',
    'contrat_archive.montant as "'.__("montant").'"',
    'contrat_archive.monnaie as "'.__("monnaie").'"',
    "case contrat_archive.valide when 't' then 'Oui' else 'Non' end as \"".__("valide")."\"",
    );
//
$champNonAffiche = array(
    'contrat_archive.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'contrat_archive.contrat as "'.__("contrat").'"',
    'contrat_archive.emplacement as "'.__("emplacement").'"',
    'contrat_archive.origine as "'.__("origine").'"',
    'contrat_archive.terme as "'.__("terme").'"',
    'contrat_archive.duree as "'.__("duree").'"',
    'contrat_archive.montant as "'.__("montant").'"',
    'contrat_archive.monnaie as "'.__("monnaie").'"',
    );
$tri="ORDER BY contrat_archive.emplacement ASC NULLS LAST";
$edition="contrat_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

