<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("localisation")." -> ".__("plans");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."plans";
// SELECT 
$champAffiche = array(
    'plans.plans as "'.__("plans").'"',
    'plans.planslib as "'.__("planslib").'"',
    );
//
$champNonAffiche = array(
    'plans.fichier as "'.__("fichier").'"',
    );
//
$champRecherche = array(
    'plans.plans as "'.__("plans").'"',
    'plans.planslib as "'.__("planslib").'"',
    );
$tri="ORDER BY plans.planslib ASC NULLS LAST";
$edition="plans";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'emplacement',
);

