<?php
//$Id$ 
//gen openMairie le 23/03/2023 17:30

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("sepulture_type");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."sepulture_type";
// SELECT 
$champAffiche = array(
    'sepulture_type.sepulture_type as "'.__("sepulture_type").'"',
    'sepulture_type.code as "'.__("code").'"',
    'sepulture_type.libelle as "'.__("libelle").'"',
    'sepulture_type.description as "'.__("description").'"',
    'sepulture_type.colonne as "'.__("colonne").'"',
    'sepulture_type.ligne as "'.__("ligne").'"',
    'sepulture_type.libelle_edition as "'.__("libelle_edition").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(sepulture_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(sepulture_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'sepulture_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'sepulture_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'sepulture_type.sepulture_type as "'.__("sepulture_type").'"',
    'sepulture_type.code as "'.__("code").'"',
    'sepulture_type.libelle as "'.__("libelle").'"',
    'sepulture_type.description as "'.__("description").'"',
    'sepulture_type.colonne as "'.__("colonne").'"',
    'sepulture_type.ligne as "'.__("ligne").'"',
    'sepulture_type.libelle_edition as "'.__("libelle_edition").'"',
    );
$tri="ORDER BY sepulture_type.libelle ASC NULLS LAST";
$edition="sepulture_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((sepulture_type.om_validite_debut IS NULL AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)) OR (sepulture_type.om_validite_debut <= CURRENT_DATE AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((sepulture_type.om_validite_debut IS NULL AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)) OR (sepulture_type.om_validite_debut <= CURRENT_DATE AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'emplacement',
    //'emplacement_archive',
    //'tarif',
);

