<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("localisation")." -> ".__("voie");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."voie
    LEFT JOIN ".DB_PREFIXE."voie_type 
        ON voie.voietype=voie_type.voie_type 
    LEFT JOIN ".DB_PREFIXE."zone 
        ON voie.zone=zone.zone ";
// SELECT 
$champAffiche = array(
    'voie.voie as "'.__("voie").'"',
    'zone.cimetiere as "'.__("zone").'"',
    'voie_type.libelle as "'.__("voietype").'"',
    'voie.voielib as "'.__("voielib").'"',
    );
//
$champNonAffiche = array(
    'voie.geom as "'.__("geom").'"',
    );
//
$champRecherche = array(
    'voie.voie as "'.__("voie").'"',
    'zone.cimetiere as "'.__("zone").'"',
    'voie_type.libelle as "'.__("voietype").'"',
    'voie.voielib as "'.__("voielib").'"',
    );
$tri="ORDER BY zone.cimetiere ASC NULLS LAST";
$edition="voie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "voie_type" => array("voie_type", ),
    "zone" => array("zone", ),
);
// Filtre listing sous formulaire - voie_type
if (in_array($retourformulaire, $foreign_keys_extended["voie_type"])) {
    $selection = " WHERE (voie.voietype = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - zone
if (in_array($retourformulaire, $foreign_keys_extended["zone"])) {
    $selection = " WHERE (voie.zone = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'emplacement',
);

