<?php
//$Id$ 
//gen openMairie le 18/02/2020 11:18

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("courrier_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier_archive";
// SELECT 
$champAffiche = array(
    'courrier_archive.courrier as "'.__("courrier").'"',
    'courrier_archive.destinataire as "'.__("destinataire").'"',
    'to_char(courrier_archive.datecourrier ,\'DD/MM/YYYY\') as "'.__("datecourrier").'"',
    'courrier_archive.lettretype as "'.__("lettretype").'"',
    );
//
$champNonAffiche = array(
    'courrier_archive.complement as "'.__("complement").'"',
    'courrier_archive.emplacement as "'.__("emplacement").'"',
    'courrier_archive.contrat as "'.__("contrat").'"',
    );
//
$champRecherche = array(
    'courrier_archive.courrier as "'.__("courrier").'"',
    'courrier_archive.destinataire as "'.__("destinataire").'"',
    'courrier_archive.lettretype as "'.__("lettretype").'"',
    );
$tri="ORDER BY courrier_archive.destinataire ASC NULLS LAST";
$edition="courrier_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

