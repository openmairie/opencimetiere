<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("localisation")." -> ".__("cimetiere");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."cimetiere";
// SELECT 
$champAffiche = array(
    'cimetiere.cimetiere as "'.__("cimetiere").'"',
    'cimetiere.cimetierelib as "'.__("cimetierelib").'"',
    'cimetiere.adresse1 as "'.__("adresse1").'"',
    'cimetiere.adresse2 as "'.__("adresse2").'"',
    'cimetiere.cp as "'.__("cp").'"',
    'cimetiere.ville as "'.__("ville").'"',
    );
//
$champNonAffiche = array(
    'cimetiere.observations as "'.__("observations").'"',
    'cimetiere.geom as "'.__("geom").'"',
    'cimetiere.information_generale as "'.__("information_generale").'"',
    );
//
$champRecherche = array(
    'cimetiere.cimetiere as "'.__("cimetiere").'"',
    'cimetiere.cimetierelib as "'.__("cimetierelib").'"',
    'cimetiere.adresse1 as "'.__("adresse1").'"',
    'cimetiere.adresse2 as "'.__("adresse2").'"',
    'cimetiere.cp as "'.__("cp").'"',
    'cimetiere.ville as "'.__("ville").'"',
    );
$tri="ORDER BY cimetiere.cimetierelib ASC NULLS LAST";
$edition="cimetiere";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'zone',
);

