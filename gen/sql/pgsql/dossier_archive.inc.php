<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:40

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("dossier_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier_archive";
// SELECT 
$champAffiche = array(
    'dossier_archive.dossier as "'.__("dossier").'"',
    'dossier_archive.fichier as "'.__("fichier").'"',
    'to_char(dossier_archive.datedossier ,\'DD/MM/YYYY\') as "'.__("datedossier").'"',
    'dossier_archive.typedossier as "'.__("typedossier").'"',
    );
//
$champNonAffiche = array(
    'dossier_archive.emplacement as "'.__("emplacement").'"',
    'dossier_archive.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'dossier_archive.dossier as "'.__("dossier").'"',
    'dossier_archive.fichier as "'.__("fichier").'"',
    'dossier_archive.typedossier as "'.__("typedossier").'"',
    );
$tri="ORDER BY dossier_archive.emplacement ASC NULLS LAST";
$edition="dossier_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

