<?php
//$Id$ 
//gen openMairie le 22/01/2020 19:49

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("courrier");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier
    LEFT JOIN ".DB_PREFIXE."contrat 
        ON courrier.contrat=contrat.contrat 
    LEFT JOIN ".DB_PREFIXE."autorisation 
        ON courrier.destinataire=autorisation.autorisation 
    LEFT JOIN ".DB_PREFIXE."emplacement 
        ON courrier.emplacement=emplacement.emplacement ";
// SELECT 
$champAffiche = array(
    'courrier.courrier as "'.__("courrier").'"',
    'autorisation.emplacement as "'.__("destinataire").'"',
    'to_char(courrier.datecourrier ,\'DD/MM/YYYY\') as "'.__("datecourrier").'"',
    'courrier.lettretype as "'.__("lettretype").'"',
    );
//
$champNonAffiche = array(
    'courrier.complement as "'.__("complement").'"',
    'courrier.emplacement as "'.__("emplacement").'"',
    'courrier.contrat as "'.__("contrat").'"',
    );
//
$champRecherche = array(
    'courrier.courrier as "'.__("courrier").'"',
    'autorisation.emplacement as "'.__("destinataire").'"',
    'courrier.lettretype as "'.__("lettretype").'"',
    );
$tri="ORDER BY autorisation.emplacement ASC NULLS LAST";
$edition="courrier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contrat" => array("contrat", ),
    "autorisation" => array("autorisation", ),
    "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
);
// Filtre listing sous formulaire - contrat
if (in_array($retourformulaire, $foreign_keys_extended["contrat"])) {
    $selection = " WHERE (courrier.contrat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - autorisation
if (in_array($retourformulaire, $foreign_keys_extended["autorisation"])) {
    $selection = " WHERE (courrier.destinataire = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - emplacement
if (in_array($retourformulaire, $foreign_keys_extended["emplacement"])) {
    $selection = " WHERE (courrier.emplacement = ".intval($idxformulaire).") ";
}

