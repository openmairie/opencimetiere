<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:40

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("travaux_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."travaux_archive
    LEFT JOIN ".DB_PREFIXE."travaux_nature 
        ON travaux_archive.naturetravaux=travaux_nature.travaux_nature ";
// SELECT 
$champAffiche = array(
    'travaux_archive.travaux as "'.__("travaux").'"',
    'travaux_archive.entreprise as "'.__("entreprise").'"',
    'to_char(travaux_archive.datedebinter ,\'DD/MM/YYYY\') as "'.__("datedebinter").'"',
    'to_char(travaux_archive.datefininter ,\'DD/MM/YYYY\') as "'.__("datefininter").'"',
    'travaux_archive.naturedemandeur as "'.__("naturedemandeur").'"',
    'travaux_nature.libelle as "'.__("naturetravaux").'"',
    );
//
$champNonAffiche = array(
    'travaux_archive.emplacement as "'.__("emplacement").'"',
    'travaux_archive.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'travaux_archive.travaux as "'.__("travaux").'"',
    'travaux_archive.entreprise as "'.__("entreprise").'"',
    'travaux_archive.naturedemandeur as "'.__("naturedemandeur").'"',
    'travaux_nature.libelle as "'.__("naturetravaux").'"',
    );
$tri="ORDER BY travaux_archive.entreprise ASC NULLS LAST";
$edition="travaux_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "travaux_nature" => array("travaux_nature", ),
);
// Filtre listing sous formulaire - travaux_nature
if (in_array($retourformulaire, $foreign_keys_extended["travaux_nature"])) {
    $selection = " WHERE (travaux_archive.naturetravaux = ".intval($idxformulaire).") ";
}

