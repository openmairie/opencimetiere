<?php
//$Id$ 
//gen openMairie le 22/01/2020 19:49

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("autorisation_archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."autorisation_archive
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite 
        ON autorisation_archive.titre=titre_de_civilite.titre_de_civilite ";
// SELECT 
$champAffiche = array(
    'autorisation_archive.autorisation as "'.__("autorisation").'"',
    'autorisation_archive.nature as "'.__("nature").'"',
    'autorisation_archive.nom as "'.__("nom").'"',
    'autorisation_archive.marital as "'.__("marital").'"',
    'autorisation_archive.prenom as "'.__("prenom").'"',
    'to_char(autorisation_archive.datenaissance ,\'DD/MM/YYYY\') as "'.__("datenaissance").'"',
    "case autorisation_archive.dcd when 't' then 'Oui' else 'Non' end as \"".__("dcd")."\"",
    );
//
$champNonAffiche = array(
    'autorisation_archive.emplacement as "'.__("emplacement").'"',
    'autorisation_archive.titre as "'.__("titre").'"',
    'autorisation_archive.adresse1 as "'.__("adresse1").'"',
    'autorisation_archive.adresse2 as "'.__("adresse2").'"',
    'autorisation_archive.cp as "'.__("cp").'"',
    'autorisation_archive.ville as "'.__("ville").'"',
    'autorisation_archive.telephone1 as "'.__("telephone1").'"',
    'autorisation_archive.parente as "'.__("parente").'"',
    'autorisation_archive.observation as "'.__("observation").'"',
    'autorisation_archive.telephone2 as "'.__("telephone2").'"',
    'autorisation_archive.courriel as "'.__("courriel").'"',
    );
//
$champRecherche = array(
    'autorisation_archive.autorisation as "'.__("autorisation").'"',
    'autorisation_archive.nature as "'.__("nature").'"',
    'autorisation_archive.nom as "'.__("nom").'"',
    'autorisation_archive.marital as "'.__("marital").'"',
    'autorisation_archive.prenom as "'.__("prenom").'"',
    );
$tri="ORDER BY autorisation_archive.emplacement ASC NULLS LAST";
$edition="autorisation_archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "titre_de_civilite" => array("titre_de_civilite", ),
);
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (autorisation_archive.titre = ".intval($idxformulaire).") ";
}

