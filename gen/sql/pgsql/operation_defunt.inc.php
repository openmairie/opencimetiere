<?php
//$Id$ 
//gen openMairie le 20/02/2020 12:32

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("operation_defunt");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."operation_defunt
    LEFT JOIN ".DB_PREFIXE."defunt 
        ON operation_defunt.defunt=defunt.defunt 
    LEFT JOIN ".DB_PREFIXE."titre_de_civilite 
        ON operation_defunt.defunt_titre=titre_de_civilite.titre_de_civilite 
    LEFT JOIN ".DB_PREFIXE."operation 
        ON operation_defunt.operation=operation.operation ";
// SELECT 
$champAffiche = array(
    'operation_defunt.operation_defunt as "'.__("operation_defunt").'"',
    'operation.numdossier as "'.__("operation").'"',
    'defunt.nature as "'.__("defunt").'"',
    'titre_de_civilite.libelle as "'.__("defunt_titre").'"',
    'operation_defunt.defunt_nom as "'.__("defunt_nom").'"',
    'operation_defunt.defunt_marital as "'.__("defunt_marital").'"',
    'operation_defunt.defunt_prenom as "'.__("defunt_prenom").'"',
    'to_char(operation_defunt.defunt_datenaissance ,\'DD/MM/YYYY\') as "'.__("defunt_datenaissance").'"',
    'to_char(operation_defunt.defunt_datedeces ,\'DD/MM/YYYY\') as "'.__("defunt_datedeces").'"',
    'operation_defunt.defunt_lieudeces as "'.__("defunt_lieudeces").'"',
    'operation_defunt.defunt_nature as "'.__("defunt_nature").'"',
    'operation_defunt.defunt_parente as "'.__("defunt_parente").'"',
    'operation_defunt.defunt_lieunaissance as "'.__("defunt_lieunaissance").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'operation_defunt.operation_defunt as "'.__("operation_defunt").'"',
    'operation.numdossier as "'.__("operation").'"',
    'defunt.nature as "'.__("defunt").'"',
    'titre_de_civilite.libelle as "'.__("defunt_titre").'"',
    'operation_defunt.defunt_nom as "'.__("defunt_nom").'"',
    'operation_defunt.defunt_marital as "'.__("defunt_marital").'"',
    'operation_defunt.defunt_prenom as "'.__("defunt_prenom").'"',
    'operation_defunt.defunt_lieudeces as "'.__("defunt_lieudeces").'"',
    'operation_defunt.defunt_nature as "'.__("defunt_nature").'"',
    'operation_defunt.defunt_parente as "'.__("defunt_parente").'"',
    'operation_defunt.defunt_lieunaissance as "'.__("defunt_lieunaissance").'"',
    );
$tri="ORDER BY operation.numdossier ASC NULLS LAST";
$edition="operation_defunt";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "defunt" => array("defunt", ),
    "titre_de_civilite" => array("titre_de_civilite", ),
    "operation" => array("operation", "inhumation", "inhumation_colombarium", "inhumation_enfeu", "inhumation_terraincommunal", "reduction", "reduction_enfeu", "transfert", ),
);
// Filtre listing sous formulaire - defunt
if (in_array($retourformulaire, $foreign_keys_extended["defunt"])) {
    $selection = " WHERE (operation_defunt.defunt = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - titre_de_civilite
if (in_array($retourformulaire, $foreign_keys_extended["titre_de_civilite"])) {
    $selection = " WHERE (operation_defunt.defunt_titre = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - operation
if (in_array($retourformulaire, $foreign_keys_extended["operation"])) {
    $selection = " WHERE (operation_defunt.operation = ".intval($idxformulaire).") ";
}

