<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:25

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("travaux_nature");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."travaux_nature";
// SELECT 
$champAffiche = array(
    'travaux_nature.travaux_nature as "'.__("travaux_nature").'"',
    'travaux_nature.code as "'.__("code").'"',
    'travaux_nature.libelle as "'.__("libelle").'"',
    'travaux_nature.description as "'.__("description").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(travaux_nature.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(travaux_nature.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'travaux_nature.om_validite_debut as "'.__("om_validite_debut").'"',
    'travaux_nature.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'travaux_nature.travaux_nature as "'.__("travaux_nature").'"',
    'travaux_nature.code as "'.__("code").'"',
    'travaux_nature.libelle as "'.__("libelle").'"',
    'travaux_nature.description as "'.__("description").'"',
    );
$tri="ORDER BY travaux_nature.libelle ASC NULLS LAST";
$edition="travaux_nature";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((travaux_nature.om_validite_debut IS NULL AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE)) OR (travaux_nature.om_validite_debut <= CURRENT_DATE AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((travaux_nature.om_validite_debut IS NULL AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE)) OR (travaux_nature.om_validite_debut <= CURRENT_DATE AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'travaux',
    //'travaux_archive',
);

