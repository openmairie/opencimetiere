<?php
//$Id$ 
//gen openMairie le 30/12/2022 11:27

$DEBUG=0;
$serie=15;
$ent = __("administration & paramétrage")." -> ".__("divers")." -> ".__("lien_parente");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_parente";
// SELECT 
$champAffiche = array(
    'lien_parente.lien_parente as "'.__("lien_parente").'"',
    'lien_parente.libelle as "'.__("libelle").'"',
    'lien_parente.niveau as "'.__("niveau").'"',
    "case lien_parente.meme_personne when 't' then 'Oui' else 'Non' end as \"".__("meme_personne")."\"",
    'lien_parente.lien_inverse as "'.__("lien_inverse").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_parente.lien_parente as "'.__("lien_parente").'"',
    'lien_parente.libelle as "'.__("libelle").'"',
    'lien_parente.niveau as "'.__("niveau").'"',
    'lien_parente.lien_inverse as "'.__("lien_inverse").'"',
    );
$tri="ORDER BY lien_parente.libelle ASC NULLS LAST";
$edition="lien_parente";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'genealogie',
);

