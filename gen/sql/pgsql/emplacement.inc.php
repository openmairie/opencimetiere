<?php
//$Id$ 
//gen openMairie le 24/01/2023 16:18

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("emplacement");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."emplacement
    LEFT JOIN ".DB_PREFIXE."plans 
        ON emplacement.plans=plans.plans 
    LEFT JOIN ".DB_PREFIXE."sepulture_type 
        ON emplacement.sepulturetype=sepulture_type.sepulture_type 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON emplacement.voie=voie.voie ";
// SELECT 
$champAffiche = array(
    'emplacement.emplacement as "'.__("emplacement").'"',
    'emplacement.nature as "'.__("nature").'"',
    'emplacement.numero as "'.__("numero").'"',
    'emplacement.complement as "'.__("complement").'"',
    'voie.zone as "'.__("voie").'"',
    'emplacement.numerocadastre as "'.__("numerocadastre").'"',
    'emplacement.famille as "'.__("famille").'"',
    'emplacement.numeroacte as "'.__("numeroacte").'"',
    'to_char(emplacement.datevente ,\'DD/MM/YYYY\') as "'.__("datevente").'"',
    'emplacement.terme as "'.__("terme").'"',
    'emplacement.duree as "'.__("duree").'"',
    'to_char(emplacement.dateterme ,\'DD/MM/YYYY\') as "'.__("dateterme").'"',
    'emplacement.nombreplace as "'.__("nombreplace").'"',
    'emplacement.placeoccupe as "'.__("placeoccupe").'"',
    'emplacement.superficie as "'.__("superficie").'"',
    'emplacement.placeconstat as "'.__("placeconstat").'"',
    'to_char(emplacement.dateconstat ,\'DD/MM/YYYY\') as "'.__("dateconstat").'"',
    'plans.planslib as "'.__("plans").'"',
    'emplacement.positionx as "'.__("positionx").'"',
    'emplacement.positiony as "'.__("positiony").'"',
    'emplacement.photo as "'.__("photo").'"',
    'emplacement.libre as "'.__("libre").'"',
    'sepulture_type.libelle as "'.__("sepulturetype").'"',
    'to_char(emplacement.daterenouvellement ,\'DD/MM/YYYY\') as "'.__("daterenouvellement").'"',
    'emplacement.typeconcession as "'.__("typeconcession").'"',
    'emplacement.temp1 as "'.__("temp1").'"',
    'emplacement.temp2 as "'.__("temp2").'"',
    'emplacement.temp3 as "'.__("temp3").'"',
    'emplacement.temp4 as "'.__("temp4").'"',
    'emplacement.temp5 as "'.__("temp5").'"',
    'emplacement.videsanitaire as "'.__("videsanitaire").'"',
    'emplacement.semelle as "'.__("semelle").'"',
    'emplacement.etatsemelle as "'.__("etatsemelle").'"',
    'emplacement.monument as "'.__("monument").'"',
    'emplacement.etatmonument as "'.__("etatmonument").'"',
    'emplacement.largeur as "'.__("largeur").'"',
    'emplacement.profondeur as "'.__("profondeur").'"',
    'emplacement.abandon as "'.__("abandon").'"',
    'to_char(emplacement.date_abandon ,\'DD/MM/YYYY\') as "'.__("date_abandon").'"',
    'to_char(emplacement.dateacte ,\'DD/MM/YYYY\') as "'.__("dateacte").'"',
    'emplacement.geom as "'.__("geom").'"',
    'emplacement.pgeom as "'.__("pgeom").'"',
    'emplacement.etat_case_plan_en_coupe as "'.__("etat_case_plan_en_coupe").'"',
    );
//
$champNonAffiche = array(
    'emplacement.observation as "'.__("observation").'"',
    );
//
$champRecherche = array(
    'emplacement.emplacement as "'.__("emplacement").'"',
    'emplacement.nature as "'.__("nature").'"',
    'emplacement.numero as "'.__("numero").'"',
    'emplacement.complement as "'.__("complement").'"',
    'voie.zone as "'.__("voie").'"',
    'emplacement.numerocadastre as "'.__("numerocadastre").'"',
    'emplacement.famille as "'.__("famille").'"',
    'emplacement.numeroacte as "'.__("numeroacte").'"',
    'emplacement.terme as "'.__("terme").'"',
    'emplacement.duree as "'.__("duree").'"',
    'emplacement.nombreplace as "'.__("nombreplace").'"',
    'emplacement.placeoccupe as "'.__("placeoccupe").'"',
    'emplacement.superficie as "'.__("superficie").'"',
    'emplacement.placeconstat as "'.__("placeconstat").'"',
    'plans.planslib as "'.__("plans").'"',
    'emplacement.positionx as "'.__("positionx").'"',
    'emplacement.positiony as "'.__("positiony").'"',
    'emplacement.photo as "'.__("photo").'"',
    'emplacement.libre as "'.__("libre").'"',
    'sepulture_type.libelle as "'.__("sepulturetype").'"',
    'emplacement.typeconcession as "'.__("typeconcession").'"',
    'emplacement.temp1 as "'.__("temp1").'"',
    'emplacement.temp2 as "'.__("temp2").'"',
    'emplacement.temp3 as "'.__("temp3").'"',
    'emplacement.temp4 as "'.__("temp4").'"',
    'emplacement.temp5 as "'.__("temp5").'"',
    'emplacement.videsanitaire as "'.__("videsanitaire").'"',
    'emplacement.semelle as "'.__("semelle").'"',
    'emplacement.etatsemelle as "'.__("etatsemelle").'"',
    'emplacement.monument as "'.__("monument").'"',
    'emplacement.etatmonument as "'.__("etatmonument").'"',
    'emplacement.largeur as "'.__("largeur").'"',
    'emplacement.profondeur as "'.__("profondeur").'"',
    'emplacement.abandon as "'.__("abandon").'"',
    'emplacement.etat_case_plan_en_coupe as "'.__("etat_case_plan_en_coupe").'"',
    );
$tri="ORDER BY emplacement.nature ASC NULLS LAST";
$edition="emplacement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "plans" => array("plans", ),
    "sepulture_type" => array("sepulture_type", ),
    "voie" => array("voie", ),
);
// Filtre listing sous formulaire - plans
if (in_array($retourformulaire, $foreign_keys_extended["plans"])) {
    $selection = " WHERE (emplacement.plans = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - sepulture_type
if (in_array($retourformulaire, $foreign_keys_extended["sepulture_type"])) {
    $selection = " WHERE (emplacement.sepulturetype = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - voie
if (in_array($retourformulaire, $foreign_keys_extended["voie"])) {
    $selection = " WHERE (emplacement.voie = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'autorisation',
    'contrat',
    'courrier',
    'defunt',
    'dossier',
    'genealogie',
    'operation',
    'travaux',
);

