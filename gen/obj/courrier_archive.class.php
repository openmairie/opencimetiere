<?php
//$Id$ 
//gen openMairie le 18/02/2020 11:18

require_once "../obj/om_dbform.class.php";

class courrier_archive_gen extends om_dbform {

    protected $_absolute_class_name = "courrier_archive";

    var $table = "courrier_archive";
    var $clePrimaire = "courrier";
    var $typeCle = "N";
    var $required_field = array(
        "courrier"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("destinataire");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "courrier",
            "destinataire",
            "datecourrier",
            "lettretype",
            "complement",
            "emplacement",
            "contrat",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = ""; // -> requis
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
        if (!is_numeric($val['destinataire'])) {
            $this->valF['destinataire'] = NULL;
        } else {
            $this->valF['destinataire'] = $val['destinataire'];
        }
        if ($val['datecourrier'] != "") {
            $this->valF['datecourrier'] = $this->dateDB($val['datecourrier']);
        } else {
            $this->valF['datecourrier'] = NULL;
        }
        if ($val['lettretype'] == "") {
            $this->valF['lettretype'] = NULL;
        } else {
            $this->valF['lettretype'] = $val['lettretype'];
        }
            $this->valF['complement'] = $val['complement'];
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if (!is_numeric($val['contrat'])) {
            $this->valF['contrat'] = NULL;
        } else {
            $this->valF['contrat'] = $val['contrat'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("courrier", "hidden");
            $form->setType("destinataire", "text");
            $form->setType("datecourrier", "date");
            $form->setType("lettretype", "text");
            $form->setType("complement", "textarea");
            $form->setType("emplacement", "text");
            $form->setType("contrat", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("destinataire", "text");
            $form->setType("datecourrier", "date");
            $form->setType("lettretype", "text");
            $form->setType("complement", "textarea");
            $form->setType("emplacement", "text");
            $form->setType("contrat", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("destinataire", "hiddenstatic");
            $form->setType("datecourrier", "hiddenstatic");
            $form->setType("lettretype", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("contrat", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("courrier", "static");
            $form->setType("destinataire", "static");
            $form->setType("datecourrier", "datestatic");
            $form->setType("lettretype", "static");
            $form->setType("complement", "textareastatic");
            $form->setType("emplacement", "static");
            $form->setType("contrat", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('courrier','VerifNum(this)');
        $form->setOnchange('destinataire','VerifNum(this)');
        $form->setOnchange('datecourrier','fdate(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('contrat','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("courrier", 11);
        $form->setTaille("destinataire", 11);
        $form->setTaille("datecourrier", 12);
        $form->setTaille("lettretype", 30);
        $form->setTaille("complement", 80);
        $form->setTaille("emplacement", 11);
        $form->setTaille("contrat", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("courrier", 11);
        $form->setMax("destinataire", 11);
        $form->setMax("datecourrier", 12);
        $form->setMax("lettretype", 40);
        $form->setMax("complement", 6);
        $form->setMax("emplacement", 11);
        $form->setMax("contrat", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('courrier', __('courrier'));
        $form->setLib('destinataire', __('destinataire'));
        $form->setLib('datecourrier', __('datecourrier'));
        $form->setLib('lettretype', __('lettretype'));
        $form->setLib('complement', __('complement'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('contrat', __('contrat'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
