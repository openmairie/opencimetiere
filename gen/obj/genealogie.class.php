<?php
//$Id$ 
//gen openMairie le 16/10/2023 10:26

require_once "../obj/om_dbform.class.php";

class genealogie_gen extends om_dbform {

    protected $_absolute_class_name = "genealogie";

    var $table = "genealogie";
    var $clePrimaire = "genealogie";
    var $typeCle = "N";
    var $required_field = array(
        "emplacement",
        "genealogie",
        "lien_parente"
    );
    
    var $foreign_keys_extended = array(
        "autorisation" => array("autorisation", ),
        "defunt" => array("defunt", ),
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
        "lien_parente" => array("lien_parente", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("emplacement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "genealogie",
            "emplacement",
            "autorisation_p1",
            "defunt_p1",
            "personne_1",
            "personne_2",
            "lien_parente",
            "autorisation_p2",
            "defunt_p2",
            "commentaire",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p1() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation ORDER BY autorisation.emplacement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p1_by_id() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation WHERE autorisation = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p2() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation ORDER BY autorisation.emplacement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorisation_p2_by_id() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation WHERE autorisation = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p1() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt ORDER BY defunt.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p1_by_id() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt WHERE defunt = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p2() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt ORDER BY defunt.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_p2_by_id() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt WHERE defunt = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_lien_parente() {
        return "SELECT lien_parente.lien_parente, lien_parente.libelle FROM ".DB_PREFIXE."lien_parente ORDER BY lien_parente.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_lien_parente_by_id() {
        return "SELECT lien_parente.lien_parente, lien_parente.libelle FROM ".DB_PREFIXE."lien_parente WHERE lien_parente = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['genealogie'])) {
            $this->valF['genealogie'] = ""; // -> requis
        } else {
            $this->valF['genealogie'] = $val['genealogie'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = ""; // -> requis
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if (!is_numeric($val['autorisation_p1'])) {
            $this->valF['autorisation_p1'] = NULL;
        } else {
            $this->valF['autorisation_p1'] = $val['autorisation_p1'];
        }
        if (!is_numeric($val['defunt_p1'])) {
            $this->valF['defunt_p1'] = NULL;
        } else {
            $this->valF['defunt_p1'] = $val['defunt_p1'];
        }
        if (!is_numeric($val['personne_1'])) {
            $this->valF['personne_1'] = NULL;
        } else {
            $this->valF['personne_1'] = $val['personne_1'];
        }
        if (!is_numeric($val['personne_2'])) {
            $this->valF['personne_2'] = NULL;
        } else {
            $this->valF['personne_2'] = $val['personne_2'];
        }
        if (!is_numeric($val['lien_parente'])) {
            $this->valF['lien_parente'] = ""; // -> requis
        } else {
            $this->valF['lien_parente'] = $val['lien_parente'];
        }
        if (!is_numeric($val['autorisation_p2'])) {
            $this->valF['autorisation_p2'] = NULL;
        } else {
            $this->valF['autorisation_p2'] = $val['autorisation_p2'];
        }
        if (!is_numeric($val['defunt_p2'])) {
            $this->valF['defunt_p2'] = NULL;
        } else {
            $this->valF['defunt_p2'] = $val['defunt_p2'];
        }
            $this->valF['commentaire'] = $val['commentaire'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("genealogie", "hidden");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("autorisation_p1", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_p1", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt_p1", "selecthiddenstatic");
            } else {
                $form->setType("defunt_p1", "select");
            }
            $form->setType("personne_1", "text");
            $form->setType("personne_2", "text");
            if ($this->is_in_context_of_foreign_key("lien_parente", $this->retourformulaire)) {
                $form->setType("lien_parente", "selecthiddenstatic");
            } else {
                $form->setType("lien_parente", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("autorisation_p2", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_p2", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt_p2", "selecthiddenstatic");
            } else {
                $form->setType("defunt_p2", "select");
            }
            $form->setType("commentaire", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("genealogie", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("autorisation_p1", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_p1", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt_p1", "selecthiddenstatic");
            } else {
                $form->setType("defunt_p1", "select");
            }
            $form->setType("personne_1", "text");
            $form->setType("personne_2", "text");
            if ($this->is_in_context_of_foreign_key("lien_parente", $this->retourformulaire)) {
                $form->setType("lien_parente", "selecthiddenstatic");
            } else {
                $form->setType("lien_parente", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("autorisation_p2", "selecthiddenstatic");
            } else {
                $form->setType("autorisation_p2", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt_p2", "selecthiddenstatic");
            } else {
                $form->setType("defunt_p2", "select");
            }
            $form->setType("commentaire", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("genealogie", "hiddenstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("autorisation_p1", "selectstatic");
            $form->setType("defunt_p1", "selectstatic");
            $form->setType("personne_1", "hiddenstatic");
            $form->setType("personne_2", "hiddenstatic");
            $form->setType("lien_parente", "selectstatic");
            $form->setType("autorisation_p2", "selectstatic");
            $form->setType("defunt_p2", "selectstatic");
            $form->setType("commentaire", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("genealogie", "static");
            $form->setType("emplacement", "selectstatic");
            $form->setType("autorisation_p1", "selectstatic");
            $form->setType("defunt_p1", "selectstatic");
            $form->setType("personne_1", "static");
            $form->setType("personne_2", "static");
            $form->setType("lien_parente", "selectstatic");
            $form->setType("autorisation_p2", "selectstatic");
            $form->setType("defunt_p2", "selectstatic");
            $form->setType("commentaire", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('genealogie','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('autorisation_p1','VerifNum(this)');
        $form->setOnchange('defunt_p1','VerifNum(this)');
        $form->setOnchange('personne_1','VerifNum(this)');
        $form->setOnchange('personne_2','VerifNum(this)');
        $form->setOnchange('lien_parente','VerifNum(this)');
        $form->setOnchange('autorisation_p2','VerifNum(this)');
        $form->setOnchange('defunt_p2','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("genealogie", 11);
        $form->setTaille("emplacement", 11);
        $form->setTaille("autorisation_p1", 11);
        $form->setTaille("defunt_p1", 11);
        $form->setTaille("personne_1", 11);
        $form->setTaille("personne_2", 11);
        $form->setTaille("lien_parente", 11);
        $form->setTaille("autorisation_p2", 11);
        $form->setTaille("defunt_p2", 11);
        $form->setTaille("commentaire", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("genealogie", 11);
        $form->setMax("emplacement", 11);
        $form->setMax("autorisation_p1", 11);
        $form->setMax("defunt_p1", 11);
        $form->setMax("personne_1", 11);
        $form->setMax("personne_2", 11);
        $form->setMax("lien_parente", 11);
        $form->setMax("autorisation_p2", 11);
        $form->setMax("defunt_p2", 11);
        $form->setMax("commentaire", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('genealogie', __('genealogie'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('autorisation_p1', __('autorisation_p1'));
        $form->setLib('defunt_p1', __('defunt_p1'));
        $form->setLib('personne_1', __('personne_1'));
        $form->setLib('personne_2', __('personne_2'));
        $form->setLib('lien_parente', __('lien_parente'));
        $form->setLib('autorisation_p2', __('autorisation_p2'));
        $form->setLib('defunt_p2', __('defunt_p2'));
        $form->setLib('commentaire', __('commentaire'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // autorisation_p1
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "autorisation_p1",
            $this->get_var_sql_forminc__sql("autorisation_p1"),
            $this->get_var_sql_forminc__sql("autorisation_p1_by_id"),
            false
        );
        // autorisation_p2
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "autorisation_p2",
            $this->get_var_sql_forminc__sql("autorisation_p2"),
            $this->get_var_sql_forminc__sql("autorisation_p2_by_id"),
            false
        );
        // defunt_p1
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "defunt_p1",
            $this->get_var_sql_forminc__sql("defunt_p1"),
            $this->get_var_sql_forminc__sql("defunt_p1_by_id"),
            false
        );
        // defunt_p2
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "defunt_p2",
            $this->get_var_sql_forminc__sql("defunt_p2"),
            $this->get_var_sql_forminc__sql("defunt_p2_by_id"),
            false
        );
        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
        // lien_parente
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "lien_parente",
            $this->get_var_sql_forminc__sql("lien_parente"),
            $this->get_var_sql_forminc__sql("lien_parente_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('lien_parente', $this->retourformulaire))
                $form->setVal('lien_parente', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('autorisation', $this->retourformulaire))
                $form->setVal('autorisation_p1', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorisation', $this->retourformulaire))
                $form->setVal('autorisation_p2', $idxformulaire);
            if($this->is_in_context_of_foreign_key('defunt', $this->retourformulaire))
                $form->setVal('defunt_p1', $idxformulaire);
            if($this->is_in_context_of_foreign_key('defunt', $this->retourformulaire))
                $form->setVal('defunt_p2', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
