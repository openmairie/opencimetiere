<?php
//$Id$ 
//gen openMairie le 16/11/2022 16:56

require_once "../obj/om_dbform.class.php";

class entreprise_gen extends om_dbform {

    protected $_absolute_class_name = "entreprise";

    var $table = "entreprise";
    var $clePrimaire = "entreprise";
    var $typeCle = "N";
    var $required_field = array(
        "entreprise",
        "nomentreprise"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nomentreprise");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "entreprise",
            "nomentreprise",
            "pf",
            "adresse1",
            "adresse2",
            "cp",
            "ville",
            "telephone",
            "courriel",
            "numero_habilitation",
            "date_fin_validite_habilitation",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['entreprise'])) {
            $this->valF['entreprise'] = ""; // -> requis
        } else {
            $this->valF['entreprise'] = $val['entreprise'];
        }
        $this->valF['nomentreprise'] = $val['nomentreprise'];
        if ($val['pf'] == "") {
            $this->valF['pf'] = ""; // -> default
        } else {
            $this->valF['pf'] = $val['pf'];
        }
        if ($val['adresse1'] == "") {
            $this->valF['adresse1'] = NULL;
        } else {
            $this->valF['adresse1'] = $val['adresse1'];
        }
        if ($val['adresse2'] == "") {
            $this->valF['adresse2'] = NULL;
        } else {
            $this->valF['adresse2'] = $val['adresse2'];
        }
        if ($val['cp'] == "") {
            $this->valF['cp'] = NULL;
        } else {
            $this->valF['cp'] = $val['cp'];
        }
        if ($val['ville'] == "") {
            $this->valF['ville'] = NULL;
        } else {
            $this->valF['ville'] = $val['ville'];
        }
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['courriel'] == "") {
            $this->valF['courriel'] = NULL;
        } else {
            $this->valF['courriel'] = $val['courriel'];
        }
        if ($val['numero_habilitation'] == "") {
            $this->valF['numero_habilitation'] = NULL;
        } else {
            $this->valF['numero_habilitation'] = $val['numero_habilitation'];
        }
        if ($val['date_fin_validite_habilitation'] != "") {
            $this->valF['date_fin_validite_habilitation'] = $this->dateDB($val['date_fin_validite_habilitation']);
        } else {
            $this->valF['date_fin_validite_habilitation'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("entreprise", "hidden");
            $form->setType("nomentreprise", "text");
            $form->setType("pf", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("numero_habilitation", "text");
            $form->setType("date_fin_validite_habilitation", "date");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("entreprise", "hiddenstatic");
            $form->setType("nomentreprise", "text");
            $form->setType("pf", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("numero_habilitation", "text");
            $form->setType("date_fin_validite_habilitation", "date");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("entreprise", "hiddenstatic");
            $form->setType("nomentreprise", "hiddenstatic");
            $form->setType("pf", "hiddenstatic");
            $form->setType("adresse1", "hiddenstatic");
            $form->setType("adresse2", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            $form->setType("numero_habilitation", "hiddenstatic");
            $form->setType("date_fin_validite_habilitation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("entreprise", "static");
            $form->setType("nomentreprise", "static");
            $form->setType("pf", "static");
            $form->setType("adresse1", "static");
            $form->setType("adresse2", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
            $form->setType("telephone", "static");
            $form->setType("courriel", "static");
            $form->setType("numero_habilitation", "static");
            $form->setType("date_fin_validite_habilitation", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('entreprise','VerifNum(this)');
        $form->setOnchange('date_fin_validite_habilitation','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("entreprise", 11);
        $form->setTaille("nomentreprise", 30);
        $form->setTaille("pf", 10);
        $form->setTaille("adresse1", 30);
        $form->setTaille("adresse2", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
        $form->setTaille("telephone", 30);
        $form->setTaille("courriel", 30);
        $form->setTaille("numero_habilitation", 30);
        $form->setTaille("date_fin_validite_habilitation", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("entreprise", 11);
        $form->setMax("nomentreprise", 60);
        $form->setMax("pf", 3);
        $form->setMax("adresse1", 40);
        $form->setMax("adresse2", 40);
        $form->setMax("cp", 5);
        $form->setMax("ville", 40);
        $form->setMax("telephone", 40);
        $form->setMax("courriel", 100);
        $form->setMax("numero_habilitation", 40);
        $form->setMax("date_fin_validite_habilitation", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('entreprise', __('entreprise'));
        $form->setLib('nomentreprise', __('nomentreprise'));
        $form->setLib('pf', __('pf'));
        $form->setLib('adresse1', __('adresse1'));
        $form->setLib('adresse2', __('adresse2'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('courriel', __('courriel'));
        $form->setLib('numero_habilitation', __('numero_habilitation'));
        $form->setLib('date_fin_validite_habilitation', __('date_fin_validite_habilitation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : travaux
        $this->rechercheTable($this->f->db, "travaux", "entreprise", $id);
    }


}
