<?php
//$Id$ 
//gen openMairie le 11/04/2023 03:12

require_once "../obj/om_dbform.class.php";

class operation_defunt_gen extends om_dbform {

    protected $_absolute_class_name = "operation_defunt";

    var $table = "operation_defunt";
    var $clePrimaire = "operation_defunt";
    var $typeCle = "N";
    var $required_field = array(
        "operation_defunt"
    );
    
    var $foreign_keys_extended = array(
        "defunt" => array("defunt", ),
        "titre_de_civilite" => array("titre_de_civilite", ),
        "operation" => array("operation", "inhumation", "inhumation_colombarium", "inhumation_enfeu", "inhumation_terraincommunal", "reduction", "reduction_enfeu", "transfert", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("operation");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "operation_defunt",
            "operation",
            "defunt",
            "defunt_titre",
            "defunt_nom",
            "defunt_marital",
            "defunt_prenom",
            "defunt_datenaissance",
            "defunt_datedeces",
            "defunt_lieudeces",
            "defunt_nature",
            "defunt_parente",
            "defunt_lieunaissance",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt ORDER BY defunt.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_by_id() {
        return "SELECT defunt.defunt, defunt.nature FROM ".DB_PREFIXE."defunt WHERE defunt = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_titre() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE ((titre_de_civilite.om_validite_debut IS NULL AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE)) OR (titre_de_civilite.om_validite_debut <= CURRENT_DATE AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE))) ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_defunt_titre_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_operation() {
        return "SELECT operation.operation, operation.numdossier FROM ".DB_PREFIXE."operation ORDER BY operation.numdossier ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_operation_by_id() {
        return "SELECT operation.operation, operation.numdossier FROM ".DB_PREFIXE."operation WHERE operation = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['operation_defunt'])) {
            $this->valF['operation_defunt'] = ""; // -> requis
        } else {
            $this->valF['operation_defunt'] = $val['operation_defunt'];
        }
        if (!is_numeric($val['operation'])) {
            $this->valF['operation'] = NULL;
        } else {
            $this->valF['operation'] = $val['operation'];
        }
        if (!is_numeric($val['defunt'])) {
            $this->valF['defunt'] = NULL;
        } else {
            $this->valF['defunt'] = $val['defunt'];
        }
        if (!is_numeric($val['defunt_titre'])) {
            $this->valF['defunt_titre'] = NULL;
        } else {
            $this->valF['defunt_titre'] = $val['defunt_titre'];
        }
        if ($val['defunt_nom'] == "") {
            $this->valF['defunt_nom'] = NULL;
        } else {
            $this->valF['defunt_nom'] = $val['defunt_nom'];
        }
        if ($val['defunt_marital'] == "") {
            $this->valF['defunt_marital'] = NULL;
        } else {
            $this->valF['defunt_marital'] = $val['defunt_marital'];
        }
        if ($val['defunt_prenom'] == "") {
            $this->valF['defunt_prenom'] = NULL;
        } else {
            $this->valF['defunt_prenom'] = $val['defunt_prenom'];
        }
        if ($val['defunt_datenaissance'] != "") {
            $this->valF['defunt_datenaissance'] = $this->dateDB($val['defunt_datenaissance']);
        } else {
            $this->valF['defunt_datenaissance'] = NULL;
        }
        if ($val['defunt_datedeces'] != "") {
            $this->valF['defunt_datedeces'] = $this->dateDB($val['defunt_datedeces']);
        } else {
            $this->valF['defunt_datedeces'] = NULL;
        }
        if ($val['defunt_lieudeces'] == "") {
            $this->valF['defunt_lieudeces'] = NULL;
        } else {
            $this->valF['defunt_lieudeces'] = $val['defunt_lieudeces'];
        }
        if ($val['defunt_nature'] == "") {
            $this->valF['defunt_nature'] = NULL;
        } else {
            $this->valF['defunt_nature'] = $val['defunt_nature'];
        }
        if ($val['defunt_parente'] == "") {
            $this->valF['defunt_parente'] = NULL;
        } else {
            $this->valF['defunt_parente'] = $val['defunt_parente'];
        }
        if ($val['defunt_lieunaissance'] == "") {
            $this->valF['defunt_lieunaissance'] = NULL;
        } else {
            $this->valF['defunt_lieunaissance'] = $val['defunt_lieunaissance'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("operation_defunt", "hidden");
            if ($this->is_in_context_of_foreign_key("operation", $this->retourformulaire)) {
                $form->setType("operation", "selecthiddenstatic");
            } else {
                $form->setType("operation", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt", "selecthiddenstatic");
            } else {
                $form->setType("defunt", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("defunt_titre", "selecthiddenstatic");
            } else {
                $form->setType("defunt_titre", "select");
            }
            $form->setType("defunt_nom", "text");
            $form->setType("defunt_marital", "text");
            $form->setType("defunt_prenom", "text");
            $form->setType("defunt_datenaissance", "date");
            $form->setType("defunt_datedeces", "date");
            $form->setType("defunt_lieudeces", "text");
            $form->setType("defunt_nature", "text");
            $form->setType("defunt_parente", "text");
            $form->setType("defunt_lieunaissance", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("operation_defunt", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("operation", $this->retourformulaire)) {
                $form->setType("operation", "selecthiddenstatic");
            } else {
                $form->setType("operation", "select");
            }
            if ($this->is_in_context_of_foreign_key("defunt", $this->retourformulaire)) {
                $form->setType("defunt", "selecthiddenstatic");
            } else {
                $form->setType("defunt", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("defunt_titre", "selecthiddenstatic");
            } else {
                $form->setType("defunt_titre", "select");
            }
            $form->setType("defunt_nom", "text");
            $form->setType("defunt_marital", "text");
            $form->setType("defunt_prenom", "text");
            $form->setType("defunt_datenaissance", "date");
            $form->setType("defunt_datedeces", "date");
            $form->setType("defunt_lieudeces", "text");
            $form->setType("defunt_nature", "text");
            $form->setType("defunt_parente", "text");
            $form->setType("defunt_lieunaissance", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("operation_defunt", "hiddenstatic");
            $form->setType("operation", "selectstatic");
            $form->setType("defunt", "selectstatic");
            $form->setType("defunt_titre", "selectstatic");
            $form->setType("defunt_nom", "hiddenstatic");
            $form->setType("defunt_marital", "hiddenstatic");
            $form->setType("defunt_prenom", "hiddenstatic");
            $form->setType("defunt_datenaissance", "hiddenstatic");
            $form->setType("defunt_datedeces", "hiddenstatic");
            $form->setType("defunt_lieudeces", "hiddenstatic");
            $form->setType("defunt_nature", "hiddenstatic");
            $form->setType("defunt_parente", "hiddenstatic");
            $form->setType("defunt_lieunaissance", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("operation_defunt", "static");
            $form->setType("operation", "selectstatic");
            $form->setType("defunt", "selectstatic");
            $form->setType("defunt_titre", "selectstatic");
            $form->setType("defunt_nom", "static");
            $form->setType("defunt_marital", "static");
            $form->setType("defunt_prenom", "static");
            $form->setType("defunt_datenaissance", "datestatic");
            $form->setType("defunt_datedeces", "datestatic");
            $form->setType("defunt_lieudeces", "static");
            $form->setType("defunt_nature", "static");
            $form->setType("defunt_parente", "static");
            $form->setType("defunt_lieunaissance", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('operation_defunt','VerifNum(this)');
        $form->setOnchange('operation','VerifNum(this)');
        $form->setOnchange('defunt','VerifNum(this)');
        $form->setOnchange('defunt_titre','VerifNum(this)');
        $form->setOnchange('defunt_datenaissance','fdate(this)');
        $form->setOnchange('defunt_datedeces','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("operation_defunt", 11);
        $form->setTaille("operation", 11);
        $form->setTaille("defunt", 11);
        $form->setTaille("defunt_titre", 11);
        $form->setTaille("defunt_nom", 30);
        $form->setTaille("defunt_marital", 30);
        $form->setTaille("defunt_prenom", 30);
        $form->setTaille("defunt_datenaissance", 12);
        $form->setTaille("defunt_datedeces", 12);
        $form->setTaille("defunt_lieudeces", 30);
        $form->setTaille("defunt_nature", 30);
        $form->setTaille("defunt_parente", 30);
        $form->setTaille("defunt_lieunaissance", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("operation_defunt", 11);
        $form->setMax("operation", 11);
        $form->setMax("defunt", 11);
        $form->setMax("defunt_titre", 11);
        $form->setMax("defunt_nom", 50);
        $form->setMax("defunt_marital", 50);
        $form->setMax("defunt_prenom", 50);
        $form->setMax("defunt_datenaissance", 12);
        $form->setMax("defunt_datedeces", 12);
        $form->setMax("defunt_lieudeces", 50);
        $form->setMax("defunt_nature", 50);
        $form->setMax("defunt_parente", 100);
        $form->setMax("defunt_lieunaissance", 50);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('operation_defunt', __('operation_defunt'));
        $form->setLib('operation', __('operation'));
        $form->setLib('defunt', __('defunt'));
        $form->setLib('defunt_titre', __('defunt_titre'));
        $form->setLib('defunt_nom', __('defunt_nom'));
        $form->setLib('defunt_marital', __('defunt_marital'));
        $form->setLib('defunt_prenom', __('defunt_prenom'));
        $form->setLib('defunt_datenaissance', __('defunt_datenaissance'));
        $form->setLib('defunt_datedeces', __('defunt_datedeces'));
        $form->setLib('defunt_lieudeces', __('defunt_lieudeces'));
        $form->setLib('defunt_nature', __('defunt_nature'));
        $form->setLib('defunt_parente', __('defunt_parente'));
        $form->setLib('defunt_lieunaissance', __('defunt_lieunaissance'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // defunt
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "defunt",
            $this->get_var_sql_forminc__sql("defunt"),
            $this->get_var_sql_forminc__sql("defunt_by_id"),
            false
        );
        // defunt_titre
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "defunt_titre",
            $this->get_var_sql_forminc__sql("defunt_titre"),
            $this->get_var_sql_forminc__sql("defunt_titre_by_id"),
            true
        );
        // operation
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "operation",
            $this->get_var_sql_forminc__sql("operation"),
            $this->get_var_sql_forminc__sql("operation_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('defunt', $this->retourformulaire))
                $form->setVal('defunt', $idxformulaire);
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('defunt_titre', $idxformulaire);
            if($this->is_in_context_of_foreign_key('operation', $this->retourformulaire))
                $form->setVal('operation', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
