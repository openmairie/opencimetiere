<?php
//$Id$ 
//gen openMairie le 12/10/2023 13:25

require_once "../obj/om_dbform.class.php";

class operation_defunt_archive_gen extends om_dbform {

    protected $_absolute_class_name = "operation_defunt_archive";

    var $table = "operation_defunt_archive";
    var $clePrimaire = "operation_defunt";
    var $typeCle = "N";
    var $required_field = array(
        "operation",
        "operation_defunt"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("operation");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "operation_defunt",
            "operation",
            "defunt",
            "defunt_titre",
            "defunt_nom",
            "defunt_marital",
            "defunt_prenom",
            "defunt_datenaissance",
            "defunt_datedeces",
            "defunt_lieudeces",
            "defunt_nature",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['operation_defunt'])) {
            $this->valF['operation_defunt'] = ""; // -> requis
        } else {
            $this->valF['operation_defunt'] = $val['operation_defunt'];
        }
        if (!is_numeric($val['operation'])) {
            $this->valF['operation'] = ""; // -> requis
        } else {
            $this->valF['operation'] = $val['operation'];
        }
        if (!is_numeric($val['defunt'])) {
            $this->valF['defunt'] = NULL;
        } else {
            $this->valF['defunt'] = $val['defunt'];
        }
        if (!is_numeric($val['defunt_titre'])) {
            $this->valF['defunt_titre'] = NULL;
        } else {
            $this->valF['defunt_titre'] = $val['defunt_titre'];
        }
        if ($val['defunt_nom'] == "") {
            $this->valF['defunt_nom'] = NULL;
        } else {
            $this->valF['defunt_nom'] = $val['defunt_nom'];
        }
        if ($val['defunt_marital'] == "") {
            $this->valF['defunt_marital'] = NULL;
        } else {
            $this->valF['defunt_marital'] = $val['defunt_marital'];
        }
        if ($val['defunt_prenom'] == "") {
            $this->valF['defunt_prenom'] = NULL;
        } else {
            $this->valF['defunt_prenom'] = $val['defunt_prenom'];
        }
        if ($val['defunt_datenaissance'] != "") {
            $this->valF['defunt_datenaissance'] = $this->dateDB($val['defunt_datenaissance']);
        } else {
            $this->valF['defunt_datenaissance'] = NULL;
        }
        if ($val['defunt_datedeces'] != "") {
            $this->valF['defunt_datedeces'] = $this->dateDB($val['defunt_datedeces']);
        } else {
            $this->valF['defunt_datedeces'] = NULL;
        }
        if ($val['defunt_lieudeces'] == "") {
            $this->valF['defunt_lieudeces'] = NULL;
        } else {
            $this->valF['defunt_lieudeces'] = $val['defunt_lieudeces'];
        }
        if ($val['defunt_nature'] == "") {
            $this->valF['defunt_nature'] = NULL;
        } else {
            $this->valF['defunt_nature'] = $val['defunt_nature'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("operation_defunt", "hidden");
            $form->setType("operation", "text");
            $form->setType("defunt", "text");
            $form->setType("defunt_titre", "text");
            $form->setType("defunt_nom", "text");
            $form->setType("defunt_marital", "text");
            $form->setType("defunt_prenom", "text");
            $form->setType("defunt_datenaissance", "date");
            $form->setType("defunt_datedeces", "date");
            $form->setType("defunt_lieudeces", "text");
            $form->setType("defunt_nature", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("operation_defunt", "hiddenstatic");
            $form->setType("operation", "text");
            $form->setType("defunt", "text");
            $form->setType("defunt_titre", "text");
            $form->setType("defunt_nom", "text");
            $form->setType("defunt_marital", "text");
            $form->setType("defunt_prenom", "text");
            $form->setType("defunt_datenaissance", "date");
            $form->setType("defunt_datedeces", "date");
            $form->setType("defunt_lieudeces", "text");
            $form->setType("defunt_nature", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("operation_defunt", "hiddenstatic");
            $form->setType("operation", "hiddenstatic");
            $form->setType("defunt", "hiddenstatic");
            $form->setType("defunt_titre", "hiddenstatic");
            $form->setType("defunt_nom", "hiddenstatic");
            $form->setType("defunt_marital", "hiddenstatic");
            $form->setType("defunt_prenom", "hiddenstatic");
            $form->setType("defunt_datenaissance", "hiddenstatic");
            $form->setType("defunt_datedeces", "hiddenstatic");
            $form->setType("defunt_lieudeces", "hiddenstatic");
            $form->setType("defunt_nature", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("operation_defunt", "static");
            $form->setType("operation", "static");
            $form->setType("defunt", "static");
            $form->setType("defunt_titre", "static");
            $form->setType("defunt_nom", "static");
            $form->setType("defunt_marital", "static");
            $form->setType("defunt_prenom", "static");
            $form->setType("defunt_datenaissance", "datestatic");
            $form->setType("defunt_datedeces", "datestatic");
            $form->setType("defunt_lieudeces", "static");
            $form->setType("defunt_nature", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('operation_defunt','VerifNum(this)');
        $form->setOnchange('operation','VerifNum(this)');
        $form->setOnchange('defunt','VerifNum(this)');
        $form->setOnchange('defunt_titre','VerifNum(this)');
        $form->setOnchange('defunt_datenaissance','fdate(this)');
        $form->setOnchange('defunt_datedeces','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("operation_defunt", 11);
        $form->setTaille("operation", 11);
        $form->setTaille("defunt", 11);
        $form->setTaille("defunt_titre", 11);
        $form->setTaille("defunt_nom", 30);
        $form->setTaille("defunt_marital", 30);
        $form->setTaille("defunt_prenom", 30);
        $form->setTaille("defunt_datenaissance", 12);
        $form->setTaille("defunt_datedeces", 12);
        $form->setTaille("defunt_lieudeces", 30);
        $form->setTaille("defunt_nature", 15);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("operation_defunt", 11);
        $form->setMax("operation", 11);
        $form->setMax("defunt", 11);
        $form->setMax("defunt_titre", 11);
        $form->setMax("defunt_nom", 40);
        $form->setMax("defunt_marital", 40);
        $form->setMax("defunt_prenom", 40);
        $form->setMax("defunt_datenaissance", 12);
        $form->setMax("defunt_datedeces", 12);
        $form->setMax("defunt_lieudeces", 40);
        $form->setMax("defunt_nature", 15);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('operation_defunt', __('operation_defunt'));
        $form->setLib('operation', __('operation'));
        $form->setLib('defunt', __('defunt'));
        $form->setLib('defunt_titre', __('defunt_titre'));
        $form->setLib('defunt_nom', __('defunt_nom'));
        $form->setLib('defunt_marital', __('defunt_marital'));
        $form->setLib('defunt_prenom', __('defunt_prenom'));
        $form->setLib('defunt_datenaissance', __('defunt_datenaissance'));
        $form->setLib('defunt_datedeces', __('defunt_datedeces'));
        $form->setLib('defunt_lieudeces', __('defunt_lieudeces'));
        $form->setLib('defunt_nature', __('defunt_nature'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
