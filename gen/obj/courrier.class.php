<?php
//$Id$ 
//gen openMairie le 27/09/2023 14:35

require_once "../obj/om_dbform.class.php";

class courrier_gen extends om_dbform {

    protected $_absolute_class_name = "courrier";

    var $table = "courrier";
    var $clePrimaire = "courrier";
    var $typeCle = "N";
    var $required_field = array(
        "courrier"
    );
    
    var $foreign_keys_extended = array(
        "contrat" => array("contrat", ),
        "autorisation" => array("autorisation", ),
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("destinataire");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "courrier",
            "destinataire",
            "datecourrier",
            "lettretype",
            "complement",
            "emplacement",
            "contrat",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrat() {
        return "SELECT contrat.contrat, contrat.emplacement FROM ".DB_PREFIXE."contrat ORDER BY contrat.emplacement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrat_by_id() {
        return "SELECT contrat.contrat, contrat.emplacement FROM ".DB_PREFIXE."contrat WHERE contrat = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_destinataire() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation ORDER BY autorisation.emplacement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_destinataire_by_id() {
        return "SELECT autorisation.autorisation, autorisation.emplacement FROM ".DB_PREFIXE."autorisation WHERE autorisation = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = ""; // -> requis
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
        if (!is_numeric($val['destinataire'])) {
            $this->valF['destinataire'] = NULL;
        } else {
            $this->valF['destinataire'] = $val['destinataire'];
        }
        if ($val['datecourrier'] != "") {
            $this->valF['datecourrier'] = $this->dateDB($val['datecourrier']);
        } else {
            $this->valF['datecourrier'] = NULL;
        }
        if ($val['lettretype'] == "") {
            $this->valF['lettretype'] = NULL;
        } else {
            $this->valF['lettretype'] = $val['lettretype'];
        }
            $this->valF['complement'] = $val['complement'];
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if (!is_numeric($val['contrat'])) {
            $this->valF['contrat'] = NULL;
        } else {
            $this->valF['contrat'] = $val['contrat'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("courrier", "hidden");
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("destinataire", "selecthiddenstatic");
            } else {
                $form->setType("destinataire", "select");
            }
            $form->setType("datecourrier", "date");
            $form->setType("lettretype", "text");
            $form->setType("complement", "textarea");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrat", $this->retourformulaire)) {
                $form->setType("contrat", "selecthiddenstatic");
            } else {
                $form->setType("contrat", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("courrier", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("autorisation", $this->retourformulaire)) {
                $form->setType("destinataire", "selecthiddenstatic");
            } else {
                $form->setType("destinataire", "select");
            }
            $form->setType("datecourrier", "date");
            $form->setType("lettretype", "text");
            $form->setType("complement", "textarea");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrat", $this->retourformulaire)) {
                $form->setType("contrat", "selecthiddenstatic");
            } else {
                $form->setType("contrat", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("destinataire", "selectstatic");
            $form->setType("datecourrier", "hiddenstatic");
            $form->setType("lettretype", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("contrat", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("courrier", "static");
            $form->setType("destinataire", "selectstatic");
            $form->setType("datecourrier", "datestatic");
            $form->setType("lettretype", "static");
            $form->setType("complement", "textareastatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("contrat", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('courrier','VerifNum(this)');
        $form->setOnchange('destinataire','VerifNum(this)');
        $form->setOnchange('datecourrier','fdate(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('contrat','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("courrier", 11);
        $form->setTaille("destinataire", 11);
        $form->setTaille("datecourrier", 12);
        $form->setTaille("lettretype", 30);
        $form->setTaille("complement", 80);
        $form->setTaille("emplacement", 11);
        $form->setTaille("contrat", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("courrier", 11);
        $form->setMax("destinataire", 11);
        $form->setMax("datecourrier", 12);
        $form->setMax("lettretype", 255);
        $form->setMax("complement", 6);
        $form->setMax("emplacement", 11);
        $form->setMax("contrat", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('courrier', __('courrier'));
        $form->setLib('destinataire', __('destinataire'));
        $form->setLib('datecourrier', __('datecourrier'));
        $form->setLib('lettretype', __('lettretype'));
        $form->setLib('complement', __('complement'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('contrat', __('contrat'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // contrat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "contrat",
            $this->get_var_sql_forminc__sql("contrat"),
            $this->get_var_sql_forminc__sql("contrat_by_id"),
            false
        );
        // destinataire
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "destinataire",
            $this->get_var_sql_forminc__sql("destinataire"),
            $this->get_var_sql_forminc__sql("destinataire_by_id"),
            false
        );
        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contrat', $this->retourformulaire))
                $form->setVal('contrat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorisation', $this->retourformulaire))
                $form->setVal('destinataire', $idxformulaire);
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
