<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:57

require_once "../obj/om_dbform.class.php";

class travaux_gen extends om_dbform {

    protected $_absolute_class_name = "travaux";

    var $table = "travaux";
    var $clePrimaire = "travaux";
    var $typeCle = "N";
    var $required_field = array(
        "travaux"
    );
    
    var $foreign_keys_extended = array(
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
        "entreprise" => array("entreprise", ),
        "travaux_nature" => array("travaux_nature", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("entreprise");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "travaux",
            "entreprise",
            "emplacement",
            "datedebinter",
            "datefininter",
            "observation",
            "naturedemandeur",
            "naturetravaux",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_entreprise() {
        return "SELECT entreprise.entreprise, entreprise.nomentreprise FROM ".DB_PREFIXE."entreprise ORDER BY entreprise.nomentreprise ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_entreprise_by_id() {
        return "SELECT entreprise.entreprise, entreprise.nomentreprise FROM ".DB_PREFIXE."entreprise WHERE entreprise = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_naturetravaux() {
        return "SELECT travaux_nature.travaux_nature, travaux_nature.libelle FROM ".DB_PREFIXE."travaux_nature WHERE ((travaux_nature.om_validite_debut IS NULL AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE)) OR (travaux_nature.om_validite_debut <= CURRENT_DATE AND (travaux_nature.om_validite_fin IS NULL OR travaux_nature.om_validite_fin > CURRENT_DATE))) ORDER BY travaux_nature.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_naturetravaux_by_id() {
        return "SELECT travaux_nature.travaux_nature, travaux_nature.libelle FROM ".DB_PREFIXE."travaux_nature WHERE travaux_nature = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['travaux'])) {
            $this->valF['travaux'] = ""; // -> requis
        } else {
            $this->valF['travaux'] = $val['travaux'];
        }
        if (!is_numeric($val['entreprise'])) {
            $this->valF['entreprise'] = NULL;
        } else {
            $this->valF['entreprise'] = $val['entreprise'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if ($val['datedebinter'] != "") {
            $this->valF['datedebinter'] = $this->dateDB($val['datedebinter']);
        } else {
            $this->valF['datedebinter'] = NULL;
        }
        if ($val['datefininter'] != "") {
            $this->valF['datefininter'] = $this->dateDB($val['datefininter']);
        } else {
            $this->valF['datefininter'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['naturedemandeur'] == "") {
            $this->valF['naturedemandeur'] = NULL;
        } else {
            $this->valF['naturedemandeur'] = $val['naturedemandeur'];
        }
        if (!is_numeric($val['naturetravaux'])) {
            $this->valF['naturetravaux'] = NULL;
        } else {
            $this->valF['naturetravaux'] = $val['naturetravaux'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("travaux", "hidden");
            if ($this->is_in_context_of_foreign_key("entreprise", $this->retourformulaire)) {
                $form->setType("entreprise", "selecthiddenstatic");
            } else {
                $form->setType("entreprise", "select");
            }
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("datedebinter", "date");
            $form->setType("datefininter", "date");
            $form->setType("observation", "textarea");
            $form->setType("naturedemandeur", "text");
            if ($this->is_in_context_of_foreign_key("travaux_nature", $this->retourformulaire)) {
                $form->setType("naturetravaux", "selecthiddenstatic");
            } else {
                $form->setType("naturetravaux", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("travaux", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("entreprise", $this->retourformulaire)) {
                $form->setType("entreprise", "selecthiddenstatic");
            } else {
                $form->setType("entreprise", "select");
            }
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("datedebinter", "date");
            $form->setType("datefininter", "date");
            $form->setType("observation", "textarea");
            $form->setType("naturedemandeur", "text");
            if ($this->is_in_context_of_foreign_key("travaux_nature", $this->retourformulaire)) {
                $form->setType("naturetravaux", "selecthiddenstatic");
            } else {
                $form->setType("naturetravaux", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("travaux", "hiddenstatic");
            $form->setType("entreprise", "selectstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("datedebinter", "hiddenstatic");
            $form->setType("datefininter", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("naturedemandeur", "hiddenstatic");
            $form->setType("naturetravaux", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("travaux", "static");
            $form->setType("entreprise", "selectstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("datedebinter", "datestatic");
            $form->setType("datefininter", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("naturedemandeur", "static");
            $form->setType("naturetravaux", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('travaux','VerifNum(this)');
        $form->setOnchange('entreprise','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('datedebinter','fdate(this)');
        $form->setOnchange('datefininter','fdate(this)');
        $form->setOnchange('naturetravaux','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("travaux", 11);
        $form->setTaille("entreprise", 11);
        $form->setTaille("emplacement", 11);
        $form->setTaille("datedebinter", 12);
        $form->setTaille("datefininter", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("naturedemandeur", 20);
        $form->setTaille("naturetravaux", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("travaux", 11);
        $form->setMax("entreprise", 11);
        $form->setMax("emplacement", 11);
        $form->setMax("datedebinter", 12);
        $form->setMax("datefininter", 12);
        $form->setMax("observation", 6);
        $form->setMax("naturedemandeur", 20);
        $form->setMax("naturetravaux", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('travaux', __('travaux'));
        $form->setLib('entreprise', __('entreprise'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('datedebinter', __('datedebinter'));
        $form->setLib('datefininter', __('datefininter'));
        $form->setLib('observation', __('observation'));
        $form->setLib('naturedemandeur', __('naturedemandeur'));
        $form->setLib('naturetravaux', __('naturetravaux'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
        // entreprise
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "entreprise",
            $this->get_var_sql_forminc__sql("entreprise"),
            $this->get_var_sql_forminc__sql("entreprise_by_id"),
            false
        );
        // naturetravaux
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "naturetravaux",
            $this->get_var_sql_forminc__sql("naturetravaux"),
            $this->get_var_sql_forminc__sql("naturetravaux_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('entreprise', $this->retourformulaire))
                $form->setVal('entreprise', $idxformulaire);
            if($this->is_in_context_of_foreign_key('travaux_nature', $this->retourformulaire))
                $form->setVal('naturetravaux', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
