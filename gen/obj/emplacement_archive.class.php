<?php
//$Id$ 
//gen openMairie le 20/01/2023 13:03

require_once "../obj/om_dbform.class.php";

class emplacement_archive_gen extends om_dbform {

    protected $_absolute_class_name = "emplacement_archive";

    var $table = "emplacement_archive";
    var $clePrimaire = "emplacement";
    var $typeCle = "N";
    var $required_field = array(
        "emplacement"
    );
    
    var $foreign_keys_extended = array(
        "sepulture_type" => array("sepulture_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nature");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "emplacement",
            "nature",
            "numero",
            "complement",
            "voie",
            "numerocadastre",
            "famille",
            "numeroacte",
            "datevente",
            "terme",
            "duree",
            "dateterme",
            "nombreplace",
            "placeoccupe",
            "superficie",
            "placeconstat",
            "dateconstat",
            "observation",
            "plans",
            "positionx",
            "positiony",
            "photo",
            "libre",
            "sepulturetype",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulturetype() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE ((sepulture_type.om_validite_debut IS NULL AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)) OR (sepulture_type.om_validite_debut <= CURRENT_DATE AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE))) ORDER BY sepulture_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulturetype_by_id() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE sepulture_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = ""; // -> requis
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if ($val['nature'] == "") {
            $this->valF['nature'] = NULL;
        } else {
            $this->valF['nature'] = $val['nature'];
        }
        if (!is_numeric($val['numero'])) {
            $this->valF['numero'] = NULL;
        } else {
            $this->valF['numero'] = $val['numero'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if (!is_numeric($val['voie'])) {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if ($val['numerocadastre'] == "") {
            $this->valF['numerocadastre'] = NULL;
        } else {
            $this->valF['numerocadastre'] = $val['numerocadastre'];
        }
        if ($val['famille'] == "") {
            $this->valF['famille'] = NULL;
        } else {
            $this->valF['famille'] = $val['famille'];
        }
        if ($val['numeroacte'] == "") {
            $this->valF['numeroacte'] = NULL;
        } else {
            $this->valF['numeroacte'] = $val['numeroacte'];
        }
        if ($val['datevente'] != "") {
            $this->valF['datevente'] = $this->dateDB($val['datevente']);
        } else {
            $this->valF['datevente'] = NULL;
        }
        if ($val['terme'] == "") {
            $this->valF['terme'] = NULL;
        } else {
            $this->valF['terme'] = $val['terme'];
        }
        if (!is_numeric($val['duree'])) {
            $this->valF['duree'] = NULL;
        } else {
            $this->valF['duree'] = $val['duree'];
        }
        if ($val['dateterme'] != "") {
            $this->valF['dateterme'] = $this->dateDB($val['dateterme']);
        } else {
            $this->valF['dateterme'] = NULL;
        }
        if (!is_numeric($val['nombreplace'])) {
            $this->valF['nombreplace'] = NULL;
        } else {
            $this->valF['nombreplace'] = $val['nombreplace'];
        }
        if (!is_numeric($val['placeoccupe'])) {
            $this->valF['placeoccupe'] = NULL;
        } else {
            $this->valF['placeoccupe'] = $val['placeoccupe'];
        }
        if (!is_numeric($val['superficie'])) {
            $this->valF['superficie'] = NULL;
        } else {
            $this->valF['superficie'] = $val['superficie'];
        }
        if (!is_numeric($val['placeconstat'])) {
            $this->valF['placeconstat'] = NULL;
        } else {
            $this->valF['placeconstat'] = $val['placeconstat'];
        }
        if ($val['dateconstat'] != "") {
            $this->valF['dateconstat'] = $this->dateDB($val['dateconstat']);
        } else {
            $this->valF['dateconstat'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['plans'] == "") {
            $this->valF['plans'] = NULL;
        } else {
            $this->valF['plans'] = $val['plans'];
        }
        if (!is_numeric($val['positionx'])) {
            $this->valF['positionx'] = NULL;
        } else {
            $this->valF['positionx'] = $val['positionx'];
        }
        if (!is_numeric($val['positiony'])) {
            $this->valF['positiony'] = NULL;
        } else {
            $this->valF['positiony'] = $val['positiony'];
        }
        if ($val['photo'] == "") {
            $this->valF['photo'] = NULL;
        } else {
            $this->valF['photo'] = $val['photo'];
        }
        if ($val['libre'] == "") {
            $this->valF['libre'] = NULL;
        } else {
            $this->valF['libre'] = $val['libre'];
        }
        if (!is_numeric($val['sepulturetype'])) {
            $this->valF['sepulturetype'] = NULL;
        } else {
            $this->valF['sepulturetype'] = $val['sepulturetype'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("emplacement", "hidden");
            $form->setType("nature", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("numerocadastre", "text");
            $form->setType("famille", "text");
            $form->setType("numeroacte", "text");
            $form->setType("datevente", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("dateterme", "date");
            $form->setType("nombreplace", "text");
            $form->setType("placeoccupe", "text");
            $form->setType("superficie", "text");
            $form->setType("placeconstat", "text");
            $form->setType("dateconstat", "date");
            $form->setType("observation", "textarea");
            $form->setType("plans", "text");
            $form->setType("positionx", "text");
            $form->setType("positiony", "text");
            $form->setType("photo", "text");
            $form->setType("libre", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulturetype", "selecthiddenstatic");
            } else {
                $form->setType("sepulturetype", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("nature", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            $form->setType("voie", "text");
            $form->setType("numerocadastre", "text");
            $form->setType("famille", "text");
            $form->setType("numeroacte", "text");
            $form->setType("datevente", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("dateterme", "date");
            $form->setType("nombreplace", "text");
            $form->setType("placeoccupe", "text");
            $form->setType("superficie", "text");
            $form->setType("placeconstat", "text");
            $form->setType("dateconstat", "date");
            $form->setType("observation", "textarea");
            $form->setType("plans", "text");
            $form->setType("positionx", "text");
            $form->setType("positiony", "text");
            $form->setType("photo", "text");
            $form->setType("libre", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulturetype", "selecthiddenstatic");
            } else {
                $form->setType("sepulturetype", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("voie", "hiddenstatic");
            $form->setType("numerocadastre", "hiddenstatic");
            $form->setType("famille", "hiddenstatic");
            $form->setType("numeroacte", "hiddenstatic");
            $form->setType("datevente", "hiddenstatic");
            $form->setType("terme", "hiddenstatic");
            $form->setType("duree", "hiddenstatic");
            $form->setType("dateterme", "hiddenstatic");
            $form->setType("nombreplace", "hiddenstatic");
            $form->setType("placeoccupe", "hiddenstatic");
            $form->setType("superficie", "hiddenstatic");
            $form->setType("placeconstat", "hiddenstatic");
            $form->setType("dateconstat", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("plans", "hiddenstatic");
            $form->setType("positionx", "hiddenstatic");
            $form->setType("positiony", "hiddenstatic");
            $form->setType("photo", "hiddenstatic");
            $form->setType("libre", "hiddenstatic");
            $form->setType("sepulturetype", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("emplacement", "static");
            $form->setType("nature", "static");
            $form->setType("numero", "static");
            $form->setType("complement", "static");
            $form->setType("voie", "static");
            $form->setType("numerocadastre", "static");
            $form->setType("famille", "static");
            $form->setType("numeroacte", "static");
            $form->setType("datevente", "datestatic");
            $form->setType("terme", "static");
            $form->setType("duree", "static");
            $form->setType("dateterme", "datestatic");
            $form->setType("nombreplace", "static");
            $form->setType("placeoccupe", "static");
            $form->setType("superficie", "static");
            $form->setType("placeconstat", "static");
            $form->setType("dateconstat", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("plans", "static");
            $form->setType("positionx", "static");
            $form->setType("positiony", "static");
            $form->setType("photo", "static");
            $form->setType("libre", "static");
            $form->setType("sepulturetype", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('numero','VerifNum(this)');
        $form->setOnchange('voie','VerifNum(this)');
        $form->setOnchange('datevente','fdate(this)');
        $form->setOnchange('duree','VerifFloat(this)');
        $form->setOnchange('dateterme','fdate(this)');
        $form->setOnchange('nombreplace','VerifFloat(this)');
        $form->setOnchange('placeoccupe','VerifFloat(this)');
        $form->setOnchange('superficie','VerifFloat(this)');
        $form->setOnchange('placeconstat','VerifFloat(this)');
        $form->setOnchange('dateconstat','fdate(this)');
        $form->setOnchange('positionx','VerifFloat(this)');
        $form->setOnchange('positiony','VerifFloat(this)');
        $form->setOnchange('sepulturetype','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("emplacement", 11);
        $form->setTaille("nature", 20);
        $form->setTaille("numero", 20);
        $form->setTaille("complement", 10);
        $form->setTaille("voie", 11);
        $form->setTaille("numerocadastre", 15);
        $form->setTaille("famille", 30);
        $form->setTaille("numeroacte", 30);
        $form->setTaille("datevente", 12);
        $form->setTaille("terme", 20);
        $form->setTaille("duree", 20);
        $form->setTaille("dateterme", 12);
        $form->setTaille("nombreplace", 20);
        $form->setTaille("placeoccupe", 20);
        $form->setTaille("superficie", 20);
        $form->setTaille("placeconstat", 20);
        $form->setTaille("dateconstat", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("plans", 30);
        $form->setTaille("positionx", 20);
        $form->setTaille("positiony", 20);
        $form->setTaille("photo", 20);
        $form->setTaille("libre", 10);
        $form->setTaille("sepulturetype", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("emplacement", 11);
        $form->setMax("nature", 20);
        $form->setMax("numero", 20);
        $form->setMax("complement", 6);
        $form->setMax("voie", 11);
        $form->setMax("numerocadastre", 15);
        $form->setMax("famille", 40);
        $form->setMax("numeroacte", 30);
        $form->setMax("datevente", 12);
        $form->setMax("terme", 20);
        $form->setMax("duree", 20);
        $form->setMax("dateterme", 12);
        $form->setMax("nombreplace", 20);
        $form->setMax("placeoccupe", 20);
        $form->setMax("superficie", 20);
        $form->setMax("placeconstat", 20);
        $form->setMax("dateconstat", 12);
        $form->setMax("observation", 6);
        $form->setMax("plans", 50);
        $form->setMax("positionx", 20);
        $form->setMax("positiony", 20);
        $form->setMax("photo", 20);
        $form->setMax("libre", 3);
        $form->setMax("sepulturetype", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('nature', __('nature'));
        $form->setLib('numero', __('numero'));
        $form->setLib('complement', __('complement'));
        $form->setLib('voie', __('voie'));
        $form->setLib('numerocadastre', __('numerocadastre'));
        $form->setLib('famille', __('famille'));
        $form->setLib('numeroacte', __('numeroacte'));
        $form->setLib('datevente', __('datevente'));
        $form->setLib('terme', __('terme'));
        $form->setLib('duree', __('duree'));
        $form->setLib('dateterme', __('dateterme'));
        $form->setLib('nombreplace', __('nombreplace'));
        $form->setLib('placeoccupe', __('placeoccupe'));
        $form->setLib('superficie', __('superficie'));
        $form->setLib('placeconstat', __('placeconstat'));
        $form->setLib('dateconstat', __('dateconstat'));
        $form->setLib('observation', __('observation'));
        $form->setLib('plans', __('plans'));
        $form->setLib('positionx', __('positionx'));
        $form->setLib('positiony', __('positiony'));
        $form->setLib('photo', __('photo'));
        $form->setLib('libre', __('libre'));
        $form->setLib('sepulturetype', __('sepulturetype'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // sepulturetype
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "sepulturetype",
            $this->get_var_sql_forminc__sql("sepulturetype"),
            $this->get_var_sql_forminc__sql("sepulturetype_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('sepulture_type', $this->retourformulaire))
                $form->setVal('sepulturetype', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
