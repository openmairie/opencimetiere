<?php
//$Id$ 
//gen openMairie le 09/03/2022 08:41

require_once "../obj/om_dbform.class.php";

class voie_gen extends om_dbform {

    protected $_absolute_class_name = "voie";

    var $table = "voie";
    var $clePrimaire = "voie";
    var $typeCle = "N";
    var $required_field = array(
        "voie",
        "voielib",
        "voietype",
        "zone"
    );
    
    var $foreign_keys_extended = array(
        "voie_type" => array("voie_type", ),
        "zone" => array("zone", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("zone");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "voie",
            "zone",
            "voietype",
            "voielib",
            "geom",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voietype() {
        return "SELECT voie_type.voie_type, voie_type.libelle FROM ".DB_PREFIXE."voie_type WHERE ((voie_type.om_validite_debut IS NULL AND (voie_type.om_validite_fin IS NULL OR voie_type.om_validite_fin > CURRENT_DATE)) OR (voie_type.om_validite_debut <= CURRENT_DATE AND (voie_type.om_validite_fin IS NULL OR voie_type.om_validite_fin > CURRENT_DATE))) ORDER BY voie_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voietype_by_id() {
        return "SELECT voie_type.voie_type, voie_type.libelle FROM ".DB_PREFIXE."voie_type WHERE voie_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zone() {
        return "SELECT zone.zone, zone.cimetiere FROM ".DB_PREFIXE."zone ORDER BY zone.cimetiere ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zone_by_id() {
        return "SELECT zone.zone, zone.cimetiere FROM ".DB_PREFIXE."zone WHERE zone = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['voie'])) {
            $this->valF['voie'] = ""; // -> requis
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if (!is_numeric($val['zone'])) {
            $this->valF['zone'] = ""; // -> requis
        } else {
            $this->valF['zone'] = $val['zone'];
        }
        if (!is_numeric($val['voietype'])) {
            $this->valF['voietype'] = ""; // -> requis
        } else {
            $this->valF['voietype'] = $val['voietype'];
        }
        $this->valF['voielib'] = $val['voielib'];
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("voie", "hidden");
            if ($this->is_in_context_of_foreign_key("zone", $this->retourformulaire)) {
                $form->setType("zone", "selecthiddenstatic");
            } else {
                $form->setType("zone", "select");
            }
            if ($this->is_in_context_of_foreign_key("voie_type", $this->retourformulaire)) {
                $form->setType("voietype", "selecthiddenstatic");
            } else {
                $form->setType("voietype", "select");
            }
            $form->setType("voielib", "text");
            $form->setType("geom", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("voie", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("zone", $this->retourformulaire)) {
                $form->setType("zone", "selecthiddenstatic");
            } else {
                $form->setType("zone", "select");
            }
            if ($this->is_in_context_of_foreign_key("voie_type", $this->retourformulaire)) {
                $form->setType("voietype", "selecthiddenstatic");
            } else {
                $form->setType("voietype", "select");
            }
            $form->setType("voielib", "text");
            $form->setType("geom", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("voie", "hiddenstatic");
            $form->setType("zone", "selectstatic");
            $form->setType("voietype", "selectstatic");
            $form->setType("voielib", "hiddenstatic");
            $form->setType("geom", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("voie", "static");
            $form->setType("zone", "selectstatic");
            $form->setType("voietype", "selectstatic");
            $form->setType("voielib", "static");
            $form->setType("geom", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('voie','VerifNum(this)');
        $form->setOnchange('zone','VerifNum(this)');
        $form->setOnchange('voietype','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("voie", 11);
        $form->setTaille("zone", 11);
        $form->setTaille("voietype", 11);
        $form->setTaille("voielib", 30);
        $form->setTaille("geom", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("voie", 11);
        $form->setMax("zone", 11);
        $form->setMax("voietype", 11);
        $form->setMax("voielib", 40);
        $form->setMax("geom", 551444);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('voie', __('voie'));
        $form->setLib('zone', __('zone'));
        $form->setLib('voietype', __('voietype'));
        $form->setLib('voielib', __('voielib'));
        $form->setLib('geom', __('geom'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // voietype
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "voietype",
            $this->get_var_sql_forminc__sql("voietype"),
            $this->get_var_sql_forminc__sql("voietype_by_id"),
            true
        );
        // zone
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "zone",
            $this->get_var_sql_forminc__sql("zone"),
            $this->get_var_sql_forminc__sql("zone_by_id"),
            false
        );
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("voie", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('voie_type', $this->retourformulaire))
                $form->setVal('voietype', $idxformulaire);
            if($this->is_in_context_of_foreign_key('zone', $this->retourformulaire))
                $form->setVal('zone', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : emplacement
        $this->rechercheTable($this->f->db, "emplacement", "voie", $id);
    }


}
