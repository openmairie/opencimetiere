<?php
//$Id$ 
//gen openMairie le 16/12/2022 18:27

require_once "../obj/om_dbform.class.php";

class defunt_gen extends om_dbform {

    protected $_absolute_class_name = "defunt";

    var $table = "defunt";
    var $clePrimaire = "defunt";
    var $typeCle = "N";
    var $required_field = array(
        "defunt"
    );
    
    var $foreign_keys_extended = array(
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
        "titre_de_civilite" => array("titre_de_civilite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nature");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "defunt",
            "nature",
            "taille",
            "emplacement",
            "titre",
            "nom",
            "prenom",
            "marital",
            "datenaissance",
            "datedeces",
            "lieudeces",
            "dateinhumation",
            "exhumation",
            "dateexhumation",
            "observation",
            "reduction",
            "datereduction",
            "historique",
            "verrou",
            "parente",
            "lieunaissance",
            "x",
            "y",
            "genealogie",
            "id_temp",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_titre() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE ((titre_de_civilite.om_validite_debut IS NULL AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE)) OR (titre_de_civilite.om_validite_debut <= CURRENT_DATE AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE))) ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_titre_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['defunt'])) {
            $this->valF['defunt'] = ""; // -> requis
        } else {
            $this->valF['defunt'] = $val['defunt'];
        }
        if ($val['nature'] == "") {
            $this->valF['nature'] = NULL;
        } else {
            $this->valF['nature'] = $val['nature'];
        }
        if (!is_numeric($val['taille'])) {
            $this->valF['taille'] = NULL;
        } else {
            $this->valF['taille'] = $val['taille'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if (!is_numeric($val['titre'])) {
            $this->valF['titre'] = NULL;
        } else {
            $this->valF['titre'] = $val['titre'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = NULL;
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = NULL;
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if ($val['marital'] == "") {
            $this->valF['marital'] = NULL;
        } else {
            $this->valF['marital'] = $val['marital'];
        }
        if ($val['datenaissance'] != "") {
            $this->valF['datenaissance'] = $this->dateDB($val['datenaissance']);
        } else {
            $this->valF['datenaissance'] = NULL;
        }
        if ($val['datedeces'] != "") {
            $this->valF['datedeces'] = $this->dateDB($val['datedeces']);
        } else {
            $this->valF['datedeces'] = NULL;
        }
        if ($val['lieudeces'] == "") {
            $this->valF['lieudeces'] = NULL;
        } else {
            $this->valF['lieudeces'] = $val['lieudeces'];
        }
        if ($val['dateinhumation'] != "") {
            $this->valF['dateinhumation'] = $this->dateDB($val['dateinhumation']);
        } else {
            $this->valF['dateinhumation'] = NULL;
        }
        if ($val['exhumation'] == "") {
            $this->valF['exhumation'] = NULL;
        } else {
            $this->valF['exhumation'] = $val['exhumation'];
        }
        if ($val['dateexhumation'] != "") {
            $this->valF['dateexhumation'] = $this->dateDB($val['dateexhumation']);
        } else {
            $this->valF['dateexhumation'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['reduction'] == "") {
            $this->valF['reduction'] = NULL;
        } else {
            $this->valF['reduction'] = $val['reduction'];
        }
        if ($val['datereduction'] != "") {
            $this->valF['datereduction'] = $this->dateDB($val['datereduction']);
        } else {
            $this->valF['datereduction'] = NULL;
        }
            $this->valF['historique'] = $val['historique'];
        if ($val['verrou'] == "") {
            $this->valF['verrou'] = NULL;
        } else {
            $this->valF['verrou'] = $val['verrou'];
        }
            $this->valF['parente'] = $val['parente'];
        if ($val['lieunaissance'] == "") {
            $this->valF['lieunaissance'] = NULL;
        } else {
            $this->valF['lieunaissance'] = $val['lieunaissance'];
        }
        if (!is_numeric($val['x'])) {
            $this->valF['x'] = NULL;
        } else {
            $this->valF['x'] = $val['x'];
        }
        if (!is_numeric($val['y'])) {
            $this->valF['y'] = NULL;
        } else {
            $this->valF['y'] = $val['y'];
        }
            $this->valF['genealogie'] = $val['genealogie'];
        if ($val['id_temp'] == "") {
            $this->valF['id_temp'] = NULL;
        } else {
            $this->valF['id_temp'] = $val['id_temp'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("defunt", "hidden");
            $form->setType("nature", "text");
            $form->setType("taille", "text");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("titre", "selecthiddenstatic");
            } else {
                $form->setType("titre", "select");
            }
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("marital", "text");
            $form->setType("datenaissance", "date");
            $form->setType("datedeces", "date");
            $form->setType("lieudeces", "text");
            $form->setType("dateinhumation", "date");
            $form->setType("exhumation", "text");
            $form->setType("dateexhumation", "date");
            $form->setType("observation", "textarea");
            $form->setType("reduction", "text");
            $form->setType("datereduction", "date");
            $form->setType("historique", "textarea");
            $form->setType("verrou", "text");
            $form->setType("parente", "textarea");
            $form->setType("lieunaissance", "text");
            $form->setType("x", "text");
            $form->setType("y", "text");
            $form->setType("genealogie", "textarea");
            $form->setType("id_temp", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("defunt", "hiddenstatic");
            $form->setType("nature", "text");
            $form->setType("taille", "text");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("titre", "selecthiddenstatic");
            } else {
                $form->setType("titre", "select");
            }
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("marital", "text");
            $form->setType("datenaissance", "date");
            $form->setType("datedeces", "date");
            $form->setType("lieudeces", "text");
            $form->setType("dateinhumation", "date");
            $form->setType("exhumation", "text");
            $form->setType("dateexhumation", "date");
            $form->setType("observation", "textarea");
            $form->setType("reduction", "text");
            $form->setType("datereduction", "date");
            $form->setType("historique", "textarea");
            $form->setType("verrou", "text");
            $form->setType("parente", "textarea");
            $form->setType("lieunaissance", "text");
            $form->setType("x", "text");
            $form->setType("y", "text");
            $form->setType("genealogie", "textarea");
            $form->setType("id_temp", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("defunt", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("taille", "hiddenstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("titre", "selectstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("marital", "hiddenstatic");
            $form->setType("datenaissance", "hiddenstatic");
            $form->setType("datedeces", "hiddenstatic");
            $form->setType("lieudeces", "hiddenstatic");
            $form->setType("dateinhumation", "hiddenstatic");
            $form->setType("exhumation", "hiddenstatic");
            $form->setType("dateexhumation", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("reduction", "hiddenstatic");
            $form->setType("datereduction", "hiddenstatic");
            $form->setType("historique", "hiddenstatic");
            $form->setType("verrou", "hiddenstatic");
            $form->setType("parente", "hiddenstatic");
            $form->setType("lieunaissance", "hiddenstatic");
            $form->setType("x", "hiddenstatic");
            $form->setType("y", "hiddenstatic");
            $form->setType("genealogie", "hiddenstatic");
            $form->setType("id_temp", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("defunt", "static");
            $form->setType("nature", "static");
            $form->setType("taille", "static");
            $form->setType("emplacement", "selectstatic");
            $form->setType("titre", "selectstatic");
            $form->setType("nom", "static");
            $form->setType("prenom", "static");
            $form->setType("marital", "static");
            $form->setType("datenaissance", "datestatic");
            $form->setType("datedeces", "datestatic");
            $form->setType("lieudeces", "static");
            $form->setType("dateinhumation", "datestatic");
            $form->setType("exhumation", "static");
            $form->setType("dateexhumation", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("reduction", "static");
            $form->setType("datereduction", "datestatic");
            $form->setType("historique", "textareastatic");
            $form->setType("verrou", "static");
            $form->setType("parente", "textareastatic");
            $form->setType("lieunaissance", "static");
            $form->setType("x", "static");
            $form->setType("y", "static");
            $form->setType("genealogie", "textareastatic");
            $form->setType("id_temp", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('defunt','VerifNum(this)');
        $form->setOnchange('taille','VerifFloat(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('titre','VerifNum(this)');
        $form->setOnchange('datenaissance','fdate(this)');
        $form->setOnchange('datedeces','fdate(this)');
        $form->setOnchange('dateinhumation','fdate(this)');
        $form->setOnchange('dateexhumation','fdate(this)');
        $form->setOnchange('datereduction','fdate(this)');
        $form->setOnchange('x','VerifNum(this)');
        $form->setOnchange('y','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("defunt", 11);
        $form->setTaille("nature", 30);
        $form->setTaille("taille", 20);
        $form->setTaille("emplacement", 20);
        $form->setTaille("titre", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("marital", 30);
        $form->setTaille("datenaissance", 12);
        $form->setTaille("datedeces", 12);
        $form->setTaille("lieudeces", 30);
        $form->setTaille("dateinhumation", 12);
        $form->setTaille("exhumation", 10);
        $form->setTaille("dateexhumation", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("reduction", 10);
        $form->setTaille("datereduction", 12);
        $form->setTaille("historique", 80);
        $form->setTaille("verrou", 10);
        $form->setTaille("parente", 80);
        $form->setTaille("lieunaissance", 30);
        $form->setTaille("x", 11);
        $form->setTaille("y", 11);
        $form->setTaille("genealogie", 80);
        $form->setTaille("id_temp", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("defunt", 11);
        $form->setMax("nature", 50);
        $form->setMax("taille", 20);
        $form->setMax("emplacement", 20);
        $form->setMax("titre", 11);
        $form->setMax("nom", 50);
        $form->setMax("prenom", 50);
        $form->setMax("marital", 50);
        $form->setMax("datenaissance", 12);
        $form->setMax("datedeces", 12);
        $form->setMax("lieudeces", 50);
        $form->setMax("dateinhumation", 12);
        $form->setMax("exhumation", 3);
        $form->setMax("dateexhumation", 12);
        $form->setMax("observation", 6);
        $form->setMax("reduction", 3);
        $form->setMax("datereduction", 12);
        $form->setMax("historique", 6);
        $form->setMax("verrou", 3);
        $form->setMax("parente", 6);
        $form->setMax("lieunaissance", 50);
        $form->setMax("x", 11);
        $form->setMax("y", 11);
        $form->setMax("genealogie", 6);
        $form->setMax("id_temp", 255);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('defunt', __('defunt'));
        $form->setLib('nature', __('nature'));
        $form->setLib('taille', __('taille'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('titre', __('titre'));
        $form->setLib('nom', __('nom'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('marital', __('marital'));
        $form->setLib('datenaissance', __('datenaissance'));
        $form->setLib('datedeces', __('datedeces'));
        $form->setLib('lieudeces', __('lieudeces'));
        $form->setLib('dateinhumation', __('dateinhumation'));
        $form->setLib('exhumation', __('exhumation'));
        $form->setLib('dateexhumation', __('dateexhumation'));
        $form->setLib('observation', __('observation'));
        $form->setLib('reduction', __('reduction'));
        $form->setLib('datereduction', __('datereduction'));
        $form->setLib('historique', __('historique'));
        $form->setLib('verrou', __('verrou'));
        $form->setLib('parente', __('parente'));
        $form->setLib('lieunaissance', __('lieunaissance'));
        $form->setLib('x', __('x'));
        $form->setLib('y', __('y'));
        $form->setLib('genealogie', __('genealogie'));
        $form->setLib('id_temp', __('id_temp'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
        // titre
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "titre",
            $this->get_var_sql_forminc__sql("titre"),
            $this->get_var_sql_forminc__sql("titre_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('titre', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "defunt_p1", $id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "defunt_p2", $id);
        // Verification de la cle secondaire : operation_defunt
        $this->rechercheTable($this->f->db, "operation_defunt", "defunt", $id);
    }


}
