<?php
//$Id$ 
//gen openMairie le 21/10/2018 15:57

require_once "../obj/om_dbform.class.php";

class dossier_gen extends om_dbform {

    protected $_absolute_class_name = "dossier";

    var $table = "dossier";
    var $clePrimaire = "dossier";
    var $typeCle = "N";
    var $required_field = array(
        "dossier",
        "fichier"
    );
    
    var $foreign_keys_extended = array(
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("emplacement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier",
            "emplacement",
            "fichier",
            "datedossier",
            "observation",
            "typedossier",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier'])) {
            $this->valF['dossier'] = ""; // -> requis
        } else {
            $this->valF['dossier'] = $val['dossier'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        $this->valF['fichier'] = $val['fichier'];
        if ($val['datedossier'] != "") {
            $this->valF['datedossier'] = $this->dateDB($val['datedossier']);
        } else {
            $this->valF['datedossier'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['typedossier'] == "") {
            $this->valF['typedossier'] = NULL;
        } else {
            $this->valF['typedossier'] = $val['typedossier'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier", "hidden");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("fichier", "text");
            $form->setType("datedossier", "date");
            $form->setType("observation", "textarea");
            $form->setType("typedossier", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("fichier", "text");
            $form->setType("datedossier", "date");
            $form->setType("observation", "textarea");
            $form->setType("typedossier", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier", "hiddenstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("fichier", "hiddenstatic");
            $form->setType("datedossier", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("typedossier", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier", "static");
            $form->setType("emplacement", "selectstatic");
            $form->setType("fichier", "static");
            $form->setType("datedossier", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("typedossier", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('datedossier','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier", 11);
        $form->setTaille("emplacement", 11);
        $form->setTaille("fichier", 30);
        $form->setTaille("datedossier", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("typedossier", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier", 11);
        $form->setMax("emplacement", 11);
        $form->setMax("fichier", 40);
        $form->setMax("datedossier", 12);
        $form->setMax("observation", 6);
        $form->setMax("typedossier", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier', __('dossier'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('fichier', __('fichier'));
        $form->setLib('datedossier', __('datedossier'));
        $form->setLib('observation', __('observation'));
        $form->setLib('typedossier', __('typedossier'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
