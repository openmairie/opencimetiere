<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:10

require_once "../obj/om_dbform.class.php";

class plans_gen extends om_dbform {

    protected $_absolute_class_name = "plans";

    var $table = "plans";
    var $clePrimaire = "plans";
    var $typeCle = "N";
    var $required_field = array(
        "fichier",
        "plans",
        "planslib"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("planslib");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "plans",
            "planslib",
            "fichier",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['plans'])) {
            $this->valF['plans'] = ""; // -> requis
        } else {
            $this->valF['plans'] = $val['plans'];
        }
        $this->valF['planslib'] = $val['planslib'];
        $this->valF['fichier'] = $val['fichier'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("plans", "hidden");
            $form->setType("planslib", "text");
            $form->setType("fichier", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("plans", "hiddenstatic");
            $form->setType("planslib", "text");
            $form->setType("fichier", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("plans", "hiddenstatic");
            $form->setType("planslib", "hiddenstatic");
            $form->setType("fichier", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("plans", "static");
            $form->setType("planslib", "static");
            $form->setType("fichier", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('plans','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("plans", 11);
        $form->setTaille("planslib", 30);
        $form->setTaille("fichier", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("plans", 11);
        $form->setMax("planslib", 40);
        $form->setMax("fichier", 100);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('plans', __('plans'));
        $form->setLib('planslib', __('planslib'));
        $form->setLib('fichier', __('fichier'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : emplacement
        $this->rechercheTable($this->f->db, "emplacement", "plans", $id);
    }


}
