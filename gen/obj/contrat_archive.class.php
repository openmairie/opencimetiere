<?php
//$Id$ 
//gen openMairie le 10/07/2020 17:49

require_once "../obj/om_dbform.class.php";

class contrat_archive_gen extends om_dbform {

    protected $_absolute_class_name = "contrat_archive";

    var $table = "contrat_archive";
    var $clePrimaire = "contrat";
    var $typeCle = "N";
    var $required_field = array(
        "contrat",
        "datedemande",
        "datevente",
        "duree",
        "emplacement",
        "origine",
        "terme"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("emplacement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "contrat",
            "emplacement",
            "datedemande",
            "datevente",
            "origine",
            "dateterme",
            "terme",
            "duree",
            "montant",
            "monnaie",
            "valide",
            "observation",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['contrat'])) {
            $this->valF['contrat'] = ""; // -> requis
        } else {
            $this->valF['contrat'] = $val['contrat'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = ""; // -> requis
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if ($val['datedemande'] != "") {
            $this->valF['datedemande'] = $this->dateDB($val['datedemande']);
        }
        if ($val['datevente'] != "") {
            $this->valF['datevente'] = $this->dateDB($val['datevente']);
        }
        $this->valF['origine'] = $val['origine'];
        if ($val['dateterme'] != "") {
            $this->valF['dateterme'] = $this->dateDB($val['dateterme']);
        } else {
            $this->valF['dateterme'] = NULL;
        }
        $this->valF['terme'] = $val['terme'];
        if (!is_numeric($val['duree'])) {
            $this->valF['duree'] = ""; // -> requis
        } else {
            $this->valF['duree'] = $val['duree'];
        }
        if (!is_numeric($val['montant'])) {
            $this->valF['montant'] = NULL;
        } else {
            $this->valF['montant'] = $val['montant'];
        }
        if ($val['monnaie'] == "") {
            $this->valF['monnaie'] = NULL;
        } else {
            $this->valF['monnaie'] = $val['monnaie'];
        }
        if ($val['valide'] == 1 || $val['valide'] == "t" || $val['valide'] == "Oui") {
            $this->valF['valide'] = true;
        } else {
            $this->valF['valide'] = false;
        }
            $this->valF['observation'] = $val['observation'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("contrat", "hidden");
            $form->setType("emplacement", "text");
            $form->setType("datedemande", "date");
            $form->setType("datevente", "date");
            $form->setType("origine", "text");
            $form->setType("dateterme", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("montant", "text");
            $form->setType("monnaie", "text");
            $form->setType("valide", "checkbox");
            $form->setType("observation", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("contrat", "hiddenstatic");
            $form->setType("emplacement", "text");
            $form->setType("datedemande", "date");
            $form->setType("datevente", "date");
            $form->setType("origine", "text");
            $form->setType("dateterme", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("montant", "text");
            $form->setType("monnaie", "text");
            $form->setType("valide", "checkbox");
            $form->setType("observation", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("contrat", "hiddenstatic");
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("datedemande", "hiddenstatic");
            $form->setType("datevente", "hiddenstatic");
            $form->setType("origine", "hiddenstatic");
            $form->setType("dateterme", "hiddenstatic");
            $form->setType("terme", "hiddenstatic");
            $form->setType("duree", "hiddenstatic");
            $form->setType("montant", "hiddenstatic");
            $form->setType("monnaie", "hiddenstatic");
            $form->setType("valide", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("contrat", "static");
            $form->setType("emplacement", "static");
            $form->setType("datedemande", "datestatic");
            $form->setType("datevente", "datestatic");
            $form->setType("origine", "static");
            $form->setType("dateterme", "datestatic");
            $form->setType("terme", "static");
            $form->setType("duree", "static");
            $form->setType("montant", "static");
            $form->setType("monnaie", "static");
            $form->setType("valide", "checkboxstatic");
            $form->setType("observation", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('contrat','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('datedemande','fdate(this)');
        $form->setOnchange('datevente','fdate(this)');
        $form->setOnchange('dateterme','fdate(this)');
        $form->setOnchange('duree','VerifFloat(this)');
        $form->setOnchange('montant','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("contrat", 11);
        $form->setTaille("emplacement", 11);
        $form->setTaille("datedemande", 12);
        $form->setTaille("datevente", 12);
        $form->setTaille("origine", 30);
        $form->setTaille("dateterme", 12);
        $form->setTaille("terme", 15);
        $form->setTaille("duree", 20);
        $form->setTaille("montant", 30);
        $form->setTaille("monnaie", 30);
        $form->setTaille("valide", 1);
        $form->setTaille("observation", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("contrat", 11);
        $form->setMax("emplacement", 11);
        $form->setMax("datedemande", 12);
        $form->setMax("datevente", 12);
        $form->setMax("origine", 50);
        $form->setMax("dateterme", 12);
        $form->setMax("terme", 15);
        $form->setMax("duree", 20);
        $form->setMax("montant", 720898);
        $form->setMax("monnaie", 30);
        $form->setMax("valide", 1);
        $form->setMax("observation", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('contrat', __('contrat'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('datedemande', __('datedemande'));
        $form->setLib('datevente', __('datevente'));
        $form->setLib('origine', __('origine'));
        $form->setLib('dateterme', __('dateterme'));
        $form->setLib('terme', __('terme'));
        $form->setLib('duree', __('duree'));
        $form->setLib('montant', __('montant'));
        $form->setLib('monnaie', __('monnaie'));
        $form->setLib('valide', __('valide'));
        $form->setLib('observation', __('observation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
