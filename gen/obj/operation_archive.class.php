<?php
//$Id$ 
//gen openMairie le 14/09/2023 16:33

require_once "../obj/om_dbform.class.php";

class operation_archive_gen extends om_dbform {

    protected $_absolute_class_name = "operation_archive";

    var $table = "operation_archive";
    var $clePrimaire = "operation";
    var $typeCle = "N";
    var $required_field = array(
        "operation"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("numdossier");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "operation",
            "numdossier",
            "date",
            "heure",
            "emplacement",
            "societe_coordonnee",
            "pf_coordonnee",
            "etat",
            "categorie",
            "particulier",
            "emplacement_transfert",
            "observation",
            "consigne_acces",
            "prive",
            "consigne_acces_transfert",
            "edition_operation",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['operation'])) {
            $this->valF['operation'] = ""; // -> requis
        } else {
            $this->valF['operation'] = $val['operation'];
        }
        if ($val['numdossier'] == "") {
            $this->valF['numdossier'] = NULL;
        } else {
            $this->valF['numdossier'] = $val['numdossier'];
        }
        if ($val['date'] != "") {
            $this->valF['date'] = $this->dateDB($val['date']);
        } else {
            $this->valF['date'] = NULL;
        }
            $this->valF['heure'] = $val['heure'];
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
            $this->valF['societe_coordonnee'] = $val['societe_coordonnee'];
            $this->valF['pf_coordonnee'] = $val['pf_coordonnee'];
        if ($val['etat'] == "") {
            $this->valF['etat'] = NULL;
        } else {
            $this->valF['etat'] = $val['etat'];
        }
        if ($val['categorie'] == "") {
            $this->valF['categorie'] = NULL;
        } else {
            $this->valF['categorie'] = $val['categorie'];
        }
        if ($val['particulier'] == "") {
            $this->valF['particulier'] = NULL;
        } else {
            $this->valF['particulier'] = $val['particulier'];
        }
        if (!is_numeric($val['emplacement_transfert'])) {
            $this->valF['emplacement_transfert'] = NULL;
        } else {
            $this->valF['emplacement_transfert'] = $val['emplacement_transfert'];
        }
            $this->valF['observation'] = $val['observation'];
        if ($val['consigne_acces'] == "") {
            $this->valF['consigne_acces'] = NULL;
        } else {
            $this->valF['consigne_acces'] = $val['consigne_acces'];
        }
        if ($val['prive'] == 1 || $val['prive'] == "t" || $val['prive'] == "Oui") {
            $this->valF['prive'] = true;
        } else {
            $this->valF['prive'] = false;
        }
        if ($val['consigne_acces_transfert'] == "") {
            $this->valF['consigne_acces_transfert'] = NULL;
        } else {
            $this->valF['consigne_acces_transfert'] = $val['consigne_acces_transfert'];
        }
        if ($val['edition_operation'] == "") {
            $this->valF['edition_operation'] = NULL;
        } else {
            $this->valF['edition_operation'] = $val['edition_operation'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("operation", "hidden");
            $form->setType("numdossier", "text");
            $form->setType("date", "date");
            $form->setType("heure", "text");
            $form->setType("emplacement", "text");
            $form->setType("societe_coordonnee", "textarea");
            $form->setType("pf_coordonnee", "textarea");
            $form->setType("etat", "text");
            $form->setType("categorie", "text");
            $form->setType("particulier", "text");
            $form->setType("emplacement_transfert", "text");
            $form->setType("observation", "textarea");
            $form->setType("consigne_acces", "text");
            $form->setType("prive", "checkbox");
            $form->setType("consigne_acces_transfert", "text");
            $form->setType("edition_operation", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("operation", "hiddenstatic");
            $form->setType("numdossier", "text");
            $form->setType("date", "date");
            $form->setType("heure", "text");
            $form->setType("emplacement", "text");
            $form->setType("societe_coordonnee", "textarea");
            $form->setType("pf_coordonnee", "textarea");
            $form->setType("etat", "text");
            $form->setType("categorie", "text");
            $form->setType("particulier", "text");
            $form->setType("emplacement_transfert", "text");
            $form->setType("observation", "textarea");
            $form->setType("consigne_acces", "text");
            $form->setType("prive", "checkbox");
            $form->setType("consigne_acces_transfert", "text");
            $form->setType("edition_operation", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("operation", "hiddenstatic");
            $form->setType("numdossier", "hiddenstatic");
            $form->setType("date", "hiddenstatic");
            $form->setType("heure", "hiddenstatic");
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("societe_coordonnee", "hiddenstatic");
            $form->setType("pf_coordonnee", "hiddenstatic");
            $form->setType("etat", "hiddenstatic");
            $form->setType("categorie", "hiddenstatic");
            $form->setType("particulier", "hiddenstatic");
            $form->setType("emplacement_transfert", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("consigne_acces", "hiddenstatic");
            $form->setType("prive", "hiddenstatic");
            $form->setType("consigne_acces_transfert", "hiddenstatic");
            $form->setType("edition_operation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("operation", "static");
            $form->setType("numdossier", "static");
            $form->setType("date", "datestatic");
            $form->setType("heure", "static");
            $form->setType("emplacement", "static");
            $form->setType("societe_coordonnee", "textareastatic");
            $form->setType("pf_coordonnee", "textareastatic");
            $form->setType("etat", "static");
            $form->setType("categorie", "static");
            $form->setType("particulier", "static");
            $form->setType("emplacement_transfert", "static");
            $form->setType("observation", "textareastatic");
            $form->setType("consigne_acces", "static");
            $form->setType("prive", "checkboxstatic");
            $form->setType("consigne_acces_transfert", "static");
            $form->setType("edition_operation", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('operation','VerifNum(this)');
        $form->setOnchange('date','fdate(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('emplacement_transfert','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("operation", 11);
        $form->setTaille("numdossier", 10);
        $form->setTaille("date", 12);
        $form->setTaille("heure", 8);
        $form->setTaille("emplacement", 11);
        $form->setTaille("societe_coordonnee", 80);
        $form->setTaille("pf_coordonnee", 80);
        $form->setTaille("etat", 10);
        $form->setTaille("categorie", 20);
        $form->setTaille("particulier", 10);
        $form->setTaille("emplacement_transfert", 11);
        $form->setTaille("observation", 80);
        $form->setTaille("consigne_acces", 30);
        $form->setTaille("prive", 1);
        $form->setTaille("consigne_acces_transfert", 30);
        $form->setTaille("edition_operation", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("operation", 11);
        $form->setMax("numdossier", 10);
        $form->setMax("date", 12);
        $form->setMax("heure", 8);
        $form->setMax("emplacement", 11);
        $form->setMax("societe_coordonnee", 6);
        $form->setMax("pf_coordonnee", 6);
        $form->setMax("etat", 6);
        $form->setMax("categorie", 20);
        $form->setMax("particulier", 3);
        $form->setMax("emplacement_transfert", 11);
        $form->setMax("observation", 6);
        $form->setMax("consigne_acces", 255);
        $form->setMax("prive", 1);
        $form->setMax("consigne_acces_transfert", 255);
        $form->setMax("edition_operation", 40);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('operation', __('operation'));
        $form->setLib('numdossier', __('numdossier'));
        $form->setLib('date', __('date'));
        $form->setLib('heure', __('heure'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('societe_coordonnee', __('societe_coordonnee'));
        $form->setLib('pf_coordonnee', __('pf_coordonnee'));
        $form->setLib('etat', __('etat'));
        $form->setLib('categorie', __('categorie'));
        $form->setLib('particulier', __('particulier'));
        $form->setLib('emplacement_transfert', __('emplacement_transfert'));
        $form->setLib('observation', __('observation'));
        $form->setLib('consigne_acces', __('consigne_acces'));
        $form->setLib('prive', __('prive'));
        $form->setLib('consigne_acces_transfert', __('consigne_acces_transfert'));
        $form->setLib('edition_operation', __('edition_operation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
