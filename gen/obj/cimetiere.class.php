<?php
//$Id$ 
//gen openMairie le 16/08/2022 16:48

require_once "../obj/om_dbform.class.php";

class cimetiere_gen extends om_dbform {

    protected $_absolute_class_name = "cimetiere";

    var $table = "cimetiere";
    var $clePrimaire = "cimetiere";
    var $typeCle = "N";
    var $required_field = array(
        "cimetiere",
        "cimetierelib"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("cimetierelib");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "cimetiere",
            "cimetierelib",
            "adresse1",
            "adresse2",
            "cp",
            "ville",
            "observations",
            "geom",
            "information_generale",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['cimetiere'])) {
            $this->valF['cimetiere'] = ""; // -> requis
        } else {
            $this->valF['cimetiere'] = $val['cimetiere'];
        }
        $this->valF['cimetierelib'] = $val['cimetierelib'];
        if ($val['adresse1'] == "") {
            $this->valF['adresse1'] = NULL;
        } else {
            $this->valF['adresse1'] = $val['adresse1'];
        }
        if ($val['adresse2'] == "") {
            $this->valF['adresse2'] = NULL;
        } else {
            $this->valF['adresse2'] = $val['adresse2'];
        }
        if ($val['cp'] == "") {
            $this->valF['cp'] = NULL;
        } else {
            $this->valF['cp'] = $val['cp'];
        }
        if ($val['ville'] == "") {
            $this->valF['ville'] = NULL;
        } else {
            $this->valF['ville'] = $val['ville'];
        }
            $this->valF['observations'] = $val['observations'];
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
            $this->valF['information_generale'] = $val['information_generale'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("cimetiere", "hidden");
            $form->setType("cimetierelib", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("observations", "textarea");
            $form->setType("geom", "geom");
            $form->setType("information_generale", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("cimetiere", "hiddenstatic");
            $form->setType("cimetierelib", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("observations", "textarea");
            $form->setType("geom", "geom");
            $form->setType("information_generale", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("cimetiere", "hiddenstatic");
            $form->setType("cimetierelib", "hiddenstatic");
            $form->setType("adresse1", "hiddenstatic");
            $form->setType("adresse2", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
            $form->setType("observations", "hiddenstatic");
            $form->setType("geom", "geom");
            $form->setType("information_generale", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("cimetiere", "static");
            $form->setType("cimetierelib", "static");
            $form->setType("adresse1", "static");
            $form->setType("adresse2", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
            $form->setType("observations", "textareastatic");
            $form->setType("geom", "geom");
            $form->setType("information_generale", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('cimetiere','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("cimetiere", 11);
        $form->setTaille("cimetierelib", 30);
        $form->setTaille("adresse1", 30);
        $form->setTaille("adresse2", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
        $form->setTaille("observations", 80);
        $form->setTaille("geom", 30);
        $form->setTaille("information_generale", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("cimetiere", 11);
        $form->setMax("cimetierelib", 40);
        $form->setMax("adresse1", 40);
        $form->setMax("adresse2", 40);
        $form->setMax("cp", 5);
        $form->setMax("ville", 40);
        $form->setMax("observations", 6);
        $form->setMax("geom", 551444);
        $form->setMax("information_generale", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('cimetiere', __('cimetiere'));
        $form->setLib('cimetierelib', __('cimetierelib'));
        $form->setLib('adresse1', __('adresse1'));
        $form->setLib('adresse2', __('adresse2'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
        $form->setLib('observations', __('observations'));
        $form->setLib('geom', __('geom'));
        $form->setLib('information_generale', __('information_generale'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("cimetiere", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : zone
        $this->rechercheTable($this->f->db, "zone", "cimetiere", $id);
    }


}
