<?php
//$Id$ 
//gen openMairie le 14/09/2023 16:33

require_once "../obj/om_dbform.class.php";

class emplacement_gen extends om_dbform {

    protected $_absolute_class_name = "emplacement";

    var $table = "emplacement";
    var $clePrimaire = "emplacement";
    var $typeCle = "N";
    var $required_field = array(
        "emplacement"
    );
    
    var $foreign_keys_extended = array(
        "plans" => array("plans", ),
        "sepulture_type" => array("sepulture_type", ),
        "voie" => array("voie", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nature");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "emplacement",
            "nature",
            "numero",
            "complement",
            "voie",
            "numerocadastre",
            "famille",
            "numeroacte",
            "datevente",
            "terme",
            "duree",
            "dateterme",
            "nombreplace",
            "placeoccupe",
            "superficie",
            "placeconstat",
            "dateconstat",
            "observation",
            "plans",
            "positionx",
            "positiony",
            "photo",
            "libre",
            "sepulturetype",
            "daterenouvellement",
            "typeconcession",
            "temp1",
            "temp2",
            "temp3",
            "temp4",
            "temp5",
            "videsanitaire",
            "semelle",
            "etatsemelle",
            "monument",
            "etatmonument",
            "largeur",
            "profondeur",
            "abandon",
            "date_abandon",
            "dateacte",
            "geom",
            "pgeom",
            "etat_case_plan_en_coupe",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plans() {
        return "SELECT plans.plans, plans.planslib FROM ".DB_PREFIXE."plans ORDER BY plans.planslib ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plans_by_id() {
        return "SELECT plans.plans, plans.planslib FROM ".DB_PREFIXE."plans WHERE plans = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulturetype() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE ((sepulture_type.om_validite_debut IS NULL AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)) OR (sepulture_type.om_validite_debut <= CURRENT_DATE AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE))) ORDER BY sepulture_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulturetype_by_id() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE sepulture_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voie() {
        return "SELECT voie.voie, voie.zone FROM ".DB_PREFIXE."voie ORDER BY voie.zone ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voie_by_id() {
        return "SELECT voie.voie, voie.zone FROM ".DB_PREFIXE."voie WHERE voie = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = ""; // -> requis
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        if ($val['nature'] == "") {
            $this->valF['nature'] = NULL;
        } else {
            $this->valF['nature'] = $val['nature'];
        }
        if (!is_numeric($val['numero'])) {
            $this->valF['numero'] = NULL;
        } else {
            $this->valF['numero'] = $val['numero'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if (!is_numeric($val['voie'])) {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if ($val['numerocadastre'] == "") {
            $this->valF['numerocadastre'] = NULL;
        } else {
            $this->valF['numerocadastre'] = $val['numerocadastre'];
        }
        if ($val['famille'] == "") {
            $this->valF['famille'] = NULL;
        } else {
            $this->valF['famille'] = $val['famille'];
        }
        if ($val['numeroacte'] == "") {
            $this->valF['numeroacte'] = NULL;
        } else {
            $this->valF['numeroacte'] = $val['numeroacte'];
        }
        if ($val['datevente'] != "") {
            $this->valF['datevente'] = $this->dateDB($val['datevente']);
        } else {
            $this->valF['datevente'] = NULL;
        }
        if ($val['terme'] == "") {
            $this->valF['terme'] = NULL;
        } else {
            $this->valF['terme'] = $val['terme'];
        }
        if (!is_numeric($val['duree'])) {
            $this->valF['duree'] = NULL;
        } else {
            $this->valF['duree'] = $val['duree'];
        }
        if ($val['dateterme'] != "") {
            $this->valF['dateterme'] = $this->dateDB($val['dateterme']);
        } else {
            $this->valF['dateterme'] = NULL;
        }
        if (!is_numeric($val['nombreplace'])) {
            $this->valF['nombreplace'] = NULL;
        } else {
            $this->valF['nombreplace'] = $val['nombreplace'];
        }
        if (!is_numeric($val['placeoccupe'])) {
            $this->valF['placeoccupe'] = NULL;
        } else {
            $this->valF['placeoccupe'] = $val['placeoccupe'];
        }
        if (!is_numeric($val['superficie'])) {
            $this->valF['superficie'] = NULL;
        } else {
            $this->valF['superficie'] = $val['superficie'];
        }
        if (!is_numeric($val['placeconstat'])) {
            $this->valF['placeconstat'] = NULL;
        } else {
            $this->valF['placeconstat'] = $val['placeconstat'];
        }
        if ($val['dateconstat'] != "") {
            $this->valF['dateconstat'] = $this->dateDB($val['dateconstat']);
        } else {
            $this->valF['dateconstat'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if (!is_numeric($val['plans'])) {
            $this->valF['plans'] = NULL;
        } else {
            $this->valF['plans'] = $val['plans'];
        }
        if (!is_numeric($val['positionx'])) {
            $this->valF['positionx'] = NULL;
        } else {
            $this->valF['positionx'] = $val['positionx'];
        }
        if (!is_numeric($val['positiony'])) {
            $this->valF['positiony'] = NULL;
        } else {
            $this->valF['positiony'] = $val['positiony'];
        }
        if ($val['photo'] == "") {
            $this->valF['photo'] = NULL;
        } else {
            $this->valF['photo'] = $val['photo'];
        }
        if ($val['libre'] == "") {
            $this->valF['libre'] = NULL;
        } else {
            $this->valF['libre'] = $val['libre'];
        }
        if (!is_numeric($val['sepulturetype'])) {
            $this->valF['sepulturetype'] = NULL;
        } else {
            $this->valF['sepulturetype'] = $val['sepulturetype'];
        }
        if ($val['daterenouvellement'] != "") {
            $this->valF['daterenouvellement'] = $this->dateDB($val['daterenouvellement']);
        } else {
            $this->valF['daterenouvellement'] = NULL;
        }
        if ($val['typeconcession'] == "") {
            $this->valF['typeconcession'] = NULL;
        } else {
            $this->valF['typeconcession'] = $val['typeconcession'];
        }
        if ($val['temp1'] == "") {
            $this->valF['temp1'] = NULL;
        } else {
            $this->valF['temp1'] = $val['temp1'];
        }
        if ($val['temp2'] == "") {
            $this->valF['temp2'] = NULL;
        } else {
            $this->valF['temp2'] = $val['temp2'];
        }
        if ($val['temp3'] == "") {
            $this->valF['temp3'] = NULL;
        } else {
            $this->valF['temp3'] = $val['temp3'];
        }
        if ($val['temp4'] == "") {
            $this->valF['temp4'] = NULL;
        } else {
            $this->valF['temp4'] = $val['temp4'];
        }
        if ($val['temp5'] == "") {
            $this->valF['temp5'] = NULL;
        } else {
            $this->valF['temp5'] = $val['temp5'];
        }
        if ($val['videsanitaire'] == "") {
            $this->valF['videsanitaire'] = NULL;
        } else {
            $this->valF['videsanitaire'] = $val['videsanitaire'];
        }
        if ($val['semelle'] == "") {
            $this->valF['semelle'] = NULL;
        } else {
            $this->valF['semelle'] = $val['semelle'];
        }
        if ($val['etatsemelle'] == "") {
            $this->valF['etatsemelle'] = NULL;
        } else {
            $this->valF['etatsemelle'] = $val['etatsemelle'];
        }
        if ($val['monument'] == "") {
            $this->valF['monument'] = NULL;
        } else {
            $this->valF['monument'] = $val['monument'];
        }
        if ($val['etatmonument'] == "") {
            $this->valF['etatmonument'] = NULL;
        } else {
            $this->valF['etatmonument'] = $val['etatmonument'];
        }
        if (!is_numeric($val['largeur'])) {
            $this->valF['largeur'] = NULL;
        } else {
            $this->valF['largeur'] = $val['largeur'];
        }
        if (!is_numeric($val['profondeur'])) {
            $this->valF['profondeur'] = NULL;
        } else {
            $this->valF['profondeur'] = $val['profondeur'];
        }
        if ($val['abandon'] == "") {
            $this->valF['abandon'] = NULL;
        } else {
            $this->valF['abandon'] = $val['abandon'];
        }
        if ($val['date_abandon'] != "") {
            $this->valF['date_abandon'] = $this->dateDB($val['date_abandon']);
        } else {
            $this->valF['date_abandon'] = NULL;
        }
        if ($val['dateacte'] != "") {
            $this->valF['dateacte'] = $this->dateDB($val['dateacte']);
        } else {
            $this->valF['dateacte'] = NULL;
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
        if ($val['pgeom'] == "") {
            unset($this->valF['pgeom']);
        } else {
            $this->valF['pgeom'] = $val['pgeom'];
        }
        if ($val['etat_case_plan_en_coupe'] == "") {
            $this->valF['etat_case_plan_en_coupe'] = NULL;
        } else {
            $this->valF['etat_case_plan_en_coupe'] = $val['etat_case_plan_en_coupe'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("emplacement", "hidden");
            $form->setType("nature", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            $form->setType("numerocadastre", "text");
            $form->setType("famille", "text");
            $form->setType("numeroacte", "text");
            $form->setType("datevente", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("dateterme", "date");
            $form->setType("nombreplace", "text");
            $form->setType("placeoccupe", "text");
            $form->setType("superficie", "text");
            $form->setType("placeconstat", "text");
            $form->setType("dateconstat", "date");
            $form->setType("observation", "textarea");
            if ($this->is_in_context_of_foreign_key("plans", $this->retourformulaire)) {
                $form->setType("plans", "selecthiddenstatic");
            } else {
                $form->setType("plans", "select");
            }
            $form->setType("positionx", "text");
            $form->setType("positiony", "text");
            $form->setType("photo", "text");
            $form->setType("libre", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulturetype", "selecthiddenstatic");
            } else {
                $form->setType("sepulturetype", "select");
            }
            $form->setType("daterenouvellement", "date");
            $form->setType("typeconcession", "text");
            $form->setType("temp1", "text");
            $form->setType("temp2", "text");
            $form->setType("temp3", "text");
            $form->setType("temp4", "text");
            $form->setType("temp5", "text");
            $form->setType("videsanitaire", "text");
            $form->setType("semelle", "text");
            $form->setType("etatsemelle", "text");
            $form->setType("monument", "text");
            $form->setType("etatmonument", "text");
            $form->setType("largeur", "text");
            $form->setType("profondeur", "text");
            $form->setType("abandon", "text");
            $form->setType("date_abandon", "date");
            $form->setType("dateacte", "date");
            $form->setType("geom", "geom");
            $form->setType("pgeom", "geom");
            $form->setType("etat_case_plan_en_coupe", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("nature", "text");
            $form->setType("numero", "text");
            $form->setType("complement", "text");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            $form->setType("numerocadastre", "text");
            $form->setType("famille", "text");
            $form->setType("numeroacte", "text");
            $form->setType("datevente", "date");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("dateterme", "date");
            $form->setType("nombreplace", "text");
            $form->setType("placeoccupe", "text");
            $form->setType("superficie", "text");
            $form->setType("placeconstat", "text");
            $form->setType("dateconstat", "date");
            $form->setType("observation", "textarea");
            if ($this->is_in_context_of_foreign_key("plans", $this->retourformulaire)) {
                $form->setType("plans", "selecthiddenstatic");
            } else {
                $form->setType("plans", "select");
            }
            $form->setType("positionx", "text");
            $form->setType("positiony", "text");
            $form->setType("photo", "text");
            $form->setType("libre", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulturetype", "selecthiddenstatic");
            } else {
                $form->setType("sepulturetype", "select");
            }
            $form->setType("daterenouvellement", "date");
            $form->setType("typeconcession", "text");
            $form->setType("temp1", "text");
            $form->setType("temp2", "text");
            $form->setType("temp3", "text");
            $form->setType("temp4", "text");
            $form->setType("temp5", "text");
            $form->setType("videsanitaire", "text");
            $form->setType("semelle", "text");
            $form->setType("etatsemelle", "text");
            $form->setType("monument", "text");
            $form->setType("etatmonument", "text");
            $form->setType("largeur", "text");
            $form->setType("profondeur", "text");
            $form->setType("abandon", "text");
            $form->setType("date_abandon", "date");
            $form->setType("dateacte", "date");
            $form->setType("geom", "geom");
            $form->setType("pgeom", "geom");
            $form->setType("etat_case_plan_en_coupe", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("emplacement", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("voie", "selectstatic");
            $form->setType("numerocadastre", "hiddenstatic");
            $form->setType("famille", "hiddenstatic");
            $form->setType("numeroacte", "hiddenstatic");
            $form->setType("datevente", "hiddenstatic");
            $form->setType("terme", "hiddenstatic");
            $form->setType("duree", "hiddenstatic");
            $form->setType("dateterme", "hiddenstatic");
            $form->setType("nombreplace", "hiddenstatic");
            $form->setType("placeoccupe", "hiddenstatic");
            $form->setType("superficie", "hiddenstatic");
            $form->setType("placeconstat", "hiddenstatic");
            $form->setType("dateconstat", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("plans", "selectstatic");
            $form->setType("positionx", "hiddenstatic");
            $form->setType("positiony", "hiddenstatic");
            $form->setType("photo", "hiddenstatic");
            $form->setType("libre", "hiddenstatic");
            $form->setType("sepulturetype", "selectstatic");
            $form->setType("daterenouvellement", "hiddenstatic");
            $form->setType("typeconcession", "hiddenstatic");
            $form->setType("temp1", "hiddenstatic");
            $form->setType("temp2", "hiddenstatic");
            $form->setType("temp3", "hiddenstatic");
            $form->setType("temp4", "hiddenstatic");
            $form->setType("temp5", "hiddenstatic");
            $form->setType("videsanitaire", "hiddenstatic");
            $form->setType("semelle", "hiddenstatic");
            $form->setType("etatsemelle", "hiddenstatic");
            $form->setType("monument", "hiddenstatic");
            $form->setType("etatmonument", "hiddenstatic");
            $form->setType("largeur", "hiddenstatic");
            $form->setType("profondeur", "hiddenstatic");
            $form->setType("abandon", "hiddenstatic");
            $form->setType("date_abandon", "hiddenstatic");
            $form->setType("dateacte", "hiddenstatic");
            $form->setType("geom", "geom");
            $form->setType("pgeom", "geom");
            $form->setType("etat_case_plan_en_coupe", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("emplacement", "static");
            $form->setType("nature", "static");
            $form->setType("numero", "static");
            $form->setType("complement", "static");
            $form->setType("voie", "selectstatic");
            $form->setType("numerocadastre", "static");
            $form->setType("famille", "static");
            $form->setType("numeroacte", "static");
            $form->setType("datevente", "datestatic");
            $form->setType("terme", "static");
            $form->setType("duree", "static");
            $form->setType("dateterme", "datestatic");
            $form->setType("nombreplace", "static");
            $form->setType("placeoccupe", "static");
            $form->setType("superficie", "static");
            $form->setType("placeconstat", "static");
            $form->setType("dateconstat", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("plans", "selectstatic");
            $form->setType("positionx", "static");
            $form->setType("positiony", "static");
            $form->setType("photo", "static");
            $form->setType("libre", "static");
            $form->setType("sepulturetype", "selectstatic");
            $form->setType("daterenouvellement", "datestatic");
            $form->setType("typeconcession", "static");
            $form->setType("temp1", "static");
            $form->setType("temp2", "static");
            $form->setType("temp3", "static");
            $form->setType("temp4", "static");
            $form->setType("temp5", "static");
            $form->setType("videsanitaire", "static");
            $form->setType("semelle", "static");
            $form->setType("etatsemelle", "static");
            $form->setType("monument", "static");
            $form->setType("etatmonument", "static");
            $form->setType("largeur", "static");
            $form->setType("profondeur", "static");
            $form->setType("abandon", "static");
            $form->setType("date_abandon", "datestatic");
            $form->setType("dateacte", "datestatic");
            $form->setType("geom", "geom");
            $form->setType("pgeom", "geom");
            $form->setType("etat_case_plan_en_coupe", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('numero','VerifNum(this)');
        $form->setOnchange('voie','VerifNum(this)');
        $form->setOnchange('datevente','fdate(this)');
        $form->setOnchange('duree','VerifFloat(this)');
        $form->setOnchange('dateterme','fdate(this)');
        $form->setOnchange('nombreplace','VerifFloat(this)');
        $form->setOnchange('placeoccupe','VerifFloat(this)');
        $form->setOnchange('superficie','VerifFloat(this)');
        $form->setOnchange('placeconstat','VerifFloat(this)');
        $form->setOnchange('dateconstat','fdate(this)');
        $form->setOnchange('plans','VerifNum(this)');
        $form->setOnchange('positionx','VerifFloat(this)');
        $form->setOnchange('positiony','VerifFloat(this)');
        $form->setOnchange('sepulturetype','VerifNum(this)');
        $form->setOnchange('daterenouvellement','fdate(this)');
        $form->setOnchange('largeur','VerifFloat(this)');
        $form->setOnchange('profondeur','VerifFloat(this)');
        $form->setOnchange('date_abandon','fdate(this)');
        $form->setOnchange('dateacte','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("emplacement", 11);
        $form->setTaille("nature", 20);
        $form->setTaille("numero", 11);
        $form->setTaille("complement", 10);
        $form->setTaille("voie", 11);
        $form->setTaille("numerocadastre", 15);
        $form->setTaille("famille", 30);
        $form->setTaille("numeroacte", 30);
        $form->setTaille("datevente", 12);
        $form->setTaille("terme", 15);
        $form->setTaille("duree", 20);
        $form->setTaille("dateterme", 12);
        $form->setTaille("nombreplace", 20);
        $form->setTaille("placeoccupe", 20);
        $form->setTaille("superficie", 20);
        $form->setTaille("placeconstat", 20);
        $form->setTaille("dateconstat", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("plans", 11);
        $form->setTaille("positionx", 20);
        $form->setTaille("positiony", 20);
        $form->setTaille("photo", 20);
        $form->setTaille("libre", 10);
        $form->setTaille("sepulturetype", 11);
        $form->setTaille("daterenouvellement", 12);
        $form->setTaille("typeconcession", 30);
        $form->setTaille("temp1", 30);
        $form->setTaille("temp2", 30);
        $form->setTaille("temp3", 30);
        $form->setTaille("temp4", 30);
        $form->setTaille("temp5", 30);
        $form->setTaille("videsanitaire", 10);
        $form->setTaille("semelle", 20);
        $form->setTaille("etatsemelle", 10);
        $form->setTaille("monument", 30);
        $form->setTaille("etatmonument", 10);
        $form->setTaille("largeur", 20);
        $form->setTaille("profondeur", 20);
        $form->setTaille("abandon", 10);
        $form->setTaille("date_abandon", 12);
        $form->setTaille("dateacte", 12);
        $form->setTaille("geom", 30);
        $form->setTaille("pgeom", 30);
        $form->setTaille("etat_case_plan_en_coupe", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("emplacement", 11);
        $form->setMax("nature", 20);
        $form->setMax("numero", 11);
        $form->setMax("complement", 6);
        $form->setMax("voie", 11);
        $form->setMax("numerocadastre", 15);
        $form->setMax("famille", 40);
        $form->setMax("numeroacte", 30);
        $form->setMax("datevente", 12);
        $form->setMax("terme", 15);
        $form->setMax("duree", 20);
        $form->setMax("dateterme", 12);
        $form->setMax("nombreplace", 20);
        $form->setMax("placeoccupe", 20);
        $form->setMax("superficie", 20);
        $form->setMax("placeconstat", 20);
        $form->setMax("dateconstat", 12);
        $form->setMax("observation", 6);
        $form->setMax("plans", 11);
        $form->setMax("positionx", 20);
        $form->setMax("positiony", 20);
        $form->setMax("photo", 20);
        $form->setMax("libre", 3);
        $form->setMax("sepulturetype", 11);
        $form->setMax("daterenouvellement", 12);
        $form->setMax("typeconcession", 40);
        $form->setMax("temp1", 100);
        $form->setMax("temp2", 100);
        $form->setMax("temp3", 100);
        $form->setMax("temp4", 100);
        $form->setMax("temp5", 100);
        $form->setMax("videsanitaire", 3);
        $form->setMax("semelle", 20);
        $form->setMax("etatsemelle", 10);
        $form->setMax("monument", 255);
        $form->setMax("etatmonument", 10);
        $form->setMax("largeur", 20);
        $form->setMax("profondeur", 20);
        $form->setMax("abandon", 3);
        $form->setMax("date_abandon", 12);
        $form->setMax("dateacte", 12);
        $form->setMax("geom", 551424);
        $form->setMax("pgeom", 551444);
        $form->setMax("etat_case_plan_en_coupe", 255);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('nature', __('nature'));
        $form->setLib('numero', __('numero'));
        $form->setLib('complement', __('complement'));
        $form->setLib('voie', __('voie'));
        $form->setLib('numerocadastre', __('numerocadastre'));
        $form->setLib('famille', __('famille'));
        $form->setLib('numeroacte', __('numeroacte'));
        $form->setLib('datevente', __('datevente'));
        $form->setLib('terme', __('terme'));
        $form->setLib('duree', __('duree'));
        $form->setLib('dateterme', __('dateterme'));
        $form->setLib('nombreplace', __('nombreplace'));
        $form->setLib('placeoccupe', __('placeoccupe'));
        $form->setLib('superficie', __('superficie'));
        $form->setLib('placeconstat', __('placeconstat'));
        $form->setLib('dateconstat', __('dateconstat'));
        $form->setLib('observation', __('observation'));
        $form->setLib('plans', __('plans'));
        $form->setLib('positionx', __('positionx'));
        $form->setLib('positiony', __('positiony'));
        $form->setLib('photo', __('photo'));
        $form->setLib('libre', __('libre'));
        $form->setLib('sepulturetype', __('sepulturetype'));
        $form->setLib('daterenouvellement', __('daterenouvellement'));
        $form->setLib('typeconcession', __('typeconcession'));
        $form->setLib('temp1', __('temp1'));
        $form->setLib('temp2', __('temp2'));
        $form->setLib('temp3', __('temp3'));
        $form->setLib('temp4', __('temp4'));
        $form->setLib('temp5', __('temp5'));
        $form->setLib('videsanitaire', __('videsanitaire'));
        $form->setLib('semelle', __('semelle'));
        $form->setLib('etatsemelle', __('etatsemelle'));
        $form->setLib('monument', __('monument'));
        $form->setLib('etatmonument', __('etatmonument'));
        $form->setLib('largeur', __('largeur'));
        $form->setLib('profondeur', __('profondeur'));
        $form->setLib('abandon', __('abandon'));
        $form->setLib('date_abandon', __('date_abandon'));
        $form->setLib('dateacte', __('dateacte'));
        $form->setLib('geom', __('geom'));
        $form->setLib('pgeom', __('pgeom'));
        $form->setLib('etat_case_plan_en_coupe', __('etat_case_plan_en_coupe'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // plans
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "plans",
            $this->get_var_sql_forminc__sql("plans"),
            $this->get_var_sql_forminc__sql("plans_by_id"),
            false
        );
        // sepulturetype
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "sepulturetype",
            $this->get_var_sql_forminc__sql("sepulturetype"),
            $this->get_var_sql_forminc__sql("sepulturetype_by_id"),
            true
        );
        // voie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "voie",
            $this->get_var_sql_forminc__sql("voie"),
            $this->get_var_sql_forminc__sql("voie_by_id"),
            false
        );
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("emplacement", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
        // pgeom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("emplacement", $this->getParameter("idx"), "1");
            $form->setSelect("pgeom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('plans', $this->retourformulaire))
                $form->setVal('plans', $idxformulaire);
            if($this->is_in_context_of_foreign_key('sepulture_type', $this->retourformulaire))
                $form->setVal('sepulturetype', $idxformulaire);
            if($this->is_in_context_of_foreign_key('voie', $this->retourformulaire))
                $form->setVal('voie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorisation
        $this->rechercheTable($this->f->db, "autorisation", "emplacement", $id);
        // Verification de la cle secondaire : contrat
        $this->rechercheTable($this->f->db, "contrat", "emplacement", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "emplacement", $id);
        // Verification de la cle secondaire : defunt
        $this->rechercheTable($this->f->db, "defunt", "emplacement", $id);
        // Verification de la cle secondaire : dossier
        $this->rechercheTable($this->f->db, "dossier", "emplacement", $id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "emplacement", $id);
        // Verification de la cle secondaire : operation
        $this->rechercheTable($this->f->db, "operation", "emplacement", $id);
        // Verification de la cle secondaire : travaux
        $this->rechercheTable($this->f->db, "travaux", "emplacement", $id);
    }


}
