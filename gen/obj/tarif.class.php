<?php
//$Id$ 
//gen openMairie le 09/09/2020 11:38

require_once "../obj/om_dbform.class.php";

class tarif_gen extends om_dbform {

    protected $_absolute_class_name = "tarif";

    var $table = "tarif";
    var $clePrimaire = "tarif";
    var $typeCle = "N";
    var $required_field = array(
        "annee",
        "monnaie",
        "montant",
        "origine",
        "tarif",
        "terme"
    );
    var $unique_key = array(
      array("annee","duree","nature","origine","sepulture_type","terme"),
    );
    var $foreign_keys_extended = array(
        "sepulture_type" => array("sepulture_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("annee");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "tarif",
            "annee",
            "origine",
            "terme",
            "duree",
            "nature",
            "sepulture_type",
            "montant",
            "monnaie",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulture_type() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE ((sepulture_type.om_validite_debut IS NULL AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE)) OR (sepulture_type.om_validite_debut <= CURRENT_DATE AND (sepulture_type.om_validite_fin IS NULL OR sepulture_type.om_validite_fin > CURRENT_DATE))) ORDER BY sepulture_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_sepulture_type_by_id() {
        return "SELECT sepulture_type.sepulture_type, sepulture_type.libelle FROM ".DB_PREFIXE."sepulture_type WHERE sepulture_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['tarif'])) {
            $this->valF['tarif'] = ""; // -> requis
        } else {
            $this->valF['tarif'] = $val['tarif'];
        }
        if (!is_numeric($val['annee'])) {
            $this->valF['annee'] = ""; // -> requis
        } else {
            $this->valF['annee'] = $val['annee'];
        }
        $this->valF['origine'] = $val['origine'];
        $this->valF['terme'] = $val['terme'];
        if (!is_numeric($val['duree'])) {
            $this->valF['duree'] = NULL;
        } else {
            $this->valF['duree'] = $val['duree'];
        }
        if ($val['nature'] == "") {
            $this->valF['nature'] = NULL;
        } else {
            $this->valF['nature'] = $val['nature'];
        }
        if (!is_numeric($val['sepulture_type'])) {
            $this->valF['sepulture_type'] = NULL;
        } else {
            $this->valF['sepulture_type'] = $val['sepulture_type'];
        }
        if (!is_numeric($val['montant'])) {
            $this->valF['montant'] = ""; // -> requis
        } else {
            $this->valF['montant'] = $val['montant'];
        }
        $this->valF['monnaie'] = $val['monnaie'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("tarif", "hidden");
            $form->setType("annee", "text");
            $form->setType("origine", "text");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulture_type", "selecthiddenstatic");
            } else {
                $form->setType("sepulture_type", "select");
            }
            $form->setType("montant", "text");
            $form->setType("monnaie", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("tarif", "hiddenstatic");
            $form->setType("annee", "text");
            $form->setType("origine", "text");
            $form->setType("terme", "text");
            $form->setType("duree", "text");
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("sepulture_type", $this->retourformulaire)) {
                $form->setType("sepulture_type", "selecthiddenstatic");
            } else {
                $form->setType("sepulture_type", "select");
            }
            $form->setType("montant", "text");
            $form->setType("monnaie", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("tarif", "hiddenstatic");
            $form->setType("annee", "hiddenstatic");
            $form->setType("origine", "hiddenstatic");
            $form->setType("terme", "hiddenstatic");
            $form->setType("duree", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("sepulture_type", "selectstatic");
            $form->setType("montant", "hiddenstatic");
            $form->setType("monnaie", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("tarif", "static");
            $form->setType("annee", "static");
            $form->setType("origine", "static");
            $form->setType("terme", "static");
            $form->setType("duree", "static");
            $form->setType("nature", "static");
            $form->setType("sepulture_type", "selectstatic");
            $form->setType("montant", "static");
            $form->setType("monnaie", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('tarif','VerifNum(this)');
        $form->setOnchange('annee','VerifNum(this)');
        $form->setOnchange('duree','VerifFloat(this)');
        $form->setOnchange('sepulture_type','VerifNum(this)');
        $form->setOnchange('montant','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("tarif", 11);
        $form->setTaille("annee", 11);
        $form->setTaille("origine", 30);
        $form->setTaille("terme", 15);
        $form->setTaille("duree", 20);
        $form->setTaille("nature", 15);
        $form->setTaille("sepulture_type", 11);
        $form->setTaille("montant", 30);
        $form->setTaille("monnaie", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("tarif", 11);
        $form->setMax("annee", 11);
        $form->setMax("origine", 50);
        $form->setMax("terme", 15);
        $form->setMax("duree", 20);
        $form->setMax("nature", 15);
        $form->setMax("sepulture_type", 11);
        $form->setMax("montant", 720898);
        $form->setMax("monnaie", 30);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('tarif', __('tarif'));
        $form->setLib('annee', __('annee'));
        $form->setLib('origine', __('origine'));
        $form->setLib('terme', __('terme'));
        $form->setLib('duree', __('duree'));
        $form->setLib('nature', __('nature'));
        $form->setLib('sepulture_type', __('sepulture_type'));
        $form->setLib('montant', __('montant'));
        $form->setLib('monnaie', __('monnaie'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // sepulture_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "sepulture_type",
            $this->get_var_sql_forminc__sql("sepulture_type"),
            $this->get_var_sql_forminc__sql("sepulture_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('sepulture_type', $this->retourformulaire))
                $form->setVal('sepulture_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
