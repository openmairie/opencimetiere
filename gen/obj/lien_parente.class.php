<?php
//$Id$ 
//gen openMairie le 12/10/2022 18:22

require_once "../obj/om_dbform.class.php";

class lien_parente_gen extends om_dbform {

    protected $_absolute_class_name = "lien_parente";

    var $table = "lien_parente";
    var $clePrimaire = "lien_parente";
    var $typeCle = "N";
    var $required_field = array(
        "lien_parente"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_parente",
            "libelle",
            "niveau",
            "meme_personne",
            "lien_inverse",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_parente'])) {
            $this->valF['lien_parente'] = ""; // -> requis
        } else {
            $this->valF['lien_parente'] = $val['lien_parente'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['niveau'])) {
            $this->valF['niveau'] = NULL;
        } else {
            $this->valF['niveau'] = $val['niveau'];
        }
        if ($val['meme_personne'] == 1 || $val['meme_personne'] == "t" || $val['meme_personne'] == "Oui") {
            $this->valF['meme_personne'] = true;
        } else {
            $this->valF['meme_personne'] = false;
        }
        if ($val['lien_inverse'] == "") {
            $this->valF['lien_inverse'] = NULL;
        } else {
            $this->valF['lien_inverse'] = $val['lien_inverse'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_parente", "hidden");
            $form->setType("libelle", "text");
            $form->setType("niveau", "text");
            $form->setType("meme_personne", "checkbox");
            $form->setType("lien_inverse", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_parente", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("niveau", "text");
            $form->setType("meme_personne", "checkbox");
            $form->setType("lien_inverse", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_parente", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("niveau", "hiddenstatic");
            $form->setType("meme_personne", "hiddenstatic");
            $form->setType("lien_inverse", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_parente", "static");
            $form->setType("libelle", "static");
            $form->setType("niveau", "static");
            $form->setType("meme_personne", "checkboxstatic");
            $form->setType("lien_inverse", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_parente','VerifNum(this)');
        $form->setOnchange('niveau','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_parente", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("niveau", 11);
        $form->setTaille("meme_personne", 1);
        $form->setTaille("lien_inverse", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_parente", 11);
        $form->setMax("libelle", 255);
        $form->setMax("niveau", 11);
        $form->setMax("meme_personne", 1);
        $form->setMax("lien_inverse", 255);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_parente', __('lien_parente'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('niveau', __('niveau'));
        $form->setLib('meme_personne', __('meme_personne'));
        $form->setLib('lien_inverse', __('lien_inverse'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "lien_parente", $id);
    }


}
