<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:10

require_once "../obj/om_dbform.class.php";

class zone_gen extends om_dbform {

    protected $_absolute_class_name = "zone";

    var $table = "zone";
    var $clePrimaire = "zone";
    var $typeCle = "N";
    var $required_field = array(
        "cimetiere",
        "zone",
        "zonelib",
        "zonetype"
    );
    
    var $foreign_keys_extended = array(
        "cimetiere" => array("cimetiere", ),
        "zone_type" => array("zone_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("cimetiere");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "zone",
            "cimetiere",
            "zonetype",
            "zonelib",
            "geom",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_cimetiere() {
        return "SELECT cimetiere.cimetiere, cimetiere.cimetierelib FROM ".DB_PREFIXE."cimetiere ORDER BY cimetiere.cimetierelib ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_cimetiere_by_id() {
        return "SELECT cimetiere.cimetiere, cimetiere.cimetierelib FROM ".DB_PREFIXE."cimetiere WHERE cimetiere = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zonetype() {
        return "SELECT zone_type.zone_type, zone_type.libelle FROM ".DB_PREFIXE."zone_type WHERE ((zone_type.om_validite_debut IS NULL AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE)) OR (zone_type.om_validite_debut <= CURRENT_DATE AND (zone_type.om_validite_fin IS NULL OR zone_type.om_validite_fin > CURRENT_DATE))) ORDER BY zone_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_zonetype_by_id() {
        return "SELECT zone_type.zone_type, zone_type.libelle FROM ".DB_PREFIXE."zone_type WHERE zone_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['zone'])) {
            $this->valF['zone'] = ""; // -> requis
        } else {
            $this->valF['zone'] = $val['zone'];
        }
        if (!is_numeric($val['cimetiere'])) {
            $this->valF['cimetiere'] = ""; // -> requis
        } else {
            $this->valF['cimetiere'] = $val['cimetiere'];
        }
        if (!is_numeric($val['zonetype'])) {
            $this->valF['zonetype'] = ""; // -> requis
        } else {
            $this->valF['zonetype'] = $val['zonetype'];
        }
        $this->valF['zonelib'] = $val['zonelib'];
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("zone", "hidden");
            if ($this->is_in_context_of_foreign_key("cimetiere", $this->retourformulaire)) {
                $form->setType("cimetiere", "selecthiddenstatic");
            } else {
                $form->setType("cimetiere", "select");
            }
            if ($this->is_in_context_of_foreign_key("zone_type", $this->retourformulaire)) {
                $form->setType("zonetype", "selecthiddenstatic");
            } else {
                $form->setType("zonetype", "select");
            }
            $form->setType("zonelib", "text");
            $form->setType("geom", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("zone", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("cimetiere", $this->retourformulaire)) {
                $form->setType("cimetiere", "selecthiddenstatic");
            } else {
                $form->setType("cimetiere", "select");
            }
            if ($this->is_in_context_of_foreign_key("zone_type", $this->retourformulaire)) {
                $form->setType("zonetype", "selecthiddenstatic");
            } else {
                $form->setType("zonetype", "select");
            }
            $form->setType("zonelib", "text");
            $form->setType("geom", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("zone", "hiddenstatic");
            $form->setType("cimetiere", "selectstatic");
            $form->setType("zonetype", "selectstatic");
            $form->setType("zonelib", "hiddenstatic");
            $form->setType("geom", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("zone", "static");
            $form->setType("cimetiere", "selectstatic");
            $form->setType("zonetype", "selectstatic");
            $form->setType("zonelib", "static");
            $form->setType("geom", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('zone','VerifNum(this)');
        $form->setOnchange('cimetiere','VerifNum(this)');
        $form->setOnchange('zonetype','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("zone", 11);
        $form->setTaille("cimetiere", 11);
        $form->setTaille("zonetype", 11);
        $form->setTaille("zonelib", 30);
        $form->setTaille("geom", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("zone", 11);
        $form->setMax("cimetiere", 11);
        $form->setMax("zonetype", 11);
        $form->setMax("zonelib", 40);
        $form->setMax("geom", 551444);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('zone', __('zone'));
        $form->setLib('cimetiere', __('cimetiere'));
        $form->setLib('zonetype', __('zonetype'));
        $form->setLib('zonelib', __('zonelib'));
        $form->setLib('geom', __('geom'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // cimetiere
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "cimetiere",
            $this->get_var_sql_forminc__sql("cimetiere"),
            $this->get_var_sql_forminc__sql("cimetiere_by_id"),
            false
        );
        // zonetype
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "zonetype",
            $this->get_var_sql_forminc__sql("zonetype"),
            $this->get_var_sql_forminc__sql("zonetype_by_id"),
            true
        );
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("zone", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('cimetiere', $this->retourformulaire))
                $form->setVal('cimetiere', $idxformulaire);
            if($this->is_in_context_of_foreign_key('zone_type', $this->retourformulaire))
                $form->setVal('zonetype', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : voie
        $this->rechercheTable($this->f->db, "voie", "zone", $id);
    }


}
