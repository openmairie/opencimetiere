<?php
//$Id$ 
//gen openMairie le 14/12/2022 15:57

require_once "../obj/om_dbform.class.php";

class autorisation_gen extends om_dbform {

    protected $_absolute_class_name = "autorisation";

    var $table = "autorisation";
    var $clePrimaire = "autorisation";
    var $typeCle = "N";
    var $required_field = array(
        "autorisation",
        "nature"
    );
    
    var $foreign_keys_extended = array(
        "emplacement" => array("emplacement", "colombarium", "concession", "depositoire", "enfeu", "ossuaire", "terraincommunal", ),
        "titre_de_civilite" => array("titre_de_civilite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("emplacement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "autorisation",
            "emplacement",
            "nature",
            "titre",
            "nom",
            "marital",
            "prenom",
            "datenaissance",
            "adresse1",
            "adresse2",
            "cp",
            "ville",
            "telephone1",
            "dcd",
            "parente",
            "observation",
            "telephone2",
            "courriel",
            "genealogie",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement ORDER BY emplacement.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_emplacement_by_id() {
        return "SELECT emplacement.emplacement, emplacement.nature FROM ".DB_PREFIXE."emplacement WHERE emplacement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_titre() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE ((titre_de_civilite.om_validite_debut IS NULL AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE)) OR (titre_de_civilite.om_validite_debut <= CURRENT_DATE AND (titre_de_civilite.om_validite_fin IS NULL OR titre_de_civilite.om_validite_fin > CURRENT_DATE))) ORDER BY titre_de_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_titre_by_id() {
        return "SELECT titre_de_civilite.titre_de_civilite, titre_de_civilite.libelle FROM ".DB_PREFIXE."titre_de_civilite WHERE titre_de_civilite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['autorisation'])) {
            $this->valF['autorisation'] = ""; // -> requis
        } else {
            $this->valF['autorisation'] = $val['autorisation'];
        }
        if (!is_numeric($val['emplacement'])) {
            $this->valF['emplacement'] = NULL;
        } else {
            $this->valF['emplacement'] = $val['emplacement'];
        }
        $this->valF['nature'] = $val['nature'];
        if (!is_numeric($val['titre'])) {
            $this->valF['titre'] = NULL;
        } else {
            $this->valF['titre'] = $val['titre'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = NULL;
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['marital'] == "") {
            $this->valF['marital'] = NULL;
        } else {
            $this->valF['marital'] = $val['marital'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = NULL;
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if ($val['datenaissance'] != "") {
            $this->valF['datenaissance'] = $this->dateDB($val['datenaissance']);
        } else {
            $this->valF['datenaissance'] = NULL;
        }
        if ($val['adresse1'] == "") {
            $this->valF['adresse1'] = NULL;
        } else {
            $this->valF['adresse1'] = $val['adresse1'];
        }
        if ($val['adresse2'] == "") {
            $this->valF['adresse2'] = NULL;
        } else {
            $this->valF['adresse2'] = $val['adresse2'];
        }
        if ($val['cp'] == "") {
            $this->valF['cp'] = NULL;
        } else {
            $this->valF['cp'] = $val['cp'];
        }
        if ($val['ville'] == "") {
            $this->valF['ville'] = NULL;
        } else {
            $this->valF['ville'] = $val['ville'];
        }
        if ($val['telephone1'] == "") {
            $this->valF['telephone1'] = NULL;
        } else {
            $this->valF['telephone1'] = $val['telephone1'];
        }
        if ($val['dcd'] == 1 || $val['dcd'] == "t" || $val['dcd'] == "Oui") {
            $this->valF['dcd'] = true;
        } else {
            $this->valF['dcd'] = false;
        }
            $this->valF['parente'] = $val['parente'];
            $this->valF['observation'] = $val['observation'];
        if ($val['telephone2'] == "") {
            $this->valF['telephone2'] = NULL;
        } else {
            $this->valF['telephone2'] = $val['telephone2'];
        }
        if ($val['courriel'] == "") {
            $this->valF['courriel'] = NULL;
        } else {
            $this->valF['courriel'] = $val['courriel'];
        }
            $this->valF['genealogie'] = $val['genealogie'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("autorisation", "hidden");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("titre", "selecthiddenstatic");
            } else {
                $form->setType("titre", "select");
            }
            $form->setType("nom", "text");
            $form->setType("marital", "text");
            $form->setType("prenom", "text");
            $form->setType("datenaissance", "date");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone1", "text");
            $form->setType("dcd", "checkbox");
            $form->setType("parente", "textarea");
            $form->setType("observation", "textarea");
            $form->setType("telephone2", "text");
            $form->setType("courriel", "text");
            $form->setType("genealogie", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("autorisation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("emplacement", $this->retourformulaire)) {
                $form->setType("emplacement", "selecthiddenstatic");
            } else {
                $form->setType("emplacement", "select");
            }
            $form->setType("nature", "text");
            if ($this->is_in_context_of_foreign_key("titre_de_civilite", $this->retourformulaire)) {
                $form->setType("titre", "selecthiddenstatic");
            } else {
                $form->setType("titre", "select");
            }
            $form->setType("nom", "text");
            $form->setType("marital", "text");
            $form->setType("prenom", "text");
            $form->setType("datenaissance", "date");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone1", "text");
            $form->setType("dcd", "checkbox");
            $form->setType("parente", "textarea");
            $form->setType("observation", "textarea");
            $form->setType("telephone2", "text");
            $form->setType("courriel", "text");
            $form->setType("genealogie", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("autorisation", "hiddenstatic");
            $form->setType("emplacement", "selectstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("titre", "selectstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("marital", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("datenaissance", "hiddenstatic");
            $form->setType("adresse1", "hiddenstatic");
            $form->setType("adresse2", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
            $form->setType("telephone1", "hiddenstatic");
            $form->setType("dcd", "hiddenstatic");
            $form->setType("parente", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("telephone2", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            $form->setType("genealogie", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("autorisation", "static");
            $form->setType("emplacement", "selectstatic");
            $form->setType("nature", "static");
            $form->setType("titre", "selectstatic");
            $form->setType("nom", "static");
            $form->setType("marital", "static");
            $form->setType("prenom", "static");
            $form->setType("datenaissance", "datestatic");
            $form->setType("adresse1", "static");
            $form->setType("adresse2", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
            $form->setType("telephone1", "static");
            $form->setType("dcd", "checkboxstatic");
            $form->setType("parente", "textareastatic");
            $form->setType("observation", "textareastatic");
            $form->setType("telephone2", "static");
            $form->setType("courriel", "static");
            $form->setType("genealogie", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('autorisation','VerifNum(this)');
        $form->setOnchange('emplacement','VerifNum(this)');
        $form->setOnchange('titre','VerifNum(this)');
        $form->setOnchange('datenaissance','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("autorisation", 11);
        $form->setTaille("emplacement", 11);
        $form->setTaille("nature", 15);
        $form->setTaille("titre", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("marital", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("datenaissance", 12);
        $form->setTaille("adresse1", 30);
        $form->setTaille("adresse2", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
        $form->setTaille("telephone1", 15);
        $form->setTaille("dcd", 1);
        $form->setTaille("parente", 80);
        $form->setTaille("observation", 80);
        $form->setTaille("telephone2", 15);
        $form->setTaille("courriel", 30);
        $form->setTaille("genealogie", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("autorisation", 11);
        $form->setMax("emplacement", 11);
        $form->setMax("nature", 15);
        $form->setMax("titre", 11);
        $form->setMax("nom", 50);
        $form->setMax("marital", 50);
        $form->setMax("prenom", 50);
        $form->setMax("datenaissance", 12);
        $form->setMax("adresse1", 40);
        $form->setMax("adresse2", 40);
        $form->setMax("cp", 5);
        $form->setMax("ville", 50);
        $form->setMax("telephone1", 15);
        $form->setMax("dcd", 1);
        $form->setMax("parente", 6);
        $form->setMax("observation", 6);
        $form->setMax("telephone2", 15);
        $form->setMax("courriel", 100);
        $form->setMax("genealogie", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('autorisation', __('autorisation'));
        $form->setLib('emplacement', __('emplacement'));
        $form->setLib('nature', __('nature'));
        $form->setLib('titre', __('titre'));
        $form->setLib('nom', __('nom'));
        $form->setLib('marital', __('marital'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('datenaissance', __('datenaissance'));
        $form->setLib('adresse1', __('adresse1'));
        $form->setLib('adresse2', __('adresse2'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
        $form->setLib('telephone1', __('telephone1'));
        $form->setLib('dcd', __('dcd'));
        $form->setLib('parente', __('parente'));
        $form->setLib('observation', __('observation'));
        $form->setLib('telephone2', __('telephone2'));
        $form->setLib('courriel', __('courriel'));
        $form->setLib('genealogie', __('genealogie'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // emplacement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "emplacement",
            $this->get_var_sql_forminc__sql("emplacement"),
            $this->get_var_sql_forminc__sql("emplacement_by_id"),
            false
        );
        // titre
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "titre",
            $this->get_var_sql_forminc__sql("titre"),
            $this->get_var_sql_forminc__sql("titre_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('emplacement', $this->retourformulaire))
                $form->setVal('emplacement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('titre_de_civilite', $this->retourformulaire))
                $form->setVal('titre', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "destinataire", $id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "autorisation_p1", $id);
        // Verification de la cle secondaire : genealogie
        $this->rechercheTable($this->f->db, "genealogie", "autorisation_p2", $id);
    }


}
