openCimetière ReadMe
====================

Avertissement
-------------

openCimetière est une application sensible nécessitant un paramétrage précis.
Ni l'équipe du projet openCimetière ni le chef de projet ne peuvent être tenus
pour responsables d'un éventuel dysfonctionnement comme ceci est précisé dans
la licence jointe. Vous pouvez, si vous le souhaitez, faire appel a un
prestataire spécialisé qui peut fournir support, hot-line, maintenance, et
garantir le fonctionnement en environnement de production.


Description
-----------

openCimetière est un logiciel de gestion des concessions de cimetières. Il
permet la gestion de la place (défunt) dans les concessions, la gestion des
autorisations (concessionnaire et ayant droit), la gestion du terme de la
concession (transfert de défunt, transfert à l’ossuaire), la gestion des
concessions libres, la gestion des opérations funéraires, l’archivage
systématique de l’ensemble des données pour constituer une mémoire commune et
bien plus encore.

Toutes les informations sur http://www.openmairie.org/


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=opencimetiere&format=html


Licence
-------

Voir le fichier LICENSE.txt

